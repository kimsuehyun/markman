package com.markman.patent.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.markman.common.vo.UserVo;
import com.markman.patent.dao.PatentDao;
import com.markman.service.S3Wrapper;

@Service
public class PatentService {
	@Autowired PatentDao patentDao;
	@Autowired S3Wrapper s3Wrapper;
	@Autowired HttpSession session;

	public String markModify(HttpServletRequest request){
		String retrunStr="";
		String patent_feedback = request.getParameter("patent_feedback");
		int regis_posibility = Integer.parseInt(request.getParameter("regis_posibility"));
		int mark_history_pk = Integer.parseInt(request.getParameter("mark_history_pk"));
		//여기서 현제 마크 pk에 업데이트 한다.

		Map<String,Object> map = new HashMap<String, Object>();
		map.put("patent_feedback", patent_feedback);
		map.put("regis_posibility", regis_posibility);
		map.put("mark_history_pk", mark_history_pk);
		patentDao.updateMarkHistoryWhereHistoryPk(map);

		return retrunStr;
	}
}
