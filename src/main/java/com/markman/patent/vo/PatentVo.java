package com.markman.patent.vo;

import com.markman.common.vo.UserVo;

public class PatentVo extends UserVo{
	String profile_img;
	String profile_img_name;
	int patent_num;
	String patent_regis_img;
	String introduce;
	String account_num;
	String account_bank;
	String patent_regis_img_name;


	public String getProfile_img_name() {
		return profile_img_name;
	}
	public void setProfile_img_name(String profile_img_name) {
		this.profile_img_name = profile_img_name;
	}
	public String getProfile_img() {
		return profile_img;
	}
	public void setProfile_img(String profile_img) {
		this.profile_img = profile_img;
	}
	public int getPatent_num() {
		return patent_num;
	}
	public void setPatent_num(int patent_num) {
		this.patent_num = patent_num;
	}
	public String getPatent_regis_img() {
		return patent_regis_img;
	}
	public void setPatent_regis_img(String patent_regis_img) {
		this.patent_regis_img = patent_regis_img;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getAccount_num() {
		return account_num;
	}
	public void setAccount_num(String account_num) {
		this.account_num = account_num;
	}
	public String getAccount_bank() {
		return account_bank;
	}
	public void setAccount_bank(String account_bank) {
		this.account_bank = account_bank;
	}
	public String getPatent_regis_img_name() {
		return patent_regis_img_name;
	}
	public void setPatent_regis_img_name(String patent_regis_img_name) {
		this.patent_regis_img_name = patent_regis_img_name;
	}
}
