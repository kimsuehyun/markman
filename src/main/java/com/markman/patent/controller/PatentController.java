package com.markman.patent.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.markman.patent.service.PatentService;

@Controller
@RequestMapping("/patent")
public class PatentController {
	@Autowired PatentService patentService;

	@RequestMapping(value="/mark/modifyOk", method = RequestMethod.POST)
	@ResponseBody
	public String markModify(HttpServletRequest request) {
		return patentService.markModify(request);
	}

}
