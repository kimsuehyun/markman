package com.markman.patent.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.markman.patent.vo.PatentVo;

@Mapper
public interface PatentDao {
	public int updateMarkHistoryWhereHistoryPk(Map<String ,Object> map);
	public void insertPatent(PatentVo patentVo);
}
