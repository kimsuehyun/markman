package com.markman.common.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.markman.common.service.CommonService;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.markman.common.service.CommonService;
import com.markman.util.ConstantUtil;

@Controller
@RequestMapping("/common")
public class CommonController {
	@Autowired
	CommonService commonService;

	@RequestMapping("/payInfo")
	public ModelAndView payInfo(HttpServletRequest request){//결제 안내
		return new ModelAndView("/common/mark/pay_info");
	}
	@RequestMapping("/costInfo")
	public ModelAndView costInfo(HttpServletRequest request){//비용 안내
		return new ModelAndView("/common/mark/cost_info");
	}
	@RequestMapping("/biginnerServiceInfo")
	public ModelAndView biginnerServiceInfo(HttpServletRequest request){//도움이 필요한 고객
		return new ModelAndView("/common/mark/beginner_service_info");
	}
	@RequestMapping("/serviceInfo")
	public ModelAndView serviceInfo(HttpServletRequest request){//서비스 소개
		return new ModelAndView("/common/mark/service_info");
	}
	@RequestMapping("/applyProcessInfo")
	public ModelAndView applyProcessInfo(HttpServletRequest request){//출원 과정
		return new ModelAndView("/common/mark/apply_process_info");
	}
	@RequestMapping("/seniorServiceInfo")
	public ModelAndView seniorServiceInfo(HttpServletRequest request){//도움이 필요한 고객
		return new ModelAndView("/common/mark/senior_service_info");
	}

	@RequestMapping("/seniorMarkInfo")
	public ModelAndView seniorMarkInfo(HttpServletRequest request){//도움이 필요한 고객
		return new ModelAndView("/common/mark/senior_mark_info");
	}

	@RequestMapping("/seniorInfoChoice")
	public ModelAndView seniorInfoChoice(HttpServletRequest request){//도움이 필요한 고객
		return new ModelAndView("/common/mark/senior_info_choice");
	}

	@RequestMapping("/markConsulting")
	public ModelAndView markConsulting(HttpServletRequest request){//도움이 필요한 고객
		return new ModelAndView("/common/mark/mark_consulting");
	}

	@RequestMapping("/markHistory")
	public ModelAndView markHistory(HttpServletRequest request){//도움이 필요한 고객
		return new ModelAndView("/common/mark/mark_history");
	}

	@RequestMapping("/mypageApply")
	public ModelAndView mypageApply(HttpServletRequest request){//도움이 필요한 고객
		return new ModelAndView("/common/mark/mypage_apply");
	}

	@RequestMapping("/markSearch")
	public ModelAndView markSearch(HttpServletRequest request){
		return new ModelAndView("/common/mark/mark_search");
	}

	@RequestMapping("/master")
	public ModelAndView master(HttpServletRequest request){
		return new ModelAndView("/common/mark/master");
	}

	//상표 상담
	@RequestMapping("/mark/counseling")
	public ModelAndView markCounseling(HttpServletRequest request){
		return commonService.markCounseling(request, "/user/mark/counseling/counseling_list");
	}
	@RequestMapping("/counseling/mvDetail")
	public ModelAndView mvcounselingDetail(HttpServletRequest request){
		return commonService.mvcounselingDetail(request,"/user/mark/counseling/detail_view");
	}
	@RequestMapping("/counseling/mvWrite")
	public ModelAndView markwrite(HttpServletRequest request){
		return new ModelAndView("/user/mark/counseling/write");
	}

	//문의하기
	@RequestMapping("/support/inquisition")
	public ModelAndView markInquisition(HttpServletRequest request){
		return new ModelAndView("/user/support/inquisition");
	}
	@RequestMapping(value="/support/inquisition/regisOk", method=RequestMethod.POST)
	@ResponseBody
	public String regisOkMarkInquisition(HttpServletRequest request) throws IOException, EmailException{
		return commonService.regisOkMarkInquisition(request);
	}
	//자주 묻는 질문
	@RequestMapping("/support/OftenQuestion")
	public ModelAndView markQuestion(HttpServletRequest request){
		return new ModelAndView("/user/support/OftenQuestion");
	}
	@RequestMapping("/support/OftenQuestion/detail/{board_pk}")
	public ModelAndView popupDetailOftenQuestion(@PathVariable int board_pk){
		return commonService.popupDetailOftenQuestion(board_pk,"/user/support/detail_faq");
	}
	//후기
	@RequestMapping("/support/review")
	public ModelAndView markReview(HttpServletRequest request){
		return getReviewList(1,request);
	}
	@RequestMapping("support/review/{pageNum}")
	public ModelAndView getReviewList(@PathVariable int pageNum, HttpServletRequest request){//후기 목록
		return commonService.getReviewList(pageNum, request, "/user/support/review");
	}
	@RequestMapping("/support/review/detail/{board_pk}")
	public ModelAndView mvDetailMarkReview(@PathVariable int board_pk){
		return commonService.mvDetailMarkReview(board_pk, "/user/support/detail_review");
	}
	@RequestMapping("/support/review/mvEdit")
	public ModelAndView mvEditReview(HttpServletRequest request){
		return commonService.mvEditReview(request,"/user/support/edit_review");
	}
	@RequestMapping(value="/support/review/editOkReview",method=RequestMethod.POST)
	public String editOkReview(HttpServletRequest request){
		return commonService.editOkReview(request);
	}
	@RequestMapping(value="/support/review/deleteOk",method=RequestMethod.POST)
	@ResponseBody
	public String deleteOkReview(HttpServletRequest request){
		return commonService.deleteBoard(request, ConstantUtil.REVIEW);
	}

	//결제 안내
	@RequestMapping("/support/payment")
	public ModelAndView markPayment(HttpServletRequest request){
		return new ModelAndView("/user/support/payment");
	}

	//상표 지식
	@RequestMapping("/support/information")
	public ModelAndView markInformation(HttpServletRequest request){
		return new ModelAndView("/user/support/information");
	}

	//공지사항
	@RequestMapping("/noticeList")
	public ModelAndView noticeList(HttpServletRequest request){
		return commonService.noticeList(request, "/common/support/notice");
	}
	@RequestMapping("/notice/mvDetail")
	public ModelAndView mvDetailNotice(HttpServletRequest request){//공지사항상세보기
		return commonService.mvDetailNotice(request, "/common/support/detail_notice");
	}

}
