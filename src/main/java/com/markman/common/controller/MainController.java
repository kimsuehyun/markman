package com.markman.common.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.markman.common.service.CommonService;
import com.markman.common.service.MainService;
import com.markman.util.ConstantUtil;


@Controller
public class MainController {
	@Autowired
	MainService mainService;
	@Autowired
	CommonService commonService;
	@RequestMapping("/")
	public ModelAndView index(){
		ModelAndView model = new ModelAndView("/home/index");
		//배너 가져오기
		//model.addObject("banner", mainService.getBanner(location));
		Map<String, String> map = new HashMap<String, String>();
		map.put("start_index", 0+"");
		map.put("end_index", ConstantUtil.SMALL_LIST_SIZE+"");
		//상표 상담
		model.addObject("counselingList", commonService.getmarkCounselingList(map));
		//자주 묻는 질문
		model.addObject("faqList", commonService.getFaqList(map));
		return model;
	}
	@RequestMapping(value="/logoutOk.do", method = RequestMethod.GET)
	public String loginOut(){//로그인 화면 이동
		return "home/index";
	}

	@RequestMapping("/loginPage")
	public String loginPage(){//로그인 화면 이동
		return "home/signIn";
	}
	@RequestMapping("/joinHowPage")
	public String joinHowPage(){//회원가입 페이지 이동
		return "home/join_how";
	}

	@RequestMapping("/signUpPage")
	public String signUpPage(){//회원가입 페이지 이동
		return "home/signUp";
	}

	@RequestMapping(value = "/checkSameId", method = RequestMethod.POST)
	@ResponseBody
	public int checkSameId(HttpServletRequest request){
		return mainService.checkSameId(request.getParameter("id"));
	}

	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	public String signUp(HttpServletRequest request){
		mainService.insertUser(request);
		return "home/index";
	}

	@RequestMapping(value= "/introduce")
	public String introduce(){
		return "home/introduce";
	}
	//상표 검색
	/*
	@RequestMapping(value= "/markSearchInfo")
	public String markSearchInfo(){
		return "common/mark/search_info";
	}
	*/
}
