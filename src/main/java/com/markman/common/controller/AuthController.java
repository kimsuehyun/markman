package com.markman.common.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.markman.common.dao.UserDao;
import com.markman.common.vo.UserVo;
import com.markman.util.ConstantUtil;

@Controller
public class AuthController {
	@Autowired
	UserDao userDao;
	@Autowired
	HttpSession session;
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	@RequestMapping("/beforeLogin.do")
	public String beforeLogin()
	{
		return "redirect:/loginPage";
	}
	
	@RequestMapping("/loginProcess.do")
	public String loginProcess(HttpServletRequest request){
		System.out.println("loginProcess......................");
		String ip = request.getHeader("X-FORWARDED-FOR");
		if(ip==null||ip.length()==0){
			ip=request.getHeader("Proxy-Client-IP");
		}
		 if(ip == null || ip.length() == 0) {
	         ip = request.getHeader("WL-Proxy-Client-IP");  // 웹로직
	     }
	     if(ip == null || ip.length() == 0) {
	         ip = request.getRemoteAddr() ;
	     }
		try{
			String userId = SecurityContextHolder.getContext().getAuthentication().getName();		
			UserVo currentUser = userDao.selectUserById(userId);
			// 인증 정보가 없으면 userId = anonymousUser			// currnetUser = null
			if(currentUser == null){
				logger.info("로그인실패 : "+ip);
				return "redirect:/loginPage";		
			}else{
				session.setAttribute("currentUser", currentUser);
				System.out.println("세션셋:"+currentUser);
				logger.info("로그인 :"+currentUser.getName()+" : "+ip);
				if(currentUser.getUser_type().equals(ConstantUtil.ADMIN)){
					logger.info("관리자 로그인");
					return "redirect:/admin/";
				}else{
					logger.info("회원 로그인");
					return "redirect:/user/main";
				}
			}
		}
		catch(Exception e)
		{
			logger.info("비정상 접근입니다(/loginProcess.do): "+e.getMessage());
			return "redirect:/";
		}
	}
	
	@RequestMapping("/logoutProcess.do")
	public String logoutProcess(HttpServletRequest req, HttpServletResponse res)
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null)
		{
			new SecurityContextLogoutHandler().logout(req, res, auth);
		}
		session.invalidate();
		SecurityContextHolder.clearContext();
		return "redirect:/";
	}

}

