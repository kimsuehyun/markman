package com.markman.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.markman.common.service.DocumentService;

@Controller
public class DocumentController {
	@Autowired
	DocumentService documentService;
	
	@RequestMapping(value = "/call/callDownload.do")
	public ResponseEntity<byte[]> callDownload(@RequestParam("fileFullPath") String fileFullPath,@RequestParam("fileOriginName") String fileOriginName) throws Exception {
		return documentService.callDownload(fileFullPath, fileOriginName);
	}
}
