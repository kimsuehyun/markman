package com.markman.common.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.markman.common.service.UserService;
import com.markman.common.vo.UserVo;
import com.markman.util.ConstantUtil;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired UserService userService;

	@RequestMapping("/support/review/mvRegis")
	public ModelAndView mvRegisMarkReview(HttpServletRequest request){
		return new ModelAndView("/user/support/regis_review"); //글쓰기
	}
	@RequestMapping("/mvMark_search")
	public ModelAndView markSearch(HttpServletRequest request){
		return new ModelAndView("/user/mark/mark_search");
	}
	@RequestMapping("/mvMark_apply")
	public ModelAndView apply(HttpServletRequest request){
		return new ModelAndView("/user/mark/mark_apply");
	}

	@RequestMapping("/main")
	public ModelAndView mainPage(HttpServletRequest request){
		return main(1, request);
	}
	@RequestMapping("/main/{pageNum}")
	public ModelAndView main(@PathVariable int pageNum, HttpServletRequest request){
		return userService.main(pageNum, request, "/user/mypage/main");
	}
	@RequestMapping("/myInfo")
	public ModelAndView myInfo(HttpServletRequest request){
		return userService.myInfo(request,"/user/mypage/information/my_info");
	}
	@RequestMapping("/mark/mvDetail")
	public ModelAndView mvDetail(HttpServletRequest request){
		return userService.mvMarkDetail(request);
	}
	@RequestMapping("/mark/uploadFileList")
	public ModelAndView uploadFileList(HttpServletRequest request){
		return userService.mvUploadFileList(request, "/user/mypage/mark/upload_file");
	}
	@RequestMapping("/mark/mvMarkInfo")
	public ModelAndView mvMarkInfo(HttpServletRequest request){
		return  userService.mvMarkInfo(request, "/user/mypage/mark/mark_info");
	}
	@RequestMapping(value="/getUserInfo")
	@ResponseBody
	public UserVo getUserInfo(HttpServletRequest request){
		return userService.getUserInfo(request);
	}
	@RequestMapping("/detailPhoto")
	public ModelAndView detailPhoto(HttpServletRequest request){
		return new ModelAndView("/common/support/detail_photo");
	}
	@RequestMapping("/counseling/userWrite2")
	public ModelAndView userWrite2(HttpServletRequest request){
		return new ModelAndView("/user/mark/counseling/write2");
	}
	@RequestMapping("/counseling/userWrite3")
	public ModelAndView userWrite3(HttpServletRequest request){
		return userService.userWrite3(request, "/user/mark/counseling/write3");
		//return new ModelAndView("/user/mark/counseling/write3");
	}
	@RequestMapping(value="/counseling/reviewOK",method=RequestMethod.POST)
	public String reviewOK(HttpServletRequest request){
		return userService.uploadReview(request);
	}
	@RequestMapping("/detailView1")
	public ModelAndView detailView1(HttpServletRequest request){
		return new ModelAndView("/user/support/detail_view1");
	}
	@RequestMapping("/detailView2")
	public ModelAndView detailView2(HttpServletRequest request){
		return new ModelAndView("/user/support/detail_view2");
	}
	@RequestMapping("/detailView3")
	public ModelAndView detailView3(HttpServletRequest request){
		return new ModelAndView("/user/support/detail_view3");
	}
	@RequestMapping("/detailView4")
	public ModelAndView detailView4(HttpServletRequest request){
		return new ModelAndView("/user/support/detail_view4");
	}
	@RequestMapping("/detailView5")
	public ModelAndView detailView5(HttpServletRequest request){
		return new ModelAndView("/user/support/detail_view5");
	}
	@RequestMapping("/detailView6")
	public ModelAndView detailView6(HttpServletRequest request){
		return new ModelAndView("/user/support/detail_view6");
	}
	@RequestMapping("/detailView7")
	public ModelAndView detailView7(HttpServletRequest request){
		return new ModelAndView("/user/support/detail_view7");
	}
	@RequestMapping("/counseling/mvWrite")
	public ModelAndView markwrite(HttpServletRequest request){
		return new ModelAndView("/user/mark/counseling/write");
	}
	@RequestMapping(value="/counseling/regisOk",method=RequestMethod.POST)
	public String regisOkCounseling(HttpServletRequest request){
		return userService.regisOkCounseling(request);
	}
	@RequestMapping(value="/counseling/deleteOk",method=RequestMethod.POST)
	@ResponseBody
	public String deleteOkCounseling(HttpServletRequest request){
		return userService.deleteBoard(request, ConstantUtil.QNA);
	}
	@RequestMapping(value="/counseling/mvEdit",method=RequestMethod.POST)
	public ModelAndView mvEditCounseling(HttpServletRequest request){
		return userService.mvEditCounseling(request, "/user/mark/counseling/edit");
	}
	@RequestMapping(value="/counseling/editOk",method=RequestMethod.POST)
	public String editOkCounseling(HttpServletRequest request){
		return userService.editOkCounseling(request);
	}
	@RequestMapping("/counseling/mvMarkDetail")
	public ModelAndView mvMarkDetail(HttpServletRequest request){
		return new ModelAndView("/user/mark/counseling/mv_mark_detail");
	}
	/*@RequestMapping("/mark/MarkAccount")
	public ModelAndView mvMarkAccount(HttpServletRequest request){
		return  userService.mvMarkInfo(request, "/user/mypage/mark/mark_account");
	}
	@RequestMapping("/mark/mvMarkConsulting")
	public ModelAndView mvMarkConsulting(HttpServletRequest request){
		return  userService.mvMarkInfo(request, "/user/mypage/mark/mark_consulting");
	}
	@RequestMapping("/mark/mvMarkHistory")
	public ModelAndView mvMarkHistory(HttpServletRequest request){
		return  userService.mvMarkInfo(request, "/user/mypage/mark/mark_history");
	}
	@RequestMapping("/mark/mvMypageApply")
	public ModelAndView mvMypageApply(HttpServletRequest request){
		return  userService.mvMarkInfo(request, "/user/mypage/mark/mypage_apply");
	}*/

}
