package com.markman.common.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.markman.common.service.CommonAjaxFileUploadService;


@Controller
public class CommonAjaxFileUploadController {

	@Autowired CommonAjaxFileUploadService commonAjaxFileUploadService;

	@RequestMapping("/ajax/fileupload")
	@ResponseBody
	public String ajaxFileUpload(HttpServletRequest request){
		return commonAjaxFileUploadService.ajaxFileupLoad(request);
	}

}
