package com.markman.common.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.markman.common.dao.MainDao;
import com.markman.common.dao.UserDao;
import com.markman.common.vo.UserVo;
import com.markman.patent.vo.PatentVo;
import com.markman.security.SecAlgorithm;
import com.markman.service.S3Wrapper;
import com.markman.util.ConstantUtil;
import com.markman.util.FileUtil;

@Service
public class MainService {
	@Autowired
	MainDao mainDao;
	@Autowired
	SecAlgorithm secAlgorithm;
	@Autowired
	CommonAjaxFileUploadService commonAjaxFileUploadService;
	@Autowired
	S3Wrapper s3Wrapper;
	@Autowired
	UserDao userDao;

	public Map<String,String> getBanner(String location){
		return mainDao.selectBannerData(location);
	}
	public int checkSameId(String id){
		return mainDao.checkSameId(id);
	}

	public int insertUser(HttpServletRequest request){//회원가입
		String pw = request.getParameter("pw");
		String user_type = request.getParameter("user_type");
		String hashPw = "";
		try {
			hashPw = secAlgorithm.createHash(pw);
		} catch (Exception e) {
			return 0;
		}
		UserVo userVo = new UserVo();
		userVo.setId(request.getParameter("id"));
		userVo.setPw(hashPw);
		userVo.setName(request.getParameter("name"));
		userVo.setEmail(request.getParameter("email"));
		userVo.setCell_num(request.getParameter("cell_num"));
		userVo.setTel_num(request.getParameter("tel_num"));
		userVo.setAddr1(request.getParameter("addr1"));
		userVo.setAddr2(request.getParameter("addr2"));
		userVo.setAddr3(request.getParameter("addr3"));
		userVo.setSns_receive(0);//일단 0으로 해놓음
		userVo.setUser_type(user_type);
		if(user_type.equals(ConstantUtil.PATENT)) {
			this.insertPatent(request);
			this.insertPatentImgs(request, request.getParameter("id"));
		}
		return mainDao.insertUser(userVo);
	}
	public void insertPatentImgs(HttpServletRequest request, String id) {
		MultipartHttpServletRequest multipartRequest =  (MultipartHttpServletRequest)request;
		MultipartFile profile_file = multipartRequest.getFile(request.getParameter("profile"));
		MultipartFile regis_file = multipartRequest.getFile(request.getParameter("regis"));

		String regis_img_name = profile_file.getOriginalFilename();
		String profile_img_name = regis_file.getOriginalFilename();
		String regis_save_name = FileUtil.getToday(1)+"."+FileUtil.getFileType(regis_img_name);
		String profile_save_name = FileUtil.getToday(1)+"."+FileUtil.getFileType(profile_img_name);

		String pro_db_url = "";
		String regis_db_url = "";

		String pro_filePath = ConstantUtil.USER_UPLODER_ROOT+"/"+id+"/" + ConstantUtil.USER_PATENT_PROFILEIMG+"/"+regis_save_name;
		String regis_filePath = ConstantUtil.USER_UPLODER_ROOT+"/"+id+"/" + ConstantUtil.USER_PATENT_REGIS+"/"+profile_save_name;

		pro_db_url = ConstantUtil.S3DEFAULT+"/"+pro_filePath;
		regis_db_url= ConstantUtil.S3DEFAULT+"/"+regis_filePath;

		Map<String, String> map = new HashMap<String, String>();
		map.put("profile_img_name", profile_img_name);
		map.put("profile_img", pro_db_url);
		map.put("patent_regis_img_name", regis_img_name);
		map.put("patent_regis_img", regis_db_url);
		map.put("id", id);


		userDao.updatePatentWhereId(map);
		s3Wrapper.upload(profile_file, pro_filePath);
		s3Wrapper.upload(regis_file, regis_filePath);

	}

	public int insertPatent(HttpServletRequest request){
		PatentVo patentVo = new PatentVo();
		patentVo.setId(request.getParameter("id"));
		patentVo.setProfile_img("");
		patentVo.setProfile_img_name("");
		patentVo.setPatent_regis_img("");
		patentVo.setPatent_regis_img_name("");
		patentVo.setPatent_num(Integer.parseInt(request.getParameter("patent_num")));
		patentVo.setIntroduce(request.getParameter("introduce"));
		patentVo.setAccount_bank(request.getParameter("account_bank"));
		patentVo.setAccount_num(request.getParameter("account_num"));



		return mainDao.insertPatent(patentVo);
	}
}
