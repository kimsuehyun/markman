package com.markman.common.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.markman.common.dao.CommonAjaxFileUploadDao;
import com.markman.common.dao.UserDao;
import com.markman.common.vo.BoardImgVo;
import com.markman.common.vo.ImageVo;
import com.markman.common.vo.UserVo;
import com.markman.mark.vo.MarkAttachFIlesVo;
import com.markman.mark.vo.MarkFileVo;
import com.markman.service.S3Wrapper;
import com.markman.util.ConstantUtil;
import com.markman.util.FileUtil;

@Service
public class CommonAjaxFileUploadService {

	@Autowired UserService userService;
	@Autowired HttpSession session;
	@Autowired S3Wrapper s3Wrapper;
	@Autowired CommonAjaxFileUploadDao commonAjaxFileUploadDao;
	@Autowired UserDao userDao;


	//uploadPageName만 잘 지켜서 보내쟈 //파일이 여러개일때..
	public String ajaxFileListupLoad(HttpServletRequest request){

		String uploadPageName = request.getParameter("uploadPageName");
		String returnStr = "";
		String id = "";
		String imgElementName = request.getParameter("imgElementName");
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> imageFiles = multipartRequest.getFiles(request.getParameter("imgElementName"));
		int filesSize = imageFiles.size();
		List<ImageVo> imageVoList = new ArrayList<ImageVo>();
		String url[] = new String[filesSize];
		id = userService.getCurrentUserId(session);
		//여기서 파일 네임 얻자.
		for(MultipartFile file:imageFiles) {
			ImageVo vo = new ImageVo(file.getOriginalFilename());
			imageVoList.add(vo);
		}

		//여기서 if문 추가한 후 작업하면 된다.
		if("mark".equals(uploadPageName)){
			url = this.settingMarkRegister(request, filesSize,imageVoList, id);
		}
		for(int i=0;i<filesSize; i++) {
			MultipartFile file = imageFiles.get(i);
			s3Wrapper.upload(file, url[i]);
		}
		return "ok";
	}

	//uploadPageName만 잘 지켜서 보내쟈
	public String ajaxFileupLoad(HttpServletRequest request){
		System.out.println("여기까지와야해");
		String uploadPageName = request.getParameter("uploadPageName");
		String returnStr = "";
		String id = "";
		String imgElementName = request.getParameter("imgElementName");
		System.out.println("imgElementName:::::"+imgElementName);
		MultipartHttpServletRequest multipartRequest =  (MultipartHttpServletRequest)request;
		MultipartFile file = multipartRequest.getFile(imgElementName);
		if(file != null){
			System.out.println("이미지 있어요!!");
			int img_size = (int) file.getSize();
			String img_origin_name = file.getOriginalFilename();
			String file_name = FileUtil.getToday(1)+"."+FileUtil.getFileType(img_origin_name);
			String url = "";
			id = userService.getCurrentUserId(session);
			if("mark".equals(uploadPageName)){
				url = this.settingMarkFileUpload(request, imgElementName, img_origin_name, file_name, id);
			}else if("signUp".equals(uploadPageName)){
				url = ConstantUtil.S3DEFAULT+"/"+ConstantUtil.PROFILE_IMG_PATH+"/"+id+"/"+file_name;
			}else if("reView".equals(uploadPageName)){
				url = this.settingReviewFileUpload(request,img_origin_name, file_name, id, img_size);
			}else if("sigUpPatent".equals(uploadPageName)){
			}
			if(!url.equals("")) {
				s3Wrapper.upload(file, url);
			}
		}else{
			System.out.println("이미지없어요");
		}
		return "ok";
	}

	public String[] settingMarkRegister(HttpServletRequest request,int filesSize,List<ImageVo> imageVoList,String file_uploder){
		//상표등록
		int mark_history_pk = Integer.parseInt(request.getAttribute("mark_history_pk")+"");
		String filePath="";
		String db_url = "";
		String url[] = new String[filesSize];

		for(int i=0;i<filesSize;i++) {
			ImageVo imageVo = imageVoList.get(i);
			filePath = ConstantUtil.USER_UPLODER_ROOT+"/"+file_uploder+"/"+ConstantUtil.USER_UPLODER_FILES;
			filePath = filePath +"/"+imageVo.getSave_full_name();
			db_url = ConstantUtil.S3DEFAULT+"/"+filePath;
			url[i] = filePath;

			MarkAttachFIlesVo vo = new MarkAttachFIlesVo();
			vo.setFile_name(imageVo.getSave_file_name()+"."+imageVo.getFile_extension());
			vo.setUrl(db_url);
			vo.setMark_history_pk(mark_history_pk);
			userDao.insertMarkAttachFiles(vo);
		}
		return url;
	}
	public String settingMarkFileUpload(HttpServletRequest request,String imgElementName,String img_origin_name, String file_name,String file_uploder){
		//상표등록
		int file_type =0;
		int mark_pk = Integer.parseInt(request.getParameter("mark_pk"));
		String filePath = ConstantUtil.USER_UPLODER_ROOT+"/"+file_uploder+"/";
		String url = "";
		String db_url = "";
		if("entrust".equals(imgElementName)){
			filePath += ConstantUtil.USER_UPLODER_MARK_ENTRUST;
			file_type = ConstantUtil.MARK_UPLOADFILE_TYPE_ENTRUST;
		}else if("seal".equals(imgElementName)){
			filePath += ConstantUtil.USER_UPLODER_MARK_SEAL;
			file_type = ConstantUtil.MARK_UPLOADFILE_TYPE_SEAL;
		}else{
			filePath += ConstantUtil.USER_UPLODER_MARK_JUMIN;
			file_type = ConstantUtil.MARK_UPLOADFILE_TYPE_JUMIN;
		}
		url =filePath+"/"+file_name;
		db_url= ConstantUtil.S3DEFAULT+"/"+url;

		//insert하기전에 이미 있는지 없는지 확인
		String file_url = this.selectMarkFileFromMarkPkAndFileType(mark_pk, file_type);
		if("empty".equals(file_url)){
			MarkFileVo markFileVo = this.settingMarkFileVo(mark_pk, db_url, file_name, file_uploder, file_type);
			this.insertMarkFile(markFileVo);
		}else{
			//이미 있다. 즉, 수정을 해야된다. 그래서 아마존 파일 지우고, 이 함수를 호출한곳에서 새로운 사진을 insert 한다.
			s3Wrapper.deleteObject(FileUtil.ParsingS3ImgUrl(file_url));
			this.updateMarkFileFromMarkPkAndFileType(db_url, mark_pk, file_type);
		}
		return url;

	}

	public String settingReviewFileUpload(HttpServletRequest request,String img_origin_name, String file_name,String file_uploder, int img_size){
		//리뷰등록
		UserVo userVo = (UserVo) session.getAttribute("currentUser");
		userVo.getId();
		int board_pk = Integer.parseInt(request.getAttribute("board_pk")+"");
		String filePath = ConstantUtil.USER_UPLODER_ROOT+"/"+file_uploder+"/" + ConstantUtil.USER_UPLODER_SUPPORT_REVIEW;
		String url = "";
		String db_url = "";
		url =filePath+"/"+file_name;
		db_url= ConstantUtil.S3DEFAULT+"/"+url;
		//insert하기전에 이미 있는지 없는지 확인
		String img_url = this.selectReviewFileFromBoardPk(board_pk);
		if("empty".equals(img_url)){
			System.out.println("없을때");
			BoardImgVo boardImgVo = this.settingBoardFileVo(board_pk, img_origin_name, db_url, img_size);
			this.insertReviewFile(boardImgVo);
		}else{
			System.out.println("있을때");
			//이미 있다. 즉, 수정을 해야된다. 그래서 아마존 파일 지우고, 이 함수를 호출한곳에서 새로운 사진을 insert 한다.
			s3Wrapper.deleteObject(FileUtil.ParsingS3ImgUrl(db_url));
			this.updateReviewFileFromBoardPk(db_url, board_pk);
		}
		return url;
	}

	public MarkFileVo settingMarkFileVo(int mark_pk, String db_url, String file_name, String file_uploder,int file_type){

		MarkFileVo markFileVo = new MarkFileVo();
		markFileVo.setMark_pk(mark_pk);
		markFileVo.setFile_url(db_url);
		markFileVo.setFile_name(file_name);
		markFileVo.setFile_uploder(file_uploder);
		markFileVo.setFile_type(file_type);

		return markFileVo;
	}

	public List<MarkFileVo> selectMarkFile(int mark_pk){
		return commonAjaxFileUploadDao.selectMarkFile(mark_pk);
	}
	public int insertMarkFile(MarkFileVo markFileVo){
		return commonAjaxFileUploadDao.insertMarkFile(markFileVo);
	}
	public String selectMarkFileFromMarkPkAndFileType(int mark_pk, int file_type){
		Map<String, Integer >map = new HashMap<String,Integer>();
		map.put("mark_pk", mark_pk);
		map.put("file_type", file_type);
		return commonAjaxFileUploadDao.selectMarkFileFromMarkPkAndFileType(map);
	}
	public int updateMarkFileFromMarkPkAndFileType(String file_url, int mark_pk, int file_type){
		Map<String, String> map = new HashMap<String, String>();
		map.put("file_url", file_url);
		map.put("mark_pk", mark_pk+"");
		map.put("file_type", file_type+"");
		return commonAjaxFileUploadDao.updateMarkFileFromMarkPkAndFileType(map);
	}

	public String selectReviewFileFromBoardPk(int board_pk) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("board_pk", board_pk);
		return userDao.selectReviewFileFromBoardPk(map);
	}

	public BoardImgVo settingBoardFileVo(int board_pk, String img_origin_name, String db_url, int img_size){

		BoardImgVo boardImgVo = new BoardImgVo();
		boardImgVo.setBoard_pk(board_pk);
		boardImgVo.setImg_origin_name(img_origin_name);
		boardImgVo.setImg_url(db_url);
		boardImgVo.setImg_size(img_size);

		return boardImgVo;
	}

	public int insertReviewFile(BoardImgVo boardImgVo){
		return userDao.insertReviewFile(boardImgVo);
	}

	public int updateReviewFileFromBoardPk(String db_url, int board_pk){
		Map<String, String> map = new HashMap<String, String>();
		map.put("img_url", db_url);
		map.put("board_pk", board_pk+"");
		return userDao.updateReviewFileFromBoardPk(map);
	}
}
