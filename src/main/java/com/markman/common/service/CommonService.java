package com.markman.common.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.markman.common.dao.CommonDao;
import com.markman.common.vo.FaqVo;
import com.markman.common.dao.UserDao;
import com.markman.common.vo.BoardVo;
import com.markman.common.vo.FaqVo;
import com.markman.common.vo.UserVo;
import com.markman.util.ConstantUtil;
import com.markman.util.PageNavigator;
import com.markman.util.SendEmailUtils;

@Service
public class CommonService {
	@Autowired CommonDao commonDao;
	@Autowired HttpSession session;
	@Autowired UserService userService;
	@Autowired CommonAjaxFileUploadService commonAjaxFileUploadService;
	@Autowired UserDao userDao;
	//상표 상담
	public ModelAndView markCounseling(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int pageNum = 1;
		if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		int totalContents = this.selectBoardCount(ConstantUtil.QNA);
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun", ConstantUtil.PAGING, ConstantUtil.SMALL_LIST_SIZE);
		model.addObject("qnaList", this.getmarkCounselingList(nation.getPageMap()));
		model.addObject("nation", nation);
		return model;
	}
	public List<Map<String, Object>> getmarkCounselingList(Map<String,String> map){
		return commonDao.selectQnaList(map);
	}
	public ModelAndView mvcounselingDetail(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int pk = Integer.parseInt(request.getParameter("pk"));
		this.increseBoardHits(pk);//조회수 증가
		model.addObject("qnaVo", commonDao.selectQnaVo(pk));
		model.addObject("replyList", commonDao.selectReplyList(pk));
		return model;
	}

	//자주 묻는 질문
	public ModelAndView FaqList(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int pageNum = 1;
		if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		int	totalContents= this.selectBoardCount(ConstantUtil.FAQ);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		model.addObject("faqList", this.getFaqList(nation.getPageMap()));
		model.addObject("nation", nation);
		return model;
	}
	public List<FaqVo> getFaqList(Map<String,String> map){
		return commonDao.selectFaqList(map);
	}
	//공지사항
	public ModelAndView noticeList(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int pageNum = 1;
		if(request.getParameter("pageNum")!=null){
			pageNum = Integer.parseInt(request.getParameter("pageNum"));
		}
		int	totalContents=selectBoardCount(ConstantUtil.NOTICE);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		List<Map<String,String>> noticeList = commonDao.selectNoticeList(nation.getPageMap());

		model.addObject("noticeList", noticeList);
		model.addObject("nation", nation);
		return model;
	}
	public ModelAndView mvDetailNotice(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int board_pk = Integer.parseInt(request.getParameter("pk"));
		//조회수 증가
		this.increseBoardHits(board_pk);
		model.addObject("noticeVo", commonDao.selectNoticeVo(board_pk));
		model.addObject("boardImgList", commonDao.selectBoardImgList(board_pk));
		model.addObject("replyList", commonDao.selectReplyList(board_pk));
		return model;
	}
	public void increseBoardHits(int board_pk){
		commonDao.updateBoardHits(board_pk);
	}
	public int selectBoardCount(String board_name){//게시판 수 가져오기(notice,qna,faq,review)
		Map<String,String> map = new HashMap<String,String>();
		map.put("board_name", board_name);
		return commonDao.selectBoardCount(map);
	}

	public ModelAndView getReviewList(@PathVariable int pageNum, HttpServletRequest request, String viewName){
		System.out.println("????");
		String board_name = ConstantUtil.REVIEW;
		ModelAndView model = new ModelAndView(viewName);
		int	totalContents=selectBoardCount(board_name);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.REVIEW_LIST_SIZE);
		List<Map<String,String>> reviewList = commonDao.selectReviewList(nation.getPageMap());
		for(Map<String, String>map : reviewList) {
			System.out.println(map);
		}
		model.addObject("reviewList", reviewList);
		model.addObject("nation", nation);
		
		return model;
	}

	public ModelAndView mvDetailMarkReview(@PathVariable int board_pk, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		//조회수 증가
		increseBoardHits(board_pk);
		model.addObject("reviewVo", commonDao.selectReviewVo(board_pk));
		model.addObject("boardImgList", commonDao.selectBoardImgList(board_pk));
		return model;
	}
	public String getCurrentUserId(HttpSession session) {// 회원 id
		UserVo userVo = (UserVo) session.getAttribute("currentUser");
		String userId = userVo.getId();
		return userId;
	}
	public ModelAndView mvEditReview(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		if(request.getParameter("board_pk") == null) {
			System.out.println(" null??????");
			return model;
		}
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		String user_id = getCurrentUserId(session);
		UserVo user = userDao.selectUserById(user_id);
		model.addObject("user", user);
		model.addObject("reviewVo", commonDao.selectReviewVo(board_pk));
		model.addObject("boardImgList", commonDao.selectBoardImgList(board_pk));
		return model;
	}


	public int updateBoard(HttpServletRequest request){
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
//		String writer = userService.getCurrentUserId(session);
		String title = request.getParameter("title");
		String content = request.getParameter("ir1");
		BoardVo boardVo = new BoardVo();
		boardVo.setBoard_pk(board_pk);
		boardVo.setTitle(title);
		boardVo.setContent(content);
		commonDao.updateBoard(boardVo);
		return board_pk;
	}

	public String editOkReview(HttpServletRequest request){
		System.out.println("여기는???");
		int board_pk = updateBoard(request);//게시판 업데이트
		request.setAttribute("board_pk", board_pk);
		
		if("ok".equals(request.getParameter("imgModifyCheck"))) {
			commonAjaxFileUploadService.ajaxFileupLoad(request);
		}
		
		return "redirect:/common/support/review";
	}

	public String deleteBoard(HttpServletRequest request, String board_name){
		System.out.println("제발!!");
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("board_pk", board_pk);
		map.put("board_name", board_name);
		return  commonDao.deleteBoard(map)>0 ? "success" : "fail";
	}



	//자주묻는 질문 팝업(index)
	public ModelAndView popupDetailOftenQuestion(int board_pk, String viewName){
		System.out.println("board_pk::::"+board_pk);
		ModelAndView model = new ModelAndView(viewName);
		increseBoardHits(board_pk);//조회수 증가
		model.addObject("faqVo", commonDao.selectFaqVo(board_pk));
		return model;
	}

	//문의하기 등록
	public String regisOkMarkInquisition(HttpServletRequest request) throws IOException, EmailException{
			String title = request.getParameter("title");
			String content = request.getParameter("content");
			List<Map<String, String>> list = new ArrayList<Map<String, String>>();//파일 첨부시 필요. 현제 0
			return SendEmailUtils.sendSimpleEmail(ConstantUtil.ADMIN_MAIL, title, content);
	}

}
