package com.markman.common.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.apache.ibatis.javassist.expr.Instanceof;

import org.apache.commons.mail.EmailException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.markman.admin.dao.AdminDao;
import com.markman.admin.service.AdminService;
import com.markman.common.dao.CommonDao;
import com.markman.common.dao.UserDao;
import com.markman.common.vo.BoardVo;
import com.markman.common.vo.QnaVo;
import com.markman.common.vo.ReviewVo;
import com.markman.common.vo.UserVo;
import com.markman.mark.vo.MarkAttachFIlesVo;
import com.markman.mark.vo.MarkFileVo;
import com.markman.mark.vo.MarkHistoryVo;
import com.markman.mark.vo.MarkVo;
import com.markman.util.ConstantUtil;
import com.markman.util.PageNavigator;
import com.markman.util.SendEmailUtils;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	@Autowired
	HttpSession session;
	@Autowired
	CommonAjaxFileUploadService commonAjaxFileUploadService;
	@Autowired
	AdminDao adminDao;
	@Autowired
	AdminService adminService;
	@Autowired
	CommonDao commonDao;

	public MarkVo selectMarkDetail(int mark_pk) {
		MarkVo markVo = userDao.selectMarkDetail(mark_pk);
		List<MarkFileVo> marFileVoList = commonAjaxFileUploadService.selectMarkFile(mark_pk);
		// 히스토리
		markVo.setMarkHistoryVoList(this.getMarkHistoryVoList(mark_pk));
		// 업로드한 파일 위임장, 인감증명서, 주민등록등본
		markVo.setMarkFileVoList(marFileVoList);
		return markVo;
	}
	public List<MarkHistoryVo> getMarkHistoryVoList(int mark_pk){

		List<MarkHistoryVo> markHistoryVoList = userDao.selectMarkHistoryDetail(mark_pk);
		for(MarkHistoryVo hisVo : markHistoryVoList) {
			List<MarkAttachFIlesVo> markAttachFIlesVoList = userDao.selectMarkAttachFilesWhereMarkPk(hisVo.getMark_history_pk());
			// 업로드한 파일 -> 상표등록할때 했던 첨부파일들은 계속 변경할 수 있기때문에 history에 담겨져 있다.
			hisVo.setMarkAttachFIlesVoList(markAttachFIlesVoList);
		}
		return markHistoryVoList;
	}

	public String getCurrentUserId(HttpSession session) {// 회원 id
		UserVo userVo = (UserVo) session.getAttribute("currentUser");
		String userId = userVo.getId();
		return userId;
	}

	public static String getCurrentUserType(HttpSession session) {// 회원 유형
		UserVo userVo = (UserVo) session.getAttribute("currentUser");
		String userType = userVo.getUser_type();
		return userType;
	}

	public ModelAndView myInfo(HttpServletRequest request, String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		model.addObject("userVo", getUserInfo(request));
		return model;
	}

	public ModelAndView main(@PathVariable int pageNum, HttpServletRequest request, String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		String userType = getCurrentUserType(session);
		String userId = getCurrentUserId(session);
		String searchAbout = request.getParameter("searchAbout");
		String searchWord = request.getParameter("searchWord");
		String partnerType = "";
		if (ConstantUtil.APPLICANT.equals(userType)) {
			userType = "applicant";
			partnerType = "patent";
		} else if (ConstantUtil.PATENT.equals(userType)) {
			userType = "patent";
			partnerType = "applicant";
		}
		String queryStr = "left join (select id partner_id, name partner_name from user) user on user.partner_id = mark."
				+ partnerType + "_id" // 매칭 상대 이름 가져오기
				+ " where mark." + userType + "_id = #{userId}"; // 내가
																		// 출원/관리하는
																		// 상표
		System.out.println("queryStr:::"+queryStr);
		if (searchAbout != null) {
			if (searchAbout.equals("상표명")) {

			} else if (searchAbout.equals("")) {

			}
		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("userId", userId);
		map.put("userType", userType);
		int totalContents = userDao.selectMarkCount(map);
		if (totalContents == 0)
			totalContents = 1;

		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun", ConstantUtil.PAGING,
				ConstantUtil.SMALL_LIST_SIZE);
		nation.getPageMap().put("userId", userId);
		nation.getPageMap().put("queryStr", queryStr);

		List<Map<String, Object>> markList = this.getAndSettingMarkVoList(userDao.selectMarkList(nation.getPageMap()));

		model.addObject("markList", markList);
		model.addObject("nation", nation);
		return model;
	}

	public List<Map<String, Object>> getAndSettingMarkVoList(List<Map<String, Object>> markVoList) {
		int mark_pk = 0;
		int size = markVoList.size();

		if(markVoList.size() > 0){
			for(int i=0; i<size;i++){
				Map<String, Object> map = markVoList.get(i);
				mark_pk = Integer.parseInt(map.get("mark_pk") + "");
				MarkHistoryVo markHistoryVo = userDao.selectMarkHistoryDetail(mark_pk).get(0);
				map.put("markHistoryVo", markHistoryVo);
				markVoList.set(i, map);
			}
		}

		return markVoList;
	}

	public List<MarkVo> getAndSettingMarkVoListFromMarkVo(List<MarkVo> markVoList) {
		int mark_pk = 0;
		int size = markVoList.size();
		List<MarkHistoryVo> markHistoryVoList = new ArrayList<MarkHistoryVo>();

		if(markVoList.size() > 0){
			if(markVoList.size() > 0){
				for(int i=0; i<size;i++){
					MarkVo vo = markVoList.get(i);
					mark_pk = vo.getMark_pk();
					MarkHistoryVo markHistoryVo = userDao.selectMarkHistoryDetail(mark_pk).get(0);
					markHistoryVoList.add(markHistoryVo);
					vo.setMarkHistoryVoList(markHistoryVoList);
					markVoList.set(i, vo);
				}
			}
		}

		return markVoList;
	}

	public UserVo getUserInfo(HttpServletRequest request) {
		String id = request.getParameter("id");
		return userDao.selectUserById(id);
	}

	public ModelAndView mvUploadFileList(HttpServletRequest request, String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		int mark_pk = Integer.parseInt(request.getParameter("mark_pk"));
		MarkVo markVo = this.selectMarkDetail(mark_pk);
		Map<String, List<String>> userUploadStatusMap = this.userUploadStatusSettingArray(markVo.getMarkFileVoList());
		model.addObject("mark_pk", mark_pk);
		model.addObject("markVo", markVo);
		model.addObject("userUploadStatusMap", userUploadStatusMap);
		return model;
	}

	public Map<String, List<String>> userUploadStatusSettingArray(List<MarkFileVo> markFileList) {
		// 사용자가 업로드한 파일과 업로드 안한 파일 목록을 분류해서 jsp에 뿌려준당.
		// jsp에서 if문 하면 복잡하기때문에 컨트롤단에서 실행중
		int array[] = new int[3];
		array[0] = ConstantUtil.MARK_UPLOADFILE_TYPE_SEAL;
		array[1] = ConstantUtil.MARK_UPLOADFILE_TYPE_ENTRUST;
		array[2] = ConstantUtil.MARK_UPLOADFILE_TYPE_JUMIN;

		List<String> okUploadList = new ArrayList<String>();
		List<String> noUploadList = new ArrayList<String>();

		int arrayLength = array.length;

		for (MarkFileVo vo : markFileList) {
			for (int i = 0; i < arrayLength; i++) {
				if (vo.getFile_type() == array[i]) {
					okUploadList.add(this.getMarkFileTypeName(array[i]));
					array[i] = -99;
					break;
				}
			}
		}
		for(int i=0; i<arrayLength; i ++){
			if(array[i] != -99){
				noUploadList.add(this.getMarkFileTypeName(array[i]));
			}
		}
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		map.put("okUploadList", okUploadList);
		map.put("noUploadList", noUploadList);
		return map;
	}

	public String getMarkFileTypeName(int type) {
		String returnStr = "";
		if (type == ConstantUtil.MARK_UPLOADFILE_TYPE_SEAL) {
			returnStr = "seal";
		} else if (type == ConstantUtil.MARK_UPLOADFILE_TYPE_ENTRUST) {
			returnStr = "entrust";
		} else if (type == ConstantUtil.MARK_UPLOADFILE_TYPE_JUMIN) {
			returnStr = "jumin";
		} else if (type == ConstantUtil.MARK_UPLOADFILE_TYPE_ATTACH) {
			returnStr = "attach";
		}
		return returnStr;
	}

	public ModelAndView mvMarkDetail(HttpServletRequest request) {
		String viewName = "";
		String userType = getCurrentUserType(session);
		if (ConstantUtil.APPLICANT.equals(userType)) {
			viewName = "/user/mypage/mark/mark_detail";
		} else if (ConstantUtil.PATENT.equals(userType)) {
			viewName = "/user/mypage/mark/mv_mark_detail";
		}
		ModelAndView model = new ModelAndView(viewName);
		int mark_pk = Integer.parseInt(request.getParameter("mark_pk"));
		model.addObject("mark_pk", mark_pk);
		model.addObject("markVo", this.selectMarkDetail(mark_pk));
		return model;
	}

	public ModelAndView mvMarkInfo(HttpServletRequest request, String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		int mark_pk = Integer.parseInt(request.getParameter("mark_pk"));
		int file_type = ConstantUtil.MARK_UPLOADFILE_TYPE_ATTACH;
		// 현재 상표
		model.addObject("markVo", this.selectMarkDetail(mark_pk));
		model.addObject("markAttachFile", selectMarkAttachFile(mark_pk, file_type));
		return model;
	}

	public MarkVo selectMarkVo(int mark_pk) {
		MarkVo markVo = new MarkVo();
		List<MarkHistoryVo> markHistoryVoList = userDao.selectMarkHistoryListFromMarkPk(mark_pk);
		markVo.setMarkHistoryVoList(markHistoryVoList);
		return markVo;
	}

	public MarkFileVo selectMarkAttachFile(int mark_pk, int file_type) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("mark_pk", mark_pk);
		map.put("file_type", file_type);

		return userDao.selectMarkAttachFile(map);
	}
	public String regisOkCounseling(HttpServletRequest request){
		QnaVo qnaVo = new QnaVo();
		int board_pk = insertBoard(request,qnaVo);
		qnaVo.setBoard_pk(board_pk);
		qnaVo.setIs_secret(0);
		userDao.insertQna(qnaVo);
		return "redirect:/common/counseling/mvDetail?pk="+board_pk;
	}
	public ModelAndView mvEditCounseling(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		model.addObject("qnaVo", commonDao.selectQnaVo(board_pk));
		return model;
	}
	public String editOkCounseling(HttpServletRequest request){
		int board_pk = this.updateBoard(request);
		return "redirect:/common/counseling/mvDetail?pk="+board_pk;
	}

	public String deleteBoard(HttpServletRequest request, String board_name){
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("board_pk", board_pk);
		map.put("board_name", board_name);
		return  userDao.deleteBoard(map)>0 ? "success" : "fail";
	}
	//게시물 등록
	public int insertBoard(HttpServletRequest request, BoardVo paramBoardVo){
		String title = request.getParameter("title");
		String writer = this.getCurrentUserId(session);
		String content = request.getParameter("ir1");
		if(content==null){//naver-editor가 아닌경우
			content = request.getParameter("content");
		}
		BoardVo boardVo = new BoardVo();
		boardVo.setTitle(title);
		boardVo.setWriter(writer);
		boardVo.setContent(content);
		boardVo.setHits(0);
		boardVo.setBoard_type_number(this.getBoardTypeNumber(paramBoardVo));
		userDao.insertBoard(boardVo);
		return boardVo.getBoard_pk();
	}
	public int getBoardTypeNumber(BoardVo boardVo) {
		int number = 0;
		if(ReviewVo.class.isInstance(boardVo)) {
			number = ConstantUtil.BOARD_TYPE_REVIEW;
		}else if(QnaVo.class.isInstance(boardVo)) {
			number = ConstantUtil.BOARD_TYPE_REVIEW;
		}

		return number;
	}
	public BoardVo settingBoardVo(HttpServletRequest request){
		String title = request.getParameter("title");
		String writer = this.getCurrentUserId(session);
		String content = request.getParameter("ir1");
		if(content==null){//naver-editor가 아닌경우
			content = request.getParameter("content");
		}
		BoardVo boardVo = new BoardVo();
		boardVo.setTitle(title);
		boardVo.setWriter(writer);
		boardVo.setContent(content);
		boardVo.setHits(0);
		return boardVo;
	}
	//게시물 수정
	public int updateBoard(HttpServletRequest request){
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		/*
		 * 수정한 사람 update할 때
		String writer = userService.getCurrentUserId(session);
		*/
		String title = request.getParameter("title");
		String content = request.getParameter("ir1");
		if(content==null){//naver-editor가 아닌경우
			content = request.getParameter("content");
		}
		BoardVo boardVo = new BoardVo();
		boardVo.setBoard_pk(board_pk);
		boardVo.setTitle(title);
		//boardVo.setWriter(writer);
		boardVo.setContent(content);

		userDao.updateBoard(boardVo);
		return board_pk;
	}

	public ModelAndView userWrite3(HttpServletRequest request, String viewName) {
		ModelAndView model = new ModelAndView(viewName);
		String user_id = getCurrentUserId(session);
		UserVo user = userDao.selectUserById(user_id);
		model.addObject("user", user);

		return model;
	}

	public String uploadReview(HttpServletRequest request) {
		ReviewVo reviewVo = new ReviewVo();
		int board_pk = insertBoard(request,reviewVo);
		System.out.println("board_pk::::"+board_pk);
		reviewVo.setBoard_pk(board_pk);
		if(userDao.selectReviewCheckWhereBoardPk(board_pk) == 0) {
			userDao.insertReview(reviewVo);
		}
		request.setAttribute("board_pk", board_pk);
		commonAjaxFileUploadService.ajaxFileupLoad(request);
		return "redirect:/common/support/review";
	}
}