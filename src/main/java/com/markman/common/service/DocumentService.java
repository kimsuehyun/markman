package com.markman.common.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.markman.service.S3Wrapper;
import com.markman.util.FileUtil;

@Service
public class DocumentService {
	@Autowired 
	S3Wrapper s3Wrapper;
	
	public ResponseEntity<byte[]> callDownload(String fileFullPath,String fileOriginName) throws IOException{
	    return s3Wrapper.download(FileUtil.ParsingS3ImgUrl(fileFullPath), fileOriginName);
	}
}
