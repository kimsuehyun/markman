package com.markman.common.vo;

import com.markman.util.FileUtil;

public class ImageVo {
	String origin_file_name;
	String save_file_name;
	String file_extension;
	String save_full_name;
	public ImageVo(String origin_file_name) {
		this.origin_file_name = origin_file_name;
		this.save_file_name = FileUtil.getToday(1);
		file_extension = FileUtil.getFileType(origin_file_name);
		this.save_full_name = this.save_file_name+"."+file_extension;
		System.out.println("origin_file_name::::::::::::::::"+origin_file_name);
	}

	public String getSave_full_name() {
		return save_full_name;
	}
	public void setSave_full_name(String save_full_name) {
		this.save_full_name = save_full_name;
	}

	public String getSave_file_name() {
		return save_file_name;
	}
	public void setSave_file_name(String save_file_name) {
		this.save_file_name = save_file_name;
	}
	public String getFile_extension() {
		return file_extension;
	}
	public void setFile_extension(String file_extension) {
		this.file_extension = file_extension;
	}


	public String getOrigin_file_name() {
		return origin_file_name;
	}

	public void setOrigin_file_name(String origin_file_name) {
		this.origin_file_name = origin_file_name;
	}




}
