package com.markman.common.vo;

public class BoardVo {
	int board_pk;
	String writer;
	String title;
	String content;
	String registration_date;
	int hits;
	int board_type_number;

	public int getBoard_type_number() {
		return board_type_number;
	}
	public void setBoard_type_number(int board_type_number) {
		this.board_type_number = board_type_number;
	}
	public int getBoard_pk() {
		return board_pk;
	}
	public void setBoard_pk(int board_pk) {
		this.board_pk = board_pk;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRegistration_date() {
		return registration_date;
	}
	public void setRegistration_date(String registration_date) {
		this.registration_date = registration_date;
	}
	public int getHits() {
		return hits;
	}
	public void setHits(int hits) {
		this.hits = hits;
	}
}
