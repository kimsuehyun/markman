package com.markman.common.vo;

public class EmailReceiverVo {
	int email_pk;
	String email;
	String id;
	int receiver_type;
	
	public int getEmail_pk() {
		return email_pk;
	}
	public void setEmail_pk(int email_pk) {
		this.email_pk = email_pk;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getReceiver_type() {
		return receiver_type;
	}
	public void setReceiver_type(int receiver_type) {
		this.receiver_type = receiver_type;
	}
}
