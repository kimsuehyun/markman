package com.markman.common.vo;

public class EmailFileVo {
	int email_pk;
	String file_url;
	String file_origin_name;
	
	public int getEmail_pk() {
		return email_pk;
	}
	public void setEmail_pk(int email_pk) {
		this.email_pk = email_pk;
	}
	public String getFile_url() {
		return file_url;
	}
	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}
	public String getFile_origin_name() {
		return file_origin_name;
	}
	public void setFile_origin_name(String file_origin_name) {
		this.file_origin_name = file_origin_name;
	}
}
