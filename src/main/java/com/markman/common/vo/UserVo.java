package com.markman.common.vo;

public class UserVo {
	String id; //아이디
	String pw; //비밀번호
	String email; //이메일
	String user_type; //사용자 유형
	String cell_num; //핸드폰 번호
	String tel_num; //유선전화번호
	String addr1; // 주소번호 
	String addr2; //기본주소
	String addr3; //상세주소
	String sign_up_date; //가입일
	String name; //사용자 이름(실명)
	int sns_receive;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String user_name) {
		this.name = user_name;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	public String getCell_num() {
		return cell_num;
	}
	public void setCell_num(String cell_num) {
		this.cell_num = cell_num;
	}
	public String getTel_num() {
		return tel_num;
	}
	public void setTel_num(String tel_num) {
		this.tel_num = tel_num;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getAddr3() {
		return addr3;
	}
	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}
	public String getSign_up_date() {
		return sign_up_date;
	}
	public void setSign_up_date(String sign_up_date) {
		this.sign_up_date = sign_up_date;
	}
	public int getSns_receive() {
		return sns_receive;
	}
	public void setSns_receive(int sns_receive) {
		this.sns_receive = sns_receive;
	}
}
