package com.markman.common.vo;

public class BoardImgVo {
	int board_img_seq;
	int board_pk;
	String img_url;
	String img_origin_name;
	int img_size;

	public int getBoard_img_seq() {
		return board_img_seq;
	}
	public void setBoard_img_seq(int board_img_seq) {
		this.board_img_seq = board_img_seq;
	}
	public int getBoard_pk() {
		return board_pk;
	}
	public void setBoard_pk(int board_pk) {
		this.board_pk = board_pk;
	}
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getImg_origin_name() {
		return img_origin_name;
	}
	public void setImg_origin_name(String img_origin_name) {
		this.img_origin_name = img_origin_name;
	}
	public int getImg_size() {
		return img_size;
	}
	public void setImg_size(int img_size) {
		this.img_size = img_size;
	}
}
