package com.markman.common.vo;

public class MarkInfoVo extends BoardVo{
	int board_pk;
	String access_link;
	String thumnail_img;
	
	public int getBoard_pk() {
		return board_pk;
	}
	public void setBoard_pk(int board_pk) {
		this.board_pk = board_pk;
	}
	public String getAccess_link() {
		return access_link;
	}
	public void setAccess_link(String access_link) {
		this.access_link = access_link;
	}
	public String getThumnail_img() {
		return thumnail_img;
	}
	public void setThumnail_img(String thumnail_img) {
		this.thumnail_img = thumnail_img;
	}
}
