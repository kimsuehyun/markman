package com.markman.common.vo;

public class EmailVo {
	int email_pk;
	String sender;
	String title;
	String content;
	String send_date;
	int mail_type;
	
	public int getEmail_pk() {
		return email_pk;
	}
	public void setEmail_pk(int email_pk) {
		this.email_pk = email_pk;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSend_date() {
		return send_date;
	}
	public void setSend_date(String send_date) {
		this.send_date = send_date;
	}
	public int getMail_type() {
		return mail_type;
	}
	public void setMail_type(int mail_type) {
		this.mail_type = mail_type;
	}
}
