package com.markman.common.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.markman.common.vo.BoardImgVo;
import com.markman.common.vo.BoardVo;
import com.markman.common.vo.FaqVo;

@Mapper
public interface CommonDao {
	public List<Map<String,Object>> selectQnaList(Map<String,String> map);//상표 상담 리스트
	public List<FaqVo> selectFaqList(Map<String,String> map);//자주 묻는 질문 리스트
	public List<Map<String,Object>> selectReplyList(int board_pk); //댓글 리스트
	public List<Map<String,String>> selectNoticeList(Map<String,String> map);//공지사항 리스트
	public Map<String,Object> selectBoardImgList(int board_pk); //게시판 이미지 리스트
	
	public Map<String,Object> selectQnaVo(int board_pk);//qna 상세 정보 가져오기
	public Map<String,Object> selectNoticeVo(int board_pk);//공지사항 상세 정보 가져오기
	public Map<String,Object> selectReviewVo(int board_pk);//후기 상세 정보 가져오기
	public FaqVo selectFaqVo(int board_pk);//자주묻는질문 상세 정보 가져오기
	
	public int selectBoardCount(Map<String,String> map);//게시판 수
	
	public void updateBoardHits(int board_pk);//조회수 증가
	public void updateBoard(BoardVo boardVo);//게시물
	
	public List<Map<String,String>> selectReviewList(Map<String,String> map);//리뷰 리스트
	
	public int deleteBoard(Map<String,Object> map);
	
}
