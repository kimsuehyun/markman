package com.markman.common.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.markman.mark.vo.MarkFileVo;

@Mapper
public interface CommonAjaxFileUploadDao {
//	CommonAjaxFileUploadService
	public int insertMarkFile(MarkFileVo markFileVo);
	public List<MarkFileVo> selectMarkFile(int mark_pk);

	public String selectMarkFileFromMarkPkAndFileType(Map<String, Integer >map);

	public int updateMarkFileFromMarkPkAndFileType(Map<String, String> map);
}
