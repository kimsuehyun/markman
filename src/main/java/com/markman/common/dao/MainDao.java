package com.markman.common.dao;

import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.markman.common.vo.UserVo;
import com.markman.patent.vo.PatentVo;

@Mapper
public interface MainDao {
	public Map<String,String> selectBannerData(String location);
	public int checkSameId(String id);
	public int insertUser(UserVo userVo);
	public int insertPatent(PatentVo patentVo);
}
