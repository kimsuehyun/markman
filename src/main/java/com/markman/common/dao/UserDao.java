package com.markman.common.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.markman.common.vo.BoardImgVo;
import com.markman.common.vo.BoardVo;
import com.markman.common.vo.QnaVo;
import com.markman.common.vo.ReviewVo;
import com.markman.common.vo.UserVo;
import com.markman.mark.vo.MarkAttachFIlesVo;
import com.markman.mark.vo.MarkFileVo;
import com.markman.mark.vo.MarkHistoryVo;
import com.markman.mark.vo.MarkVo;
import com.markman.patent.vo.PatentVo;

@Mapper
public interface UserDao {
	public List<MarkAttachFIlesVo> selectMarkAttachFilesWhereMarkPk(int mark_history_pk);

	public int selectReviewCheckWhereBoardPk(int board_pk);

	public String selectPatentImg(String id);
	public int insertPatentUser(PatentVo patentVo);
	public int updatePatentWhereId(Map<String,String> map);

	public int insertBoardNew(BoardVo boardVo);
	public int insertMarkAttachFiles(MarkAttachFIlesVo markAttachFIlesVo);
	public UserVo selectUserById(String user_id);
	public List<String> getUncheckedUser();
	//개수
	public int selectMarkCount(Map<String,String> map);
	//목록
	public List<Map<String,Object>> selectMarkList(Map<String,String> map); //상표 출원 내역

	//마크상세보기
	public MarkVo selectMarkDetail(int mark_pk);
	public MarkFileVo selectMarkAttachFile(Map<String,Integer> map);
	public List<MarkHistoryVo> selectMarkHistoryDetail(int mark_pk);
	public List<MarkHistoryVo> selectMarkHistoryListFromMarkPk(int mark_pk);

	public int insertBoard(BoardVo boardVo);//게시판 등록
	public int insertQna(QnaVo qnaVo);

	public int deleteBoard(Map<String,Object> map);

	public void updateBoard(BoardVo boardVo);//게시물

	public int insertReview(ReviewVo reviewVo);

	public String selectReviewFileFromBoardPk(Map<String, Integer>map);

	public int insertReviewFile(BoardImgVo boardImgVo);

	public int updateReviewFileFromBoardPk(Map<String, String> map);

}

