package com.markman.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.markman.common.vo.NaverEditorPhotoVo;
import com.markman.service.NaverEditorServce;
import com.markman.util.ConstantUtil;


@Controller
@RequestMapping("/naver")
public class NaverEditorController {
	
	@Autowired NaverEditorServce naverEditorServce;
	
	@RequestMapping(value = "/editor/add-item", method = RequestMethod.POST)
	public String detail(HttpServletRequest request, HttpServletResponse response, NaverEditorPhotoVo naverEditorPhotoVo) {
		return naverEditorServce.insertNaverEditorImg(request, naverEditorPhotoVo);
	}

	@RequestMapping(value = "/editor/image-popup", method = RequestMethod.GET)
	public ModelAndView detailGet2(HttpServletRequest request) {
		String menu = ConstantUtil.NOTICE;
		ModelAndView model = new ModelAndView("/common/editor/naver/photo_uploader");
		model.addObject("textareaName", request.getParameter("textareaName"));
		model.addObject("menu", menu);
		return model;
	}
	
	@RequestMapping(value="/hojin", method=RequestMethod.POST, produces = "application/text; charset=utf8")
	@ResponseBody
	public String reasdasd(){
		return "한글";
	}
}
