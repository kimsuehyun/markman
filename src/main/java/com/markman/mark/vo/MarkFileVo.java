package com.markman.mark.vo;

public class MarkFileVo {
	int mark_pk;
	int mark_file_pk;
	String file_url;
	String file_name;
	int file_type;
	String file_uploder;
	String logtime;
	
	public int getMark_pk() {
		return mark_pk;
	}
	public void setMark_pk(int mark_pk) {
		this.mark_pk = mark_pk;
	}
	public int getMark_file_pk() {
		return mark_file_pk;
	}
	public void setMark_file_pk(int mark_file_pk) {
		this.mark_file_pk = mark_file_pk;
	}
	public String getFile_url() {
		return file_url;
	}
	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public int getFile_type() {
		return file_type;
	}
	public void setFile_type(int file_type) {
		this.file_type = file_type;
	}
	public String getFile_uploder() {
		return file_uploder;
	}
	public void setFile_uploder(String file_uploder) {
		this.file_uploder = file_uploder;
	}
	public String getLogtime() {
		return logtime;
	}
	public void setLogtime(String logtime) {
		this.logtime = logtime;
	}
	@Override
	public String toString() {
		return "MarkFileVo [mark_pk=" + mark_pk + ", mark_file_pk=" + mark_file_pk + ", file_url=" + file_url
				+ ", file_name=" + file_name + ", file_type=" + file_type + ", file_uploder=" + file_uploder
				+ ", logtime=" + logtime + "]";
	}
}
