package com.markman.mark.vo;

public class MarkContentVo {
	int mark_num;
	String info;
	String base_right;
	String add_right;
	
	public int getMark_num() {
		return mark_num;
	}
	public void setMark_num(int mark_num) {
		this.mark_num = mark_num;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getBase_right() {
		return base_right;
	}
	public void setBase_right(String base_right) {
		this.base_right = base_right;
	}
	public String getAdd_right() {
		return add_right;
	}
	public void setAdd_right(String add_right) {
		this.add_right = add_right;
	}
}
