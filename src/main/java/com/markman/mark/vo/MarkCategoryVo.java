package com.markman.mark.vo;

public class MarkCategoryVo {
	int mark_category_seq;
	String supervisor;
	String directory;
	String sub;
	public int getMark_category_seq() {
		return mark_category_seq;
	}
	public void setMark_category_seq(int mark_category_seq) {
		this.mark_category_seq = mark_category_seq;
	}
	public String getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
}
