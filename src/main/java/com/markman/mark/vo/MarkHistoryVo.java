package com.markman.mark.vo;

import java.util.List;

public class MarkHistoryVo {

	int mark_history_pk;
	int mark_pk;
	String name;
	String type;
	String where_use;
	String category;
	String select_content;
	String regis_date;
	String feedback_date;
	String regis_posibility;
	List<MarkAttachFIlesVo> markAttachFIlesVoList;
	String patent_feedback;


	@Override
	public String toString() {
		return "MarkHistoryVo [mark_history_pk=" + mark_history_pk + ", mark_pk=" + mark_pk + ", name=" + name
				+ ", type=" + type + ", where_use=" + where_use + ", category=" + category + ", select_content="
				+ select_content + ", regis_date=" + regis_date + ", feedback_date=" + feedback_date
				+ ", regis_posibility=" + regis_posibility + ", markAttachFIlesVoList=" + markAttachFIlesVoList
				+ ", patent_feedback=" + patent_feedback + "]";
	}
	public int getMark_history_pk() {
		return mark_history_pk;
	}
	public void setMark_history_pk(int mark_history_pk) {
		this.mark_history_pk = mark_history_pk;
	}
	public int getMark_pk() {
		return mark_pk;
	}
	public void setMark_pk(int mark_pk) {
		this.mark_pk = mark_pk;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getWhere_use() {
		return where_use;
	}
	public void setWhere_use(String where_use) {
		this.where_use = where_use;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSelect_content() {
		return select_content;
	}
	public void setSelect_content(String select_content) {
		this.select_content = select_content;
	}
	public String getRegis_date() {
		return regis_date;
	}
	public void setRegis_date(String regis_date) {
		this.regis_date = regis_date;
	}
	public String getFeedback_date() {
		return feedback_date;
	}
	public void setFeedback_date(String feedback_date) {
		this.feedback_date = feedback_date;
	}
	public String getRegis_posibility() {
		return regis_posibility;
	}
	public void setRegis_posibility(String regis_posibility) {
		this.regis_posibility = regis_posibility;
	}
	public List<MarkAttachFIlesVo> getMarkAttachFIlesVoList() {
		return markAttachFIlesVoList;
	}
	public void setMarkAttachFIlesVoList(List<MarkAttachFIlesVo> markAttachFIlesVoList) {
		this.markAttachFIlesVoList = markAttachFIlesVoList;
	}
	public String getPatent_feedback() {
		return patent_feedback;
	}
	public void setPatent_feedback(String patent_feedback) {
		this.patent_feedback = patent_feedback;
	}




}
