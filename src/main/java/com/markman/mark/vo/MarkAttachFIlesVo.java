package com.markman.mark.vo;

public class MarkAttachFIlesVo {
	int mark_attach_files_pk;
	int mark_history_pk;
	String url;
	String file_name;
	String regDate;

	public int getMark_history_pk() {
		return mark_history_pk;
	}
	public void setMark_history_pk(int mark_history_pk) {
		this.mark_history_pk = mark_history_pk;
	}
	public int getMark_attach_files_pk() {
		return mark_attach_files_pk;
	}
	public void setMark_attach_files_pk(int mark_attach_files_pk) {
		this.mark_attach_files_pk = mark_attach_files_pk;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}



}
