package com.markman.mark.vo;

import java.util.List;

public class MarkVo {
	int mark_pk;
	String patent_id;
	String applicant_id;
	int step;
	String mark_regisDate;

	List<MarkHistoryVo> markHistoryVoList;
	List<MarkFileVo> markFileVoList;


	@Override
	public String toString() {
		return "MarkVo [mark_pk=" + mark_pk + ", patent_id=" + patent_id + ", applicant_id=" + applicant_id + ", step="
				+ step + ", mark_regisDate=" + mark_regisDate + ", markHistoryVoList=" + markHistoryVoList
				+ ", markFileVoList=" + markFileVoList + "]";
	}

	public String getMark_regisDate() {
		return mark_regisDate;
	}

	public void setMark_regisDate(String mark_regisDate) {
		this.mark_regisDate = mark_regisDate;
	}

	public List<MarkHistoryVo> getMarkHistoryVoList() {
		return markHistoryVoList;
	}

	public void setMarkHistoryVoList(List<MarkHistoryVo> markHistoryVoList) {
		this.markHistoryVoList = markHistoryVoList;
	}

	public int getMark_pk() {
		return mark_pk;
	}


	public void setMark_pk(int mark_pk) {
		this.mark_pk = mark_pk;
	}


	public String getPatent_id() {
		return patent_id;
	}


	public void setPatent_id(String patent_id) {
		this.patent_id = patent_id;
	}


	public String getApplicant_id() {
		return applicant_id;
	}


	public void setApplicant_id(String applicant_id) {
		this.applicant_id = applicant_id;
	}


	public int getStep() {
		return step;
	}


	public void setStep(int step) {
		this.step = step;
	}


	public List<MarkFileVo> getMarkFileVoList() {
		return markFileVoList;
	}


	public void setMarkFileVoList(List<MarkFileVo> markFileVoList) {
		this.markFileVoList = markFileVoList;
	}

}
