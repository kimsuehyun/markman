package com.markman.mark.vo;

public class MarkRegisterFileVo {
	
	int mark_register_file_pk;
	int mark_pk;
	String file_url;
	String file_name;
	int file_index;
	
	public int getMark_register_file_pk() {
		return mark_register_file_pk;
	}
	public void setMark_register_file_pk(int mark_register_file_pk) {
		this.mark_register_file_pk = mark_register_file_pk;
	}
	public int getMark_pk() {
		return mark_pk;
	}
	public void setMark_pk(int mark_pk) {
		this.mark_pk = mark_pk;
	}
	public String getFile_url() {
		return file_url;
	}
	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public int getFile_index() {
		return file_index;
	}
	public void setFile_index(int file_index) {
		this.file_index = file_index;
	}
	@Override
	public String toString() {
		return "MarkRegisterFileVo [mark_register_file_pk=" + mark_register_file_pk + ", mark_pk=" + mark_pk
				+ ", file_url=" + file_url + ", file_name=" + file_name + ", file_index=" + file_index + "]";
	}
	
	

}
