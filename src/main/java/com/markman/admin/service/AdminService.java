package com.markman.admin.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.markman.util.FileUtil;
import com.markman.admin.dao.AdminDao;
import com.markman.common.dao.MainDao;
import com.markman.common.service.CommonService;
import com.markman.common.service.UserService;
import com.markman.common.vo.BannerVo;
import com.markman.common.vo.BoardImgVo;
import com.markman.common.vo.BoardVo;
import com.markman.common.vo.EmailReceiverVo;
import com.markman.common.vo.EmailVo;
import com.markman.common.vo.FaqVo;
import com.markman.common.vo.MarkInfoVo;
import com.markman.common.vo.ReplyVo;
import com.markman.common.vo.UserVo;
import com.markman.patent.vo.PatentVo;
import com.markman.service.S3Wrapper;
import com.markman.util.ConstantUtil;
import com.markman.util.PageNavigator;
import com.markman.util.SendEmailUtils;

@Service
public class AdminService {
	@Autowired 	AdminDao adminDao;
	@Autowired HttpSession session;
	@Autowired CommonService commonService;
	@Autowired UserService userService;
	@Autowired MainDao mainDao;
	@Autowired S3Wrapper s3Wrapper;
	public ModelAndView getUserList(String userType, @PathVariable int pageNum, @PathVariable String search, String viewName){//회원 목록
		ModelAndView model = new ModelAndView();
		String sub="";
		if(userType.equals("ROLE_APPLICANT")) 	sub = "applicant";
		else if(userType.equals("ROLE_PATENT"))	sub = "patent";
		else									sub = "all";

		int totalContents = selectUserCount(userType, search);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		nation.getPageMap().put("user_type",userType);
		nation.getPageMap().put("search", search);
		List<UserVo> userList=adminDao.selectUserList(nation.getPageMap());

		model.addObject("search", search);
		model.addObject("nation", nation);
		model.addObject("userList", userList);
		model.addObject("sub", sub);
		model.setViewName(viewName);
		return model;
	}
	public int selectUserCount(String userType, String search){
		Map<String, String> map = new HashMap<String,String>();
		map.put("user_type", userType);
		map.put("search", search);
		return adminDao.selectUserCount(map);
	}
	public HashMap<String, Object> getUserInfo(String id){ //회원 정보 가져오기
		HashMap<String, Object> map = new HashMap<String, Object>();
		UserVo userVo = adminDao.selectUserVo(id);
		if(userVo.getUser_type().equals(ConstantUtil.PATENT)){//회원이 변리사인 경우
			map.put("patentVo", getPatentInfo(id));
		}
		map.put("userVo", userVo);
		return map;
	}

	public int deleteUser(String userId){//회원 삭제
		return adminDao.deleteUser(userId);
	}

	public PatentVo getPatentInfo(String id){//변리사 추가 정보 가져오기
		return adminDao.selectPatentVo(id);
	}
	public ModelAndView getAllMarkList(@PathVariable int pageNum, HttpServletRequest request, String viewName){//전체 상표 목록
		ModelAndView model = new ModelAndView();
		String step = request.getParameter("step");//하위 메뉴 활성화 변수
		String queryStr = "";//쿼리문
		String type = request.getParameter("filter_type");
		String start_date = request.getParameter("start_date");
		String end_date = request.getParameter("end_date");
		String search_type = request.getParameter("search_type");
		String search_data = request.getParameter("search_data");
		if(type!=null&&start_date!=null&&end_date!=null&&search_data!=null){//첫 화면이 아닐 때
			if(!start_date.isEmpty()&&!end_date.isEmpty()){
				queryStr +=" and mark.regis_date >= '"+start_date+"' and mark.regis_date < '"+end_date+"'";
			}
			if(!type.isEmpty()&&!type.equals("all")){//상표 유형
				queryStr += "and mark.type=";
				if(type.equals("kor"))	queryStr += "'KOR_MARK'";
				else if(type.equals("eng")) queryStr += "'ENG_MARK'";
				else if(type.equals("figure")) queryStr += "'FIGURE_MARK'";
			}
			if(!search_data.isEmpty()){
				if(search_type.equals("applicant")){
					queryStr += "and (mark.applicant_id like concat('%','"+search_data+"','%')"
							+ "or mark.applicant_id in(select id from user where user.name like concat('%','"+search_data+"','%')))";
				}else if(search_type.equals("patent")){
					queryStr += "and (mark.patent_id like concat('%','"+search_data+"','%') "
							+ "or mark.patent_id in(select id from user where user.name like concat('%','"+search_data+"','%')))";
				}else{
					queryStr += "and (mark.name like concat('%','"+search_data+"','%'))";
				}
			}
		}
		System.err.println("type"+type+"start_date"+start_date+"end_date"+end_date+"search_data"+search_data);
		HashMap<String, String> searchMap = new HashMap<String,String>();
		searchMap.put("step", step);
		searchMap.put("queryStr", queryStr);
		int	totalContents=adminDao.selectMarkCount(searchMap);

		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		nation.getPageMap().put("queryStr", queryStr);
		nation.getPageMap().put("step", step);
		List<HashMap<String,Object>> markList = adminDao.selectMarkList(nation.getPageMap());

		model.addObject("search", request.getParameter("search_word"));
		model.addObject("nation", nation);
		model.addObject("markList", markList);
		model.addObject("type", type);
		model.addObject("start_date", start_date);
		model.addObject("end_date", end_date);
		model.addObject("search_type", search_type);
		model.addObject("search_data", search_data);
		model.addObject("patentList", this.findPatentAjax(request));
		model.setViewName(viewName);
		return model;
	}

	public ModelAndView getNoticeList(@PathVariable int pageNum, HttpServletRequest request, String viewName){
		String board_name = ConstantUtil.NOTICE;
		ModelAndView model = new ModelAndView(viewName);
		int	totalContents=selectBoardCount(board_name);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		List<Map<String,String>> noticeList = adminDao.selectNoticeList(nation.getPageMap());

		model.addObject("noticeList", noticeList);
		model.addObject("nation", nation);
		return model;
	}

	public ModelAndView getBannerList(@PathVariable int pageNum, HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int	totalContents=adminDao.selectBannerCount();
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.SMALL_LIST_SIZE);
		List<BannerVo> bannerList = adminDao.selectBannerList(nation.getPageMap());

		model.addObject("nation", nation);
		model.addObject("bannerList", bannerList);
		return model;
	}

	public ModelAndView mvRegisBanner(String viewName){
		ModelAndView model = new ModelAndView(viewName);
		model.addObject("bannerName", adminDao.selectBannerNameList());
		return model;
	}

	public String regisOkBanner(HttpServletRequest request){
		String location = "";
		String banner_name=request.getParameter("banner_name");
		//이미지 업로드
		Map<String,String> map = imageUpload(request, "bannerimg", ConstantUtil.BANNER_IMG_PATH);

		//배너 위치
		if(banner_name.equals("main")) location = ConstantUtil.MAIN_BANNER_LOCATION;
		//등록자
		String userId = userService.getCurrentUserId(session);

		BannerVo bannerVo = new BannerVo();
		bannerVo.setBanner_name(banner_name);
		bannerVo.setBanner_img(map.get("img_url"));
		bannerVo.setAccess_link(request.getParameter("link"));
		bannerVo.setLocation(location);
		bannerVo.setRegistrant(userId);
		adminDao.insertBanner(bannerVo);
		return ConstantUtil.redirect("bannerList");
	}

	public int checkBanner(HttpServletRequest request){
		String banner_name = request.getParameter("banner_name");
		return adminDao.checkBanner(banner_name);
	}

	public ModelAndView mvRegisNotice(String viewName){
		ModelAndView model = new ModelAndView(viewName);
		return model;
	}

	public ModelAndView getQnaList(@PathVariable int pageNum, HttpServletRequest request, String viewName){
		String board_name = ConstantUtil.QNA;
		ModelAndView model = new ModelAndView(viewName);
		int	totalContents= selectBoardCount(board_name);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		List<Map<String,Object>> qnaList = adminDao.selectQnaList(nation.getPageMap());
		model.addObject("qnaList", qnaList);
		model.addObject("nation", nation);
		return model;
	}

	public ModelAndView mvDetailQna(int board_pk, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		model.addObject("qnaVo", adminDao.selectQnaVo(board_pk));
		model.addObject("replyList", adminDao.selectReplyList(board_pk));
		return model;
	}

	public ModelAndView getFaqList(@PathVariable int pageNum, HttpServletRequest request, String viewName){
		String board_name = ConstantUtil.FAQ;
		ModelAndView model = new ModelAndView(viewName);
		int	totalContents= selectBoardCount(board_name);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		List<FaqVo> faqList = adminDao.selectFaqList(nation.getPageMap());
		model.addObject("faqList", faqList);
		model.addObject("nation", nation);
		return model;
	}

	public ModelAndView mvDetailNotice(int board_pk, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		//조회수 증가
		increseBoardHits(board_pk);
		model.addObject("noticeVo", adminDao.selectNoticeVo(board_pk));
		model.addObject("boardImgList", adminDao.selectBoardImgList(board_pk));
		model.addObject("replyList", adminDao.selectReplyList(board_pk));
		return model;
	}

	public void increseBoardHits(int board_pk){
		adminDao.updateBoardHits(board_pk);
	}
	public ModelAndView mvDetailFaq(int board_pk, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		//조회수 증가
		increseBoardHits(board_pk);
		model.addObject("faqVo", adminDao.selectFaqVo(board_pk));
		return model;
	}

	public String deleteBoard(HttpServletRequest request, String board_name){
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("board_pk", board_pk);
		map.put("board_name", board_name);
		return  adminDao.deleteBoard(map)>0 ? "success" : "fail";
	}

	public String deleteOkReply(HttpServletRequest request){
		int reply_seq = Integer.parseInt(request.getParameter("reply_seq"));
		return adminDao.deleteReply(reply_seq)>0 ? "success" : "fail";
	}

	public String regisOkReply(HttpServletRequest request){
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		String content = request.getParameter("ir1");
		String writer = userService.getCurrentUserId(session);
		ReplyVo replyVo = new ReplyVo();
		replyVo.setBoard_pk(board_pk);
		replyVo.setContent(content);
		replyVo.setWriter(writer);
		adminDao.insertReply(replyVo);
		return ConstantUtil.redirect("notice/mvDetail/"+board_pk+"");
	}

	public String regisOkNotice(HttpServletRequest request){
		int board_pk = insertBoard(request);
		adminDao.insertNotice(board_pk);
		for(int i = 0 ; i <ConstantUtil.MAX_FILE_NUM ; i++ ){
			Map<String,String> map = imageUpload(request, "img"+i+"", ConstantUtil.NOTICE_IMG_PATH);
			if(!map.isEmpty()){
				insertBoardImg(board_pk, map);
			}
		}
		return ConstantUtil.redirect("notice/mvDetail/"+board_pk+"");
	}
	public int insertBoardImg(int board_pk, Map<String,String> map){//db 이미지 저장
		BoardImgVo boardImgVo = new BoardImgVo();
		boardImgVo.setBoard_pk(board_pk);
		boardImgVo.setImg_url(map.get("img_url"));
		boardImgVo.setImg_origin_name(map.get("img_origin_name"));
		boardImgVo.setImg_size(Integer.parseInt(map.get("img_size")));
		return adminDao.insertBoardImg(boardImgVo);
	}

	public int insertBoard(HttpServletRequest request){
		String title = request.getParameter("title");
		String writer = userService.getCurrentUserId(session);//작성자
		String content = request.getParameter("ir1");
		if(content==null){//naver-editor가 아닌경우
			content = request.getParameter("content");
		}
		BoardVo boardVo = new BoardVo();
		boardVo.setTitle(title);
		boardVo.setWriter(writer);
		boardVo.setContent(content);
		boardVo.setHits(0);
		adminDao.insertBoard(boardVo);
		return boardVo.getBoard_pk();
	}
	public ModelAndView mvRegisFaq(String viewName){
		ModelAndView model = new ModelAndView(viewName);
		return model;
	}
	public ModelAndView mvEditNotice(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		model.addObject("noticeVo", adminDao.selectNoticeVo(board_pk));
		model.addObject("boardImgList", adminDao.selectBoardImgList(board_pk));
		return model;
	}
	public ModelAndView getMarkInfoList(@PathVariable int pageNum, HttpServletRequest request, String viewName){
		String board_name = ConstantUtil.MARKINFO;
		ModelAndView model = new ModelAndView(viewName);
		int	totalContents=selectBoardCount(board_name);
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		List<MarkInfoVo> markInfoList = adminDao.selectMarkInfoList(nation.getPageMap());
		model.addObject("markInfoList", markInfoList);
		model.addObject("nation", nation);
		return model;
	}
	public ModelAndView getEmailList(@PathVariable int pageNum, HttpServletRequest reqeust, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int	totalContents= adminDao.selectEmailCount();
		if(totalContents==0)totalContents=1;
		PageNavigator nation = new PageNavigator(pageNum, totalContents, "suehyun" , ConstantUtil.PAGING, ConstantUtil.LIST_SIZE);
		List<Map<String,Object>> emailList = adminDao.selectEmailList(nation.getPageMap());
		model.addObject("emailList", emailList);
		model.addObject("nation", nation);
		return model;
	}

	public String editOkNotice(HttpServletRequest request){
		int board_pk = updateBoard(request);//게시판 업데이트
		adminDao.updateNotice(board_pk);//공지사항 업데이트
		for(int i=0;i<ConstantUtil.MAX_FILE_NUM;i++){
			String file = "img"+i+"";
			if(request.getParameter(file)!=null){
				Map<String,String> map = imageUpload(request, file, ConstantUtil.NOTICE_IMG_PATH);
				if(!map.isEmpty()){
					insertBoardImg(board_pk, map);
				}
			}
		}
		return ConstantUtil.redirect("notice/mvDetail/"+board_pk+"");
	}
	public ModelAndView mvRegisMarkInfo(String viewName){
		ModelAndView model = new ModelAndView(viewName);
		return model;
	}
	public int updateBoard(HttpServletRequest request){
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		/*
		 * 수정한 사람 update할 때
		String writer = userService.getCurrentUserId(session);
		*/
		String title = request.getParameter("title");
		String content = request.getParameter("ir1");

		BoardVo boardVo = new BoardVo();
		boardVo.setBoard_pk(board_pk);
		boardVo.setTitle(title);
		//boardVo.setWriter(writer);
		boardVo.setContent(content);

		adminDao.updateBoard(boardVo);
		return board_pk;
	}
	public String regisOkMarkInfo(HttpServletRequest request){
		int board_pk = insertBoard(request);

		//이미지 업로드
		Map<String,String> map = imageUpload(request, "thumnailimg", ConstantUtil.MARKINFO_IMG_PATH);
		MarkInfoVo markInfoVo = new MarkInfoVo();
		markInfoVo.setBoard_pk(board_pk);
		markInfoVo.setAccess_link(request.getParameter("access_link"));
		markInfoVo.setThumnail_img(map.get("img_url"));
		adminDao.insertMarkInfo(markInfoVo);

		return ConstantUtil.redirect("markInfoList");
	}

	public ModelAndView mvRegisMail(String viewName){
		ModelAndView model = new ModelAndView(viewName);
		return model;
	}

	public void regisOkEmail(HttpServletRequest request) throws IOException, EmailException{
		int receiverNum = Integer.parseInt(request.getParameter("receiverNum"));//수신인 수
		String title = request.getParameter("title");
		String content = request.getParameter("ir1");
		int receiver_type = ConstantUtil.EMAIL_TO;
		int mail_type = ConstantUtil.SEND_MAIL;
		int email_pk = insertEmail(title, content, mail_type);
		System.out.println("나온 이메일 핔에이::"+email_pk);
		String receiver[] = request.getParameter("receiver").split(">>");
		for(int i = 0; i<receiver.length ; i++){
			List<Map<String,String>> list = new ArrayList<Map<String,String>>();
			for(int j=0 ; j<ConstantUtil.MAX_FILE_NUM ; j++){//파일 업로드
				Map<String,String>map = imageUpload(request, "img"+j+"", ConstantUtil.MAIL_IMG_PATH);
				if(!map.isEmpty()){
					list.add(map);
				}
				//파일 db저장 필요
			}
			//수신인 저장 필요
			if(receiver!=null){
				SendEmailUtils.sendEmail(list,receiver[i],title,content);
				insertEmailReceiver(receiver[i], email_pk, receiver_type);
			}
		}
	}
	public int insertEmail(String title, String content, int mail_type){
		EmailVo emailVo = new EmailVo();
		emailVo.setTitle(title);
		emailVo.setContent(content);
		emailVo.setMail_type(mail_type);
		emailVo.setSend_date(FileUtil.getToday(1));
		emailVo.setSender(userService.getCurrentUserId(session));
		adminDao.insertEmail(emailVo);
		return emailVo.getEmail_pk();
	}

	public int insertEmailReceiver(String receiver, int email_pk, int receiver_type){
		String id = adminDao.selectUserIdByEmail(receiver);
		System.out.println("emailreceiver도착");
		EmailReceiverVo emailReceiverVo = new EmailReceiverVo();
		emailReceiverVo.setEmail_pk(email_pk);
		emailReceiverVo.setEmail(receiver);
		emailReceiverVo.setId(id);
		emailReceiverVo.setReceiver_type(receiver_type);
		return adminDao.insertEmailReceiver(emailReceiverVo);
	}
	public ModelAndView addressList(HttpServletRequest request,String viewName){
		ModelAndView model = new ModelAndView(viewName);
		String search = request.getParameter("search");
		Map<String,String> map = new HashMap<String,String>();
		map.put("user_type", "all");
		map.put("search", search);
		model.addObject("userList", adminDao.selectUserList(map));
		model.addObject("search", search);
		model.addObject("userCount", selectUserCount("all", ""));
		model.addObject("patentCount", selectUserCount(ConstantUtil.PATENT, ""));
		model.addObject("applicantCount", selectUserCount(ConstantUtil.APPLICANT, ""));
		return model;
	}
	public Map<String,Object> address(HttpServletRequest request){
		String search = request.getParameter("search");
		String user_type = request.getParameter("user_type");
		if(user_type.equals("applicant")) user_type = ConstantUtil.APPLICANT;
		else if(user_type.equals("patent")) user_type = ConstantUtil.PATENT;
		else if(!user_type.isEmpty()) user_type = ConstantUtil.ADMIN;
		Map<String,String> searchMap = new HashMap<String,String>();
		Map<String,Object> map = new HashMap<String,Object>();
		searchMap.put("user_type", user_type);
		searchMap.put("search", search);
		map.put("userList", adminDao.selectUserList(searchMap));
		return map;
	}
	public String regisOkFaq(HttpServletRequest request){
		int board_pk = insertBoard(request);
		adminDao.insertFaq(board_pk);
		return ConstantUtil.redirect("faq/mvDetail/"+board_pk+"");
	}
	public ModelAndView mvEditFaq(HttpServletRequest request, String viewName){
		ModelAndView model = new ModelAndView(viewName);
		int board_pk = Integer.parseInt(request.getParameter("board_pk"));
		model.addObject("faqVo", adminDao.selectFaqVo(board_pk));
		return model;
	}
	public String editOkFaq(HttpServletRequest request){
		int board_pk = updateBoard(request);//게시판 업데이트
		return ConstantUtil.redirect("faq/mvDetail/"+board_pk+"");
	}
	public int selectBoardCount(String board_name){//게시판 수 가져오기(notice,qna,faq)
		Map<String,String> map = new HashMap<String,String>();
		map.put("board_name", board_name);
		return adminDao.selectBoardCount(map);
	}

	public List<UserVo> findPatentAjax(HttpServletRequest request){
		Map<String, String> map = new HashMap<String, String>();
		map.put("user_type", ConstantUtil.PATENT);
		map.put("search", request.getParameter("search"));
		return adminDao.selectUserList(map);
	}

	public ModelAndView matchPatent(HttpServletRequest request){
		int mark_pk = Integer.parseInt(request.getParameter("mark_pk"));
		String patent_id = request.getParameter("patent_id");
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("mark_pk", mark_pk);
		map.put("patent_id", patent_id);
		adminDao.updateMarkPatentId(map);
		return this.getAllMarkList(1, request, "/admin/mark/mark");
	}


	public  Map<String,String> imageUpload(HttpServletRequest request, String file, String path){
		//이미지 업로드
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
		MultipartFile imageFile = multipartRequest.getFiles(file).get(0);
		String img_origin_name = imageFile.getOriginalFilename();
		Map<String,String> map = new HashMap<String,String>();
		if(!"".equals(img_origin_name)){
			String file_name = FileUtil.getToday(1)+"."+ FileUtil.getFileType(img_origin_name);
			String file_path = path+"/"+file_name;
			String img_url =ConstantUtil.S3DEFAULT+"/"+file_path;
			int img_size = (int) imageFile.getSize();
			System.out.println("img_origin_name"+img_origin_name+"img_url"+img_url);
			s3Wrapper.deleteObject(FileUtil.ParsingS3ImgUrl(img_url));
			s3Wrapper.upload(imageFile, file_path);

			map.put("file_name", file_name);//서버에 저장되는 파일명
			map.put("img_origin_name", img_origin_name);//원래 파일명
			map.put("img_size", img_size+"");
			map.put("img_url", img_url);
		}
		return map;
	}
}