package com.markman.admin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.markman.admin.dao.AdminDao;
import com.markman.admin.service.AdminService;
import com.markman.common.vo.UserVo;
import com.markman.util.ConstantUtil;


@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired AdminService adminService;
	@Autowired AdminDao adminDao;

	@RequestMapping(value="/redirect.do", method=RequestMethod.GET)
	public String redirect(HttpServletRequest request){
		String path = request.getParameter("path");
		return "redirect:/admin/"+path;
	}

	//회원 관리
	@RequestMapping("/")
	public ModelAndView userList(HttpServletRequest request){//처음 관리자 화면(하위 메뉴 비활성화)
		return getAllUserList(1, request.getParameter("search_word"));
	}
	@RequestMapping("/userList")
	public ModelAndView allUserList(HttpServletRequest request){//전체 회원 목록(하위메뉴 활성화)
		return getAllUserList(1, request.getParameter("search_word"));
	}
	@RequestMapping("/userList/{pageNum}")
	public ModelAndView getAllUserList(@PathVariable int pageNum, @PathVariable String search){//전체 회원 목록
		return adminService.getUserList("all",pageNum, search, "/admin/user/user");
	}
	@RequestMapping("/applicantList")
	public ModelAndView applicantList(HttpServletRequest request){
		return getApplicantList(1,request.getParameter("search_word"));
	}
	@RequestMapping("/applicantList/{pageNum}")
	public ModelAndView getApplicantList(@PathVariable int pageNum, @PathVariable String search){//출원인 목록
		return adminService.getUserList(ConstantUtil.APPLICANT,pageNum, search, "/admin/user/applicant");
	}
	@RequestMapping("/patentList")
	public ModelAndView patentList(HttpServletRequest request){
		return getPatentList(1, request.getParameter("search_word"));
	}
	@RequestMapping("/patentList/{pageNum}")
	public ModelAndView getPatentList(@PathVariable int pageNum, @PathVariable String search ){//변리사 목록
		return adminService.getUserList(ConstantUtil.PATENT,pageNum, search, "/admin/user/patent");
	}
	@RequestMapping("/user/info")
	@ResponseBody
	public HashMap<String, Object> userInfo(String id){//회원 정보 가져오기
		return adminService.getUserInfo(id);
	}
	@RequestMapping(value="/user/deleteOk", method = RequestMethod.POST)
	@ResponseBody
	public int deleteUser(String userId){//회원 삭제
		return adminService.deleteUser(userId);
	}
	@RequestMapping("/markList")
	public ModelAndView allMarkList(HttpServletRequest request){//전체 상표 목록 페이지 이동
		return getAllMarkList(1,request);
	}
	@RequestMapping("markList/{pageNum}")
	public ModelAndView getAllMarkList(@PathVariable int pageNum, HttpServletRequest request){//전체 상표 목록
		return adminService.getAllMarkList(pageNum, request, "/admin/mark/mark");
	}

	@RequestMapping("/noticeList")
	public ModelAndView noticeList(HttpServletRequest request){//공지사항 목록 페이지 이동
		return getNoticeList(1,request);
	}
	@RequestMapping("noticeList/{pageNum}")
	public ModelAndView getNoticeList(@PathVariable int pageNum, HttpServletRequest request){//공지사항 목록
		return adminService.getNoticeList(pageNum, request, "/admin/board/notice");
	}
	@RequestMapping(value="/notice/mvDetail/{board_pk}")
	public ModelAndView mvDetailNotice(@PathVariable int board_pk){
		return adminService.mvDetailNotice(board_pk, "/admin/board/detail_notice");
	}
	@RequestMapping("/notice/mvRegis")
	public ModelAndView mvRegisNotice(){//공지사항 등록페이지 이동
		return adminService.mvRegisNotice("/admin/board/regis_notice");
	}
	@RequestMapping(value="/notice/deleteOk",method=RequestMethod.POST)
	@ResponseBody
	public String deleteOkNotice(HttpServletRequest request){
		return adminService.deleteBoard(request, ConstantUtil.NOTICE);
	}
	@RequestMapping(value="/notice/regisOk",method=RequestMethod.POST)
	public String regisOkNotice(HttpServletRequest request){
		return adminService.regisOkNotice(request);
	}
	@RequestMapping("/notice/mvEdit")
	public ModelAndView mvEditNotice(HttpServletRequest request){
		return adminService.mvEditNotice(request,"/admin/board/edit_notice");
	}
	@RequestMapping("/notice/editOk")
	public String editOkNotice(HttpServletRequest request){
		return adminService.editOkNotice(request);
	}

	@RequestMapping("/bannerList")
	public ModelAndView bannerList(HttpServletRequest request){
		return getBannerList(1,request);
	}
	@RequestMapping("/bannerList/{pageNum}")
	public ModelAndView getBannerList(@PathVariable int pageNum, HttpServletRequest request){
		return adminService.getBannerList(pageNum, request, "/admin/setting/banner");
	}
	@RequestMapping("/banner/mvRegis")
	public ModelAndView mvRegisBanner(){//배너 등록 페이지 이동
		return adminService.mvRegisBanner("/admin/setting/regis_banner");
	}
	@RequestMapping(value="/banner/regisOk", method=RequestMethod.POST)
	public String regisOkBanner(HttpServletRequest request){//배너 등록
		return adminService.regisOkBanner(request);
	}
	@RequestMapping(value="/banner/check", method=RequestMethod.POST)
	@ResponseBody
	public int checkBanner(HttpServletRequest request){
		return adminService.checkBanner(request);
	}

	@RequestMapping("/qnaList")
	public ModelAndView qnaList(HttpServletRequest request){//qna 목록 페이지 이동
		return getQnaList(1,request);
	}

	@RequestMapping("/qnaList/{pageNum}")
	public ModelAndView getQnaList(@PathVariable int pageNum, HttpServletRequest request){//qna 목록 페이지
		return adminService.getQnaList(pageNum, request, "/admin/board/qna");
	}

	@RequestMapping("/qna/mvDetail/{board_pk}")
	public ModelAndView mvDetailQna(@PathVariable int board_pk){
		return adminService.mvDetailQna(board_pk, "/admin/board/detail_qna");
	}

	@RequestMapping("/faqList")
	public ModelAndView faqList(HttpServletRequest request){//faq 목록 페이지 이동
		return getFaqList(1,request);
	}
	@RequestMapping("/faqList/{pageNum}")
	public ModelAndView getFaqList(@PathVariable int pageNum, HttpServletRequest request){//faq 목록 페이지
		return adminService.getFaqList(pageNum, request, "/admin/board/faq");
	}
	@RequestMapping("/faq/mvDetail/{board_pk}")
	public ModelAndView mvDetailFaq(@PathVariable int board_pk){
		return adminService.mvDetailFaq(board_pk, "/admin/board/detail_faq");
	}
	@RequestMapping("/faq/mvRegis")
	public ModelAndView mvRegisFaq(){
		return adminService.mvRegisFaq("/admin/board/regis_faq");
	}
	@RequestMapping(value="/faq/deleteOk",method=RequestMethod.POST)
	@ResponseBody
	public String deleteOkFaq(HttpServletRequest request){
		return adminService.deleteBoard(request, ConstantUtil.FAQ);
	}
	@RequestMapping(value="/faq/regisOk",method=RequestMethod.POST)
	public String regisOkFaq(HttpServletRequest request){
		return adminService.regisOkFaq(request);
	}
	@RequestMapping("/faq/mvEdit")
	public ModelAndView mvEditFaq(HttpServletRequest request){
		return adminService.mvEditFaq(request,"/admin/board/edit_faq");
	}
	@RequestMapping("/faq/editOk")
	public String editOkFaq(HttpServletRequest request){
		return adminService.editOkFaq(request);
	}

	@RequestMapping("/emailList")
	public ModelAndView emailList(HttpServletRequest request){
		return getEmailList(1,request);
	}
	@RequestMapping("/emailList/{pageNum}")
	public ModelAndView getEmailList(@PathVariable int pageNum, HttpServletRequest request){
		return adminService.getEmailList(pageNum, request, "/admin/sns/email");
	}
	@RequestMapping(value="/qna/deleteOk",method=RequestMethod.POST)
	@ResponseBody
	public String deleteOkQna(HttpServletRequest request){
		return adminService.deleteBoard(request, ConstantUtil.QNA);
	}
	@RequestMapping(value="/reply/deleteOk",method=RequestMethod.POST)
	@ResponseBody
	public String deleteOkReply(HttpServletRequest request){
		return adminService.deleteOkReply(request);
	}
	@RequestMapping(value="/reply/regisOk",method=RequestMethod.POST)
	public String regisOkReply(HttpServletRequest request){
		return adminService.regisOkReply(request);
	}
	@RequestMapping("/markInfoList")
	public ModelAndView MarkInfoList(HttpServletRequest request){//상표 정보
		return getMarkInfoList(1,request);
	}
	@RequestMapping("/markInfoList/{pageNum}")
	public ModelAndView getMarkInfoList(@PathVariable int pageNum, HttpServletRequest request){
		return adminService.getMarkInfoList(pageNum, request, "/admin/board/markinfo");
	}
	@RequestMapping("/markInfo/mvRegis")
	public ModelAndView mvRegisMarkInfo(){//공지사항 등록페이지 이동
		return adminService.mvRegisMarkInfo("/admin/board/regis_markinfo");
	}
	@RequestMapping(value="/markInfo/regisOk", method=RequestMethod.POST)
	public String regisOkMarkInfo(HttpServletRequest request){
		return adminService.regisOkMarkInfo(request);
	}
	@RequestMapping("/mail/mvRegis")
	public ModelAndView mvRegisMail(){//이메일 작성 페이지 이동
		return adminService.mvRegisMail("/admin/sns/regis_email");
	}
	@RequestMapping(value="/email/regisOk", method=RequestMethod.POST)
	public void regisOkEmail(HttpServletRequest request) throws IOException, EmailException{
		adminService.regisOkEmail(request);
	}
	@RequestMapping(value="/addressList")
	public ModelAndView addressList(HttpServletRequest request){//주소록 팝업창
		return adminService.addressList(request,"/common/pop-up/address");
	}
	@RequestMapping(value="/address")
	@ResponseBody
	public Map<String,Object> address(HttpServletRequest request){//주소록 팝업창
		return adminService.address(request);
	}
	@RequestMapping(value="/findPatentAjax")
	@ResponseBody
	public List<UserVo> findPatentAjax(HttpServletRequest request){//변리사 매칭 팝업창
		return adminService.findPatentAjax(request);
	}
	@RequestMapping(value="/mark/matchPatent", method=RequestMethod.POST)
	public ModelAndView matchPatent(HttpServletRequest request){
		return adminService.matchPatent(request);
	}
	@RequestMapping("/inquisition")
	public ModelAndView inquisition(HttpServletRequest request){
		return new ModelAndView("/admin/board/inquisition");
	}
}
