package com.markman.admin.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.markman.common.vo.BannerNameVo;
import com.markman.common.vo.BannerVo;
import com.markman.common.vo.BoardImgVo;
import com.markman.common.vo.BoardVo;
import com.markman.common.vo.EmailReceiverVo;
import com.markman.common.vo.EmailVo;
import com.markman.common.vo.FaqVo;
import com.markman.common.vo.MarkInfoVo;
import com.markman.common.vo.NoticeVo;
import com.markman.common.vo.QnaVo;
import com.markman.common.vo.ReplyVo;
import com.markman.common.vo.UserVo;
import com.markman.patent.vo.PatentVo;

@Mapper
public interface AdminDao {
	//게시물 수
	public int selectUserCount(Map<String,String>map);//회원 목록 수
	public int selectMarkCount(Map<String,String> map);//전체 상표 수 
	public int selectBannerCount();//배너 개수
	public int selectBoardCount(Map<String,String> map);//게시판 수
	public int selectEmailCount();//메일 수
	
	//목록
	public List<UserVo> selectUserList(Map<String,String> map);//회원 목록
	public List<HashMap<String,Object>> selectMarkList(Map<String,String> map); //전체 상표 목록
	public List<BannerNameVo> selectBannerNameList(); //배너 위치 목록 
	public List<BannerVo> selectBannerList(Map<String,String> map);//배너 리스트
	public List<Map<String,Object>> selectQnaList(Map<String,String> map);//qna 리스트
	public List<Map<String,String>> selectNoticeList(Map<String,String> map);//공지사항 리스트
	public List<FaqVo> selectFaqList(Map<String,String> map);//자주 묻는 질문 리스트
	public List<Map<String,Object>> selectReplyList(int board_pk); //댓글 리스트
	public List<BoardImgVo> selectBoardImgList(int board_pk); //게시판 이미지 리스트
	public List<MarkInfoVo> selectMarkInfoList(Map<String,String> map); //상표정보 리스트
	public List<Map<String,Object>> selectEmailList(Map<String,String> map); //이메일 리스트
	
	
	//상세정보
	public UserVo selectUserVo(String id);
	public PatentVo selectPatentVo(String userId); //변리사 정보 가져오기
	public Map<String,Object> selectQnaVo(int board_pk);//qna 상세 정보 가져오기
	public Map<String,Object> selectNoticeVo(int board_pk);//공지사항 상세 정보 가져오기
	public FaqVo selectFaqVo(int board_pk);
	
	//등록
	public int insertBanner(BannerVo bannerVo);//배너등록
	public int insertReply(ReplyVo replyVo);//답변 등록
	public int insertBoard(BoardVo boardVo);//게시판 등록
	public int insertNotice(int board_pk);//공지사항 등록
	public int insertBoardImg(BoardImgVo boardImgVo);//게시판 이미지 등록
	public int insertMarkInfo(MarkInfoVo markInfoVo);//상표정보 등록
	public int insertEmail(EmailVo emailVo); //이메일 등록
	public int insertEmailReceiver(EmailReceiverVo emailReceiverVo);
	public int insertFaq(int board_pk);//자주묻는질문 등록
	
	//삭제
	public int deleteUser(String userId);
	public int deleteBoard(Map<String,Object> map);
	public int deleteReply(int reply_seq);
	
	//확인
	public int checkBanner(String banner_name);//등록된 배너인지 확인
	
	//업데이트
	public void updateBoardHits(int board_pk);
	public void updateBoard(BoardVo boardVo);//게시물
	public void updateNotice(int board_pk);//공지사항
	public void updateMarkPatentId(Map<String, Object> map);
	
	//일부
	public String selectUserIdByEmail(String email); //이메일 주소로 회원id 가져오기
	
}
