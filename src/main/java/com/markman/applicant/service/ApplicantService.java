package com.markman.applicant.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.markman.applicant.dao.ApplicantDao;
import com.markman.common.service.CommonAjaxFileUploadService;
import com.markman.common.service.UserService;
import com.markman.mark.vo.MarkHistoryVo;
import com.markman.mark.vo.MarkVo;
import com.markman.util.ConstantUtil;

@Service
public class ApplicantService {
	@Autowired UserService userService;
	@Autowired ApplicantDao applicantDao;
	@Autowired HttpSession session;
	@Autowired CommonAjaxFileUploadService commonAjaxFileUploadService;

	public void insertApplicant(String id){//출원인테이블에 회원 추가
		applicantDao.insertApplicant(id);
	}
	public ModelAndView mvMarkApply(String viewName){
		ModelAndView model = new ModelAndView(viewName);
		model.addObject("markContentVo", applicantDao.selectMarkContentVo());
		model.addObject("bigCategoryList", applicantDao.selectBigCategoryList());
		return model;
	}
	public ModelAndView MarkApplyOk(HttpServletRequest request, MarkHistoryVo markHistoryVo,String viewName){
		//마크 테이블과, 마크 히스토리 테이블에 insert한다.
		//mark_attach_files에는 mark_pk가 있는게 아니라. 히스토리 pk가 있어야 한다. 이미지도 계속 바꿀수 있기 때문
		ModelAndView model = new ModelAndView(viewName);
		MarkVo markVo = new MarkVo();
		String applicant_id = userService.getCurrentUserId(session);
		markVo.setPatent_id("");
		markVo.setStep(0);
		markVo.setApplicant_id(applicant_id);
		int mark_pk = this.insertMarkVoAndReturnMarkPk(markVo);
		markHistoryVo.setCategory(request.getParameter("markContetnArray"));
		markHistoryVo.setMark_pk(mark_pk);
		applicantDao.insertMarkHistory(markHistoryVo);
		request.setAttribute("mark_history_pk", markHistoryVo.getMark_history_pk());
		//이미지목록들 업데이트 하는 곳
		//TABLE : mark_attach_files에 insert한다. (n개)
		commonAjaxFileUploadService.ajaxFileListupLoad(request);
		return model;
	}
	public int insertMarkVoAndReturnMarkPk(MarkVo markVo){
		applicantDao.insertMark(markVo);
		return markVo.getMark_pk();
	}
	public List getMidCategory(HttpServletRequest request){
		return this.getMarkCategory(request, ConstantUtil.MID_MARK);
	}
	public List getServiceCategory(HttpServletRequest request){
		return this.getMarkCategory(request, ConstantUtil.SERVICE_MARK);
	}
	public List getMarkContent(HttpServletRequest request){
		return this.getMarkCategory(request, ConstantUtil.MARK_CONTENT);
	}

	public List getMarkCategory(HttpServletRequest request,int arrNum){
		List<String> directoryList = applicantDao.selectMarkDirectory();
		Set<String> category = new HashSet<String>();
		String bigCategory = request.getParameter("big");
		String midCategory = request.getParameter("mid");
		String serviceCategory = request.getParameter("service");
		for(int i=0;i<directoryList.size();i++){
			String [] arr = directoryList.get(i).split(">");
			if(arrNum==1){
				if(arr[0].equals(bigCategory))
				 category.add(arr[arrNum]);
			}else if(arrNum==2){
				if(arr[0].equals(bigCategory)&&arr[1].equals(midCategory))
				 category.add(arr[arrNum]);
			}else{
				if(arr[0].equals(bigCategory)&&arr[1].equals(midCategory)&&arr[2].equals(serviceCategory))
				 category.add(arr[arrNum]);
			}
		}
		List<String> categoryList = new ArrayList<String>(category);
		return categoryList;
	}
}
