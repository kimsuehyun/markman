package com.markman.applicant.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.markman.mark.vo.MarkContentVo;
import com.markman.mark.vo.MarkHistoryVo;
import com.markman.mark.vo.MarkRegisterFileVo;
import com.markman.mark.vo.MarkVo;

@Mapper
public interface ApplicantDao {
	public void insertApplicant(String id);
	public int insertMark(MarkVo markVo);
	public int insertMarkHistory(MarkHistoryVo markHistoryVo);
	public int insertMarkRegisterFile(MarkRegisterFileVo markRegisterFileVo);
	public List<MarkRegisterFileVo> selectMarkRegisterFileFromMarkPk(int mark_pk);

	//select
	public List<MarkContentVo> selectMarkContentVo();
	public List<Map<String,String>> selectBigCategoryList();
	public List<String> selectMarkDirectory();
}
