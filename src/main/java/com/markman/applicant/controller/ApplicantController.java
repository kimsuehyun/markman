package com.markman.applicant.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.markman.applicant.service.ApplicantService;
import com.markman.mark.vo.MarkHistoryVo;

@Controller
@RequestMapping("/applicant")
public class ApplicantController {
	@Autowired
	ApplicantService applicantService;

	@RequestMapping("/mark/mvApply")
	public ModelAndView mvMarkApply(){
		return applicantService.mvMarkApply("/user/mark/mark_apply");
	}
	@RequestMapping(value="/mark/applyOk", method=RequestMethod.POST)
	public ModelAndView MarkApplyOk(HttpServletRequest request,MarkHistoryVo markHistoryVo){
		return applicantService.MarkApplyOk(request,markHistoryVo,  "/user/mark/mark_apply");
		//return applicantService.MarkApplyOk(markHistoryVo,  "/common/mark/apply_complete");//출원 신청 완료 페이지
	}

	@RequestMapping(value= "/mark/getMidCategory", method=RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> getMidCategory(HttpServletRequest request){
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("midCategoryList",applicantService.getMidCategory(request));
		return map;
	}
	@ResponseBody
	@RequestMapping(value= "/mark/getServiceCategory", method=RequestMethod.POST)
	public Map<String,Object> getServiceCategory(HttpServletRequest request){
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("serviceCategoryList",applicantService.getServiceCategory(request));
		return map;
	}

	@ResponseBody
	@RequestMapping(value= "/mark/getMarkContent", method=RequestMethod.POST)
	public Map<String,Object> getMarkContent(HttpServletRequest request){
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("markContentList", applicantService.getMarkContent(request));
		return map;
	}
	@RequestMapping("/test")
	public ModelAndView markPayment(HttpServletRequest request){
		return new ModelAndView("/user/mark/mark_apply_mv");
	}
}
