package com.markman.util;

public class ConstantUtil {
	//사용자 유형
	public static final String ADMIN = "ROLE_ADMIN";
	public static final String APPLICANT = "ROLE_APPLICANT";
	public static final String PATENT = "ROLE_PATENT";

	//PageNation
	public static final int LIST_SIZE = 15;//한 페이지당 게시물 수
	public static final int PAGING = 5; //페이지네이션 수
	public static final int SMALL_LIST_SIZE = 5;
	public static final int REVIEW_LIST_SIZE = 10;

	//파일 업로드
	public static final String BUCKET = "ideamarkman";
	public static final String S3DEFAULT= "https://s3.ap-northeast-2.amazonaws.com/"+BUCKET;
	//파일 경로
	public static final String PROFILE_IMG_PATH = "user/profile";
	public static final String BANNER_IMG_PATH = "admin/image/banner";
	public static final String NOTICE_IMG_PATH = "admin/image/notice";
	public static final String MARKINFO_IMG_PATH = "admin/image/markinfo";
	public static final String MAIL_IMG_PATH = "admin/image/mail";
	public static final String FAQ_IMG_PATH = "admin/image/faq";
	public static final String QNA_IMG_PATH = "admin/image/qna";
	public static final String USER_UPLODER_ROOT = "user/upload";
	public static final String USER_UPLODER_MARK_SEAL = "mark/seal";
	public static final String USER_UPLODER_MARK_ENTRUST = "mark/entrust";
	public static final String USER_UPLODER_MARK_JUMIN = "mark/jumin";
	public static final String USER_UPLODER_FILES = "mark/attach";

	//리뷰
	public static final String USER_UPLODER_SUPPORT_REVIEW = "support/review";

	//변리사 프로필 사진
	public static final String USER_PATENT_PROFILEIMG = "profile";
	public static final String USER_PATENT_REGIS = "regis";



	//상표등록 서류관리 파일 유형
	public static final int MARK_UPLOADFILE_TYPE_SEAL = 1;
	public static final int MARK_UPLOADFILE_TYPE_ENTRUST = 2;
	public static final int MARK_UPLOADFILE_TYPE_JUMIN = 3;
	public static final int MARK_UPLOADFILE_TYPE_ATTACH = 4; //상표 첨부파일

	//상표 등록 단계
	public static final int MARK_IN_WRITE = 0;//상표 정보 작성중(임시 저장)
	public static final int MARK_IN_REQUEST = 1;//상표 출원 신청(변리사 매칭 전)

	//상표 종류
	public static final String KOR = "KOR_MARK";
	public static final String ENG = "ENG_MARK";
	public static final String FIGURE = "FIGURE_MARK";

	//메뉴
	public static final String NOTICE = "notice"; //공지사항 관리
	public static final String QNA = "qna";//상표 상담 관리
	public static final String FAQ = "faq";//자주 묻는 질문 관리
	public static final String MARKINFO = "mark_info";
	public static final String REVIEW = "review";


	//배너 위치
	public static final String MAIN_BANNER_LOCATION = "home/index";

	//etc
	public static final int MAX_FILE_NUM = 3;//첨부파일 최대 개수

	//redirect
	public static String redirect(String path){
		String url = "redirect:/admin/redirect.do?path="+path;
		return url;
	}
	//관리자 메일 정보
	public static final String ADMIN_MAIL = "관리자 메일 주소 설정 필요";
	public static final String ADMIN_MAIL_PASSWORD = "관리자 메일 비밀번호 설정 필요";
	public static final String ADMIN_MAIL_NAME = "마크맨";

	//메일 유형
	public static final int SEND_MAIL = 1;
	public static final int SAVE_MAIL = 0;
	public static final int RESERVE_MAIL = -1;

	//수신인 유형
	public static final int EMAIL_TO = 0;
	public static final int EMAIL_CC = 1;
	public static final int EMAIL_BCC = 2;

	//상표 카테고리 번호
	public static final int BIG_MARK = 0;
	public static final int MID_MARK = 1;
	public static final int SERVICE_MARK = 2;
	public static final int MARK_CONTENT = 3;

	//게시판 유형
	public static final int BOARD_TYPE_REVIEW = 1;
	public static final int BOARD_TYPE_QNA = 2;
}
