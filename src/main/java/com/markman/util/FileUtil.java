package com.markman.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.markman.service.S3Wrapper;


public class FileUtil {
	public static String getFileType(String fileName){
		int pathPoint = fileName.trim().lastIndexOf(".");
		String filePoint = fileName.trim().substring(pathPoint + 1,fileName.trim().length());
		String fileType = filePoint.toLowerCase();
		return fileType;
	}
	public static String getToday(int i){
		Calendar calendar = Calendar.getInstance();
		java.util.Date date = calendar.getTime();
		String today;
		if(i==0){
			today = (new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(date));
		}else{
			today = (new SimpleDateFormat("yyyyMMddHHmmss").format(date));
		}
		return today;
	}

	public static String ParsingS3ImgUrl(String imgUrl){
		String str[] = imgUrl.split(ConstantUtil.BUCKET+"/");
		return str[1];
	}
}
