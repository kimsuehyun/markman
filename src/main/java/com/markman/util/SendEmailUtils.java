package com.markman.util;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

public class SendEmailUtils {
	public static String sendEmail(List<Map<String,String>> list,String receiver,String title,String content) throws IOException, EmailException{
		HtmlEmail email = new HtmlEmail();
		email.setCharset("euc-kr");
		email.setHostName("smtp.worksmobile.com");
		email.addTo(receiver,"");
		email.setFrom(ConstantUtil.ADMIN_MAIL, ConstantUtil.ADMIN_MAIL_NAME);
		email.setSubject(title);
		email.setHtmlMsg(content);
		email.setAuthentication(ConstantUtil.ADMIN_MAIL, ConstantUtil.ADMIN_MAIL_PASSWORD);
		email.setSmtpPort(465);
		email.setSSL(true); 
		email.setTLS(true);
		email.setDebug(true);
		if(list.size()!=0){
			for(int i=0 ; i<list.size(); i++){
				Map<String,String> map = list.get(i);
				EmailAttachment attachment = new EmailAttachment();
				attachment.setURL(new URL(map.get("img_url")));
				attachment.setDisposition(EmailAttachment.ATTACHMENT);
				attachment.setDescription("commons-email api");
				attachment.setName(map.get("img_origin_name"));
				email.attach(attachment);
			}
		}
		return email.send();
	}
	
	public static String sendSimpleEmail(String receiver,String title,String content) throws IOException, EmailException{
		Email email = new SimpleEmail();
		email.setCharset("euc-kr");
		email.setHostName("smtp.worksmobile.com");
		email.addTo(receiver,"");
		email.setFrom(ConstantUtil.ADMIN_MAIL, ConstantUtil.ADMIN_MAIL_NAME);
		email.setSubject(title);
		email.setMsg(content);
		email.setAuthentication(ConstantUtil.ADMIN_MAIL, ConstantUtil.ADMIN_MAIL_PASSWORD);
		email.setSmtpPort(465);
		email.setSSL(true); 
		email.setTLS(true);
		email.setDebug(true);
		return email.send();
	}
}
