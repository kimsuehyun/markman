package com.markman.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;
import com.markman.util.ConstantUtil;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.MediaType;


@Service
public class S3Wrapper {

	@Autowired
	private AmazonS3Client amazonS3Client;
	
	private String bucket= ConstantUtil.BUCKET;

	public PutObjectResult upload(String filePath, String uploadKey) throws FileNotFoundException {
		return upload(new FileInputStream(filePath), uploadKey);
	}

	private PutObjectResult upload(InputStream inputStream, String uploadKey) {
		PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, uploadKey, inputStream, new ObjectMetadata());

		putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);
		
		//업로드 최대 사이즈 5MB로 설정
		putObjectRequest.getRequestClientOptions().setReadLimit(5*1024*1024);
		
		PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
		//putObjectRequest.withAccessControlList(accessControlList)
		IOUtils.closeQuietly(inputStream);

		return putObjectResult;
	}

	public List<PutObjectResult> upload(MultipartFile multipartFile,String fileName) {
		List<PutObjectResult> putObjectResults = new ArrayList<>();
			try {
				putObjectResults.add(upload(multipartFile.getInputStream(), fileName));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		return putObjectResults;
	}

	public ResponseEntity<byte[]> download(String key, String fileOriginName) throws IOException {
		GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, key);

		S3Object s3Object = amazonS3Client.getObject(getObjectRequest);

		S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

		byte[] bytes = IOUtils.toByteArray(objectInputStream);

		String fileName = fileOriginName;

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		httpHeaders.setContentLength(bytes.length);
		httpHeaders.setContentDispositionFormData("attachment", fileName);

		return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
	}

	public List<S3ObjectSummary> list() {
		ObjectListing objectListing = amazonS3Client.listObjects(new ListObjectsRequest().withBucketName(bucket));

		List<S3ObjectSummary> s3ObjectSummaries = objectListing.getObjectSummaries();

		return s3ObjectSummaries;
	}
	
	public void deleteObject(String key){
        amazonS3Client.deleteObject(bucket, key);
	}
}