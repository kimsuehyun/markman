package com.markman.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.markman.common.dao.UserDao;
import com.markman.common.vo.UserVo;

@Service("UserAuthService")
public class UserAuthService implements UserDetailsService{

	@Autowired
	UserDao userDao;
	
	//Return Spring Security User for authentication
	private User buildUserForAuth(UserVo user, List<GrantedAuthority> auths)
	{
		return new User(user.getId(), user.getPw(), auths);
	}
	
	//Get authorities name and return spring security authorities type
	private List<GrantedAuthority> buildAuth(String userRole)
	{
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(userRole));
		return authorities;
	}
	
	//return User for authentication at AuthenticationManagerBuilder
	@Override
	public User loadUserByUsername(String userid) throws UsernameNotFoundException 
	{
		UserVo userVo = userDao.selectUserById(userid);
		
//		List<String> userList = DataSaveClass.getDataSaveClass().getUserList();
		List<String> userList = userDao.getUncheckedUser();
		for(String str : userList){
			if(userid.equals(str)){
				System.out.println("사용자의 유형 NONE");
				List<GrantedAuthority> authorities = buildAuth(userVo.getUser_type());
				User user = buildUserForAuth(userVo,authorities);
				return user;
			}
		}
		if(userVo == null)
		{
			throw new UsernameNotFoundException(userid);
		}
		List<GrantedAuthority> authorities = buildAuth(userVo.getUser_type());
		
		User user = buildUserForAuth(userVo,authorities);
		return user;
		
	}

}
