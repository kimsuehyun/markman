package com.markman.service;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.markman.common.vo.NaverEditorPhotoVo;
import com.markman.util.ConstantUtil;
import com.markman.util.FileUtil;

@Service
public class NaverEditorServce {
	
	@Autowired S3Wrapper s3Wrapper;


	public String insertNaverEditorImg(HttpServletRequest request, NaverEditorPhotoVo naverEditorPhotoVo) {
		String textareaName = request.getParameter("textareaName");
		String return1 = request.getParameter("callback");
		String return2 = "?callback_func=" + request.getParameter("callback_func");
		String return3 = "";
		// 확장자가 맞는지와 다른 예외 처리 (mulitpart 객체가 있는지 없는지 )
		if (checkImgExtension(naverEditorPhotoVo)) {
			MultipartFile imageFile = naverEditorPhotoVo.getFiledata();
			String originName = imageFile.getOriginalFilename();
			String saveFileName = FileUtil.getToday(1)+"."+FileUtil.getFileType(originName);
			String saveFilePath = "";
			String fullPath = "";
			System.out.println("originName:::"+originName);
			// 아래 if은 textarea의 name으로 어디서 ajax를 보내는지 구분 지은후, 각자 다른 경로에 파일을 저장하기 위하여 만듬
			if ("notice".equals(textareaName)) {// 공지사항
				saveFilePath = ConstantUtil.NOTICE_IMG_PATH;
			}else if ("mail".equals(textareaName)) {
				saveFilePath = ConstantUtil.MAIL_IMG_PATH;
			}else if ("faq".equals(textareaName)) {
				saveFilePath = ConstantUtil.FAQ_IMG_PATH;
			}else if("qna".equals(textareaName)){
				saveFilePath = ConstantUtil.QNA_IMG_PATH;
			}else if("".equals(textareaName)){
				
			}
			
			fullPath = saveFilePath +"/"+ saveFileName;
			return3 += "&bNewLine=true";
			return3 += "&sFileName=" + originName;
			return3 += "&sFileURL=" + ConstantUtil.S3DEFAULT+"/"+fullPath;
			
			// 아마존 insert//
			s3Wrapper.upload(imageFile, fullPath);
		} else {
			return3 += "&errstr=error";
		}
		
		return "redirect:" + return1 + return2 + return3;
	}

	public boolean checkImgExtension(NaverEditorPhotoVo naverEditorPhotoVo) {
		boolean flag = true;
		String name = "";
		if (naverEditorPhotoVo.getFiledata() != null && naverEditorPhotoVo.getFiledata().getOriginalFilename() != null
				&& !naverEditorPhotoVo.getFiledata().getOriginalFilename().equals("")) {
			// 기존 상단 코드를 막고 하단코드를 이용
			name = naverEditorPhotoVo.getFiledata().getOriginalFilename()
					.substring(naverEditorPhotoVo.getFiledata().getOriginalFilename().lastIndexOf(File.separator) + 1);
			String filename_ext = name.substring(name.lastIndexOf(".") + 1);
			filename_ext = filename_ext.toLowerCase();
			String[] allow_file = { "jpg", "png", "bmp", "gif" };
			int cnt = 0;
			for (int i = 0; i < allow_file.length; i++) {
				if (filename_ext.equals(allow_file[i])) {
					cnt++;
				}
			}
			if (cnt == 0) {
				flag = false;
			}
		}
		return flag;
	}
}
