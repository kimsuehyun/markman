package com.markman.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;
@Configuration
@EnableWebSecurity
public class SecConfig extends WebSecurityConfigurerAdapter {
	public final String REMEMBER_KEY="MARKMEN_KEY";
	public final String REMEMBER_COOKIE_NAME = "MARKMEN_COOKIE";
	public final String REMEMBER_PARAMETER_NAME = "remember-me";

	@Autowired
	@Qualifier("UserAuthService")
	private UserDetailsService userAuthService;

	@Autowired
	private CustomAuthProvider customAuthProvider;

	@Override
    public void configure(WebSecurity web) throws Exception {
		//static files
		web
            .ignoring()
            .antMatchers("/resources/**","/webjars/**");
    }

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		CharacterEncodingFilter filter = new CharacterEncodingFilter();

        filter.setEncoding("UTF-8");

        filter.setForceEncoding(true);


        http.headers().frameOptions().disable();
        http.addFilterBefore(filter,CsrfFilter.class)
			.authorizeRequests()
			.antMatchers("/admin/**").hasRole("ADMIN")
			.antMatchers("/patent/**").hasRole("PATENT")
			.antMatchers("/patent**").hasRole("PATENT")
			.antMatchers("/user/**").authenticated()
			.antMatchers("/applicant/**").authenticated()
//			.antMatchers("/design/addDesignIn","/Mark/addMarkIn","/CopyRight/addCopyRightIn","/uploadPage").hasRole("INVENTOR")
//			.antMatchers("/registration/detail/**","/design/detail/**","/Mark/detail/**","/CopyRight/detail/**").hasRole("INVENTOR")
//			.antMatchers("/downLoadPagePl/**","/registration/detail/**","/design/detail/**","/Mark/detail/**","/CopyRight/detail/**").hasRole("PATENT")
//			.antMatchers("/qna/addOneQna","/copyRight/**").authenticated()
//			.antMatchers("/","/**","/notice/**","/signupPage/**","/signup/inputsignup").permitAll()
			.and()
			.formLogin()
				.loginPage("/loginPage").permitAll()
				.loginProcessingUrl("/login.do")
				.usernameParameter("id")
				.passwordParameter("pw")
				.successForwardUrl("/loginProcess.do")
				.and()
			.logout()
				.deleteCookies("JSESSIONID",REMEMBER_COOKIE_NAME)
				.logoutUrl("/logout.do")
				.logoutSuccessUrl("/logoutOk.do")
				.invalidateHttpSession(true)
				.and()
			.rememberMe()
				.key(REMEMBER_KEY)
				.rememberMeParameter(REMEMBER_PARAMETER_NAME)
				.rememberMeServices(getTokenBasedRememberMeServices()).
				and()
			.exceptionHandling()
				.accessDeniedPage("/authError")
				.and()
			.sessionManagement()
				.maximumSessions(1)
				.expiredUrl("/authError");
	}

	@Bean
    public TokenBasedRememberMeServices getTokenBasedRememberMeServices() {
        TokenBasedRememberMeServices rememberMeServices = new TokenBasedRememberMeServices(REMEMBER_KEY, userAuthService);
        //remeber키를 rememberMeServices.getParameter()값과 일치시키고, setAlwaysRemember(false)를 해야 선택으로 자동로그인 가능
        rememberMeServices.setAlwaysRemember(false);
        rememberMeServices.setCookieName(REMEMBER_COOKIE_NAME);
        rememberMeServices.setTokenValiditySeconds(60 * 60 * 24 * 31);
        return rememberMeServices;
	}


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userAuthService);
		auth.authenticationProvider(customAuthProvider);
	}

	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{

	}
}
