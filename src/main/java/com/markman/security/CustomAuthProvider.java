package com.markman.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import com.markman.common.dao.UserDao;
import com.markman.service.UserAuthService;

@Service
public class CustomAuthProvider implements AuthenticationProvider
{
	@Autowired
	SecAlgorithm secAlgo;
	@Autowired
	UserAuthService userAuthService;
	@Autowired
	UserDao userDao;
	
	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		//auth에는 현재 로그인 시도한 정보가 담김
		System.out.println("CustomAuthProvider .");
		String id  = auth.getName();
		String pwd = (String)auth.getCredentials();
		
		User validUser = userAuthService.loadUserByUsername(id);
		
	//	List<String> userList = DataSaveClass.getDataSaveClass().getUserList();
		List<String> userList = userDao.getUncheckedUser();
		String userid  = validUser.getUsername();
		for(String str : userList){
			if(userid.equals(str)){
				return new UsernamePasswordAuthenticationToken(validUser,validUser.getPassword(),validUser.getAuthorities());
			}
		}
		try{
			if(validUser == null) throw new BadCredentialsException("없는 아이디 입니다.");
			if(secAlgo.validatePassword(pwd,validUser.getPassword())){
				return new UsernamePasswordAuthenticationToken(validUser,validUser.getPassword(),validUser.getAuthorities());
			}
		}catch (Exception e){
			System.out.println("customProvider ] 뭐가 잘못된거지 (pw포맷이 다름?)");
		} 

		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}
	
	
}

