<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<link rel="stylesheet" href="/resources/common/css/guest.css"/>
<!--  부트스트랩 --><!--  -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body >
<style>
	table{text-align: center;border-bottom: 1px solid #ddd; margin-top:2%}
	table tr{height:5vh}
	table tr th{text-align: center;border-bottom:1px solid #fff !important;border-top:2px solid #fff !important;vertical-align:middle !important;font-weight:bold;background:#1D232E;color:#fff;}
	table tr td{vertical-align:middle !important;border-bottom:1px solid #ddd;text-align: center !important}
	@media (max-width:500px){
		body{width:100% !important}
		table{width:90% !important;}	
	}
</style>
<br><br><br><br>
<form id="formId" method="POST">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" id="pageNum" name="pageNum">
<input type="hidden" id="pk" name="pk">
<div class="col-lg-8 col-lg-offset-2 col-xs-12">
	<h2>공지사항</h2>
	<table class="table table-hover table-bordered  ">
		<tr>
			<th style="width:10% !important" >No</th>
			<th style="width:50% !important">제목</th>
			<th style="width:20% !important">작성자</th>
			<th style="width:20% !important">Date</th>
		</tr>
		<c:forEach items="${noticeList}" var="list">
			<tr class="notice" data-pk="${list.board_pk }">
				<td>${list.board_pk }</td>
				<td>${list.title }</td>
				<td>${list.writer }</td>
				
			    <fmt:parseDate value="${list.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
			    <fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
			    <jsp:useBean id="toDay" class="java.util.Date" />
				<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
				<c:choose>
					<c:when test="${regisDate==today}">
						<td><fmt:formatDate value="${regis_date}" pattern="HH:mm"/></td>
					</c:when>
					<c:otherwise>
						<td>${regisDate}</td>
					</c:otherwise>
				</c:choose>		
				
			</tr>
		</c:forEach>
	</table>
</div>
<div id="list_paging">
		${nation.getNavigator()}
</div>
</form>
	<article style="clear:both">
		<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
<script>
var form = $("#formId");
$(document).on("click",".notice",function(){
	var pk = $(this).data('pk');
	$("#pk").val(pk);
	form.attr("action","/common/notice/mvDetail");
	form.submit();
});
function listarticle(pageNum){
	 $("#pageNum").val(pageNum);
	 formId2.submit();
}
</script>
</html>