<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<link rel="stylesheet" href="/resources/common/css/list_view.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body style="overflow-x: inherit !important;" >
<form action="/admin/notice/mvEdit" method="POST" onsubmit="update()" enctype="multipart/form-data" >
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" name="board_pk" id="board_pk" value="${noticeVo.board_pk }">
<style>
	.table_box{border:1px solid #ddd;width:100%;height:60vh}
	.text_table_title{width:100%;height:10vh;border-bottom:1px solid #ddd;padding:1%}
	.text_table_title > div:first-child{height:100%}
	.text_table_title > div:first-child > div{height:100%;background:#0e4383;float:left;color:#fff;border-radius:10px;text-align: center;font-size:5vh;}
	.text_table_title > div:nth-child(2){height:100%}
	.text_table_title > div:nth-child(2) > div{height:50%;}
	.text_table_title > div:nth-child(3){text-align:right}
	.text_content{padding:1.5%}
	.view_text_imgs{width:30%;height:24%;float:left}
	.view_text_imgs > img{width:100%;height:100%}
	.text_content > .view_text_imgs{width:68%;float:right;word-break:break-all;}
	.footer_view{width:100%;height:8vh;border:1px solid #ddd;padding:1.2%}
	.footer_view > button{float:right;margin-right:1%}
	
	
</style>
<div class="continer">
	<div class="row">
			<div class="height_box"></div>
			<div  class="col-lg-8 col-lg-offset-2 col-xs-12 " >
				<h3>공지사항</h3>
				<div class="table_box">
					<div class="text_table_title">
						<div class="col-lg-1 col-xs-2" style="padding:0px !important">
							<div class="col-xs-12">
								Q
							</div>
						</div>
						<div class="col-lg-5 col-xs-6">
							<div class="col-lg-12 col-xs-6">${noticeVo.title}</div>
							<div class="col-lg-6 col-xs-6">${noticeVo.writer}</div>
							<div class="col-lg-6 col-xs-6">
								<!--dateFormat-->
			            		<fmt:parseDate value="${noticeVo.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
					            <fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
					            <jsp:useBean id="toDay" class="java.util.Date" />
								<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
								<c:choose>
									<c:when test="${regisDate==today}">
										<fmt:formatDate value="${regis_date}" pattern="HH:mm"/>
									</c:when>
									<c:otherwise>
										${regisDate}
									</c:otherwise>
								</c:choose>		
							</div>
						</div>
						<div class="col-lg-6 col-xs-4" style="color:red">
							조회수[${noticeVo.hits}]
						</div>
					</div>
					<div style="border-bottom:1px solid #ddd;">
						<table style="margin-left:10px;">
							<c:forEach items="${boardImgList}" var="list" varStatus="status">
								<tr>
									<td class="board_img"  style="padding:5px">
									<img  class="downloadImg icon20" src="/resources/image/download.png"/>
									<span class="img_name" data-id="${list.img_url}">${list.img_origin_name}</span>
									<span class="img_size"><fmt:formatNumber value="${list.img_size/1024}" pattern="0"/>KB</span>
								</td>
								</tr>
							</c:forEach>						
						</table>	
					</div>
					<div class="text_content">
						<div class="view_text_imgs">
							<%-- ${boardImgList.img_url} --%>
							${reviewVo.content}
						</div>	
						<div>
							${reviewVo.content}aaa
						</div>
					</div>
				</div>
				<div class="footer_view">
						<c:if test="${sessionScope.currentUser.getUser_type()=='ROLE_ADMIN'}">
   						<button class="btn btn-success  ">수정</button>
   						<button class="btn btn-danger " type="button">삭제</button>
   						</c:if>	
   						<button type="button" class="btn btn-primary" onclick="location.href='/common/noticeList'">목록으로</button>
					</div>
				<%-- <div class="panel panel-default" >
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-6">
								<h4 class="col-lg-4">${noticeVo.title}ㅁㅁㅁ</h4>
								<h4 class="col-lg-4">${noticeVo.writer}</h4>
								<h4 class="col-lg-4">${noticeVo.registration_date}</h4>
							</div>
						</div>
					</div>
      				<div class="panel-body">
      					<!--이미지 출력-->
						 <div class="col-lg-12">
						 	${noticeVo.content}
						 </div>  					
      				</div>
      				<div class="panel-footer">
   						<div class="row">
   							<div class="col-lg-12 ">
   								<div class="col-xs-12">
   									<div class="col-xs-6 pull-left">
	   									
   									</div>
   									<div class="col-xs-12 pull-right" style="text-align:right">
   									<c:if test="${sessionScope.currentUser.getUser_type()=='ROLE_ADMIN'}">
   										<button class="btn btn-success  ">수정</button>
   										<button class="btn btn-danger " type="button">삭제</button>
   									</c:if>	
   										<button type="button" class="btn btn-primary" onclick="location.href='/common/noticeList'">목록으로</button>
   									</div>
   								</div>
   								
   							</div>
   						</div>
      				</div>
				</div> --%>
			</div>
	</div>
</div>
</form>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
<script>
$('.btn-danger').click(function(){
	var data = {};
	data["board_pk"] = $('#board_pk').val();
	confirmm("해당 게시물을 삭제하시겠습니까?").then(function(){
  		$.ajax({
  			url : "/admin/notice/deleteOk",
			type : "POST",
			data : data,
			success : function(resultData) {
				if(resultData=="success"){
					alertt("삭제가 완료되었습니다.","success").then(function(){
						location.href="/common/noticeList";
					});
				}else{
					alertt("삭제 실패");
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\n" + "error:" + error);
			}
		});  
	},function(){});
});
//파일 다운로드
$('.img_name').click(function(){
	fileUpload($(this));
});
function fileUpload(obj){
	var fileFullPath = obj.data('id');
	var fileOriginName = obj.html();
	location.href="/call/callDownload.do?fileFullPath="+fileFullPath+"&fileOriginName="+fileOriginName;
}
</script>
</html>