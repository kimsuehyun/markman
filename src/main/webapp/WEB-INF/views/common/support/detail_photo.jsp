<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/list_view.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body style="overflow-x:visible !important">
<div class="continer">
	<div class="row">
			<div class="height_box"></div>
			<div  class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1" >
				<h3>상표 지식사전</h3>
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-6">
								<h4 class="col-lg-4">제목 출력</h4>
								<h4 class="col-lg-4">작성자</h4>
								<h4 class="col-lg-4">작성일 출력</h4>
							</div>
						</div>
					</div>
      				<div class="panel-body">
      					<!--이미지 출력-->
						 <img src="/resources/image/0-2_INDEX_contents_3-02.png" class="col-lg-12 col-xs-12">
						 <div class="col-lg-12">
						 	글씨 출력글씨 출력글씨 출력글씨 출력글씨 출력글씨 출력글씨 출력글씨 출력글씨 출력글씨 출력
						 </div>  					
      				</div>
      				<div class="panel-footer">
   						<div class="row">
   							<div class="col-lg-12 ">
   								<div class="col-xs-12">
   									<div class="col-xs-6 pull-left">
	   									
   									</div>
   									<div class="col-xs-12 pull-right" style="text-align:right">
   										<button class="btn btn-success  ">수정</button>
   										<button class="btn btn-danger ">삭제</button>
   										<button class="btn btn-primary"  onclick="mvPage('information')">목록으로</button>
   									</div>
   								</div>
   								
   							</div>
   						</div>
      				</div>
				</div>
			</div>
	</div>
</div>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
</html>