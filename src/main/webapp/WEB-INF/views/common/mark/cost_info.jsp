<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>
<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/apply_sub.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>



</head>
<body>	
<style>
	@media(max-width:900px){
	.guide_p_title1{font-size:2.5vh}
	.f1_guide_p{font-size:2vh}
	.price_guide_background{height:20vh}
	.f2_guide_p{font-size:1.5vh}
	.guide_p_titlebar{font-size:2vh}
	.guide_p_titlebar2{font-size:2vh}
	.guide_p_info_p > div:first-child{font-size:2vh;border:none}
	.pr1{font-size:1.5vh;font-weight: bold}
	.f3_guide_p{font-weight: bold;text-align:center;font-size:1.5vh}
	}
</style>
<div class="container-fluid">
	<div class="row padding_none" >
		<div class="price_guide_background col-lg-12 padding_none">
			<div class="h20  visible-lg-block"></div>
			<div class="price_guide_text1 h60 ">
				<div class="height_box "></div>
				<span class="guide_p_title1">마크맨 비용안내</span><br>
				<div class="height_box visible-lg-block"></div>
				<span class="f1_guide_p">
					합리적이고 투명한 가격책정으로<br>
					더욱 믿음이 가는 마크맨!
				</span>
			</div>
			<div class="h20"></div>
		</div>
	</div>
	<div class="height_box2 visible-lg-block"></div>
	<div class="row" >
		<div class="col-lg-6 col-lg-offset-3 guide_p_content1">
			<!--content_title-->		
			<div class="col-lg-12 guide_p_titlebar" style="margin-top:2vh">
				<div class="h25"></div>
				마크맨을 이용해야 하는 4가지 이유!
			</div>
			<!--content_title-->		
			<div class="height_box2"></div>
			<div class="guide_p_info col-lg-12 padding_none">
				<div class="guide_p_info_row h25">
					<div class="col-lg-3 col-xs-5 h100 pull-left">
						<div class="h10"></div>
						<img src="/resources/image/D-2_contents-04.png" class="col-lg-11 img-responsive " alt="bakcgrond">
					</div>
					<div class="col-lg-9 h100">
						<div class="h25"></div>
						<div class="col-lg-12 f2_guide_p">
							상표등록 서비스'키프리스(kipris)'와 연동하여<br>
							등록하고자 하는 상표명을 손쉽게 검색할 수 있습니다.
						</div>
					</div>
				</div>
				<!--content-->
				<div class="guide_p_info_row h25">
					<div class="col-lg-3 col-xs-5 h100 pull-right">
						<div class="h10"></div>
						<img src="/resources/image/D-2_contents-05.png" class="col-lg-11 img-responsive"  alt="icon">
					</div>
					<div class="col-lg-9 h100">
						<div class="h25"></div>
						<div class="col-lg-12 f2_guide_p f2-right">
							고객별 타입에 맞춰 마크맨 내에서 맞춤 서비스!<br>
							출원준비-등록 완료까지의 전 과정을<br>
							꼼꼼히 해드립니다.
						</div>
					</div>
				</div>
				<!--content-->
				<div class="guide_p_info_row h25">
					<div class="col-lg-3 col-xs-5 h100 pull-left">
						<div class="h10"></div>
						<img src="/resources/image/D-2_contents-06.png" class="col-lg-10 img-responsive" alt="icon">
					</div>
					<div class="col-lg-9 h100">
						<div class="h25"></div>
						<div class="col-lg-12 f2_guide_p">
							전문성을 가진 변리사와 1:1 컨설팅/코멘트로<br>
							더욱 신뢰가 가는 마크맨!
						</div>
					</div>
				</div>
				<!--content-->
				<div class="guide_p_info_row h25 border_none">
					<div class="col-lg-3 col-xs-5 h100 pull-right">
						<div class="h10"></div>
						<img src="/resources/image/D-2_contents-07.png " class="col-lg-10 img-responsive" alt="icon">
					</div>
					<div class="col-lg-9 h100">
						<div class="h25"></div>
						<div class="col-lg-12 f2_guide_p f2-right ">
							출원 수수료 단55,000원에 상표에 대한 권리 확보!<br>
							전 과정에 대한 투명한 가격 책정이 되어있어 믿을 수<br>
							있는 마크맨 서비스 입니다.
						</div>
					</div>
				</div>
				<!--content-->
			</div>
		</div>
	</div>
	<div class="height_box"></div>
	<div class="col-lg-6 col-lg-offset-3">
		<div class="col-lg-12 btn_guide_p1 ">
			<button class="col-lg-4 col-lg-offset-4 col-xs-offset-4" onClick="location.href='/common/serviceInfo'">서비스 안내보기</button>
		</div>
		<div class="height_box2"></div>
			<div class="col-lg-12 guide_p_titlebar">
				<div class="h25"></div>
				마크맨 서비스 비용안내
			</div>
	</div>
	<div class="height_box"></div>
	<div class="row" >
		<div class="guide_p_titlebar2 col-lg-12">
			<div class="h20"></div>
			출원 시
		</div>
		<div class="guide_p_info_p col-lg-6 col-lg-offset-3 col-xs-12">
			<div class="col-lg-12 text-center">
				<div class="col-lg-4 col-xs-4">출원료</div>
				<div class="col-lg-4 col-xs-4">특허청 관납료</div>
				<div class="col-lg-4 col-xs-4">합계</div>
			</div>
			<div class="col-lg-12 col-xs-12 guide_p_info_b h80">
				<div class="price_text col-lg-4 col-xs-4 text-center pr1" >
					<div class="col-lg-9 col-lg-offset-1 col-xs-12" >
						55,000<br>
						
					</div>
					<div class="col-lg-2 visible-lg-block">
						<img src="/resources/image/D-2_contents-10.png" alt="+" >
					</div>
				</div>
				<div class="price_text col-lg-4  col-xs-4 text-center pr1">
					<div class="col-lg-9 col-lg-offset-1 col-xs-12">
						62,000<br>
					</div>
					<div class="col-lg-2 visible-lg-block">
						<img src="/resources/image/D-2_contents-11.png" alt="=">
					</div>
				</div>
				<div class="col-lg-4 text-center pr1">
					117,000
				</div>
			</div>
		</div>
	</div>
	<div class="height_box"></div>
	<div class="row" style="clear:both;height:40vh">
		<div class="guide_p_titlebar2 col-lg-12">
			<div class="h20"></div>
			등록결정시(추가금액 지불)
		</div>
		<div class="guide_p_info_p col-lg-6 col-lg-offset-3">
			<div class="col-lg-12 text-center">
				<div class="col-lg-4 col-xs-4">등록료</div>
				<div class="col-lg-4 col-xs-4">성공보수</div>
				<div class="col-lg-4 col-xs-4">합계</div>
			</div>
			<div class="col-lg-12 col-xs-12 guide_p_info_b h80">
				<div class="price_text col-xs-4 col-lg-4 text-center pr1">
					<div class="col-lg-9 col-lg-offset-1 col-xs-12">
						221,000<br>
					</div>
					<div class="col-lg-2 visible-lg-block ">
						<img src="/resources/image/D-2_contents-10.png" alt="+">
					</div>
				</div>
				<div class="price_text col-lg-4 col-xs-4 text-center pr1">
					<div class="col-lg-9 col-lg-offset-1 col-xs-12">
						50,000<br>
					</div>
					<div class="col-lg-2 visible-lg-block">
						<img src="/resources/image/D-2_contents-11.png" alt="=">
					</div>
				</div>
				<div class="col-lg-4 text-center pr1 col-xs-4">
					271,000
				</div>
			</div>
			<div class="f3_guide_p col-xs-12">
				-특허청에서 등록 거절 시 대응(의견서/보정서)최초 1회 무료
			</div>
			<div class="height_box"></div>
			<div class="col-lg-6  col-lg-offset-3 guide_p_btn_wrap ">
				<button class="col-lg-5 col-xs-5" onClick="location.href='/common/applyProcessInfo'">출원과정 보기</button>
				<div class="col-lg-2 col-xs-2"></div>
				<button class="col-lg-5 col-xs-5" onClick="location.href='/applicant/mark/mvApply'">출원하러 가기</button>
			</div>
		</div>
	</div>
</div>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
</html>