<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">


<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->

<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<style>
	.apply_process_btn > button{border:none;background:rgba(0,0,0,0);border:1px solid #ddd}
	.height_box_apply{height:3vh;clear:both}
	.height_box_apply2{height:6vh;clear:both}
</style>
<c:import url="/WEB-INF/views/import/header.jsp"/>
	
		<img src='/resources/image/d-1_img-header.png' alt="d-1" style="margin-top:-1%;padding:0px !important"  class="img-responsive col-xs-12 col-md-12 padding_">
		<div class="col-md-12 col-xs-12">
			<div class=" col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3  apply_process_btn" style="height:3vh">
				<button class =" col-md-5 col-xs-12 pull-left h100"  onClick="location.href='/common/costInfo'">비용안내 보기</button>
				<div class="visible-sm-block visible-xs-block height_box_apply"></div>
				<button class =" col-md-5 col-xs-12 pull-right h100" onClick="location.href='/applicant/mark/mvApply'">출원하러 가기</button>
			</div>
		</div>
		<div class="height_box_apply"></div>
		<div class="height_box_apply2 visible-xs-block"></div>
	


		<img src='/resources/image/d-1_img-footer.png' alt="d-1"   class="img-responsive col-xs-12 col-md-12">
	
</body>
</html>