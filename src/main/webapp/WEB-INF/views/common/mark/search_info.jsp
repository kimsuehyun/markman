<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>


<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="common/js/jquery-ui-1.11.4/external/jquery/jquery.js"></script>
  
<!--css-->
<link rel="stylesheet" href="common/css/css.css">

<script src="common/js/js.js"></script>

<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">


<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

</head>
<body>
<style>
	*{margin: 0 auto;padding:0;list-style: none;text-decoration: none;box-sizing: border-box;font-family: "맑은 고딕"}
	.padding_none{padding:0px !important}
	.h100{height:100% !important;}
	.h50{height:25% !important;}
	
	
	.title_wrap{width:100%;height:30vh;background:#fff;background-image: url("resources/image/searh_goods.png")}
	.title_wrap > div{padding-top:3%}
	.searh_text_wrap{font-size:3vh;height:15vh;line-height:5vh;font-weight:500}
	.pink{color:#cd4d83}
	.hiright{background:#c7fffc}
	.decoration_img{height:15vh}
	.per_img_wrap img{margin-right:8%;margin-top:-2%}
	.search_info > div > img{width:60%;}
	
	.search_info_text{font-size:1.8vh;margin-top:3%;line-height:5vh;font-weight: bold}
	.content_search_info{border-top:3px solid #e1effa;margin-top:4%}
	.search_title_wrap{height:20vh;background-image: url("resources/image/B-1.상표검색_contents2-01-06.png");background-size:contain;background-repeat: no-repeat;position:relative}
	.search_title_wrap > div{position:absolute;top:50%;left:5%;font-weight:700;font-size:3vh}
	.search_info_wrap2{height:auto;}
	.search_info_wrap2 > ul  > li{margin-top:5vh;height:25vh}
	.text_info > div{font-size:2vh}
	.text_info  div:first-child{line-height:10vh}
	.decoration_title{width:1%;height:3vh;background:#77bfe8;float:left;margin-top:4%}
	.search_info_wrap2 > ul button{border:none;height:4vh;background:rgba(0,0,0,0);border:1px solid #ddd;border-radius:5px;}
	
	.prejudice_title{height:10vh;background-image: url("resources/image/B-1_img20.png");background-size:contain;background-repeat: no-repeat;}
	.border-line{border-top:5px solid #e1effa;border-bottom:5px solid #e1effa;height:10vh}
	.goods_prejudice{margin-top:5%;height:70vh}
	.goods_prejudice   > div  > div > div {border:1px solid #ddd}	
	.goods_bottom{border-bottom:1px solid #ddd;height:35vh}
	.line_gr{height:0.7vh;background:#666}
	.height_box{height:5vh;width:100%}
	.title_text_prejudice{font-size:3vh;margin-top:6%;margin-left:2%}
	.info_text_pre{margin-top:5%;line-height:3vh;color:#5e6a6e;font-size:1.5vh}
</style>
<div class="title_wrap col-lg-12 padding_none">
	<div class="col-lg-6 col-lg-offset-3">
		<img src="resources/image/B-1_img19.png" class="col-lg-offset-3">
	</div>
</div>	

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="col-lg-12 img_row"> 
				<div class="col-lg-8  col-lg-offset-2 ">
					<div class="row">
						<img src="resources/image/B-1.상표검색_contents2-01-03.png" alt="삼각형 이미지" class="pull-right decoration_img">
					</div>
					<div class="col-lg-12 searh_text_wrap text-center">
						매년 평큔 출원 건수는 <span class="pink">70,000</span>여건,<br>
						<span class="hiright">유사상표로 인한 등록거절 확률</span>은<br>
						<span class="pink">무려40%</span>나 된다는 사실,<br>
						알고 계시나요?
					</div>
					<div class="per_img_wrap">
						<img src="resources/image/B-1.상표검색_contents2-01-04.png" alt="per" class="pull-right img-responsive ">
					</div>
					<div class="row" style="clear: both">
						<img src="/resources/image/B-1.상표검색_contents2-01-03.png">
					</div>
					<div class="search_info col-lg-12">
						<div class="col-lg-6 col-lg-offset-3">
							<img src="/resources/image/B-1.상표검색_contents2-01-05.png" alt="상표검색" class="img-responsive col-lg-offset-2">
							<div class="col-lg-12  text-center search_info_text">
								이제,특허청키프리스DB연동 마크맨 검색 서비스와<br>
								함께하세요.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 content_search_info" >
			<div class="search_title_wrap col-lg-8 col-lg-offset-2">
				<div>
					상표검색,왜 필요한가요?
				</div>
			</div>
			
			<div class="col-lg-12 padding_none">
				<div class="col-lg-8 col-lg-offset-2 padding_none search_info_wrap2" >
				<!--icon list-->
					<ul class="col-lg-12 h100 ">
						<li>
							<div class=" h100 col-lg-12" >
								<div class="col-lg-2 col-sm-2 h100" >
									<div class="h50"></div>
									<img src="resources/image/B-1.상표검색_contents2-01-15.png" class="col-lg-12">
								</div>
								<div class="col-lg-9 col-lg-offset-1 col-sm-10 h100" >
									<div class="h50"></div>
									<div class="text_info">
										<div>
											<div class="decoration_title"></div>
											유사상표를 방지하기 위해
										</div>
										<div>검색을 통해 선등록 되어있는 타인의 상표를 조사할 수 있습니다</div>
										<div>때문에 유사상표가 되는 것을 미연에 방지할 수 있습니다.</div>
									</div>
								</div>
							</div>
						</li>
						
						<li>
							<div class=" h100 col-lg-12" >
								<div class="col-lg-2 col-sm-2 h100" >
									<div class="h50"></div>
									<img src="resources/image/B-1.상표검색_contents2-01-16.png" class="col-lg-10">
								</div>
								<div class="col-lg-9 col-lg-offset-1 col-sm-10 h100">
									<div class="h50"></div>
									<div class="text_info">
										<div>
											<div class="decoration_title"></div>
											등록가능성을 높이기 위해
										</div>
										<div>상표명으로 인해 거절당할 우려가 있는 상표명 우회가 가능합니다</div>
										<div>사전 검색 없이 등록하게 된다면, 등록 가능성이낮아질 수 있습니다</div>
									</div>
								</div>
							</div>
						</li>
						
						
						<li>
							<div class=" h100 col-lg-12" >
								<div class="col-lg-2 col-sm-2 h100" >
									<div class="h50"></div>
									<img src="resources/image/B-1.상표검색_contents2-01-17.png" class="col-lg-11">
								</div>
								<div class="col-lg-9 col-lg-offset-1  col-sm-10 h100">
									<div class="h50"></div>
									<div class="text_info">
										<div>
											<div class="decoration_title"></div>
											본인이 등록하고자 하는 상표 권리화를 위해
										</div>
										<div>등록 전까진 내 것이 아닌 상표,안심할 수 없습니다.</div>
										<div>등록까지의 첫 딛음인 상표검색이 중요합니다</div>
									</div>
								</div>
							</div>
							
						</li>
						<div class="col-lg-6 col-lg-offset-3" style="margin-top:6%">
							<button class="col-lg-4 col-lg-offset-4" onClick="location.href='/common/mvMark_search'">검색하러 가기</button>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row padding_none" style="clear: both;margin-top:5%">
		<div class="col-lg-12 border-line">
			<div class="h50"></div>
			<div class="col-lg-4 prejudice_title col-lg-offset-5 "></div>
		</div>
		<!--상표 침해 사례-->
		<div class="col-lg-10 col-lg-offset-1 goods_prejudice">
			<div class="col-lg-12 text-center   h100 ">
				<div class="col-lg-4 col-sm-12 h100">
					<div class="col-lg-12 col-sm-12 h100 padding_none">
						<div class="col-lg-12 col-sm-12 padding_none pull-left goods_bottom">
							<img src="resources/image/B-1.상표검색_contents2-01-09.png" class=" col-sm-12 col-lg-12 padding_none" alt="search">
							<img src="resources/image/B-1.상표검색_contents2-01-11.png" class=" col-sm-offset-2 col-sm-8">
						</div>
						
						<div class="col-lg-10 col-sm-12 col-sm-offset-1 col-lg-offset-1">
							<div class="height_box"></div>
							<div class="col-lg-12">
								<div class="line_gr  col-sm-4 col-sm-offset-4 col-lg-4 col-lg-offset-4"></div>
								<b class=" col-lg-12  title_text_prejudice">다이소VS다사소</b>
							</div>
							
							<span class="col-lg-12 info_text_pre">
								누가봐도 혼동의 여지가 있는 두 상표.<br>
								다사소 측은 다이소는 다있소~,<br>
								다사소는 다사이소~를 주장하며<br>
								다사소와 다이소는 다르다고 하였으나<br>
								결국 다이소 측이 상표분쟁에서<br>
								승소하게 되었다.
							</span>
						</div>
					</div>
				</div>
				
				<div class="col-lg-4 h100">
					<div class="col-lg-12 h100 padding_none">
						<div class="col-lg-12 col-sm-12 padding_none pull-left goods_bottom">
							<img src="resources/image/B-1.상표검색_contents2-01-09.png" class=" col-sm-12 col-lg-12 padding_none" alt="search">
							<img src="resources/image/B-1.상표검색_contents2-01-10.png" class=" col-sm-offset-2 col-sm-8" style="height: 200px !important">
						</div>
						
						<div class="col-lg-10 col-sm-12 col-sm-offset-1 col-lg-offset-1">
							<div class="height_box"></div>
							<div class="col-lg-12">
								<div class="line_gr  col-sm-4 col-sm-offset-4 col-lg-4 col-lg-offset-4"></div>
								<b class=" col-lg-12  title_text_prejudice">누가바VS누그바</b>
							</div>
							
							<span class="col-lg-12 info_text_pre">
								'누가바'는 1974년에 해태제과에서 생산시작,<br>
								'누크바'는 1996년에 롯데제과에서<br>
								생산시작하였으나,<br>
								그 형ㄷ태와 포장이<br>
								비슷하여2013년 해태과자에서 소송을 제기했고<br>
								승소하게 되었다.
							</span>
						</div>
					</div>
				</div>
				<div class="col-lg-4 h100">
					<div class="col-lg-12 h100 padding_none">
						<div class="col-lg-12 col-sm-12 padding_none pull-left goods_bottom">
							<img src="resources/image/B-1.상표검색_contents2-01-09.png" class=" col-sm-12 col-lg-12 padding_none" alt="search">
							<img src="resources/image/B-1.상표검색_contents2-01-12.png" class=" col-sm-offset-2 col-sm-8" style="height:200px">
						</div>
						<div class="col-lg-10 col-sm-12 col-sm-offset-1 col-lg-offset-1">
							<div class="height_box"></div>
							<div class="col-lg-12">
								<div class="line_gr  col-sm-4 col-sm-offset-4 col-lg-4 col-lg-offset-4"></div>
								<b class=" col-lg-12  title_text_prejudice">이가탄VS이가탄탄</b>
							</div>
							<span class="col-lg-12 info_text_pre">
								이가탄은 우리나라 대표 잇몸약이다.뒤이어<br>
								애경산업에서'이가탄탄'상표를 출원하고자 했다<br>
								특허청은 2016년1월까지 명인제약과<br>
								동일한 이름을 상표등록을 해야하는 이유를<br>
								애경산업에 요청했으나<br>
								의견서 체출이 없어 등록거절<br>
								 결정을 내렸다고 한다.
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</body>
</html>