<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<style>
	*{margin:0 auto ;padding:0;list-style: none;text-decoration:none;box-sizing: border-box;font-family:"맑은 고딕";}
	
	/*option*/
	.padding_none{padding:0px !important}
	.h50{height:50%}
	.h25{height:25%}
	.h20{height:20%}
	.h60{height:60%}
	.h100{height:100%}
	.h33{height:30%}
	.h90{height:90%}
	
	.price_info_content{height:60vh;border-top:4px solid #3aa9c4;border-bottom: 4px solid #3aa9c4;color:#305669}
	.price_info_text{width:80%;}
	.price_info_box{width:100%}
	.price_info_box >  div > li{float:left;font-size:1.5vw}
	.price_info_box >  div > li:first-child{width:30%;text-align: left;}
	.price_info_box >  div > li:last-child{width:70%;text-align:center;}
	.f1_price_info{font-weight: bold;font-size:1vw}
	
	.f2_price_info{font-size:1.6vw;font-weight: bold}
	.f3_price_info{font-size:1.2vw}
	.price_info_contnet2{height:60vh}
	.price_info_box2{width:80%;color:#305669}
	.back_btn{height:5vh;border-radius:8%;border:none;background:#3799b8;color:#fff}
	.light{color:#3799b8}
	.light2{color:#305669}
	
</style>
<div class="container-fluid">
	<div class="row">
		<div class="total_info_header ">
			<h3 class="light2"><b>결제 안내</b></h3>
		</div>
	</div>
	<div class="row">
		<div class="price_info_content">
			<div class="h20"></div>
			<div class="price_info_text  h60">
				<div class="col-lg-8 pull-left h100">
				<!--content-->
					<ul class="col-lg-12 price_info_box h100">
						<div class="col-lg-12 h33">
							<li class="h100">
								<div class="h25"></div>
								입금은행
							</li>
							<li>
								<img src="img/F-5_contents-03.png" alt="은행" style="margin-top: 2%;width:40%" >
							</li>
						</div>
						<div class="col-lg-12 h33">
							<li>계좌</li>
							<li>932018322-01-010</li>
						</div>
						
						<div class="col-lg-12 h33">
							<li>예금주</li>
							<li>(주)아이디어콘서트</li>
						</div>
						<div class="f1_price_info">
							입금자와 출원인이 상이한 경우에는 반드시 연락주기 기바랍니다. 
						</div>
					</ul>
					<!--content-->
				</div>
				<div class="col-lg-4 pull-right img_wrap_price h100">
					<img src="img/bank-flat_img_png.png" class="h100">
				</div>
			</div>
			<div class="h20"></div>
		</div>
	</div>
	<!--content2-->
	<div class="row">
		<div class="price_info_contnet2">
			<div class="h20"></div>
			<!--content2-->
			<div class="h60  col-lg-12">
				<div class="col-lg-12 h100">
					<div class="price_info_box2 h33">
						<div class="f2_price_info"><span class="light">카드결제시</span>전화주세요</div>
					</div>
					<div class="h33 price_info_box2 f3_price_info">
						전문 상담원이 결재하여 신속하고 안전하게 결재하실 수 있습니다<br>
						모든 카드 3~5개월 무이자 행사중입니다.
					</div>
					<div  class="h33 price_info_box2 f3_price_info">
						<span class="light">☏</span> 070.8825.5004
					</div>
				</div>
			</div>
			<!--content2-->
			<div class="h20  price_info_box2 " style="clear:both">
				<div class="col-lg-2 col-lg-offset-5 h100">
					<div class="h50"></div>
					<button class="col-lg-6 col-lg-offset-3 back_btn">메인으로</button>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>