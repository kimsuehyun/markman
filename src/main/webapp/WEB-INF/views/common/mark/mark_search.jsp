<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description=" Content="상표등록 마크맨 메인페이지">
<c:import url="/WEB-INF/views/import/header.jsp"></c:import>
<title>Markman</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/resources/common/css/style.css"/>

<!-- 부트스트랩 -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<iframe src="http://kdtj.kipris.or.kr/kdtj/searchLogina.do?method=loginTM" width="100%" id="view"></iframe>
<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
<script>
var ytResize=function(){
   	$("#view").height($(window).width()).height($(window).width()/16*9);
 }
 $(window).on("load resize",ytResize)	

</script>
</body>
</html>