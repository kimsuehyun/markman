<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->

<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
	<style>
		.btn_show_wrap{padding:0px !important}
		.btn_show_wrap > button{border:1px solid #999;background:rgba(0,0,0,0);color:#999;height:3vh;margin-top:2vh}
	</style>
	<div class="padding_none container-fluid">
		<div class="row">
			<img src="/resources/images/C-2_서비스소개.png"  alt="서비스" class="img-responsive">
			<div class="col-lg-2 col-lg-offset-5 col-xs-6 col-xs-offset-3 btn_show_wrap " >
				<button class="pull-left col-lg-5 col-xs-12" onClick="location.href='/common/costInfo'" >비용안내</button>
				<button class="pull-right col-lg-6 col-xs-12" onclick="location.href='/applicant/mark/mvApply'">출원하러 가기</button>
			</div>
		</div>
	</div>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
</html>