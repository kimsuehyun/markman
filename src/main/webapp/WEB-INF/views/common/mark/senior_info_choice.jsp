<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>
<link rel="stylesheet" href="/resources/common/css/sub_detail.css"/>
<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->

<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
<body>
<style>
	.font_re{display:none}
	.info_btns2{right:78%;top:65%}
	
	@media(max-width:1800px){
		.user_img_box2 > img{width:35% !important}
		.user_img_box2 {position: absolute;top: 13%;right: 21%;}
	}
	@media(max-width:1625px){
		.user_img_box2 > img{width:30% !important}
		.user_img_box2 {position: absolute;top: 15%;right:19%;}
		.info_text{font-size:1.3vh}
	}
	@media(max-width:1400px){
		.user_img_box2 > img{width:28% !important}
		.user_img_box2 {position: absolute;top: 17%;right:16%;}
		.info_text{font-size:1.3vh}
		.view_imgs{display:none !important}
		.user_background_text{top:10%}
		.user_img_box2{top:23%;right:30%}
		.text-_font{display:none}
		.user_img_box2 > img{width:50% !important}
		.user_text  > img{width:60%}
		.user_text{bottom:13%}
		.user_text > div{font-size:3vh;line-height:3.5em}
		.content_text_wrap{width:100% !important;margin-left:0% !important}
		.content_text_wrap2 {width:90% !important;margin-left:5% !important}
		.view_box{display:none}
		.view_block{display:block;}
		.view_block > img:nth-child(odd){width:100%;margin-top:5%;cursor:pointer;}
		.view_block > img:nth-child(even){width:20%;text-align:center;display:block;margin-top:2%}
		.content3_wrap {width:90% !important;margin-left:5% !important}
		.content3_wrap > div > img{width:100% !important;}
		.info_btns2{right:78%;top:65%;height:20% !important}
		.font-re{display:block !important}
	}
	@media(max-width:1000px){
		.user_img_box2 {right:25%}
		.user_text{bottom:10%}
		
	}
	@media(max-width:890px){
		.user_img_box2{right:20%}
		.font-re{font-size:1.7vh !important}	
	}
	@media(max-width:830px){
		.user_img_box2{right:20%}
			
	}
	@media(max-width:780px){
		.user_img_box2{right:18%;top:27%}
		.user_text{bottom:5%}
		.user_text > div{font-size:2.3vh}
	}
	@media(max-width:700px){
		.user_img_box2{right:12%}
		.font-re{font-size:1.5vh !important}
	}
	@media(max-width:630px){
		.user_img_box2{right:10%;top:29%}
		.user_background_text{width:100%;left:0% !important}
		.font-re{font-size:2vh !important}
	}
	
	@media(max-width:550px){
		.user_img_box2{right:0%;top:33.5%}
		.user_img_box2 > img{width:35% !important}
		.user_background_text{width:100%;left:0% !important}
		.font-re{font-size:2vh !important}
		.content_text_wrap  > div > img{width:100% !important}
		.user_text > div{font-size:1.5vh}
		.info_btns2{width:35%;right:60%}
	}
</style>

<div>
	<div class="user_background_wrap">
		<img src="/resources/images/user2_bg.jpg" class="img-responsive view_imgs">
		<img src="/resources/images/user2_bg_m.jpg" class="img-responsive hidden-lg" style="width:100%">
		<div class="user_background_text">
			<div>
				<div>더 상세한 내용을 원하는,</div><br>
				<div class="pointer_font" style="width:100%;float:left">어드밴스Advanced</div><br><br>
				<div style="text-align: center;font-size: 2vh;line-height: 2em;" class="text-_font" >
					여기저기 검색하느라 시간 낭비말고,마크맨에서 스마트한 상표 출원을!<br>
					저렴한 가격에 상표 출원 노하우를 얻어가세요.
				</div>
				<div style="text-align: center;font-size: 2vh;display:none;line-height: 2em;" class=" font-re" >
					여기저기 검색하느라 시간 낭비말고,<br>
					마크맨에서 스마트한 상표 출원을!<br>
					저렴한 가격에 상표 출원 노하우를 얻어가세요.
				</div>
				
			</div>
		</div>
		<div class="user_img_box2">
			<img src="/resources/images/user2.png" class="img-responsive">
		</div>
		<div class="user_text">
			<img src="/resources/images/user2_comment.png" alt="img">
			<div>
				상표VS서비스표<br>
				진행하는 사업의 방향에 따라 선택할<br>
				상표의 분류가 달라진다는 사실!
			</div>
		</div>
	</div>
	<div class="content_text_wrap col-md-8 col-md-offset-2 col-xs-12">
		<div>
			<img src="/resources/images/user2_markman.png" style="width:50%">
		</div>
		<div>
			마크맨이 추천하는 상표출원전략<br>
			<b>요점만 콕콕!노하우로 빠르게</b>
		</div>
	</div>
	<div class="col-md-8 col-md-offset-2 content_text_wrap2 col-xs-12 " style="margin-top:5%">
		<div class="hidden-lg view_block">
			<img src="/resources/images/user2_step1_m.png" onClic="location.href='/common/seniorMarkInfo'">
			<img src="/resources/images/arrow_down_m.png">
			<img src="/resources/images/user2_step2_m.png" onClick="location.href='/common/markSearch'">
			<img src="/resources/images/arrow_down_m.png">
			<img src="/resources/images/user2_step3_m.png">
			
		</div>
		<div class="view_box">
			<div style="margin-left:13%">
				<div class="title_step" style="background:#f7c169;color:#fff;height:10%;font-size:2vh;padding:2%">step1.</div>
				<div class="info_box_img">
					<img src="/resources/images/icon_info.png" class="img-responsive" >
					상표vs서비스표<br>
					상표분류 알기<br>
					<b>&#8726</b>
				</div>
				<div class="info_text">
					사업의 방향에 맞는<br>
					상표분류에 대해 알아보세요.<br>
					아는 사람들만 알아요!
					
				</div>
				<button class="info_btns" onClick="location.href='/common/seniorMarkInfo'">보러가기</button>
			</div>
			
			<div>
				<div style="height:50%"></div>
				<img src="/resources/images/arrow.png" alt="화살표">
			</div>
			
			<div>
				<div class="title_step" style="background:#ee7b00;color:#fff;height:10%;font-size:2vh;padding:2%">step2.</div>
				<div class="info_box_img">
					<img src="/resources/images/icon_search.png" class="img-responsive" >
					이미등록된<br>
					상표인지 검색<br>
					<b>&#8726</b>
				</div>
				<div class="info_text">
					출원하려고 하는 상표가<br>
					특허청에 이미 등록이 되어있는지<br>
					검색해보세요!					
				</div>
				<button class="info_btns" onClick="location.href='/common/markSearch'">보러가기</button>
			</div>
			
			
			<div>
				<div style="height:50%"></div>
				<img src="/resources/images/arrow.png" alt="화살표">
			</div>
			
			<div>
				<div class="title_step" style="background:#e73828;color:#fff;height:10%;font-size:2vh;padding:2%">step3.</div>
				<div class="info_box_img">
					<img src="/resources/images/icon_payment.png" class="img-responsive" >
					상표출원에 필요한<br>
					서류 챙기기<br>
					<b>&#8726</b>
				</div>
				<div class="info_text">
					출원에 필요한 서류양식들을<br>
					꼼꼼히 챙겨드립니다.<br>
					바로 다운로드 가능합니다
				</div>
				<button class="info_btns">보러가기</button>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-md-offset-2 content3_wrap padding_none col-xs-12">
		<div>
			<img src="/resources/images/banner_gogo.png">
			<button class="info_btns2" onclick="location.href='/applicant/mark/mvApply'">바로출원하러가기</button>
		</div>
	</div>
	
</div>



<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
</body>
</html>