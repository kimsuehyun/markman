<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/counseling.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
<title>Insert title here</title>
</head>
<body>
<div class="container-fulid" style="width:100%">
	<div class="row">
		<div class="col-lg-12 col-sm-12  col-xs-12" style="margin-top:5%">
			<div class="height_box"></div>
			<!-- 상표상담-->
			<div class="col-lg-12 title_bar col-xs-12 padding_none">
				<div  class="board_title col-lg-6 col-xs-5 padding_none h100">
					<b>상표 상담</b>
					
					<b class="hidden-xs hidden-sm">상표와 관련해서 궁금한 점을 물어보세요!</b>
				</div>
				<div class="col-lg-3 col-xs-7 pull-right h70 btn_wraps  padding_none">
					<div class="h20"></div>
						<button type="button" class=" col-xs-6 pull-right" onClick="location.href='/'">메인으로</button>
						<button type="button" class=" col-xs-5 pull-left" onClick="mvWrite()">글쓰기</button>						
				</div>
			</div>
			<!-- table -->
			<div style="width:100%;;height:2vh;clear:both"></div>
			<table class="table table-hover  table_take text-center col-lg-12">
	  			<tr>
	  				<td onClick='location.href="/user/counseling/mvDetail"'>1</td>
	  				<td>test</td>
	  				<td>qwer917</td>
	  				<td>2017.05.05</td>
	  			</tr>
			</table>
			<div>아마도 페이지 네이션?</div>
			<br>
		</div>
	</div>
</div>
<script>
function mvWrite(){
	var formId2 = $("#formId2");
	var action = "/user/counseling/mvWrite";
	formId2.attr("action",action);
	formId2.attr("method","get");
	formId2.submit();
}
</script>

	
</body>
</html>