<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/sub_detail.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<style>
	.font_re{display:none}
	.info_btns2{right:78%;top:65%}
	@media(max-width:1800px){
		.user_img_box > img{width:35% !important}
		.user_img_box {position: absolute;top: 13%;right: 21%;}
	}
	@media(max-width:1625px){
		.user_img_box > img{width:30% !important}
		.user_img_box {position: absolute;top: 15%;right:19%;}
		.info_text{font-size:1.3vh}
	}
	@media(max-width:1400px){
	.font-re{display:block  !important}
		.user_img_box > img{width:28% !important}
		.user_img_box {position: absolute;top: 17%;right:16%;}
		.info_text{font-size:1.3vh}
		.view_imgs{display:none !important}
		.user_background_text{top:10%}
		.user_img_box{top:23%;right:30%}
		.text-_font{display:none}
		.user_img_box > img{width:50% !important}
		.user_text  > img{width:60%}
		.user_text{bottom:13%}
		.user_text > div{font-size:3vh;line-height:3.5em}
		.content_text_wrap{width:100% !important;margin-left:0% !important}
		.content_text_wrap2 {width:90% !important;margin-left:5% !important}
		.view_box{display:none}
		.view_block{display:block;}
		.view_block > img:nth-child(odd){width:100%;margin-top:5%}
		.view_block > img:nth-child(even){width:20%;text-align:center;display:block;margin-top:2%}
		.content3_wrap {width:90% !important;margin-left:5% !important}
		.content3_wrap > div > img{width:100% !important;}
		
	}
	@media(max-width:1000px){
		.user_img_box {right:25%}
		.user_text{bottom:10%}
		
	}
	@media(max-width:890px){
		.user_img_box{right:20%}
		.font-re{font-size:1.7vh !important}	
	}
	@media(max-width:830px){
		.user_img_box{right:20%}
			
	}
	@media(max-width:780px){
		.user_img_box{right:18%;top:27%}
		.user_text{bottom:5%}
		.user_text > div{font-size:2.3vh}
	}
	@media(max-width:700px){
		.user_img_box{right:12%}
		.font-re{font-size:1.5vh !important}
	}
	@media(max-width:630px){
		.user_img_box{right:10%;top:29%}
		.user_background_text{width:100%;left:0% !important}
		.font-re{font-size:2vh !important}
	}
	
	@media(max-width:550px){
		.user_img_box{right:0%;top:33.5%}
		.user_img_box > img{width:35% !important}
		.user_background_text{width:100%;left:0% !important}
		.font-re{font-size:2vh !important}
		.content_text_wrap  > div > img{width:100% !important}
		.user_text > div{font-size:1.5vh}
		.info_btns2{width:35%;right:60%;height:20% !important}
		
	}
</style>
<div>
	<div class="user_background_wrap">
		<img src="/resources/images/user3_bg.jpg" class="img-responsive view_imgs">
		<img src="/resources/images/user3_bg_m.jpg" class="img-responsive hidden-lg" style="width:100%">
		
		<div class="user_background_text">
			<div>
				<div>상표 출원서를 숙지한,</div><br>
				<div class="pointer_font" style="width:100%;float:left">마스터Master</div><br><br>
				<div style="text-align: center;font-size: 2vh;line-height: 2em;" class="text-_font" >
					상표 출원에 이미 일가견이 있는 당신, 마크맨의 진가를 알아보시는군요!<br>
					스마트한 가격으로 쉽고 빠르게 출원하세요.
				</div>	
				<div style="text-align: center;font-size: 2vh;line-height: 2em;display:none;" class="hiddne-lg font-re" >
					상표 출원에 이미 일가견이 있는 당신.<br> 
					마크맨의 진가를 알아보시는군요!<br>
					스마트한 가격으로 쉽고 빠르게 출원하세요.
				</div>
			</div>
		</div>
		<div class="user_img_box">
			<img src="/resources/images/user3.png" class="img-responsive">
		</div>
		<div class="user_text">
			<img src="/resources/images/user3_comment.png" alt="img">
			<div>
				아침에 작성한 출원서가<br>
				그 날 저녁에 바로 출원되는<br>
				놀라운 마크맨의 서비스를 추천해드립니다!
			</div>
		</div>
	</div>
	<div class="content_text_wrap col-md-8 col-md-offset-2 col-xs-12">
		<div>
			<img src="/resources/images/user3_markman.png" style="width:50%">
		</div>
		<div>
			마크맨이 추천하는 상표출원전략<br>
			<b>마크맨과 초고속 상표출원!</b>
		</div>
	</div>
	<div class="col-md-8 col-md-offset-2 content_text_wrap2 col-xs-12 " style="margin-top:5%"></div>
	<div class="col-md-8 col-md-offset-2 content3_wrap padding_none col-xs-12">
		<div>
			<img src="/resources/images/banner_gogo.png">
			<button class="info_btns2" onclick="location.href='/applicant/mark/mvApply'">바로출원하러가기</button>
		</div>
	</div>
	<div class="col-md-8 col-md-offset-2 content4_wrap" style="margin-top:3%">
		
		<h2 class="text-center">아래의 내용도 추천드려요</h2>
		<div style="width:100%;height:3vh"></div>
		<div class="col-md-6 position_master" style="margin-top:1%">
			<img src="/resources/images/banner_user1.png" style="width:100%">
			<button class="master_btns" style="left:11%" onClick="location.href='/common/biginnerServiceInfo'">보러가기</button>
		</div>
		<div class="col-md-6 position_master" style="margin-top:1%">
			<img src="/resources/images/banner_user2.png" style="width:100%">
			<button class="master_btns" style="left:50%" onClick="location.href='/common/seniorInfoChoice'">보러가기</button>
		</div>
	</div>
</div>


 <article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
</body>
</html>