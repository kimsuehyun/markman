<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link href="/resources/common/css/pop-up.css" rel="stylesheet">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>Insert title here</title>
</head>
<body>
	<header class="popup_header">
		<div><b>메일 주소록</b></div>
	</header>
	
	<section class="popup_main">
	
		<div>
			<div class="f1">
				<div>최근 사용한 주소<b>30</b></div>
			</div>	
			
			<div class="phone">
				<div id="all">전체 <b>${userCount}</b></div> 
				<div id="applicant">출원인 <b>${applicantCount}</b></div> 
				<div id="patent">변리사 <b>${patentCount}</b></div> 
			</div>
		</div>
		
		<div>
			<form id="formId" >
				<input type="hidden" id="search" name="search"/>
				<input type="hidden" id="user_type" name="user_type" value="all"/>
				<div class="phone_wrap">
					<div>
						<div class="search_box">
							<div><input type="text" id="search-input" placeholder="메일주소 찾기" value="${search}"><button type="button" class="search-address">검색</button></div>
						</div>
							
						<div class="check_box">
							<input type="checkbox" class="select-all" id="select-all"> 아이디/이름/이메일 주소
						</div>
						
						<div class="email_box">
							<div class="scroll_box">
									<c:forEach items="${userList}" var="list">
									<div>
										<input type="checkbox" class="select-user"><span class="user">"${list.id}" ,&nbsp"${list.name}"&nbsp&#60${list.email}&#62</span>
									</div>
									</c:forEach>
									
							</div><!-- scroll -->
							
						</div><!-- email --> 
					</div>
					
					<div class="box_btn">
						<div class="height">
							<button  type="button" class="mvList" data-from="receiver">&lt;</button>
							<br>
							<button type="button" class="mvCart" data-to="receiver">&gt;</button>
						</div>
						
						<div>
							<button type="button" class="mvList" data-from="cc">&lt;</button>
							<br>
							<button type="button" class="mvCart" data-to="cc">&gt;</button>
						</div>
						
						<div>
							<button type="button" class="mvList" data-from="bcc">&lt;</button>
							<br>
							<button type="button" class="mvCart" data-to="bcc">&gt;</button>
						</div>
					</div>
					
					
					<div class="right_box">
						<b>받는사람</b>
						<div class="receiver cart h45">
						</div>
						
						<br>
						
						<b>참조</b>
						<div class="cc cart h20">
						</div>
						
						<b>숨은참조</b>
						<div class="bcc cart h20">
						</div>
					</div>
					
				</div>		
			</form>
		</div>
		
	</section>
	<footer>
		<div>
			<button type="button" onclick="addAddress();">확인</button>
			<button type="button" onclick="window.close()">취소</button>
		</div>
	</footer>
</body>
<script src="/resources/common/js/global.js"></script>
<script>
	
	$(document).on("keydown","#search-input",function(){
		if(window.event.keyCode==13){
			searchAddress();
			return false;
		}
	});
	$('.search-address').click(function(){
		searchAddress();
	});
	function searchAddress(){
		var data = {};
		data["user_type"] = $("#user_type").val();
		data["search"] = $("#search-input").val();
		$.ajax({
			url : "/admin/address",
			data : data,
			type : "GET",
			dataType : "json",
			success : function(resultData){
				setList(resultData)
			},
			error : function(request, status, error) {
		 	      alert("code:" + request.status + "\n" + "error:" + error);
		 	}
		});
	}
	
	function setList(resultData){
		var data = resultData.userList;
		var appendStr = "";
		var div = $('.scroll_box');
		for(var i=0 ; i<data.length ; i++){
			appendStr += '<input type="checkbox" class="select-user">'
			+'<span class="user">"'+data[i].id+'" ,&nbsp"'+data[i].name+'"&nbsp&#60'+data[i].email+'&#62</span><br>';
		}
		div.empty();
		div.append(appendStr);
	}
	$('.select-all').click(function(){
		if($(this).prop("checked")) { //해당화면에 전체 checkbox들을 체크해준다
			$(".scroll_box input[type=checkbox]").prop("checked",true); // 전체선택 체크박스가 해제된 경우
		} else { //해당화면에 모든 checkbox들의 체크를해제시킨다. 
			$(".scroll_box input[type=checkbox]").prop("checked",false); 
		}
	});
	
	$(document).on("click",".select-user",function(){
		if($(".scroll_box input[type=checkbox]:checked").length!=$(".scroll_box input[type=checkbox]").length){
			$('.select-all').prop("checked",false);
		}
		else{
			if($('.select-all').prop("checked"))
				$('.select-all').prop("checked",false)
			else	
			$('.select-all').prop("checked",true);
		}
	});
	
	$(".mvCart").click(function(){
		var to = $(this).data("to");
		$(".scroll_box input[class=select-user]:checked").each(function(index){
			var val = $(this).next('span').html();
			$("."+to+"").append('<div><input type="checkbox" class="select-user"><span class="user">'+val+'</span></div>');
		});
		$("input:checkbox:checked").prop("checked",false);
	});
	
	$(".mvList").click(function(){
		var checked = $("."+$(this).data('from')+" input:checkbox:checked");
		checked.next('span').remove();
		checked.remove();
	});
	
	$('.phone div').click(function(){
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		$("#user_type").val($(this).attr('id'));
		searchAddress();
	});
	
	$(document).on("click",".user",function(){
		$(this).prev('input').prop("checked",true);
	});
	
	function addAddress(){
		var receiver = new Array();
		var cc = new Array();
		var bcc = new Array();
		$(".receiver input:checkbox").each(function(index){//받는 사람
			receiver.push($(this).next('span').text().split("<")[1].replace(">",""));
		});
		$(".cc input:checkbox").each(function(index){//참조
			cc.push($(this).next('span').text().split("<")[1].replace(">",""));
		});
		$(".bcc input:checkbox").each(function(index){//숨김참조
			bcc.push($(this).next('span').text().split("<")[1].replace(">",""));
		});
		$('#receiver',opener.document).val(receiver);
		$('#cc',opener.document).val(cc);
		$('#bcc',opener.document).val(bcc);
		opener.parent.addAddress();
		window.close();
	}
	
</script>
</html>