<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/guest.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<style>
	.photo{height:43vh;margin-top:8%;cursor:pointer;}
	.photo > div:hover{ box-shadow:3px 3px 3px #999;}
	.view_imgs{width:100%;display:block;transition:0.5s;border:1px solid #999;box-shadow:3px 3px 3px #ddd;}
	.photo > div:hover .view_imgs{transform : scale(1.2,1.2)}
	.photo > div{height:100%;border:1px solid #ddd;padding:7% 7% 7% 7%}
	.photo > div > div:first-child{width:90%;height:90%;overflow: hidden;}
	.photo > div > div:last-child{width:100%;color:#999;}
	
	@media (max-width:1700px){
		
	}
</style>
<div class="w70">
	<div class="col-lg-12 col-xs-12" >
		<!-- 여기 -->
		<div class="col-lg-4 col-xs-12 photo" >
			<div class="col-lg-12 " >
				<div>
					<img src="/resources/images/content_img_10.png" alt="img" class="view_imgs" onClick="location.href='/user/detailView5'">
				</div>
				<div>
					
					<div>상표등록</div>
				</div>
			</div>
		</div>
		<!-- 여기 -->
		
		<!-- 여기 -->
		<div class="col-lg-4 col-xs-12 photo" >
			<div class="col-lg-12 " >
				<div>
					<img src="/resources/images/content_img_08.png" alt="img" class="view_imgs" onClick="location.href='/user/detailView4'">
				</div>
				<div>
					
					<div>상표등록TIP!</div>
				</div>
			</div>
		</div>
		
		<!-- 여기 -->
		<div class="col-lg-4 col-xs-12 photo" >
			<div class="col-lg-12 " >
				<div>
					<img src="/resources/images/content_img_06.png" alt="img" class="view_imgs" onClick="location.href='/user/detailView2'">
				</div>
				<div>
					
					<div>상표 등록 과정</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4 col-xs-12 photo" >
			<div class="col-lg-12 " >
				<div>
					<img src="/resources/images/content_02-2.png" alt="img" class="view_imgs" onClick="location.href='/user/detailView1'">
				</div>
				<div>
					
					<div>상표의 개념</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4 col-xs-12 photo" >
			<div class="col-lg-12 " >
				<div>
					<img src="/resources/images/content_img_02.png" alt="img" class="view_imgs" onClick="location.href='/user/detailView3'">
				</div>
				<div>
					
					<div>상호와 상표의 차이</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4 col-xs-12 photo" >
			<div class="col-lg-12 " >
				<div>
					<img src="/resources/images/content_img_12.png" alt="img" class="view_imgs" onClick="location.href='/user/detailView6'">
				</div>
				<div>
					
					<div>상표검색</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4 col-xs-12 photo" >
			<div class="col-lg-12 " >
				<div>
					<img src="/resources/images/content_img_14.png" alt="img" class="view_imgs" onClick="location.href='/user/detailView7'">
				</div>
				<div>
					
					<div>내가 지킨다!</div>
				</div>
			</div>
		</div>
		
	</div>
	
</div>
<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
</html>