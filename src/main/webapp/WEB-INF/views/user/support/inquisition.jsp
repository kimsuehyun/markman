<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/guest.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
<style>
	#priTitle{
		float: left;
		padding: 5px 0px;
	}
</style>
<title>Insert title here</title>
</head>
<body style="overflow-x: visible !important;">
<div class="container" style="height:80vh">
	<div class="row">
		<div  class="height_box"></div>
		<div class="col-lg-10 col-lg-offset-1" >
			<h2>문의하기</h2><br>
			<div class="lines"></div>
			<table class="inquiry_table col-xs-12 col-lg-12" >
				<tr class="h33">
					<td class="col-lg-4">이름</td>
					<td>
						<input type="text"  class="input-xs" id="senderName">
					</td>
				</tr>
				<tr class="h33">
					<td class="col-lg-4">이메일</td>
					<td>
						<input type="email" class="input-xs" value="${sessionScope.currentUser.getEmail() }" id="sender">
					</td>
				</tr>
			</table>
			<div class="clear"></div>
			<div class="height_box"></div>
			<div class="col-lg-12 padding_none inquiry_box content_inquiry ">
				<div class="col-lg-12 col-xs-12 padding_none h100 ">
					<div  class="col-lg-12 col-xs-12 padding_none h100">
						<div class="col-lg-2 col-xs-3 h100 text-center">
							<h5>분류</h5>
						</div>
						<div class="col-lg-4 col-xs-4 h100 padding_none">
							<select class="col-lg-12 h100 col-xs-12 " id="type" onchange="selectType(this.value)">
								<option value="개선사항">개선사항</option>
								<option value="불편사항">불편사항</option>
								<option value="오류 신고">오류 신고</option>
								<option value="결제 관련 문의">결제 관련 문의</option>
								<option value="기타">기타</option>
							</select>
						</div>
					</div>
				</div>
				<div class='inquiry_box'></div>
				<div class="col-lg-12 col-xs-12 padding_none h100 ">
					<div  class="col-lg-12 col-xs-12 padding_none h100">
						<div class="col-lg-2 col-xs-3 h100 text-center">
							<h5>제목</h5>
						</div>
						<div class="col-lg-10  col-xs-9 h100 padding_none">
							<div class="h100 col-lg-12 col-xs-12" style="border: 1px solid ;">
								<span id="priTitle"></span>
								<input type="text" class="h100 col-lg-10 col-xs-10" style="border: none;padding: 0;" id="title">
							</div>
						</div>
					</div>
				</div>
				<div class='inquiry_box'></div>
				<div class="col-lg-12 padding_none">
					<textarea class="col-lg-12 col-xs-12" id="content"></textarea>
				</div>
				<div  class="inquiry_box"></div>
				<button class="btn btn-info pull-right" onclick="sendMail()">전송하기</button>
			</div>
		</div>
	</div>
</div>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
<script src="/resources/common/js/global.js"></script>
<script>
$(document).ready(function(){
	var priTitle = '['+$("#type").val()+']';//기본 문의 유형
	$("#priTitle").append(priTitle);
});
function selectType(val){
	var priTitle = "["+val+"]";///선택한 문의 유형
	$("#priTitle").empty();
	$("#priTitle").append(priTitle);
}
function sendMail(){
	var data = {};
	var title = $('#priTitle').text()+$('#title').val();
	var defaultContent = '보내는 사람 : '+$('#senderName').val()+'<'+$('#sender').val()+'>\n';
    data["title"] = title;
    data["content"] = defaultContent+$('#content').val();
    alert(defaultContent+$('#content').val());
    $.ajax({
    	url : "/common/support/inquisition/regisOk",
    	type : "POST",
    	data : data,
    	success: function(resultData) {
    		//전송 완료 페이지로 이동해야함
    	},
    	error:function(request,status,error){
    	    alertt("code:"+request.status+"\n"+"error:"+error);
    	}
    });   
} 
</script>
</html>