<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/guest.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>

<script src="/resources/common/js/bootstrap.js"></script>
</head>
<style>
html{    overflow-x: inherit;}
	@media(max-width:1100px){
		.img_de{display:none !important}
		.img_show{display:block !important;}
		
		
		.price_info_box2{margin-top:10%}
	}
</style>
<body>

<div class="container-fluid">
	
		<div class="total_info_header col-lg-8 col-lg-offset-2">
			<h3 class="light2"><b>결제 안내</b></h3>
		</div>
	</div>
	<div class="row">
		<div class="price_info_content col-lg-8 col-lg-offset-2">
			<div class="h20"></div>
			<div class="price_info_text  h60">
				<div class="col-lg-8 pull-left h100">
				<!--content-->
					<ul class="col-lg-12 price_info_box h100">
						<div class="col-lg-12 h33">
							<li class="h100">
								<div class="h25"></div>
								입금은행
							</li>
							<li>
								<img src="/resources/image/F-5_contents-03.png" class="img-responsive" alt="은행" style="margin-top: 2%;" >
							</li>
						</div>
						<div class="col-lg-12 h33">
							<li>계좌</li>
							<li>932018322-01-010</li>
						</div>
						
						<div class="col-lg-12 h33">
							<li>예금주</li>
							<li>(주)아이디어콘서트</li>
						</div>
						<div class="f1_price_info">
							입금자와 출원인이 상이한 경우에는 반드시 연락주기바랍니다.
						</div>
					</ul>
					<!--content-->
				</div>
				<div class="col-lg-4 pull-right img_wrap_price h100">
					<img src="/resources/image/bank-flat_img_png.png" class="h100 img-responsive img_de" >
				</div>
			</div>
			<div class="h20"></div>
		</div>
	</div>
	<!--content2-->
	<div class="row">
		<div class="price_info_contnet2 col-lg-8 col-lg-offset-2 col-xs-12">
			<div class="h20"></div>
			<!--content2-->
			<img src="/resources/image/bank-flat_img_png.png" class="h100 img-responsive  img_show "  style="display:none" >
			<div class="h60  col-lg-12">
				<div class="col-lg-12 h100 box_top">
					<div class="price_info_box2 h33">
						<div class="f2_price_info"><span class="light">카드결제시</span>전화주세요</div>
					</div>
					<div class="h33 price_info_box2 f3_price_info">
						전문 상담원이 결제하여 신속하고 안전하게 결제하실 수 있습니다<br>
						모든 카드 3~5개월 무이자 행사중입니다.
					</div><br>
					<div  class="h33 price_info_box2 f3_price_info">
						<span class="light">☏</span> 070.8825.5004
					</div>
				</div>
			</div>
			<!--content2-->
			<div class="h20  price_info_box2 " style="clear:both">
				<div class="col-lg-2 col-lg-offset-5 h100">
					<div class="h50"></div>
					<button class="col-lg-8 col-lg-offset-2 back_btn" onClick="location.href='/'">메인으로</button>
				</div>
			</div>
		</div>
	
</div>

<article style="clear:both;margin-top:45%" class="footer_box">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
</html>