<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>자주 묻는 질문</title>
</head>
<!-- 임시로 해놓은 css, 수정 필요 -->
<style>
	div{
		text-align: center;
	}
	table{
		text-align: center;
		width: 100%; 
		margin-bottom:20px;
	}
	table tr td{
		border: 1px solid #555;
	}
</style>
<body>
	<div>
		<table>
			<tr>
				<td>${faqVo.title }</td>
			</tr>
			<tr>
				<td>${faqVo.content }</td>
			</tr>
		</table>
		<button onclick="window.close()">확인</button>
	</div>
</body>
</html>