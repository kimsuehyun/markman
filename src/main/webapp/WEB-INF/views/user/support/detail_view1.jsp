<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--css-->
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<c:import url="/WEB-INF/views/import/header.jsp"></c:import>
<!-- 부트스트랩 -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
<!-- 마크맨 main  <script src="/resources/common/js/main.js"></script> -->
<!--loading -->
<script src="/resources/common/js/global.js"></script>
</head>
<title>Insert title here</title>
</head>
<body>
<div class="container-fulid">
	<img src="/resources/images/content_img_03.png" class="img-responsive">
</div>
<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
</body>
</html>