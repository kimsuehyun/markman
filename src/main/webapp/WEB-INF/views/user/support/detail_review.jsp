<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!--css-->
<link rel="stylesheet" href="/resources/common/css/list_view.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<link rel="stylesheet" href="/resources/common/css/list_view.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body style="overflow-x:visible !important">
<style>
	.table_box{border:1px solid #ddd;width:100%;height:60vh}
	.text_table_title{width:100%;height:10vh;border-bottom:1px solid #ddd;padding:1%}
	.text_table_title > div:first-child{height:100%}
	.text_table_title > div:first-child > div{height:100%;background:#0e4383;float:left;color:#fff;border-radius:10px;text-align: center;font-size:5vh;}
	.text_table_title > div:nth-child(2){height:100%}
	.text_table_title > div:nth-child(2) > div{height:50%;}
	.text_table_title > div:nth-child(3){text-align:right}
	.text_content{padding:1.5%}
	.view_text_imgs{width:100%;height:24%;float:left}
	.view_text_imgs  > *{float:left}
	.view_text_imgs > img{width:30%;height:100%}
	.view_text_imgs > div{word-break:break-all;width:65%;}
	.footer_view{width:100%;height:8vh;border:1px solid #ddd;padding:1.2%}
	.footer_view > button{float:right;margin-right:1%}


</style>
<div class="continer">
	<div class="row">
		<div class="height_box"></div>
			<form id="formId" method="POST" enctype="multipart/form-data" >
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
				<input type="hidden" id="board_pk" name="board_pk" value="${reviewVo.board_pk}"/>
				<div  class="col-lg-8 col-lg-offset-2 col-xs-12 " >
					<h3>후기</h3>
					<div class="table_box">
						<div class="text_table_title">
							<div class="col-lg-1 col-xs-2" style="padding:0px !important">
								<div class="col-xs-12">
									Q
								</div>
							</div>
							<div class="col-lg-5 col-xs-6">
								<div class="col-lg-12 col-xs-6">제목: ${reviewVo.title}</div>
								<div class="col-lg-6 col-xs-6 ">작성자: ${reviewVo.writer}</div>
								<div class="col-lg-6 col-xs-6">
									<fmt:parseDate value="${reviewVo.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
		                 			<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/>
		                 			<jsp:useBean id="toDay" class="java.util.Date" />
									<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
									<c:choose>
										<c:when test="${regisDate==today}">
											<fmt:formatDate value="${regis_date}" pattern="HH:mm"/>
										</c:when>
										<c:otherwise>
											<div class="col-lg-12">
												${regisDate}
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
							<div class="col-lg-6 col-xs-4" style="color:red">
								조회수[${reviewVo.hits}]
							</div>
						</div>
						<div class="text_content">
						<c:if test="${boardImgList.img_url != null}">
							<div class="view_text_imgs">
								<img src="${boardImgList.img_url}">
								<div class="text">${reviewVo.content}</div>
							</div>	
						</c:if>
						<c:if test="${boardImgList.img_url == null}">
							<div>
								${reviewVo.content}
							</div>
						</c:if>
						</div>
					</div>
					<div class="footer_view">
					<c:if test="${sessionScope.currentUser.getId()==reviewVo.writer }">
							<button class="btn btn-success"   onclick="modifyOk()">수정하기</button>
							<button type="button" class="btn btn-danger delete" type="button">삭제</button>
					</c:if>
	   						<button type="button" class="btn btn-primary" onclick="location.href='/common/support/review'">목록으로</button>
						</div>
				</div>
			</form>
	</div>
</div>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
</body>
<script>
	function modifyOk(){
		var formId = $("#formId");
		formId.attr("method","post");
		formId.attr("action","/common/support/review/mvEdit");
		formId.submit();
		return false;
	}
	$('.delete').click(function(){
		var data = {};
		data["board_pk"] = $('#board_pk').val();
		confirmm("해당 게시물을 삭제하시겠습니까?").then(function(){
      		$.ajax({
      			url : "/common/support/review/deleteOk",
				type : "POST",
				data : data,
				success : function(resultData) {
					if(resultData=="success"){
						alertt("삭제가 완료되었습니다.","success").then(function(){
							location.href="/common/support/review";
						});
					}else{
						alertt("삭제 실패");
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\n" + "error:" + error);
				}
			});  
    	},function(){});
	});
</script>

</html>