<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/guest.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<style>
	.list_views{margin-top:4%}
	.list_views > li{width:25%;float: left;text-align: center;border-top:2px solid #999;border-bottom:1px solid #fff;background:#1D232E;color:#fff;border-right:1px solid #fff}
	.list_views > li:last-child{text-align:center;}
	.list_views > li:nth-child(2){width:50%}
	
	.row_view  > li{text-align: center;width:25%;padding:1% 0% 0% 0% !important;height:7vh;border-right:1px solid #ddd}
	.row_view  > li:nth-child(2){width:50%}
	.row_view  > li:nth-child(1){border-left:1px solid #ddd}
	.review_view {display:block;text-align:center;margin:0 auto;padding:0;float: right !important;}
	.review_text{overflow-y:scroll;height:15vh}
	.row_view > li:nth-child(2){
		overflow: hidden; 
		text-overflow: ellipsis;
		white-space: nowrap; 
		width: 50%;
	} 
	h3{margin-top:3%}
	
</style>
<body style="overflow-x:visible !important">





	<div  style="height:85vh;width:95%">
		<br>
		<h2>자주묻는 질문</h2>
		<br>
		<ul class="nav nav-tabs">
    		<li class="active"><a data-toggle="tab" href="#home">회원</a></li>
    		<li><a data-toggle="tab" href="#menu11">기간</a></li>
    		<li><a data-toggle="tab" href="#menu22">비용</a></li>
    		<li><a data-toggle="tab" href="#menu31">상표출원</a></li>
  		</ul>


  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      
      <div class="col-lg-12 review_wrap padding_none">
      	<div class="col-lg-12 review_wrap padding_none">
		 <div class="col-lg-12 col-xs-12 text-center qfent-table padding_none">
		 	<div class="col-lg-12" style="padding:0px !important;">
		 		<ul class="list_views">
		 			<li>No</li>
		 			<li>Title</li>
		 			<li>비고</li>
		 		</ul>
			</div>
			
			<div class="views">	
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >1</li>
							<li class="col-lg-2 col-xs-2 " >[회원]마크맨 서비슷는 회원등록을<br>해야만 이용할 수 있나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						마크맨의 상표출원 서비스를 이요하시려면 회원가입을 하셔야 합니다.<a onclick="loginOutFunction('회원가입')">회원가입 바로가기</a><!--답변 content-->
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					
					<!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >2</li>
							<li class="col-lg-2 col-xs-2 " >[회원]회원 탈퇴는 어떻게 하나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						관리자에게 별도로 문의 부탁드립니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					
					
					<!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >3</li>
							<li class="col-lg-2 col-xs-2 " >[회원]회원을 탈퇴하면 제가 등록한 정보는 어떻게 되나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						회원 탈퇴 시에는 회원님의 정보가 모두 폐기됩니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					</div>
			
		 </div>
		</div>
      </div>
    </div>
    
    <div id="menu11" class="tab-pane fade">
      
      <div class="col-lg-12 review_wrap padding_none">
      	<div class="col-lg-12 review_wrap padding_none">
		 <div class="col-lg-12 col-xs-12 text-center qfent-table padding_none">
		 	<div class="col-lg-12" style="padding:0px !important">
		 		<ul class="list_views">
		 			<li>No</li>
		 			<li>Title</li>
		 			<li>비고</li>
		 		</ul>
			</div>
			
			<div class="views">
					<!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >4</li>
							<li class="col-lg-2 col-xs-2 " >[기간]출원에서 등록까지 얼마나 걸리나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						특허청의 사정에 따라 다르지만 평균적으로 상표등록의 경우 10~12개월 정도 소요됩니다.<br>
				 						상표출원 후 8개월 전후로 특허청 심사결과가 나오고 이후 2개월의 공고기간을 거쳐 최종등록 됩니다.<br>
				 						심사관이 의견제출 통지서를 여러 번 발송할 경우 더 많은 기간이 소요될 수 있습니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					
					<!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >5</li>
							<li class="col-lg-2 col-xs-2 " >[기간]등록이 급히 필요합니다. 기간을 단축할 수 있을까요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						특허청의 사정에 따라 다르지만 평균적으로 상표등록의 경우 10~12개월 정도 소요됩니다.<br>
			 						        우선심사제도르 이용시, 상표는 1~2개월 정도 소요됩니다.우선심사제도를 이요하시면 제도<br>
			 							이용에 따른에 따른 추가 비용이 발생합니다. 
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					</div>
			
		 </div>
		</div>
      </div>
    </div>
    <div id="menu22" class="tab-pane fade">
      
      <div class="col-lg-12 review_wrap padding_none">
      	<div class="col-lg-12 review_wrap padding_none">
		 <div class="col-lg-12 col-xs-12 text-center qfent-table padding_none">
		 	<div class="col-lg-12" style="padding:0px !important">
		 		<ul class="list_views">
		 			<li>No</li>
		 			<li>Title</li>
		 			<li>비고</li>
		 		</ul>
			</div>
			
			<div class="views">
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >6</li>
							<li class="col-lg-2 col-xs-2 " >[비용]비용은 어떻게 되나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						통상 권리 등록절차는 총 3단계로 구성되고, 비용은 각 단계별로 발생됩니다.<br>
										1단계 - 최초 출원(=신청) 시 비용이 발생됩니다.  출원비용:특허청관납료 56,000원<br>
										2단계 - 심사 중 특허청에 서류를 제출할 경우 비용이 발생됩니다.  의견제출통지서 약 100,000원<br>
										3단계 - 심사에 통과하여 등록될 경우 비용이 발생됩니다.  상표등록비용:221,000(10년유지비용포함) / 성공보수 55,000원<br> 
										자세한 비용은 비용안내 페이지를 참고해주세요. 
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >7</li>
							<li class="col-lg-2 col-xs-2 " >[비용]상품류를 추가하면 추가비용이 발생되나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						네. 상표는 1분류당 1건으로 산정되고 1개의 권리가 발생됩니다.<br>
										각 분류별로 심사를 하게 되고 서로 독립적으로 등록됩니다.<br>
										1개의 상표라도 3개의 상품 분류에 신청하면 3개의 권리가 발생되며 따라서 비용도 각각 따로 발생됩니다.<br> 
										예시> 의류는 상표분류상 25류이고, 패션잡화는 18류 입니다. <br>
										마크맨을 의류와 패션잡화에 사용을 하려면 25류와 18류에 상표를 등록을 해야 합니다.<br>
										즉, 25류에 대한 비용과 18류에 대하여 별도로 상표를 출원하고 비용도 개별로 내야 됩니다. (비용예시를 들어 주세요)
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >8</li>
							<li class="col-lg-2 col-xs-2 " >[비용]마크맨 수수료와 특허청 관납료는 무슨 차이가 있나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						수수료는 의뢰된 상표 출원 업무에 대한 마크맨의 서비스 수수료입니다.<br>
										관납료는 출원업무 진행시 특허청에 납부하여야 하는 비용이므로 마크맨의 서비스 비용과는 무관합니다.<br>
										마크맨 수수료와 관납료를 구분해서 알려 주셨으면 합니다.<br>
										비용을 정확하게 나누어 출원인이 내는 비용을 정확하게 알려 주는 내용으로 만들어 주세요.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >9</li>
							<li class="col-lg-2 col-xs-2 " >[비용]관납료는 무엇인가요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						관납료는 출원업무 진행시 특허청에 납부하여야 하는 비용을 의미합니다.<br> 
				 						특허관납료/상표관납료/디자인관납료를 구체적으로 나타내 주시기 바랍니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >10</li>
							<li class="col-lg-2 col-xs-2 " >[비용]관납료는 카드 납부가 안되나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						관납료는 출원인이 특허청에 직접 납부해야 하는 비용이지만,<br>
				 						절차의 편의를 위해 마크맨이 대신 납부해드리는 것이므로 현금으로 입금해주시면 됩니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >11</li>
							<li class="col-lg-2 col-xs-2 " >[비용]관납료 감면 혜택은 무엇인가요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						개인 및 중소기업 등 법에서 규정된 출원인의 경우 특허청에 납부하는 관납료의 감면 혜택을 받을 수 있습니다.<br>
										다만, 관납료의 감면혜택을 받기 위해서는 특허청에서 정한 요건에 따라 해당 감면서류를 제출하여야 합니다.<br>
										출원 의뢰후 출원인/발명자 등을 검토하고 고객센터를 통해 자세하게 안내해 드립니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					</div>
			
		 </div>
		</div>
      </div>
    </div>
    <div id="menu31" class="tab-pane fade">
      <div class="col-lg-12 review_wrap padding_none">
      	<div class="col-lg-12 review_wrap padding_none">
		 <div class="col-lg-12 col-xs-12 text-center qfent-table padding_none">
		 	<div class="col-lg-12" style="padding:0px !important">
		 		<ul class="list_views">
		 			<li>No</li>
		 			<li>Title</li>
		 			<li>비고</li>
		 		</ul>
			</div>
			
				
					<div class="views">
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >12</li>
							<li class="col-lg-2 col-xs-2 " >[상표출원]상표출원은 처음인데 어떻게 하면 될까요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						마크맨은 상표출원이 처음인 분들도 쉽고 저렴하게 이용하실 수 있는 서비스입니다.<br>
										회원가입 후, 안내에 따라 출원하실 상표를 등록하시면 됩니다.<br>
										전문가가 검토 후, 진행 방향에 대해 상세히 알려드리므로 걱정하지 않으셔도 됩니다. 
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >13</li>
							<li class="col-lg-2 col-xs-2 " >[상표출원]출원과 등록은 어떤 차이가 있나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						출원은 특허청에 상표를 보호해달라고 신청하는 것이고,<br>
										등록은 출원된 상표를 심사한 후, 그 상표에 대한 권리를 인정해준다는 것입니다. 
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >14</li>
							<li class="col-lg-2 col-xs-2 " >[상표출원]상표출원에 필요한 서류는 무엇인가요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						출원인의 성명, 주소 등 인적 사항이 필요합니다.<br>
										마크맨의 서비스에 따라 출원 신청을 하시면 필요한 정보를 입력하도록 구성되어 있습니다.<br>
										이 정보들만 빠짐없이 정확하게 입력해주시면 됩니다.<br>
										출원인이 법인일 경우, 사업자등록증이 추가로 필요합니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >15</li>
							<li class="col-lg-2 col-xs-2 " >[상표출원]개인명의로 출원하는 것과 법인명의로 출원하는 것에는 어떤 차이가 있나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						법적인 보호측면에서는 차이가 없습니다.<br>
										다만 상표는 양도가 가능한 자산이므로, <br>
										해당 자산을 누구 명의로 취득하는 것이 좋을지를 고민하셔서 출원하시면 됩니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >16</li>
							<li class="col-lg-2 col-xs-2 " >[상표출원]상표를 등록받으면 모든 상품/서비스에 대해 보호가 되나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						상표는 등록받은 상품/서비스에 대해서만 보호가 됩니다.<br>
										같은 상표라도 상품/서비스가 다르면 각기 다른 사람의 소유가 될 수 있습니다.<br>
										다양한 상품/서비스에 대해 보호 받기를 원하시면 각각의 상품/서비스에 대해 모두 출원을 하셔야 합니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >17</li>
							<li class="col-lg-2 col-xs-2 " >[상표출원]영문상표, 한글상표는 따로 등록해야 하나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						한글상표가 영문상표를 그대로 표기한 것이라면 따로 등록할 필요없이, 1건으로 등록이 가능합니다.<br>
										(예 - 아이디어콘서트 / ideaconcert => 1건<br>
										그러나, 한글과 영문이 서로 다른 경우에는 따로 등록해야 합니다.<br>
										(예 - 아이디어콘서트 / apple => 각각 등록)
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" onClick="location.href='/user/counseling/mvDetail'" >18</li>
							<li class="col-lg-2 col-xs-2 " >[상표출원]영문상표의 경우 대문자/소문자 구분이 있나요?</li>
							<li class="col-lg-3 col-xs-3 padding_none" >
								<button class=" review_view col-lg-6 col-xs-11">보기</button>
							</li>
						</div>	
					<!--답변 리스트 -->
					<div class="hiddne_box_review col-lg-12 col-md-12 col-xs-12 padding_none" style="background:#f1f1f1;height:30vh">	
				 			<div class="col-lg-12 padding_none h10 ">
				 				<button class="pull-right h100 reive_x_btn">x</button>
				 			</div>
				 			<div class="col-lg-12 col-md-12 col-xs-12 padding h90">
				 				<div class="h10"></div>
				 				<div class="h90 col-lg-8 col-lg-offset-2">
				 					<h2>Q 마크맨</h2><br><!--답변 title -->
				 					<div class="review_text">
				 						영문상표는 대/소문자는 무관합니다.<br>
										철자가 동일하다면 한 가지 버전만 등록하셔도 대/소문자 모두 보호가 가능합니다.
				 					</div>
				 				</div>
				 			</div>
				 		</div>
					</div><!-- 여기 -->
					</div>
			
		 </div>
		</div>
      </div>
    </div>
  </div>
  </div>
	
	<article>
		<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
	</article>	
<script>
 	$(".hiddne_box_review").hide();
	$(document).on("click",".review_view",function(){
		$(this).parents(".text_box").find(".hiddne_box_review").slideDown();
	
		$(this).siblings("li").find("ul").is(":visible")
		if($(this).parents(".text_box").siblings("div").find(".hiddne_box_review").is(":visible")){
			$(this).parents(".text_box").siblings("div").find(".hiddne_box_review").slideUp();
		}
		
	})

	$(document).on("click",".reive_x_btn",function(){
		$(this).parents(".text_box").find(".hiddne_box_review").slideUp();

	});
	
	var category = 0;
	$(".views:eq('"+category+"')").show().siblings(".views").hide();
	
	$(".select_box").change(function(){
		 category = $(this).val()
		$(".views:eq('"+category+"')").show().siblings(".views").hide();
	})
	
	
</script>
</body>
</html>