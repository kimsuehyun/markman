<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/guest.css"/>
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<style>
	.list_views > li{width:25%;float:left;text-align: center;border-top:2px solid #fff;border-bottom:1px solid #fff;background:#1D232E;color:#fff;border-left:1px solid #fff}
	.row_view  > li{text-align: center;width:25%;padding:1% 0% 0% 0% !important;border-right: 1px solid #ddd}
	.row_view  > li:first-child{border-left:1px solid #ddd}
	.review_view {display:block;text-align:center;} 
</style>
<body style="overflow-x:visible !important">
	<div class="container" style="height:80vh">
		<div class="row">
		<div class="inquiry_box"></div>
			<h3>후기</h3>
			<form id="formId">
			<input type="hidden" name="pageNum" id="pageNum"/>
			<input type="hidden" id="seq" name="seq"/>
			<!-- <input type="hidden" id="board_pk" name="board_pk"> -->
			<div class="col-lg-12 review_wrap padding_none">
				 <div class="col-lg-12 col-xs-12 text-center qfent-table padding_none">
				 	<div class="col-lg-12" style="padding:0px !important">
				 		<ul class="list_views">
				 			<li>No</li>
				 			<li>Title</li>
				 			<li>작성자</li>
				 			<li>Date</li>
				 		</ul>
				 	</div>
<c:forEach items="${reviewList}" var="reviewList" varStatus="status">
					<div class="text_box text-center">
						<div class="row_view col-lg-12 padding_none">
							<li class="col-lg-2 col-xs-1" ><span class="title" data-pk="${reviewList.board_pk}">${reviewList.board_pk}</li>
							<li class="col-lg-2 col-xs-2 " ><span class="title" data-pk="${reviewList.board_pk}">${reviewList.title}</li>
							<li class="col-lg-2 col-xs-3" ><span class="title" data-pk="${reviewList.board_pk}">${reviewList.writer}</li>
							<li class="col-lg-3  col-xs-3" ><span class="title" data-pk="${reviewList.board_pk}">
								<fmt:parseDate value="${reviewList.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
		                 		<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
		                 		<jsp:useBean id="toDay" class="java.util.Date" />
								<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
								<c:choose>
									<c:when test="${regisDate==today}">
										<fmt:formatDate value="${regis_date}" pattern="HH:mm"/> 
									</c:when>
									<c:otherwise>
										${regisDate}
									</c:otherwise>
								</c:choose>
								</span>
							</li>
						</div>	
					</div>
</c:forEach>
				</div>
				<button type="button" onClick="location.href='/user/counseling/userWrite3'" class="btn pull-right" style="margin-top:2%">write</button>
			</div>
			</form>
		</div>
	</div>
	<div id="paging" >
       			${nation.getNavigator()}
    </div>
	<article>
		<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
	</article>	

	<script>
		function review_detal(){
			
		}
		function listarticle(pageNumber){
	 	 	 $("#formId").attr("action","/common/support/review/"+pageNumber+"");
	  		 $("#pageNumber").val(pageNumber);
	  		 $("#formId").submit();
	  	}
		$('.title').click(function(){
	    	location.href="/common/support/review/detail/"+$(this).data('pk')+"";
	    });
	</script>
</body>
</html>