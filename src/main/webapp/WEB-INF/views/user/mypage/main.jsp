<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" charset="utf-8">
<title>Insert title here</title>

<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<script src="/resources/common/js/information.js"></script>
<!--css-->
<link rel="stylesheet" href="/resources/common/css/user-page.css"/>
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>


<div class="sub_menu_wrap col-lg-2  col-md-3 col-sm-12 col-xs-12 padding_none " style="clear:both;float:left">
	<div class="sub_menu_header col-lg-12 col-xs-12 ">
		<div  class="h50"></div>
		<div class="col-lg-6" style="color:#fff">프로필</div>
		<div class="my_info col-lg-6" style="color:#fff"  data-url="/user/myInfo">내정보</div>
		<div class="visible-sm-block visible-xs-block view_submenu pull-right" style="color:#fff;font-size:2vh">▼</div>
	</div>
	<div class="select_menu">
		<ul>
			<li data-url="/user/main ." data-eq="0">상표 출원 내역<b class="pull-right">&gt;</b></li>
			<li data-url="/common/markConsulting" data-eq="1"  >상표 상담 내역<b class="pull-right">&gt;</b></li>
			<!-- 
			<li data-url="/common/markHistory" data-eq="2">상표 출원조사 신청내역<b class="pull-right">&gt;</b></li>
			<li data-url="/common/mypageApply" data-eq="3">계좌내역<b class="pull-right">&gt;</b></li> -->
		</ul>
	</div>
	<div>
		<div class="height_box"></div>
	</div>
</div>

<div class="container-fluid col-md-9  col-lg-10 col-sm-12  col-xs-12 " >
	<div class="row col-lg-10 col-xs-12" style="margin-left:0px !important">
	<div>		
		<div class="col-lg-10 col-xs-12 col-lg-offset-1 content_wrap">
			<div class="col-lg-12 notice padding_none">
				<h2 class="col-lg-6 col-sm-3 col-xs-12 pull-left mypage-title padding_none">전체 알림</h2>
				<div class="btns_wrap  col-lg-6  col-sm-9 col-xs-12 padding-none pull-left padding_none ">
					<button class="col-md-3 col-sm-4 col-xs-5 pull-right">전체삭제</button>
					<button class=" col-md-3 col-sm-4 col-xs-5 pull-right ">읽은 알림 삭제</button>
				</div>	
				<div class="col-lg-12 col-xs-12 notice_board">
					<div class="notice_row ">
						<div>2017.05.05</div>
						<div>상표 등록이 완료되었습니다.</div>
						<div>
							<button>삭제</button>
						</div>
					</div>
					<div class="notice_row">
						<div>2017.05.05</div>
						<div>상표 등록이 완료되었습니다.</div>
						<div>
							<button>삭제</button>
						</div>
					</div>
					<div class="notice_row">
						<div>2017.05.05</div>
						<div>상표 등록이 완료되었습니다.</div>
						<div>
							<button>삭제</button>
						</div>
					</div>
					<div class="notice_row">
						<div>2017.05.05</div>
						<div>상표 등록이 완료되었습니다.</div>
						<div>
							<button>삭제</button>
						</div>
					</div>
				</div>
			</div>
				<!-- 상표 출원 내역-->
<form id="formId" action="/user/main">
	<input type="hidden" id="pageNumber" name="pageNumber">
<!-- 	<input type="hidden" id="searchWord" name="searchWord">
	<input type="hidden" id="searchAbout" name="searchAbout"> -->
	<input type="hidden" id="mark_pk" name="mark_pk">
	
			<div class="visible-xs-block height"></div>
			<div class="content_chang padding_none">
				<div class="col-lg-12 col-xs-12 padding_none board_wrap ">
					<div class="col-lg-12 padding_none">
						<h2 class=" col-lg-6 col-sm-5 col-xs-12 pull-left mypage-title">상표 출원 내역</h2>
						<div class="search_wrap col-lg-6  col-sm-6 col-xs-12 pull-right board padding_none">
							<div class="col-lg-12 col-sm-12 col-xs-12">
								<div class="h25"></div>
								<div class="input-group   col-lg-4  col-sm-6 col-xs-4 pull-left ">
									<select class="form-control select_box col-xs-12 " style="position:relative;z-index:0">
										<option class="if_applicant hide">담당 변리사</option>
										<option class="if_patent hide">출원인</option>
										<option>상표명</option>
									</select>
								</div>
								<div class="input-group search_wrap col-lg-6 col-sm-6 col-xs-7 pull-right" style="position:relative;z-index:0">
									  <input type="text" class=" form-control" id="exampleInputAmount" placeholder="내용을  입력해주세요">
									  <div class="input-group-addon search_btn" onclick="search();"></div>
								</div>
								<div class="h25"></div>
								
							</div>
						</div>
					</div>
					
					<div class="col-lg-12 table_wrap " style="padding:0px !important;">
						<div class="visible-xs-block height_box2"></div>
						<table class="table table_main">
							<tr>
								<th class="w5">No</th>
								<th class="w25">상표명</th>
								<th class="w35">
									<span class="if_applicant hide">담당 변리사</span>
									<span class="if_patent hide">출원인</span>
								</th>
								<th class="w25">업데이트 날짜</th>
								<th>상태</th>
							</tr>
<c:forEach var="markMap" items="${markList}" >
	<c:set var="markHistroVo" value="${markMap.markHistoryVo }"/>
							<tr>
								<td>${markMap.mark_pk}</td>
								<td><a onclick="mvMarkDetail('${markMap.mark_pk}')">${markHistroVo.name}</a></td>
								<td class="if_applicant hide">
									<c:choose>
										<c:when test="${markMap.patent_id==null}">
											--
										</c:when>
										<c:otherwise>
											<a class="partner" data-id="${markMap.patent_id}">
												${markMap.partner_name}
											</a>
										</c:otherwise>
									</c:choose>
								</td>
								<td class="if_patent hide"><a class="partner" data-id="${markMap.applicant_id}">${markMap.partner_name}</a></td>
								<td>
									${markMap.printLogtime}
								</td>
								<td>${markMap.step}</td>
							</tr>
</c:forEach>
						<!--if user have no log -->
							<c:if test="${fn:length(markList)==0}">
								<tr class="no-list">
									<td colspan='5'><b>상표 출원 내역이 없습니다.</b><p>마크맨에서 저렴한 비용으로 상표 출원을 진행해 보세요.</p>
									<span class="link-line" onclick="location.href='/applicant/mark/mvApply'">상표 출원 신청하기</span></td>
								</tr>
							</c:if>
						<!--if user have no log -->
						</table>
					</div>
				</div>							
				<div id="list_paging">
					${nation.getNavigator()}
				</div>
				<div class="col-lg-12 colsm-12 col-xs12 padding_none">
					<div class="col-lg-8 pull-left" style="color:#808797">
						상태<br>
						-미접수:줄원인과 변리사 간에 의견을 조율하는 중입니다.<br>
						-접수 완료 : 출원서 제출은 완료된 상태입니다.<br>
						-심사 완료 : 심사 결과가 나온 상태로,등록과 거절 두 가지 결과로 나뉩니다.거절시 담당변리사분과
						상담하여 주세요.<br>
						-등록 완료 : 특허청에 등록료납부까지 완료하여 상표 등록이 모두 완료된 상태입니다
					</div>
				</div>
			</div><!--change--->
		</form>	
		</div>
  </div>
</div>
</div>
<article style="clear:both;margin-top:150vh">
	<c:import url="/WEB-INF/views/import/user/footer.jsp" />
</article>	

</body>
<script src="/resources/common/js/information.js"></script>
<script>

$(document).on("click",".view_submenu",function(){
	var obj = $(".select_menu");
 	$(".select_menu").slideToggle();
	
 	
	
})



$(".my_info").click(function(){
	
	var url = $(this).data("url")
	$(".notice ").hide();
	$(".content_chang").load(url);
})


var id = "${sessionScope.currentUser.getId()}";	//sessionScope js에서 사용시 문자열로 읽힘 왜?
$(document).ready(function(){
	//페이지 네이션
	var role="${sessionScope.currentUser.getUser_type()}";//sessionScope알아두기!!
	if(role=="ROLE_APPLICANT"){
		$('.if_applicant').removeClass('hide');
	}else if(role=="ROLE_PATENT"){
		$('.if_patent').removeClass('hide');
	}
});
function listarticle(pageNumber){
	 $("#formId").attr("action","/user/main/"+pageNumber+"");
	 $("#pageNumber").val(pageNumber);
	 $("#formId").submit();
}
$(document).on('keydown','.search_wrap input', function(){
	if(window.event.keyCode==13){
		search();
		return false;
	}
})
function search(){
	var searchWord = $('#exampleInputAmount').val();
	var searchAbout =  $('.select_box').val();
	$('#searchWord').val(searchWord);
	$('#searchAbout').val(searchAbout);
	if(searchWord!=''){
		$('#formId').submit();
	}
}
//partner information modal
$(".partner").click(function(){
	var data = {};
	var id = $(this).data('id');
	data["id"] = id;
	$.ajax({
		url : "/user/getUserInfo",
		type : "GET",
		data : data,
		success : function(resultData){
			var user = resultData;
			setUserModal(user);
			$(".modals").show();
			$("html").css({"overflow-y":"scroll"});
			$()
		},
		error : function(request, status, error) {
	 	      alert("code:" + request.status + "\n" + "error:" + error);
	 	}
	})
});

$(".select_menu ul li").on("click",function(){
	localStorage.setItem('eq', $(this).data("eq"));
	if($(this).data("eq")=="0"){
			location.href="/user/main"
			$(".content_wrap2").hide();
			$(this).addClass("active").siblings("li").removeClass("active");
			
	}else{
		$(".notice ").show()
		var url =$(".select_menu ul li").eq(localStorage.getItem('eq')).data("url");
		$(".select_menu ul li").eq(localStorage.getItem('eq')).addClass("active").siblings("li").removeClass("active");
		$(".content_chang").load(url);
	}
});

var url =$(".select_menu ul li").eq(localStorage.getItem('eq')).data("url");
$(".select_menu ul li").eq(localStorage.getItem('eq')).addClass("active").siblings("li").removeClass("active");

if(localStorage.getItem('eq')=="0" || localStorage.getItem('eq')==null ){
	
	localStorage.setItem('eq','0');
	
}else{
	$(".content_chang").load(url);
}

$(".hidden_btn").click(function(){
	$(this).parents(".xs_box").find(".hidden_text").slideToggle();
});
function mvMarkDetail(pk){
	$("#mark_pk").val(pk);
	var action = "/user/mark/mvDetail";
	var formId = $("#formId");
	formId.attr("action",action);
	formId.attr("method","get");
	formId.submit();
}

</script>
</html>