<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!--css-->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/user-page.css"/>
<script src="/resources/common/js/main.js"></script>
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>

<section>
	<div class="col-lg-10  col-xs-12">
		<div class="container xs-padding">
			<div class="row">
				<div class="content5">
					<h2>내정보</h2>
					<table class="table table3 col-lg-12 col-xs-12 hidden-xs  ">
						<tr>
							<td>사용자 이름</td>
							<td>
								<div class="col-lg-12 infomation_box">
									<div>${userVo.name}</div>
									<div>
										실명정보(이름,생년월일,성별 개인 고유 식별 정보)가 변경된 본인 확인을 통해 정보를 수정하실 수 있습니다.
									</div>
									<div>
										<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
									</div>
								</div>
							</td>
						</tr>
						
						<tr>
							<td>휴대전화</td>
							<td>
								<div class="col-lg-12 infomation_box">
									<div id="cell_num">+82&nbsp
									</div>
									<div>
										아이디,비밀번호 찾기 등 본인확인이 필요한 경우 또는 유료 결제 등 마크맨으로부터 알림을 받을 때 사용할 휴대전화입니다.
									</div>
									<div>
										
										<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td>기본 이메일</td>
							<td>
								<div class="col-lg-12 infomation_box">
									<div>${userVo.email}</div>
									<div>
										이벤트 등 다양한  마크맨의 소식 및 알림을 받기 위해 사용할 메일주소 입니다.<br>
										<input type="checkbox">마크맨의 이벤트 등 프로모션 관련 안내 이메일을 수신하겠습니다.
									</div>
									<div>
										<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
									</div>
								</div>
							</td>
						</tr>
						<tr>
						<td>주소</td>
							<td>
								<div class="col-lg-12 infomation_box">
									<div>${userVo.addr1}&nbsp${userVo.addr3}&nbsp${userVo.addr3}&nbsp</div>
									<div>
										<!--멘트 -->
									</div>
									<div>
										<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
									</div>
								</div>
							</td>
						</tr>
					</table><!-- xs-->
					
					<div class="visible-xs-block">
						<div class="col-xs-12 name_box xs_box">
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								사용자 이름<span class="hidden_btn">▼</span>
								<div class="hidden_text">
									실명정보(이름,생년월일,성별 개인 고유 식별 정보)가 변경된 본인 확인을 통해 정보를 수정하실 수 있습니다.
								</div>
							</div>
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								배혜우 
								<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
							</div>
						</div>
						
						<div class="height_box2"></div>
							
						<div class="email_box xs_box">
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								휴대 전화<span class="hidden_btn">▼</span>
								<div class="hidden_text">
									아이디,비밀번호 찾기 등 본인확인이 필요한 경우 또는 유료 결제 등 마크맨으로부터 알림을 받을 때 사용할 휴대전화입니다.
								</div>
							</div>
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								0000
								<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
							</div>
								
						</div>
						
						
						
						
						<div class="height_box2"></div>
							
						<div class="phone_box xs_box">
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								기본 이메일<span class="hidden_btn">▼</span>
								<div class="hidden_text">
									이벤트 등 다양한 마크맨의 소식 및 알림을 받기 위해 사용할 메일주소 입니다.
									마크맨의 이벤트 등 프로모션 관련 안내 이메일을 수신하겠습니다.<input type="checkbox">
									
								</div>
							</div>
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								abc@abc
								<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
							</div>
								
						</div>	
						
						<div class="height_box2"></div>
						
						<div class="address_box xs_box">
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								주소<span class="hidden_btn">▼</span>
								<div class="hidden_text">
									
								</div>
							</div>
							<div  class="h50 col-xs-12">
								<div class="h25"></div>
								주소
								<button class="w13 myinfo_modify_btn" onClick="modify()">수정</button>
							</div>
								
						</div>
							
					</div>
					<!--  -->
				</div>
			</div>
		</div>
	</div>
</section>
</body>
<script src="/resources/common/js/information.js"></script>
<script>
	$(document).ready(function(){
		var cell = document.getElementById("cell_num");
		var cell_num = "${userVo.cell_num}";
		if(cell_num.length==13){
			cell_num = cell_num.replace(/(\d{2})(\d{4})(\d{4})/,'$1-****-$3');
		}
		cell.append(cell_num+"::");
		
			$(".hidden_btn").click(function(){
				$(this).parents(".xs_box").find(".hidden_text").slideToggle();
			})
		
	});
	
	
	
	
</script>
</html>