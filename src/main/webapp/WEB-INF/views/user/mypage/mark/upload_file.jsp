<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/resources/common/css/dtl.css">
<script src="/resources/common/js/dtl.js"></script>
<title>Insert title here</title>
</head>
<body style="overflow-x: initial !important;">
<style>
	.height{width:100%;height:6vh}
</style>
<div class="content_box_wrap" style="clear:both">
<form id="formId" enctype="multipart/form-data">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" id="${mark_pk}" value="${mark_pk }">
<input type="hidden" id="upload_page_name" name="upload_page_name" value="mark"/>
		<div class="height_box2"></div>
		<div class="text_box_wrap" >
				<div class="col-lg-8 col-lg-offset-2 col-xs-10 col-xs-offset-1" style="padding-top:5%">
					<div class="col-lg-12">

						<div class="col-lg-6 col-lg-offset-3 col-xs-12 text-center font_boxs">
							*이미지 용량은 <span>8MB</span>입니다.<br>
							 업로드 가능한 확장자는 <span> JPG&PNG</span>입니다.<br>

						</div>
						<div class="height_box3"></div>
<c:forEach var="markFileVo" items="${markVo.markFileVoList }">
	<c:choose>
		<c:when test="${markFileVo.file_type == 1}">
			<c:set var="file_type_name" value="seal"> </c:set>
			<c:set var="file_type_name_kor" value="위임장"> </c:set>
		</c:when>
		<c:when test="${markFileVo.file_type == 2}">
			<c:set var="file_type_name" value="entrust"> </c:set>
			<c:set var="file_type_name_kor" value="인감증명서"> </c:set>
		</c:when>
		<c:when test="${markFileVo.file_type == 3}">
			<c:set var="file_type_name" value="jumin"> </c:set>
			<c:set var="file_type_name_kor" value="주민등록등본"> </c:set>
		</c:when>
	</c:choose>
						<div class="visible-xs-block visible-sm-block height"></div>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12  upload_wrap">
							<div class="col-lg-12 col-xs-12">
								<div class="col-lg-4 col-md-6 col-xs-6">제출 완료</div>
								<b>${file_type_name_kor }</b>
							</div>
							<!-- content -->
							<div class="col-lg-12 top_wrap">
								<div class="col-lg-12 img_border_box" data-index="1">
									<span class="btn btn-default btn-file col-lg-12 " >
    									<input type="file" class="input_imgs" name="${file_type_name }" data-imgtype="${file_type_name }" >
    									<img class="img_files" src='${markFileVo.file_url }' alt="upload_img">
									</span>
								</div>
								<!-- btn -->
								<div class="btn_upload_wrap">
									<div class="height_box"></div>
									<div class="col-lg-6 col-md-12 ">
									<div class="btn btn-defaults" style="width:100% !important;padding-top:0.5%">
										<input type="file" class="input_imgs_modify col-lg-12"/>
										<p style="vertical-align:top">수정</p>
									</div>
									</div>
									<div class="height_box hidden-lg "></div>
									<div class="col-lg-6  col-sm-12 col-xs-12 ">
										<button class="col-lg-12 col-xs-12 btn_download" type="button" style="border-radius:5px" onclick="fileDownload('${markFileVo.file_url }','${markFileVo.file_name}')" >다운로드</button>
									</div>
								</div>
							</div>
						</div>
</c:forEach>

<c:forEach var ="file_type_name" items="${userUploadStatusMap.noUploadList }">
	<c:choose>
		<c:when test="${file_type_name eq 'entrust'}">
			<c:set var="file_type_name_kor" value="인감증명서"> </c:set>
		</c:when>
		<c:when test="${file_type_name eq 'jumin'}">
			<c:set var="file_type_name_kor" value="주민등록등본"> </c:set>
		</c:when>
		<c:when test="${file_type_name eq 'seal'}">
			<c:set var="file_type_name_kor" value="위임장"> </c:set>
		</c:when>
	</c:choose>
						<div class="height visible-xs-block visible-sm-block " style="clear:both"></div>
						<div class="col-lg-4  col-xs-12 not_upload_wrap">
							<div class="col-lg-12 col-xs-12">
								<div class="col-lg-4  col-xs-6">미제출</div>
								<b>${file_type_name_kor }</b>
							</div>
							<div class="col-lg-12">
								<div class="col-lg-12 padding-none img_wrap_change_box" style="position:relative;">
										<img src='/resources/image/upload-document.png' class='col-lg-12 col-xs-12 padding-none img_re' style="height:105%">
										<div class="img_result_wrap" style="position:absolute;">
											<input type="file" class="input_imgs_2" name="${file_type_name }" data-imgtype="${file_type_name }">
										</div>
								</div>
							</div>
						</div>
</c:forEach>
					</div>
				</div>
		</div>
<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
<script>

var file_data;
var obj = $(".img_border_box");
var index;
var files_result = [];
var img_html;
var img_name;



$(document).ready(function(){
	var markFileTypeNumberArray = $("#markFileTypeNumberArray").val();

	$(".sub_drop button").click(function(){
		$(this).addClass("active_btn").siblings("button").removeClass("active_btn");
		$(".text_box_wrap").eq($(this).index()).css({"display":"block"}).siblings(".text_box_wrap").css({"display":"none"});
	})
}) //dom
function ajaxFileUpload(type){
	var mark_pk = $("#mark_pk").val();
	var element = $("#upload_file_type_name");
	var form = $('form')[1];
	var formData = new FormData(form);
	formData.append("imgElementName",type);
	formData.append("uploadPageName","mark");
	formData.append("mark_pk",mark_pk);
	$.ajax({
        url: '/ajax/fileupload',
        processData: false,
        contentType: false,
        data: formData,
        type: 'POST',
        success: function(result){
            alert(result);
        }
    });
}

$(".input_imgs_2").on("change",function(e){
	var files  = e.target.files[0];
	var imgtype = $(this).data("imgtype");
	var obj  = $(this).parents(".img_wrap_change_box").find(".img_re");
	var reader = new FileReader();
	reader.onload = function(e){
		console.log(obj);
		obj.attr('src',e.target.result);
	}
	reader.readAsDataURL(files);
	ajaxFileUpload(imgtype);
});

$(".input_imgs").on("change",function(e){
	var text=  $(this).parents(".upload_wrap").find("b").text();
	index = $(this).parents(".img_border_box").data("index");
	var img_type = $(this).data("imgtype");
	$(".upload_wrap").find(".img_border_box").each(function(idx,item){
		if(idx == index){
			obj = item;
		}
	});
	file_data = e.target.files[0];
	files_result.push(file_data);
	img_name = files_result[index].name;
	var reader = new FileReader();

	reader.onload = function(e){
 		img_html = "<div class='img_wrap_li' ><img  class='img_files'  src='"+e.target.result+"'><div class='btn_img_wrap'><button onClick='img_file_del("+index+")'>x</button></div></li>";
		obj.innerHTML = img_html;
	}
	reader.readAsDataURL(file_data);
	ajaxFileUpload(img_type);
});

$(document).on("click",".btn_img_wrap",function(){
	$(this).parents(".img_wrap_li").remove();
});

function img_file_del(num){
	delete files_result[num];
	console.log(files_result)
}
// 수정 input_imgs
$(".input_imgs_modify").on("change",function(e){
	var obj  = $(this).parents(".top_wrap").find(".img_files");

	var imgtype = $(this).data("imgtype");

	var files  = e.target.files[0];
	var reader = new FileReader();
	reader.onload = function(e){
		obj.attr('src',e.target.result);
	}
	reader.readAsDataURL(files);
	//ajaxFileUpload(imgtype);
});

function fileDownload(fileFullPath, fileOriginName){
	location.href="/call/callDownload.do?fileFullPath="+fileFullPath+"&fileOriginName="+fileOriginName;
}

</script>
</body>
</html>