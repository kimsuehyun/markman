<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>Insert title here</title>
<c:import url="/WEB-INF/views/import/header.jsp"/>

<link rel="stylesheet" href="/resources/common/css/dtl.css"/>
<script src="/resources/common/js/dtl.js"></script>

</head>
<body>
<style>
	.selected{background:#21b6de}
	.an-selected{background:rgba(0,0,0,0)}
</style>
<c:set var="markHistoryVo" value="${markVo.markHistoryVoList.get(0) }" />
<input type="hidden" id="markHistoryLength" value="${fn:length(markVo.markHistoryVoList)}">

<form id="formId">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" id="mark_pk" name="mark_pk" value="${markVo.mark_pk }">
<c:if test="${fn:length(markVo.markHistoryVoList) > 0}">
	<c:set var="preMarkHistoryVo" value="${markVo.markHistoryVoList.get(0) }" />
	<input type="hidden" id="mark_history_pk" name="mark_history_pk" value="${preMarkHistoryVo.mark_history_pk }">
</c:if>
<input type="hidden" id="regis_posibility" name="regis_posibility">

   <!--menu1-->
    <div id="home" class="tab-pane fade in active ">
      	<div class="col-lg-12 col-xs-12 info_wrap">
      		<div class="col-lg-6 col-lg-offset-3 ">
      			<!--img wrap-->
      			<div class="col-lg-12 col-sm-12">
      				<div class="col-lg-12" style="height:89%"></div>
      				<img src="/resources/image/mark-ribbon.png" alt="리본" style="width:5%">
      			</div>
      			<div class="col-lg-12 col-sm-12 f1 f2">
      				<div class="col-lg-10 col-sm-10 col-sm-offset-1 col-lg-offset-1 info_content">
      					<div class="info_content_title col-lg-12 ">
      						<h3 class="col-xs-12">상표 정보</h3>
      						<c:set var="userType" value="${sessionScope.currentUser.getUser_type()}"/>
      					</div>
						<!--table-->
						<table class="col-lg-12 col-sm-12 col-xs-12 table1">
							<tr>
								<td class="col-sm-3 col-xs-5">상표 유형</td>
								<td><b>|</b>${markHistoryVo.type}</td>
							</tr>
							<tr>
								<td class="col-sm-3 col-xs-5">상표명</td>
								<td><b>|</b>${markHistoryVo.name}</td>
							</tr>

							<tr>
								<td class="col-sm-3 col-xs-4">사용처</td>
								<td><b>|</b>${markHistoryVo.where_use}</td>
							</tr>
							<tr>
								<td class="col-sm-3 col-xs-5">상표 분류</td>
								<td><b>|</b>${markHistoryVo.category}
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
								<b style="visibility:hidden;">|</b>${markHistoryVo.select_content}
								</td>
							</tr>
							<tr>
								<td class="col-sm-3 col-xs-3 ">첨부파일</td>
								<td><b>|</b><a class="attach-file" data-url="${markAttachFile.file_url}">${markAttachFile.file_name}</a>
								</td>
							</tr>
						</table>
						<div class="precontent">
							<div style="display:none;">
								<span class="regis-date">
									<fmt:parseDate value="${markVo.mark_regisDate}" var="regisDate" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:formatDate value="${regisDate}" var="regis_date" pattern="yyyy-MM-dd"/>
									${regis_date} 작성됨
								</span>
							<table class="col-lg-12 col-xs-12 col-sm-12 table1">
								<tr>
									<td class="col-sm-3">상표 유형</td>
									<td><b>|</b>${markHistoryVo.type}</td>
								</tr>
								<tr>
									<td class="col-sm-3">상표명</td>
									<td><b>|</b>${markHistoryVo.name}</td>
								</tr>

								<tr>
									<td class="col-sm-3 col-xs-5">사용처</td>
									<td><b>|</b>${markHistoryVo.where_use}</td>
								</tr>
								<tr>
									<td class="col-sm-3 col-xs-5">상표 분류</td>
									<td><b>|</b>${markHistoryVo.category}
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
									<b style="visibility:hidden;">|</b>${markHistoryVo.select_content}
									</td>
								</tr>
								<tr>
									<td class="col-sm-3 col-xs-5">첨부파일</td>
									<td><b>|</b><a class="attach-file" data-url="${prevMarkAttachFile.file_url}">${prevMarkAttachFile.file_name}</a></td>
									<td><b>|</b>첨부파일 없습</td>
								</tr>
							</table>
							</div>
						</div>
						<button type="button" class="view-precontent">이전 작성 내용 보기 <p>▼</p></button>
						<!--content-->
						<div>
							<div class="col-lg-12 col-sm-12 title_box2 f1 f2">
								<div class="info_content_title col-lg-12">
									<h3>변리사 의견</h3>
								</div>
								<h6 style="clear:both">의견</h6>
								<!--content_wrap-->
								<div class="content_wrap ">
								<textarea name="patent_feedback" class="col-lg-12 col-xs-12 col-sm-12 text_input">${markHistoryVo.patent_feedback}</textarea>
								<button type="button" class="view-precontent">이전 작성 내용 보기 <p>▼</p></button>
									<div class="height_box2"></div>
									<h6 style="clear: both">의견 제출 가능성</h6>
									<!--progress-->
									<div class="progress_wrap f1 f2 col-lg-12 col-sm-12">
										<div class="f1 col-lg-1 col-xs-1 progress_text ">
											<b class="progress_text_b">${markHistoryVo.regis_posibility}</b><b>%</b>
										</div>
										<div class="f1 f2 progress_bar pull-right  col-lg-11 col-xs-10 col-sm-11" style="height:100%;">
											<div class=" f1 f2 progress_color col-lg-12 col-sm-12">
<c:forEach begin="10" end="100" step="10" varStatus="status">
	<div data-int="${status.index}"></div>
</c:forEach>
											</div>
										</div>
									</div>
									<div class="height_box2"></div>
									<div class="col-lg-12 col-sm-12 text_info">
										-의견제출 가능성:심사과정에서 등록을 받을 수 없는 사유가 발견된 경우,심사관은 이를 출원인에게 통보하고
										기간을 정하여 의견서 및/또는 보청서를 제출할 기회를 부여하는데, 이를 [의견제출통지서]라 합니다.<br>
										<b>따라서,의견 제출 가능성이 낮을수록 등록 가능성이 높습니다.</b>
									</div>
								</div>

								<div class="height_box"></div>

								<div class="button_wrap col-lg-6 col-sm-6 col-lg-offset-4">
									<div class="height_box"></div>
									<button type="button" onclick="regisOk()" class="col-lg-4 col-xs-5">출원접수</button>
									<button type="button" onclick="modifyOk()" class="col-lg-4 col-xs-5  col-xs-offset-1 col-lg-offset-1"><img src="/resources/image/pencil.png" alt="edit" class="icon25">수정</button>
									<button type="button" onclick="location.reload()" class="col-lg-4 col-xs-5">초기화</button>
								</div>
								<div class="col-xs-12 visible-xs-block height_box">
								</div>
								<!--content_wrap-->
							</div>
     					</div>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</form>
    <article style="margin:0 auto !important;padding:0 !important">
		<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
	</article>
<script>
function modifyOk(){
	var options = {
	        success      : ajaxAfter  // ajaxSubmit 후처리 함수
	};
	var formId = $("#formId");
	formId.attr("method","POST");
	formId.attr("action","/patent/mark/modifyOk");
	if(confirm("수정 하시겠습니까?")){
		$("#regis_posibility").val($(".progress_text_b").text());
		formId.ajaxSubmit(options);
	}
}
function ajaxAfter(resultData, statusText, xhr, $form){
	if (statusText == "success"){
		alert(resultData);
		location.reload();
	}else{
		alert("수정에 실패했습니다.");
	}
}
function regisOk(){
	if(confirm("출원 접수 하시겠습니까?")){

	}
	var formId = $("#formId");
	formId.attr("method","post");
	formId.attr("action","");
// 	formId.submit();

}

var per = $(".progress_text_b").text();
printProgress(per);


function printProgress(index){
	if(index%10=="0"){
		var all = parseInt(index)*String(0.1);
		var sum   = parseInt(all);
		for (var i = 0 ; i <10;i++){
			if(i <sum){
				$(".progress_color > div:eq('"+i+"')").addClass("selected").removeClass("an-selected");
			}else{
				$(".progress_color > div:eq('"+i+"')").addClass("selected").removeClass("selected");
			}
		}
	}else{

	}
}

$(".progress_color > div").click(function(){
	var intValue =$(this).data("int");
	printProgress(intValue);
	removeClassName = "an-selected";
	addClassName = "selected";
	$(".progress_text_b").text(intValue);
});


$('.view-precontent').click(function(){
	var str = ""
	var div = $('.precontent').children('div');
	var prevMarkPk = $("#markHistoryLength").val();
	if(prevMarkPk < 2){
		alert("이전 작성 내용이 없습니다.");
	}else{
		if($(this).hasClass('view')){
			$(this).removeClass('view');
			div.slideUp(500);
			str = "이전 작성 내용 보기<p>▼</p>"
		}else{
			$(this).addClass('view');
			div.slideDown(500);
			str = "<p>▲</p>접기"
		}
		$(this).empty();
		$(this).append(str);
	}
})
$('.attach-file').click(function(){//첨부파일 다운로드
	var fileFullPath = $(this).data('url');
	var fileOriginName = $(this).html();
	location.href="/call/callDownload.do?fileFullPath="+fileFullPath+"&fileOriginName="+fileOriginName;
});
</script>
<script src="http://malsup.github.com/jquery.form.js"></script>
</body>
</html>