<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<c:import url="/WEB-INF/views/import/header.jsp"/>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!--css-->
<link rel="stylesheet" href="/resources/common/css/dtl.css"/>
<script src="/resources/common/js/information.js"></script>
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<div class="col-lg-12 goods_title">
    <div class="col-lg-2 col-sm-2 col-lg-offset-5 col-sm-offset-5" >
    	<div class="col-lg-6 col-sm-6 col-lg-offset-3 col-sm-offset-3 text-center"></div>
    </div>
</div>
<div class="col-lg-12" style="clear: both;padding-left:0;padding-right:0">
<input type="hidden" id="mark_pk" name="mark_pk" value="${mark_pk}">
  <ul class="nav nav-tabs">
    <li class="active first_menu"><a data-toggle="tab" data-url="mark_info">출원정보</a></li>
    <li><a data-toggle="tab" data-url="upload_management" >서류관리</a></li>
  </ul>
  <div class="tab-content col-lg-12 tab_wrap" style="padding:0 !important">
  </div>
</div>
<script>
	var url  = document.location.href;
	var mark_pk = $('#mark_pk').val();
	if(url.indexOf("mvDetail")>-1){
		$(".tab_wrap").load("/user/mark/mvMarkInfo?mark_pk="+mark_pk);
	}
	$(".nav-tabs li").click(function(){
		url =  $(this).find("a").data("url");
		var mark_pk = $("#mark_pk").val();
		if(url == "upload_management"){
			url = "/user/mark/uploadFileList?mark_pk="+mark_pk;
		}else if(url == "mark_info"){
			url = "/user/mark/mvMarkInfo?mark_pk="+mark_pk;
		}
		$(".tab_wrap").load(url);
		$(this).addClass("active").siblings("li").removeClass("active");
	});
</script>
</body>
</html>