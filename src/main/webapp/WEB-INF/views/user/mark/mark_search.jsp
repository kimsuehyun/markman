<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/mark_apply.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="../resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<style>
	*{margin:0 auto ;padding:0;list-style: none;text-decoration:none;box-sizing: border-box;font-family:"맑은 고딕";}
	
	/*option*/
	.padding_none{padding:0px !important}
	.padding_left{padding-left:0px !important}
	.h50{height:50%}
	.h25{height:25%}
	.h20{height:20%}
	.h60{height:60%}
	.h100{height:100%}
	.h33{height:30%}
	.h35{height:35%}
	.h40{height:40%}
	.h80{height:80%}
	.h90{height:90%}
	.h70{height:70%}
	.h15{height:15%}
	.height_box{height:3vh;clear:both}
	.h45{height:45%}
	.w100{width:100%}
	.w30{width:30%}
	.w70{width:70%}
	.w50{width:50%}
	.search_input_page{border:2px solid #0078bf !important;border:none;border-radius:0px !important}
	.searh_page_btn{background:#0078bf;color:#fff}
	.search_page_wrap{height:45vh;background:#21b6de}
	.search_page_f1{text-align: left;color:#fff}
	.search_page_category_box{background:#21b6de;margin-top:2%}
	.search_page_category_box > div > button{float:left;width:18%;border:1px solid #fff;margin-left:2%;text-align: center;height:50%;background: rgba(0,0,0,0);color:#fff}
	.remote_wrap{position:fixed;border:1px solid #000;right:0%;}
	.search_result_wrap{padding-top:1%;width:80%}
	.line_search_page{border-top:1px solid #000}
	.remote_box{border:1px solid #000;position:fixed;right:0%;width:8%;top:45%;height:45vh;background:#fff}
	.search_result_box > div{height:100%;;float:left}
	.process_step{width:20%;float:left}
	.progress_text{margin-top:3%}
	.progress_name{width:80%;font-size:1vw;text-align:left}
	.content_info_search{marign-top:2%;}
	.content_info_search > div{font-size:1vw}
	.search_result_name{font-size:1vw;float:left;padding:0px !important}
	.progress_info{width:80%;height:30vh;background:#f4f5f9;border-radius:5px;border:1px solid #0078bf}
	.img_progress{width:8%}
	.progress_info_text{font-size:0.9vw;width:90%;float:right}
	.top_text{font-size:1.6vw}
	.top_btn{width:30%}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 search_page_wrap">
			<div class="col-lg-6 col-lg-offset-3 text-center h50">
				<div class="h25"></div>
				<span>상표검색이 어려운 분을 위한</span><br>
				<span class="title_f1_sesarh_page"><h2><b>쉽게 구성된 검색서비스입니다.</b></h2></span><br>
				<div class="col-lg-12 h25">
					<form  class="h100">
						<input type="text" class=" search_input_page col-lg-11 input-lg col-xs-10 h100">
						<button class="searh_page_btn col-lg-1 col-md-1 col-xs-2 padding_none h100">aaa</button>
						<div class="col-lg-12 search_page_f1 padding_none">
							출원하고자 하는 상표명/제춤군을 입력하시면 됩니다.
							예)마크맨,출원서비스
						</div>
						
					</form>
				</div>
			</div><!--col-lg-6-->
			<div class="col-lg-8 col-lg-offset-2 search_page_category_box h40">
				<div class="search_page_category_row h50">
					<button class="category_btn c_btn1">카테고리1</button>
					<button class="category_btn c_btn2">카테고리2</button>
					<button class="category_btn c_btn3">카테고리3</button>
					<button class="category_btn c_btn4">카테고리4</button>
					<button class="category_btn c_btn5">카테고리5</button>
				</div>
				<div class="search_page_category_row h50">
					<button class="category_btn c_btn6">카테고리6</button>
					<button class="category_btn c_btn7">카테고리7</button>
					<button class="category_btn c_btn8">카테고리8</button>
					<button class="category_btn c_btn9">카테고리9</button>
					<button class="category_btn c_btn10">카테고리10</button>
				</div>
			</div><!--lg-8-->
		</div>
	</div>
	<div class="row">
		<div class="search_result_wrap " style="height:80vh">
			<div class="col-lg-12 search_result_text">검색결과<span>108</span>건</div>
			<div class="height_box"></div>
			<div class="line_search_page"></div>
			<div class="col-lg-12  search_result_box h50 w100 padding_none">
				<div class="search_result_wrap">
					<div class="col-lg-4 h100 col-xs-10 col-lg-offset-1"><!--result_wrap-->
						<!--img-->
						<div class="h60" style="border:1px solid #000"></div>
						<!--img-->
						<div class="h40">
							<div class="col-lg-12 progress_text padding_none">
								<img src="/resources/image/icon_state_1.png" alt="등록" class="col-lg-3 col-xs-3">
								<div class="col-lg-6">마크맨test title</div>
							</div>
							<div class="col-lg-12 content_info_search padding_none">
								<div class="col-lg-12 padding_none">
									[09]컴퓨터/통신기기/SW
								</div>
							</div>
							<div class="search_result_name col-lg-12">
								이기상
							</div>
							<div class="col-lg-12 apply_date padding_none">출원일<span>2016.11.29</span></div>
							<div class="col-lg-12 registration_date padding_none ">등록일<span>2017.03.15</span></div>
						</div><!--text-->
					</div><!--result-->
				</div>
			</div><!--line1-->
			<div class="col-lg-12  search_result_box h50 w100 padding_none">
				
			</div>
			<div class="line_search_page col-lg-12"></div>
		</div>
		<!--리모콘-->
		<div class="remote_box">
			<div class="col-lg-10 col-lg-offset-1 h100 padding_left">
				<div class="col-lg-12 remote_content h33 " >
					<img src="/resources/image/icon_inquire.png" class="h80">
					<div class="h20 text-center ">
						상표검색이<br>
						필요한 이유
					</div>
				</div>
				<div class="col-lg-12 remote_content h33" >
					<img src="/resources/image/icon_method.png" class="h80">
					<div class="h20 text-center col-lg-offset-1">
						이용 방법
					</div>
				</div>
				<div class="col-lg-12 remote_content h33" >
					<img src="/resources/image/icon_tm.png" class="h80"  alt="A">
					<div class="h20 text-center col-lg-offset-1">
						문의 하기
					</div>
				</div>
			</div>
			<div class="back_to_top col-lg-12 padding_none">
				<br>
				<img src="/resources/image/icon_up.png" alt="top" class=" top_btn pull-left"> 
				<div class="col-lg-3 top_text">TOP</div>
			</div>
		</div>
	</div>
	<div class="height_box"></div>
	<div class="row">
		<div class="progress_info">
			<div class="w50 pull-left h100">
				<div class="height_box"></div>
				<div class="progress_row col-lg-12 h20">
					<img src="/resources/image//icon_state_1.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						상표를 등록받기 위해 특허청에 서류 제출을 하여 아직 심사가 진행중인 상태.
					</div>
				</div>
				<div class="progress_row col-lg-12 h20">
					<img src="/resources/image//icon_state_2.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						출원 이후에 특허청 심사가 종료된 상태.
					</div>
				</div>
				<div class="progress_row col-lg-12 h20">
					<img src="/resources/image//icon_state_3.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						심사결과 등록요건에 적합하여 등록된 상태<br>
						(단,3개월간 이의신청이 없을 경우에 한함)
					</div>
				</div>
				<div class="progress_row col-lg-12 h35">
					<img src="/resources/image//icon_state_4.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						상표 심사과정에서 등록요건을 만족하지 못한 상태.
						(단, 특허청으로 부터 의견 제출통지서를 받은 지 1개월 이내로,<br>
						의견제출서를 송부하여 여 심사결과에 대한 내용을 뒤엎을 수 있음)
					</div>
				</div>
			</div>
			<div class="w50 pull-right h100">
				<div class="height_box"></div>
				<div class="progress_row col-lg-12 h15">
					<img src="/resources/image//icon_state_5.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						상표가 등록된 후에 존속기간이 만료어 어권리가 사라진 상태.
					</div>
				</div>
				<div class="progress_row col-lg-12 h15">
					<img src="/resources/image//icon_state_6.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						특정사유로 인해 권리나 행위가 무효된 상태.
					</div>
				</div>
				<div class="progress_row col-lg-12 h15">
					<img src="/resources/image//icon_state_7.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						상표가 등록되기 전에 여러 사유로 인하여 출원이 취소된 상태.
					</div>
				</div>
				<div class="progress_row col-lg-12 h15">
					<img src="/resources/image//icon_state_8.png" alt="progress" class="img_progress">
					<div class="progress_info_text">
						<div class="h20"></div>
						출원이니 포기서를 제출하거나 등록료 불납등으로 권리를 포기한 상태.
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(".back_to_top").click(function(){
		$("body").animate({"scrollTop":"0"},800);
	})
</script>
</body>
</html>