<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<c:import url="/WEB-INF/views/import/header.jsp"></c:import>
<title>Insert title here</title>
</head>
<body>
<div class="col-lg-12 col-xs-12" style="margin-top:15%">
	<div class="text-center" >
		<h2>상표출원이 완료되었습니다.</h2><br>
		<a onclick="loginOutFunction('마이페이지')" style="text-decoration:underline ;">마이페이지 확인하기</a>
	</div>	
</div>
<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
</body>
</html>