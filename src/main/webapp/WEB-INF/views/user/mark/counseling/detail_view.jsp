<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/list_view.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body style="overflow-x:visible !important">
<form id="formId" method="POST">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" id="board_pk" name="board_pk" value="${qnaVo.board_pk }">
<style>
	.table_box{border:1px solid #ddd;width:100%;height:60vh}
	.text_table_title{width:100%;height:10vh;border-bottom:1px solid #ddd;padding:1%}
	.text_table_title > div:first-child{height:100%}
	.text_table_title > div:first-child > div{height:100%;background:#0e4383;float:left;color:#fff;border-radius:10px;text-align: center;font-size:5vh;}
	.text_table_title > div:nth-child(2){height:100%}
	.text_table_title > div:nth-child(2) > div{height:50%;}
	.text_table_title > div:nth-child(3){text-align:right}
	.text_content{padding:1.5%}
	.view_text_imgs{width:30%;height:24%;float:left}
	.view_text_imgs  img{width:100%;height:100%}
	
	.footer_view{width:100%;height:8vh;border:1px solid #ddd;padding:1.2%}
	.footer_view > button{float:right;margin-right:1%}
	
	
</style>
<div class="continer">
	<div class="row">
			<div class="height_box"></div>
			<div  class="col-lg-8 col-lg-offset-2 col-xs-12 " >
				<h3>상표상담</h3>
				<div class="table_box">
					<div class="text_table_title">
						<div class="col-lg-1 col-xs-2" style="padding:0px !important">
							<div class="col-xs-12">
								Q
							</div>
						</div>
						<div class="col-lg-5 col-xs-6">
							<div class="col-lg-12 col-xs-12">${qnaVo.title }</div>
							<div class="col-lg-6 col-xs-6">${qnaVo.writer}</div>
							<div class="col-lg-6 col-xs-6">
								<!--dateFormat-->
							    <fmt:parseDate value="${qnaVo.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
							    <fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
							    <jsp:useBean id="toDay" class="java.util.Date" />
								<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
								<c:choose>
									<c:when test="${regisDate==today}">
										<td><fmt:formatDate value="${regis_date}" pattern="HH:mm"/></td>
									</c:when>
									<c:otherwise>
										<td>${regisDate}</td>
									</c:otherwise>
								</c:choose>	
							</div>
						</div>
						<div class="col-lg-6 col-xs-4" style="color:red">
							조회수[${qnaVo.hits }]
						</div>
					</div>
					<div class="text_content">
						<div >
							
						</div>	
						<div class="view_text_imgs">
								${qnaVo.content }
							
						</div>
					</div>
				</div>
				<div class="footer_view">
						<div class="col-xs-12 pull-right" style="text-align:right">
							<c:if test="${sessionScope.currentUser.getId()==qnaVo.writer }">
								<button class="btn btn-success  ">수정</button>
								<button type='button' class="btn btn-danger ">삭제</button>
							</c:if>	
								<button class="btn btn-primary" onclick="mvPage('counseling')">목록으로</button>
							</div>
					</div>
					<br>
					<!-- <form class="form-horizontal">
						<div class="form-group">
							<label for="userid" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" placeholder="email" >
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">댓글내용</label>
							<div class="col-sm-10">
								<textarea class="form-control" rows="3"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-default">댓글저장</button>
							</div>
						</div>
					</form>

				<div class="commentlist">
					<div class="comment">
						<h3>홍길동 hongkildong@hongkildong.com 2017-03-08 10:15:09</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi numquam possimus, voluptate itaque temporibus corrupti nemo nostrum maxime hic veritatis nobis, illo beatae excepturi, voluptatem libero error vero aut enim. 
						<a href="#" class="btn btn-xs btn-danger">삭제</a></p>
						자기가 쓴 댓글
					</div>
					<div class="comment">
						<h3>김영철 kimyc@domain.com 2017-03-07 09:30:23</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi numquam possimus, voluptate itaque temporibus corrupti nemo nostrum maxime hic veritatis nobis, illo beatae excepturi, voluptatem libero error vero aut enim.</p>
					</div>			
					<div class="comment">
						<h3>박철수 cspark@test.com 2017-03-06 15:23:34</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi numquam possimus, voluptate itaque temporibus corrupti nemo nostrum maxime hic veritatis nobis, illo beatae excepturi, voluptatem libero error vero aut enim.</p>
					</div>	
				</div> 
			</div> -->
	</div>
</div>
</form>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>

</form>
</body>
<script src ="/resources/common/js/global.js"></script>
<script>
var form = $('#formId');
$('.btn-success').click(function(){
	form.attr("action","/user/counseling/mvEdit");
	form.submit();
});
$('.btn-danger').click(function(){
	var data = {};
	data["board_pk"] = $('#board_pk').val();
	confirmm("해당 게시물을 삭제하시겠습니까?").then(function(){
  		$.ajax({
  			url : "/user/counseling/deleteOk",
			type : "POST",
			data : data,
			success : function(resultData) {
				if(resultData=="success"){
					alertt("삭제가 완료되었습니다.","success").then(function(){
						location.href="/common/mark/counseling";
					});
				}else{
					alertt("삭제 실패");
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\n" + "error:" + error);
			}
		});  
	},function(){});
});
</script>
</html>