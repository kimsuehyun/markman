<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->
<link rel="stylesheet" href="/resources/common/css/counseling.css"/>
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body style="overflow-x:visible !important">
<div class="container">
	<div class="row">
<form id="formId2" method="POST">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" id="pageNum" name="pageNum">
<input type="hidden" id="pk" name="pk">
		<div class="col-lg-12  col-xs-12" style="margin-top:5%">
			<div class="col-lg-12 col-xs-12 sub_menu">
				<div class="h45 col-lg-12 col-xs-6">
					<div class="col-xs-12 h100" style="border:1px solid #ddd" >
						<div class="h25"></div>
						<h4 class="text-center">마크맨 상담 센터</h4>
						<div class="col-lg-12 col-xs-12 text-center padding_none">
							<br>
							<p class="hidden-xs">ideaconcert@ideaconcert.com</p>
							<p class="hidden-xs">평일 09:00~18:00 / 주말,공휴일 휴무</p>
							
						</div>
					</div>
				</div>
				<div  class="height_sub visible-lg-block"></div>
				<div class="col-lg-4 col-xs-6  sub_menu_mini text-center h45" onclick="location.href='/common/noticeList'">
					<div class="col-xs-12 padding_none h100">
						<div class="h20"></div>
						<div class="col-xs-6 col-xs-offset-3 padding_none">
							<div class="icon_make  col-xs-12 col-lg-offset-4 col-md-offset-4 col-xs-offset-2  text-center">!</div>
						</div>
						<div class="col-xs-12 marign_none"><h5>공지사항</h5></div>
						<div class="col-xs-12 marign_none hidden-xs "><h5>마크맨 공지사항</h5></div>
					</div>
				</div>
				<div  class="height_sub hidden-lg "></div>
				<div class="col-lg-4 col-xs-6 sub_menu_mini text-center h45" onclick="location.href='/common/support/inquisition'">
					<div class="col-xs-12 padding_none  h100">
						<div class="col-xs-12 h100">
						<div class="h20"></div>
						<div class="col-xs-6 col-xs-offset-3 padding_none">
							<div style="padding:0px !important" class="icon_make  col-xs-12 col-lg-offset-4  col-md-offset-4 col-xs-offset-2   text-center">Q</div>
						</div>
						
						<div class="col-xs-12 marign_none"><h5>문의 하기</h5></div>
						
						<div class="col-xs-12 marign_none hidden-xs "><h5>질문과 답변</h5></div>
					</div>
					</div>
				</div>
				<div class="col-lg-4 col-xs-6  sub_menu_mini  text-center h45 " onclick="location.href='/common/support/OftenQuestion'">
					<div class="col-xs-12 h100 padding_none">
						<div class="col-xs-12 padding_none h100">
						<div class="h20"></div>
						<div class="col-xs-6 col-xs-offset-3 padding_none">
							<div class="icon_make  col-xs-12 col-lg-offset-4 col-md-offset-4 col-xs-offset-2  ">i</div>
						</div>
						<div class="col-xs-12 marign_none"><h5>자주 묻는 질문</h5></div>
						<div class="col-xs-12 marign_none hidden-xs"><h5>자주 묻는 질문 입니다</h5></div>
					</div>
					</div>
				</div>
			</div>
			<div class="height_box"></div>
			<div class="col-lg-6 col-lg-offset-3 search_form" >
				<select class="h100 col-lg-3  col-xs-3 select_board">
					<option>선택하기</option>
					<option>석택하기</option>
				</select>
				<div class="input-group col-lg-8 col-sm-8 col-xs-8 pull-right h100 searh_box">
					  <input type="text" class="form-control h100" id="exampleInputAmount" placeholder="내용을  입력해주세요"  style="position:relative;z-index:0 !important ;height:100% !important">
					  <div class="input-group-addon search_btn h100 padding_none">
					  		<img src="/resources/image/search_b.png" class="h90">
					  </div>
				</div>
			</div>
			<div class="height_box"></div>
			<!-- 상표상담-->
			<div class="col-lg-12 title_bar col-xs-12 padding_none">
				<div  class="board_title col-lg-6 col-xs-5 padding_none h100">
					<b>상표 상담</b>
					
					<b class="hidden-xs hidden-sm">상표와 관련해서 궁금한 점을 물어보세요!</b>
				</div>
				<div class="col-lg-3 col-xs-7 pull-right h70 btn_wraps  padding_none">
					<div class="h20"></div>
						<button type="button" class=" col-xs-6 col-md-5 pull-right" onClick="location.href='/'">메인으로</button>
						<button type="button" class=" col-xs-5 col-md-5 pull-left" onClick="mvWrite()">글쓰기</button>
				</div>
			</div>
			<!-- table -->
			<table class="table table-hover  table_take text-center col-lg-12">
	  			<c:forEach items="${qnaList}" var="list">
	  				<tr class="qna" data-writer="${list.writer }" data-pk="${list.board_pk}">
		  				<td>${list.board_pk}</td>
		  				<td>${list.title }</td>
		  				<td>${list.writer }</td>
						<!--dateFormat-->
			            <fmt:parseDate value="${list.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
			            <fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
			            <jsp:useBean id="toDay" class="java.util.Date" />
						<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
						<c:choose>
							<c:when test="${regisDate==today}">
								<td><fmt:formatDate value="${regis_date}" pattern="HH:mm"/></td>
							</c:when>
							<c:otherwise>
								<td>${regisDate}</td>
							</c:otherwise>
						</c:choose>		
	  				</tr>
	  			</c:forEach>
			</table>
			<div id="list_paging">
					${nation.getNavigator()}
			</div>
			<br>
		</div>
</form>
	</div>
</div>

<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	

<script>
var formId2 = $("#formId2");
$(document).on("click",".back_to_btn div",function(){
	var text  =$(this).data("btn");
	if(text=="top"){
	$("body").animate({"scrollTop":"0"},500); 			
	}else{
		$("body").animate({"scrollTop":"100vh"},500);
	}
});
function mvWrite(){
	var currentUser = "${sessionScope.currentUser}";
	if(currentUser==""){
		alertt('로그인 후 이용하실 수 있습니다.');
	}else{
		var action = "/user/counseling/mvWrite";
		formId2.attr("action",action);
		formId2.attr("method","get");
		formId2.submit();
	}
}
function listarticle(pageNum){
	 $("#pageNum").val(pageNum);
	 formId2.submit();
}
$(document).on('click','.qna',function(){
	var pk = $(this).data('pk');
	$("#pk").val(pk);
	formId2.attr('action','/common/counseling/mvDetail');
	formId2.submit();
});
</script>	
</body>
</html>