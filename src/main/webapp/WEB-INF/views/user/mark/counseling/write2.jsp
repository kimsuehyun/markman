<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<c:import url="/WEB-INF/views/import/header.jsp"/>

<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>  
<!--css-->

<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<style>
	*{margin:0 auto ;padding:0;list-style: none;text-decoration:none;box-sizing: border-box;font-family:"맑은 고딕";}
	html{overflow-x: hidden;}
	/*option*/
	.padding_none{padding:0px !important}
	.margin_none{margin:0px !important}
	.padding_left{padding-left:0px !important}
	.h50{height:50%}
	.h25{height:25%}
	.h20{height:20%}
	.h60{height:60%}
	.h100{height:100%}
	.h33{height:30%}
	.h35{height:35%}
	.h40{height:40%}
	.h80{height:80%}
	.h90{height:90%}
	.h45{height:45%}
	.h70{height:70%}
	.h15{height:15%}
	.height_box{height:5vh;clear:both}
	.h45{height:45%}
	.w100{width:100%}
	.w30{width:30%}
	.w70{width:70%}
	.w50{width:50%}
	
	.fixed_box{position:fixed;height:20vh;top:15%;right:0%;}
	.back_to_btn{background:#f30}
	.back_to_btn > div:first-child{background:#000;color:#fff}
	.back_to_btn > div:last-child{background:#fff;color:#000;border:1px solid #000}
	
	
	.btn-file {
			width: 100% !important;
			height:100%;
		    position: relative;
		    overflow: hidden;
		    left: 1%;
		    height:3vh;
		    color:#fff !important;
		    background:#c01a1a !important;
		    border:none !important
		}
		
		
	
		
		
		.btn-file input[type=file] {
		    position: absolute;
		    top: 0;
		    right: 0;
		    width: 100%;
		    height: 100%;
		    font-size: 50px;
		    text-align: right !improtant;
		    filter: alpha(opacity=0);
		    opacity: 0;
		    outline: none;
		    background: white;
		    cursor: inherit;
		    display: block;
		}
		
		.btn-file {
			width: 100% !important;
			height:100% !important;
		    position: relative;
		    overflow: hidden;
		    height:3vh;
		    color:#fff !important;
		    background:#ddd !important;
		    border:none !important;
		    font-size:5vw !important;
		    font-weight: bold;
		    text-align: center;
		}
		.inputs_imgs{
			clear:both;
		    position: absolute;
		    height: 100%;
		    text-align: right !improtant;
		    filter: alpha(opacity=0);
		    opacity: 0;
		    outline: none;
		    background: white;
		    cursor: inherit;
		    display: block;
		}
		
	.photo_content_apply{width:100%;height:35vh;margin-top:5%}
	.photo_content_apply  li{float:left;height:90%}
	.change_img{width:100%;height:100%;position:relative;}
	.x_btn{position:absolute;top:0%;width:30px;height:30px;background:#999;color:#fff;text-align:center;}
</style>



<div class="continer">
	<div class="row">
			<div class="height_box"></div>
			<div  class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1" >
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Write</h3></div>
      				<div class="panel-body">
      					<form class="form-horizontal col-lg-10">
							<div class="form-group">
							  	<label for="inputEmail1" class="col-sm-2 control-label">Email</label>
							  	<div class="col-sm-10">
								    <input type="email" class="form-control" id="inputEmail1" placeholder="Email">
								 </div>
							</div>
							<div class="form-group">
							  	<label for="inputEmail2" class="col-sm-2 control-label">작성자</label>
							  	<div class="col-sm-10">
								    <input type="text" class="form-control" id="inputEmail2" placeholder="작성자">
								 </div>
							</div>
							<div class="form-group">
							  	<label for="inputEmail3" class="col-sm-2 control-label">제목</label>
							  	<div class="col-sm-10">
								    <input type="text" class="form-control" id="inputEmail3" placeholder="제목">
								 </div>
							</div>
							<div class="form-group">
							  	<label for="inputEmail4" class="col-sm-2 control-label">글본문</label>
							  	<div class="col-sm-10">
							  		<textarea id="inputEmail4"  class="form-control" style="height:30vh;" ></textarea>
								 </div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">이미지</label>
								<div class="col-sm-10 padding_none">
									<div class="col-lg-12 photo_wrap" >
										<ul class="photo_content_apply">
											<li  class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
												<span class="btn btn-default btn-file col-lg-1 col-xs-12 ">
													<div class="h25"></div>
				    								+<input type="file" class="inputs_imgs" multiple>
												</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div  class="col-sm-offset-2 col-sm-10">
								<button class="btn btn-default">확인</button>
							</div>
      					</form>				
      				</div>
				</div>
			</div>
	</div>
</div>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	

<script>
var obj_li;
var i = 0;
$(".inputs_imgs").on("change",function(e){
	i++;
	
	if(i=="3"){
		alert("초과");
	}else{
		var files  = e.target.files[0];
		var reader = new FileReader();
		reader.onload = function(e){
			var tag =  "<li class='col-lg-4 col-xs-12'>"
							+"<img src='"+e.target.result+"' class='change_img'>"
							+"<input type='file' class='inputs_imgs' value='"+e.target.result+"'>"
							+"<div class='x_btn'>"
								+"<button style='border:none;background:#ddd'>x</button>"
							+"</div>"
					   +"</li>"
					obj_li =  $(".photo_content_apply").prepend(tag);
		}
		reader.readAsDataURL(files);
	}
})

$(document).on("click",".x_btn",function(){

	--i;
	$(this).parents("li").remove();

});
</script>
	
</body>
</html>