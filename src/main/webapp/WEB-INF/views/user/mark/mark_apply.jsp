<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<c:import url="/WEB-INF/views/import/header.jsp"/>
<!--  -->
<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<!--css-->
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<link rel="stylesheet" href="/resources/common/css/mark_apply.css"/>
<!--  부트스트랩 -->
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body style="overflow-x: initial !important;">
<div class='add_modals'>
	<div class='col-xs-10 col-xs-offset-1 view_wrap col-lg-4 col-lg-offset-4 padding_none'>
	<button class='add_modal_btn' onClick='close_popup()'>x</button>
		<div class='col-xs-10 col-xs-offset-1 text-center icon_popup_wrap'>
			<div class='col-xs-6 col-lg-6 col-xs-offset-3 col-lg-offset-3  h100 mv_next_step padding_none' data-step="checkbox">
				<div class='col-xs-12 h100'>
					<img src='/resources/image/D-4_contents-03.png' class='col-xs-12 col-sm-6 col-sm-offset-3' alt="icon">
					<h4 class="col-lg-12  col-sm-12 hidden-xs">개별 선택</h4>
					<h5 class="col-xs-12 visible-xs-block ">개별<br>선택</h5>
				</div>
			</div>
		</div>
	</div>

	<div class='col-xs-10 check_box_wrap col-xs-offset-1 col-lg-4 col-lg-offset-4 padding_none' style='height:70vh;margin-top:1%;display:none;'>
	<button class='add_modal_btn' onClick='close_popup()'>x</button>
	<div class='col-xs-12  text-center icon_popup_wrap'>
	<h3 class='text-center'>개별 상표항목 선택</h3>
	<div class='col-lg-12 col-xs-12 up_content' style='height:25vh;padding-top:0%;border-bottom: 1px solid #ddd;overflow-y: scroll !important ' >
<!-- 		forEach시작 -->
	<c:forEach items="${markContentVo}" var="vo" varStatus="status">
		<div class='content-body col-lg-4 col-xs-6' style='border:none !important'>
			<div class='col-lg-12 h40 panel contents_wrpa_apply panel-default padding_none' style='float:left;maragin-left:3%'>
				<div class='col-xs-12 h30 panel-heading '>
					<input type='checkbox' class='check_boxs mark-content-check-box' value="${vo.mark_num}"><b class="mark-content-b"><a>${vo.mark_num}류</a></b>
				</div>
				<div class='col-xs-12 h70 panel-body' >
					<span class="show-simple">
					<c:choose>
						<c:when test="${fn:length(vo.info)>10}">
							${fn:substring(vo.info,0,10)}...
						</c:when>
						<c:otherwise>
							${vo.info}
						</c:otherwise>
					</c:choose>
					</span>
					<span class="show-more">${vo.info}</span></div>
			</div>
		</div>
	</c:forEach>
<!-- 		끝 -->
	</div>
	<div class='col-lg-6 col-xs-12 visible-lg-block ' style='height:20vh;margin-top:5%'>
		<div class='col-xs-12 h100 row_add_wrap ' style=';border:1px solid #ddd !important;overflow-y: scroll;'>
		</div>
	</div>
	<div class='col-lg-6 col-xs-12 visible-xs-block' style='height:10vh;margin-top:5%'>
		<div class='col-xs-12 h100 row_add_wrap ' style=';border:1px solid #ddd !important;overflow-y: scroll;'></div>
	</div>
		<div class='col-lg-6 col-xs-12 visible-lg-block' style='height:20vh;margin-top:5%'>
			<div class='col-xs-12 h100  ' style=';border:1px solid #ddd !important;'>
				<div class='h30  '></div>
				<div class='text-center'><b>[지정 상품 선택시 유의 사항]</b><br>2개 이상의 류를 선택하는 다류출원인 경우,1류 추가시 마다 56,000원의 관납료 및 추가 수수료가 부과됩니다.</div>
			</div>
		</div>
		<!-- <div class='col-lg-6 col-xs-12 visible-xs-block' style='height:auto;margin-top:5%'>
			<div class='col-xs-12 h100  ' style=';border:1px solid #ddd !important;'>
				<div class='h30 visible-lg-block visible-md-block'></div>
				<div class='text-center'><b>[지정 상품 선택시 유의 사항]</b><br>2개 이상의 류를 선택하는 다류출원인 경우,1류 추가시 마다 56,000원의 관납료 및 추가 수수료가 부과됩니다.</div>
			</div>
		</div -->
		<div class="btn-wrap">
			<button onclick="prev_popup()" class="prev-btn">이전 단계</button>
			<button onclick="addMarkContent();" class="submit-btn">완료</button>
		</div>
		</div>
	</div>
<div class='col-xs-10 dev_box_wrap col-xs-offset-1 col-lg-6 col-lg-offset-3 padding_none' style='height:65vh;margin-top:1%'>
	<button class='add_modal_btn' onClick='close_popup()'>x</button>
	<div class='col-xs-12  text-center icon_popup_wrap2'>
	<h3 class='text-center'>단계별 상표항목 선택</h3>
	<div class='col-lg-12 col-xs-12 up_content' style='height:35vh;padding-top:5%;border-bottom: 1px solid #ddd;'>
		<table class= "w100 table mt30" >
			<tr>
				<th class = "tc col-xs-3" style="font-size:1.5vh !important"> 대분류</th>
				<th class = "tc col-xs-3" style="font-size:1.5vh !important"> 중분류</th>
				<th class = "tc col-xs-3" style="font-size:1.5vh !important"> 서비스</th>
				<th class = "tc col-xs-3" style="font-size:1.5vh !important"> 상표항목</th>
			</tr>
			<tr class ="bld brd">
			<td style="height:300px;" class="mark_list">
				<ul>
					<c:forEach items="${bigCategoryList}" var="map" varStatus="status">
						<li class="bigCategory">${map.get("supervisor")}</li>
					</c:forEach>
				</ul>
			</td>
			<td class="bld brd mark_list" id="mid">
			</td>
			<td class="bld brd mark_list" id="service">
			</td>
			<td class="mark_list" id="mark">
			</td>
		</table>
	</div>
	<table style="height:50px;width:100%;border:1px solid rgba(10, 116, 138, 1);margin-top:5px">
				<tr>
				<td style="width:280px;color:rgba(10, 116, 138, 1);text-align:center;font-weight:bold;font-size:15px;border:1px solid;border-left:none">선택한 카테고리</td>
				<td style=" padding-left:20px;" id="markDirectory"><span id="1"></span><span id="2"></span><span id="3"></span><span id="4"></span></td>
				</tr>
		</table>
	<div class="btn-wrap">
			<button onclick="prev_popup()" class="prev-btn">이전 단계</button>
			<button onclick="addMarkContent();" class="submit-btn">완료</button>
	</div>
		</div>
	</div>
</div>
<form action="" id="formId" method="POST" enctype="multipart/form-data">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" id="type" name="type">
<input type="hidden" id="step" name="step">
<input type="hidden" id="select_content" name="select_content" value=""><!--상표 선택 항목 -->
<input type="hidden" id="applicant_id" name="applicant_id" value="${sessionScope.currentUser.getId()}">
<input type="hidden" id="imgElementName" name="imgElementName" value="mark_register_files">
<input type="hidden" id="uploadPageName" name="uploadPageName" value="mark">
<input type="hidden" id="markContetnArray" name="markContetnArray" value="">

	<div class="container-fluid col-lg-10 col-sm-12 col-xs-12">
		<div class="row" style="height:155vh">
		<div class="content_chang col-lg-10 col-lg-offset-1 col-xs-12" style="font-size:1.3em">
			<div class="col-lg-9 col-lg-offset-3 col-xs-12 padding_none">
				<div class="col-lg-12 content_wrap_request">
					<h2>상표 출원 신청</h2>
					<div class="request_text col-lg-12">
						<div class="first_contnet col-lg-12 col-xs-12">
							<div class="col-lg-3 col-xs-12">
								<div>상표 유형
<!-- 									<button class="btn_red" onClick="btn_red()"> -->
<!-- 									<b>?</b> -->
<!-- 									</button> -->
								</div>
							</div>
							<div class="col-lg-3 col-xs-4">
								<div><input type="radio" name="markType" class="markType" value="kor" checked="checked">한글</div>
							</div>
							<div class="col-lg-3 col-xs-4">
								<div><input type="radio" name="markType" class="markType" value="eng">영문</div>
							</div>
							<div class="col-lg-3 col-xs-4">
								<div><input type="radio" name="markType" class="markType" value="figure">도형</div>
							</div>
						</div>
						<div class="last_contnet col-lg-12 col-xs-12">
							<div class="col-lg-3 col-xs-12">
							</div>
							<div class="col-lg-3 col-xs-4">
								<br>
								<img src='/resources/image/icon_hangeul.png' class="col-lg-4 img-responsive col-xs-10 col-md-4 col-sm-4 pull-left img-responsive padding_none" alt="한글">
							</div>
							<div class="col-lg-3 col-xs-4">
								<br>
								<img src='/resources/image/icon_abc.png' class="col-lg-4  img-responsive  col-xs-10 pull-left col-md-4 img-responsive padding_none col-sm-4" alt="영어">
							</div>
							<div class="col-lg-3 col-xs-4 ">
								<br>
								<img src='/resources/image/icon_square.png' class="col-lg-4 img-responsive col-xs-10 pull-left col-md-4  img-responsive padding_none col-sm-4" alt="도형">
							</div>
						</div>
					</div>
					<div class="request_text2 col-lg-12">
						<div class="col-lg-12">
								 <div class="form-group">
								    <label for="input_title" class="col-sm-2 control-label">상표명</label>
								    <div class="col-sm-10 input_wrap">
								      <input type="text" class="form-control " id="name" name="name" placeholder="입력">
									    <span class="title_color">사용할 상표 이름을 작성해 주세요.</span>
								    </div>
								 </div>
								  <div class="form-group" style="clear:both;margin-top:7%">
								    <label for="input_title1" class="col-sm-2 control-label">사용처</label>
								    <div class="col-sm-10 input_wrap">
								      <input type="text" class="form-control" id="where_use" name="where_use" placeholder="입력">
									    <span class="title_color">서비스,상품에 대해 설명해주세요.</span>
								    </div>
								 </div>
								 <div class="col-lg-12 height_box2"></div>
								 <div class="form-group">
							    	<label for="input_title1" class="col-sm-2 control-label">상표 분류</label>
								    <div class="col-sm-10">
								      	<div class="col-lg-12" style="padding-top:1%">
								      		<input type="radio" class="category" value="select" name="radio_check" checked>분류표 선택하기
								      		 <button type="button" onclick="ch()" class="add_goods_btn categoryInput">상표분류 추가</button>
								      	</div>
									</div>
								 </div>
							<div class="col-lg-12 height_box2"></div>
							<div class="col-lg-10 col-lg-offset-2 col-xs-12   padding_none">
								<div class="col-lg-12 text_request_box ">

							      </div>
							      <div class="col-lg-12 text_request_box2 ">
							      	<div class="col-lg-12 padding_none ">
							      		<div class="col-lg-12 padding_none">
							      			<input type="radio" class="category" value="self" name="radio_check">직접입력하기<!--<button class="btn_red" onClick="btn_red()">?</button> -->
							      			<textarea class="form-control categoryInput" rows="3" id="category" name="category" disabled></textarea>
							      		</div>
							      	</div>
							     </div>
							</div>
							<div class="col-lg-12 col-sm-12 col-xs-12" style="padding-top:2%">
								<div class="col-lg-2 col-sm-2 col-xs-12 padding_none">첨부 파일</div>
								<div class="col-lg-10 padding_none">
									<div class="col-lg-12 photo_wrap" >
										<ul class="photo_content_apply">
											<br>
											<div class="height_box visible-xs-block" style="clear:both"></div>
											<li  class="col-lg-4 col-md-4 col-sm-12 col-xs-12 img_boxs" style="margin-top: -2%">
												<span class="btn btn-default btn-file col-lg-1 col-xs-12 ">
													<div class="h25"></div>
				    								+
												</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-lg-12  col-sm-12 price_wrap padding_none tops text-center" style="width:100%;clear:both;float:left">
									<ul class="col-lg-12 col-sm-12 col-xs-12  padding_none text-center" style="border-bottom:1px solid #000">
										<li class="col-lg-5 col-sm-5 col-xs-5 padding_none">사용현황</li>
										<li class="col-lg-2 col-sm-2 col-xs-2 padding_none">금액</li>
										<li class="col-lg-2 col-sm-2 col-xs-2 padding_none">수량</li>
										<li class="col-lg-3 col-sm-3 col-xs-3 padding_none ">총 금액</li>
									</ul>

							</div>
							<div class="col-lg-12">
										<div class="total_price pull-right"><h2>0</h2></div>
							</div>

							<div class="col-lg-6 col-md-6  col-md-offset-3 col-sm-12 col-xs-12 col-lg-offset-3 btn_request_wrap" style="clear:both;padding-top:5%">
								<div class="col-lg-6 col-xs-6  col-sm-6">
									<button type="button" class="col-lg-12 pull-right" onclick="saveMark()">임시 저장</button>
								</div>
								<div class="col-lg-6 col-xs-6  col-sm-6">
									<button type="button"  class="col-lg-12 btn"  onclick="applyOk()">출원 신청하기</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</form>
<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
<script src="/resources/common/js/global.js"></script>
<script src="/resources/common/js/markCategorySelect.js"></script>
<script>
$(document).on("change",".markType",function(){
	var value = $(this).val();
	var typeData = "도형";
	if(value=="kor"){
		typeData = "한글";
	}else{
		typeData = "영문";
	}
	$("#type").val(typeData);
});
//상표등록한 토탈 배열
markContentArray = new Array();
function applyOk(){
	$("#markContetnArray").val(markContentArray);
	var formId = $("#formId");
	formId.attr("action","/applicant/mark/applyOk");
	formId.attr("method","post");
	formId.submit();
}
var sum =0;
var price_num = 0;
var obj_li;
var i = 0;
var nums = 0;
$(document).ready(function(){
	$('.show-more').hide();
	$(".price_box").each(function(index,e){
		var price = parseInt($(this).find(".price").text());
		var count = parseInt($(this).find(".count").text());
		$(".total:eq('"+index+"')").text(price*count)
	 	sum +=price*count;
	})
	$(".total_price h2").text(sum);
});

function markContetnInit(){
	$(".add-mark-content").each(function(){
		$(this).remove();
	});
	$.each(markContentArray,function(array_index, array_item){
		$(".mark-content-check-box").each(function(check_index, check_item){
			console.log(check_item);
			if(array_item == $(check_item).val()){
				check_item.click();
				return false;
			}
		});
	});
}

$(document).on("click",".mark-content-check-box",function(){
	var element = $(this).parent(".panel-heading");
	markContentList(element);
});

$(".select_menu ul li").on("click",function(){
	localStorage.setItem('eqs', $(this).data("eq"));
	if($(this).data("eqs")=="0"){
			location.href="/user/main"
			$(".content_wrap2").hide();
			$(this).addClass("active").siblings("li").removeClass("active");
	}else{
		$(".notice").show();
		var url =$(".select_menu ul li").eq(localStorage.getItem('eqs')).data("url");
		$(".select_menu ul li").eq(localStorage.getItem('eqs')).addClass("active").siblings("li").removeClass("active");
		$(".content_chang").load(url);
	}
});
/*error  */
$('.category').click(function(){
	var curCategory = $('.category:checked');
	var otherCategory = $('.category').not('.category:checked');
	curCategory.parent().find('.categoryInput').attr("disabled",false);
	otherCategory.parent().find('.categoryInput').attr("disabled",true);
});
//임시저장
function saveMark(){
	var form = $("#formId");
	$('#step').val(0);//상표 진행단계0으로 설정
	setType();
	form.submit();
}
$(document).on('click','.panel-body',function(){
	var simple = $(this).children('span[class=show-simple]');
	var more = $(this).children('span[class=show-more]');
	if($(this).hasClass('more')){
		$(this).removeClass('more');
		simple.css('display','block');
		more.css('display','none');
	}else{
		$(this).addClass('more');
		simple.css('display','none');
		more.css('display','block');
	}
})

//이미지 업로드
var obj_li;
var i = 0;


$(".btn-default").on("click",function(e){

	if(i=="2"){
		alert("초과");

	}else{

	 i = i+1;
	var tag =
		"<div class='height_box visible-xs-block ' style='clear:both' ></div>"
			+"<li class='col-lg-4 col-sm-12 col-md-4 col-xs-12'>"
				+"<input type='file'  class='inputs_imgs"+i+" inputs_imgs input_box '  name='mark_register_files' >"
				+"<img src='' class='change_img"+i+" change_img'>"
				+"<div class='x_btn'>"
					+"<button>x</button>"
				+"</div>"
		   +"</li>"
		obj_li =  $(".photo_content_apply").prepend(tag);

			 $(".inputs_imgs"+i).trigger( "click");
			 $(".inputs_imgs"+i).on("change",function(e){
				 var files  = e.target.files[0];
 				 var reader = new FileReader();
 				reader.onload = function(e){
					$(".change_img"+i).attr('src',e.target.result)
 				}
 				reader.readAsDataURL(files);
			 })

			 var objs = $(".input_box").length;
			 if(objs =="2"){
				 $(".img_boxs").hide();
			 }

	}

})






$(document).on("click",".x_btn",function(){
	$(".img_boxs").show();
	i = i -1;
	$(this).parents("li").remove();

});

////popup
function ch(){
	$("html,body").animate({"scrollTop":"0"},500)
	$(".add_modals").show();
	$("html").css({"overflow-y":"hidden"})
	markContetnInit();
}

$('.mv_next_step').click(function(){
	var nextStep = $(this).data('step');
	if(nextStep=="checkbox"){
		$(".view_wrap ").hide();
		$(".check_box_wrap").show();
	}else{
		$(".view_wrap ").hide();
		$(".dev_box_wrap").show();
	}
});

function close_popup(){
	$(".add_modals").hide();
	$("html").css({"overflow-y":"scroll"})
	$(".up_content").find('input[type=checkbox]').each(function(){
		$(this).prop('disabled',false);
	});
	resetSelection();
}

var price = $(".price").text();
var count = $(".count").text();
function prev_popup(){
	$(".view_wrap ").show();
	$(".check_box_wrap").hide();
	$(".dev_box_wrap").hide();
	resetSelection();
}
//체크박스
$('.panel-heading').click(function(){
	var checkElement = $(this).children("input[type=checkbox]");
});

$('.mark-content-b').click(function(){
	$(this).siblings(".mark-content-check-box").click();
});






function markContentList(obj){
	var text = $(this).parents(".contents_wrpa_apply").find(".panel-body").text();
	var checkElement = obj.children("input[type=checkbox]");
	var checkedVal = checkElement.val();
	if(checkElement.is(":checked")){
		var text = obj.parents(".contents_wrpa_apply").find(".panel-body .show-more").text();
		var title =obj.parents(".contents_wrpa_apply").find(".panel-heading b").text();
		var line = ""
			+"<div class='col-xs-12 content-add-div"+checkedVal+"' style='height:3vh;border-bottom:1px solid #ddd'>"
			 	+"<div>"
			 		+"[<b>"+title+"</b>]"+text.substring(0,10)+"..."
			 	+"</div>"
			+"</div>";
		$(".row_add_wrap").append(line);
		checkElement.attr("checked",true);
		price_num = price_num;

	}else{
		checkElement.attr("checked",false);
		$(".content-add-div"+checkedVal).remove();
	}

}

var numsss   =0;
function addMarkContent(){
	$(".add_modals").hide();
	markContentArray = new Array();
	$('.check_boxs:checked').each(function(index,item){
		markContentArray.push($(this).val());
		numsss = index;
		$(this).parents('.contents_wrpa_apply').removeClass('h40')
		var checkedMark = $(this).parents('.content-body').html();
		$('.text_request_box ').append("<div class='col-lg-3 add_append add-mark-content' style='height:10vh; margin-bottom:20px'>"+checkedMark+"</></div>");
	});
	resetSelection();
	Total_price();
	$("html").css({"overflow-y":"scroll"});
}
function settingMarkContent(){

	$(".add-mark-content").each(function(){


	});

}

function resetSelection(){//선택 초기화 함수
	$('.check_boxs:checked').prop('checked',false);
	$(".row_add_wrap").empty();
	$('#mid').empty();
	$('#service').empty();
	$('#mark').empty();
}

function Total_price(){
	nums = markContentArray.length;
	var one_price = 55000;
	var print = "<div class='col-lg-12 text-center padding_none price_box ' style='height:5vh;margin-top:2%'>"
					+"<span class='col-lg-5 col-sm-5 col-xs-5 padding_none'>서비스 이용료</span>"
					+"<span class='col-lg-2 col-sm-2 col-xs-2 padding_none'>55000</span>"
					+"<span class='col-lg-2 col-sm-2 col-xs-2 padding_none'>"+nums+"</span>"
					+"<span class='col-lg-3 col-sm-3 col-xs-3 padding_none'>"+nums*one_price+"</span>";

				+"</div>"
		$(".price_box").remove();
		$(".price_wrap").append(print)
	$(".total_price h2").text(one_price * nums)

}

</script>
</body>
</html>