<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<link rel="icon" href="/resources/image/pavicon.png">
<c:import url="/WEB-INF/views/import/admin/header.jsp" />
<script src="/resources/editor/naver/js/service/HuskyEZCreator.js"></script>
<title>Markman</title>
</head>
<article>
<div class="admin_regis_form">
	<h1>메일 작성</h1>
	<form action="/admin/email/regisOk" id="formId" method="POST" onsubmit="update()" enctype="multipart/form-data" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" id="receiverNum" name="receiverNum"/>
		<input type="hidden" id="receiver" name="receiver"/>
		<input type="hidden" id="cc" name="cc"/>
		<input type="hidden" id="bcc" name="bcc"/>
		<table id="table">
			<tr>
				<td></td>
				<td><button type="button" class="find-address">주소록</button></td>
			</tr>
			<tr>
				<td class="w15">받는 사람</td>
				<td><div class="receivers"><textarea id="receiver0" name="receiver0" rows=1 tabindex=1></textarea></div>
			</tr>
			<tr>
				<td class="w15">제목</td>
				<td><input type="text" id="title" name="title" required></td>
			</tr>
			<tr>
				<td><span class="down5">파일첨부</span><button type="button" class="plus_btn btn20">+</button></td>
				<td><input type="file" class="addImg" id="img0" name="img0"></td>
			</tr>
			<c:forEach var="i" begin="1" end="2">
				<tr class="hide">
					<td></td>
					<td><input type="file" class="addImg" id="img${i}" name="img${i}"></td>
				</tr>
			</c:forEach>
		</table>
		<textarea name="ir1" id="ir1"></textarea>
		<div align="right">
	<button class="regis_btn"><img class="icon20" src="/resources/image/send.png" alt="send">보내기</button>
</div>
<script>
/*네이버 SmartEditor */
var editor_object = [];
var file_num = 0; //첨부파일 수
var receiverNum = 0;//수신인 수
var popOption;//자식창
nhn.husky.EZCreator.createInIFrame({
	oAppRef: editor_object,
	elPlaceHolder: "ir1",
	sSkinURI: "/resources/editor/naver/SmartEditor2Skin.jsp?textareaName=mail",
	fCreator: "createSEditor2"
});
			
$(".plus_btn").click(function(){
	if(file_num<2){//첨부파일 개수 3개로 제한
		file_num++;
		$("#img"+file_num+"").parent('td').parent('tr').removeClass("hide");
	}else	alertt("3개 이하의 파일만 첨부할 수 있습니다.");
	
});

function update(){
	naverEditorTextAreaUpdate(editor_object, "ir1");//textarea업데이트
	$('#receiverNum').val(receiverNum); //수신인 수 전송
	var receiver = "";
	$(".receivers span[name^=receiver]").each(function(index){
		receiver += $(this).text()+">>"; //이메일 + 구분자
	})
	$("#receiver").val(receiver);
}

$('.find-address').click(function(){
	    window.name = "parentForm";//부모창
		var popUrl = "/admin/addressList";	//팝업창에 출력될 페이지 URL
		popOption = "width=900, height=800, resizable=no, scrollbars=no, status=no;";    //팝업창 옵션(optoin)
			window.open(popUrl,"childForm",popOption);
});

$('.receivers').click(function(){
	$('#receiver'+receiverNum+'').focus();
}) 
var maxLen = 15;
$(document).on("keyup",".receivers textarea",function(){
	var val = $(this).val();
	if(window.event.keyCode==13){
		if(val!=""){
			alert("dfg")
			addEmail($(this));
			$("#receiver"+receiverNum+"").focus();//다음 입력창에 포커스 주기
		}
		return false;
	}else if(window.event.keyCode==9){//tab
		    if(val!="")receiverNum++;//수신인 수 증가
		    $("#title").focus();
	}else if(window.event.keyCode==8&&val==""){
		$(this).prev('div').remove();
	}
    if (val.length>maxLen) {//다른 방법 생각
        $(this).width(function(i,h){
            return h + 10;
        });
        maxLen++;
    } 
});

$(document).on("blur",".receivers",function(){
	var obj = $("#receiver"+receiverNum+"");
	if(obj.val()!=""){
		alert("sdf")
		addEmail(obj);	
	}
})
function addEmail(obj){
	receiverNum++;//수신인 수 증가
	var val = obj.val();
	checkEmailForm(obj,val);//이메일 형식 체크
	$('.receivers').append('<textarea id="receiver'+receiverNum+'" name="receiver'+receiverNum+'"></textarea>');//다음 입력창 생성
}
function checkEmailForm(obj, val){
	var id = obj.attr("id");
	var divClass = "";
	var reg_exp = new RegExp(/^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i);
	var match = reg_exp.exec(val);
	if(match==null)	divClass="error-receiver";
	else			divClass="added-receiver";
	obj.wrap("<div class='"+divClass+"'>").wrap("<span id='"+id+"' name='"+id+"'>").remove();
	$("#"+id+"").append(val);//div내부 span에 값 입력
	$("#"+id+"").parent('div').append('<span class="delete">×</span>');
}
$(document).on("click", ".delete", function(){
	var receiver = $(this).parent('div');
	receiver.remove();
});

function addAddress(){
	var receiver = $("#receiver").val().split(",");
	for(var i=0 ; i<receiver.length; i++){
		$(".receivers").append('<textarea id="receiver'+(receiverNum+i+1)+'" name="receiver'+(receiverNum+i+1)+'"></textarea>');//다음 입력창 생성
		$("#receiver"+(receiverNum+i)+"").val(receiver[i]);
		checkEmailForm($("#receiver"+(receiverNum+i)+""),receiver[i]);
	}
	receiverNum += i;
}
</script>
</form>
</div>
</article>
</body>
</html>