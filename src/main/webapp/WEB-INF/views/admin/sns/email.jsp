<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
    <c:import url="/WEB-INF/views/import/admin/header.jsp" />
        <article>
            <div id="table_header">
                <h1>메일 관리</h1>                
            </div>
            <form id="formId">
            <input type="hidden" name="pageNumber" id="pageNumber"/>
            <input type="hidden" id="seq" name="seq"/>
            <table class="email_table">
           			 <tr>
                		<td colspan=4></td>
                	</tr>
                <c:forEach items="${emailList}" var="list">
    				<tr>
    					<td>${list.id}</td>
    					<td>${list.title}</td>
    					<td>${list.content}</td>
    					<td>${list.send_date}</td>
                	</tr>            
                </c:forEach>
            </table>
            </form>
       		<div id="paging">
       			${nation.getNavigator()}
       		</div>
       		<div align="right">
       		<div align="right">
			  <button type="button" onclick="location.href='/admin/mail/mvRegis'" class="regis_btn">
				  <img alt="pencil" src="/resources/image/pencil.png" class="icon20">메일쓰기
			  </button>    
        	</div>
        	</div>
        </article>
    </div>
<%-- <c:import url="/WEB-INF/views/import/admin_footer.jsp" /> --%>   
<script>
    //페이지 네이션
    function listarticle(pageNumber){
 	 	 $("#formId").attr("action","/admin/mailList/"+pageNumber+"");
  		 $("#pageNumber").val(pageNumber);
  		 $("#formId").submit();
  	}
</script>
</body>
</html>