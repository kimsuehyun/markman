<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="/resources/common/js/bootstrap.js"></script>
<link href="/resources/common/css/bootstrap.min.css" rel="stylesheet"> 
</head>
<body>
<!--USERINFO MODAL -->
<div class="modal fade" id="userInfo" role="dialog">
  	<div class="modal-dialog mtcenter">
    <!-- Modal content-->
    	<div class="modal-content">
      		<div class="modal-header">   			
 				<h4 class="modal-title">회원 상세보기</h4>
   			</div>
			<form id="userInfo" name="userInfo" action="" method="POST">
 				<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
				<div class="modal-body tc userInfo">
					<!-- 				body -->
	     		</div>      		
	      		<div class="modal-footer">      			
	      			<div style="float:right;">
	      				<input type="submit" class='btn btn-editInventor' value="정보수정 ">
	      				<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>
	      			</div>
	    		</div>
			</form>
    	</div>      	
	</div>
</div>
    <c:import url="/WEB-INF/views/import/admin/header.jsp" />
        <article>
            <div id="table_header">
                <h1>회원 관리</h1>                
            </div>
            <div id="search_user" align="right">
            <button class="add-user" onclick="window.open('/signUpPage','','width=600,height=850')"><span>회원 추가 </span><img src="/resources/image/add-user.png" class="icon25" alt="addUser"></button><br>
            <c:choose>
	            <c:when test="${search!=null&&search!='all'}">
	            	<input type="text" id="search_about"  onkeydown="enter();" value="${search}">
	            </c:when>
	            <c:otherwise>
	            	<input type="text" id="search_about" onkeydown="enter();" placeholder="아이디/이름 검색">
	            </c:otherwise>
            </c:choose>
            	<button class="search_btn" onclick="searchUser();">검색</button>
            </div>
            <form id="formId">
            <input type="hidden" name="pageNumber" id="pageNumber"/>
            <input type="hidden" name="search_word" id="search_word"/>
            <input type="hidden" name="search" id="search" value="${search}"/>
            <table class="manage_table">
	            <tr>
    	   			<th class="w20">아이디</th>
       				<th class="w10">이름</th>
       				<th class="w25">전화번호</th>
       				<th class="w25">이메일</th>
       				<th class="w10">유형</th>
       				<th>기능</th>
       			</tr>
               	 <c:forEach items="${userList}" var="user" varStatus="status">
			   		<tr>
				   		<td class="${user.getId()}" id="id${status.count}">${user.getId()}</td>
				   		<td id="name${status.count}">${user.getName()}</td>	
				   		<td>${user.getCell_num()}</td>		 	   		
						<td>${user.getEmail()}</td>
						<c:choose>
							<c:when test="${user.getUser_type()=='ROLE_ADMIN'}">
								<td style="font-weight:bold;">관리자</td>
							</c:when>
							<c:when test="${user.getUser_type()=='ROLE_APPLICANT'}">
								<td>출원인</td>
							</c:when>
							<c:when test="${user.getUser_type()=='ROLE_PATENT'}">
								<td>변리사</td>
							</c:when>
							<c:otherwise>
								<td>회원</td>
							</c:otherwise>
						</c:choose>
						<td><img src="/resources/image/info_w.png"  class="more-modal userTd-icon" data-toggle="modal" data-target="#userInfo" data-spanid="${user.getId()}"/>
						<img src="/resources/image/delete_r.png" class="userTd-icon" onclick="deleteUser('${user.getId()}','${user.getUser_type()}')"></td>
						
				   	</tr>
				 </c:forEach>             
            </table>
            <div id="paging">
       			${nation.getNavigator()}
       		</div>
			</form>
        </article>
   <%-- <c:import url="/WEB-INF/views/import/admin/footer.jsp" /> --%>
<script type="text/javascript" src="/resources/common/js/global.js"></script>
<script>
	//회원 검색
	var search = $('#search').val();
    $(document).ready(function(){
      $('.manage_table tr:even').css("backgroundColor","#fff");     // odd 홀수
      $('.manage_table tr:odd').css("backgroundColor","#F6F6F6");   // even 짝수
      if($('#who').val()!='all'){//전체 리스트가 아닌경우(=검색리스트)
    		for(var i = 1; i<=${fn:length(userList)} ; i++){
    			var mark_id = $('#id'+i+'').text().replace(search,"<em>"+search+"</em>");
    			var mark_name = $('#name'+i+'').text().replace(search,"<em>"+search+"</em>");
    			$('#id'+i+'').empty();
    			$('#id'+i+'').append(mark_id);
    			$('#name'+i+'').empty();
    			$('#name'+i+'').append(mark_name);
    		}	
      }
    });
    function enter(){//검색창에서 엔터키를 눌렀을 때
    	if(window.event.keyCode==13){
    		searchUser();
    	}
    }
    function searchUser(){
    	var search_about=$('#search_about').val();
    	$('#formId').attr("action","/admin/userList");
    	$('#search_word').val(search_about);
    	$('#formId').submit();
    }
    //페이지 네이션
    function listarticle(pageNumber){
 	 	 $("#formId").attr("action","/admin/userList/"+pageNumber+"");
  		 $("#pageNumber").val(pageNumber);
  		 $("#formId").submit();
  	}
    
    function deleteUser(userId){
      	confirmm(""+userId+"님을 회원 명단에서\n 삭제하시겠습니까?").then(function(){
      		$.ajax({
				url : "/admin/user/deleteOk",
				type : "POST",
				data : {userId:userId},
				success : function(resultData) {
					if(resultData!=-1){
						alertt("삭제가 완료되었습니다.","success");
						$('.'+userId+'').parent().remove();
					}else{
						alert("삭제 실패");
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\n" + "error:" + error);
				}
			});  
    	},function(){});
   	 }
    $(".more-modal").click(function(){//회원 정보 가져오기
		var id = $(this).data("spanid");
		$.ajax({
			url : "/admin/user/info",
			dataType : "json",
			type : "GET",
			data : "id="+id,
			success : function(resultData){
				makeUserModal(resultData);
			},
			error : function(request, status, error) {
				alert("code:" + request.status + "\n" + "error:" + error);
			}
		});		
	});
    function makeUserModal(resultData) {//모달창 생성
    	$('.userInfo').empty();
		var user = resultData.userVo;
		var patent = resultData.patentVo;
		var userType = user.user_type;
		var role,tel;
		var appendStr = "";
		//유선 전화 번호
		if(user.tel_num=='')	tel = "기입하지 않음"
		else	tel = user.tel_num;
		
		var table = "<table class='modal-table'>";
		//회원 유형
		if(userType=="ROLE_APPLICANT")		role="출원인";
		else if(userType=="ROLE_PATENT"){
			role="변리사";
			var img = patent.profile_img;
			var num = patent.patent_num;
			var regis_img = patent.patent_regis_img_name;
			var introduce = patent.introduce;
			var bank = patent.account_bank;
			var account = patent.account_num;
			if(img==null){
				img = "/resources/image/profile.jpg";
			}
			if(num==0){
				num = "등록된 변리사 번호가 없습니다.";
			}
			if(regis_img==null){
				regis_img = "등록된 사본이 없습니다.";
			}
			if(introduce==null){
				introduce = "등록된 경력 사항이 없습니다.";
			}
			if(bank==null||account==null){
				bank = "";
				account = "등록된 계좌가 없습니다."
			}else{
				bank = "["+bank+"]"
			}
			table += "<img class='profile_img' src = '"+img+"' alt='profile'/>";
			table += "<tr><td>변리사 번호</td><td>"+num+"</td></tr>"; 
			table += "<tr><td>변리사 등록증 사본</td><td><span>"+regis_img+"</span></td></tr>";
			table += "<tr><td>변리사 소개</td><td>"+introduce+"</td></tr>"; 
 			table += "<tr><td>계좌</td><td>"+bank+account+"</td></tr>"; 
		}
		else	role="관리자";
		
		table += "<tr><td class='w30'>회원 유형</td><td>" + role + "</td></tr>";
		table += appendStr;
		table += "<tr><td>아이디</td><td>" + user.id + "</td></tr>";
		table += "<tr><td>이름</td><td>" + user.name + "</td></tr>";
		table += "<tr><td>이메일</td><td>" + user.email + "</td></tr>";
		table += "<tr><td>휴대 전화 번호</td><td>" + user.cell_num + "</td></tr>";
		table += "<tr><td>유선 전화 번호</td><td>" + tel + "</td></tr>";
		table += "<tr><td>주소</td><td>" + user.addr1 +" "+ user.addr2 +" "+ user.addr3 + "</td></tr>";
		table += "<tr><td>가입일</td><td>" + user.sign_up_date+ "</td></tr>";
		table += "</table>";
		
		$('#userid').val(user.id);
		$('.userInfo').append(table);
    }
</script>
</body>
</html>