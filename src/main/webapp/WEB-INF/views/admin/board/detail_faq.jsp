<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<c:import url="/WEB-INF/views/import/admin/header.jsp" />
<article>
<div id="board_header">
	<h1><span class="board_type" onclick="mvMenu()">자주묻는질문</span><span class="board_pk">(${faqVo.board_pk})</span></h1>
</div>
<div class="detail_div">
	<form action="/admin/faq/mvEdit" id="formId" method="POST" enctype="multipart/form-data" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
		<input type="hidden" id="board_pk" name="board_pk" value="${faqVo.board_pk}"/>
		<table>
			<tr>
				<td>
					<span class="title">${faqVo.title}</span><span class="board_type">| 자주묻는질문</span>
					<span class="regis_date">
								<fmt:parseDate value="${faqVo.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
								<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd HH:mm"/>
					</span>
					<br>
					<span class="writer down5">${faqVo.writer}</span>
					<span class="hits"><img src="/resources/image/eye_b.png" class="icon20"/>
						<fmt:formatNumber value="${faqVo.hits}" pattern="#,###"/> views
					</span>
				</td>
			</tr>
		<%-- 파일 목록			
			<c:forEach items="${boardImgList}" var="list" varStatus="status">
				<tr>
					<td class="board_img">
					<img  class="downloadImg icon20" src="/resources/image/download.png"/>
					<span class="img_name" data-id="${list.img_url}">${list.img_origin_name}</span>
					<span class="img_size"><fmt:formatNumber value="${list.img_size/1024}" pattern="0"/>KB</span>
				</td>
				</tr>
			</c:forEach> --%>
			<tr>
				<td class="board_content">${faqVo.content}</td>
			</tr>
			<tr>
		</table>
	</form>
</div>
<div class="detail_btn" align="right">
	<button type="button" class="edit"><img src="/resources/image/edit.png" class="icon20"/><b>수정</b></button>
	<button type="button" class="delete"><img src="/resources/image/trash.png" class="icon20"/><b>삭제</b></button>
</div>
</article>
</body>
<script type="text/javascript" src="/resources/common/js/global.js"></script>
<script>
function mvMenu(){location.href="/admin/faqList";}
$('.delete').click(function(){
	var data = {};
	data["board_pk"] = $('#board_pk').val();
	confirmm("해당 게시물을 삭제하시겠습니까?").then(function(){
  		$.ajax({
  			url : "/admin/faq/deleteOk",
			type : "POST",
			data : data,
			success : function(resultData) {
				if(resultData=="success"){
					alertt("삭제가 완료되었습니다.","success").then(function(){
						location.href="/admin/faqList";
					});
				}else{
				 	alertt("삭제 실패");
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\n" + "error:" + error);
			}
		});  
	},function(){});
});
$('.edit').click(function(){
	var form = $("#formId");
	form.submit();
});
</script>
</html>