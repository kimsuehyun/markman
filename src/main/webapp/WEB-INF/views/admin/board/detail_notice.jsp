<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<c:import url="/WEB-INF/views/import/admin/header.jsp" />
<article>
<div id="board_header">
	<h1><span class="board_type" onclick="mvMenu()">공지사항</span><span class="board_pk">(${noticeVo.board_pk})</span></h1>
</div>
<div class="detail_div">
	<form action="/admin/notice/mvEdit" id="formId" method="POST" enctype="multipart/form-data" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
		<input type="hidden" id="board_pk" name="board_pk" value="${noticeVo.board_pk}"/>
		<table>
			<tr>
				<td>
					<span class="title">${noticeVo.title}</span><span class="board_type">| 공지사항</span>
					<span class="regis_date">
						<c:choose>
							<c:when test="${noticeVo.update_time==noticeVo.registration_date}">
								<fmt:parseDate value="${noticeVo.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
								<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd HH:mm"/>
							</c:when>
							<c:otherwise>
								<fmt:parseDate value="${noticeVo.update_time}" var="update_time" pattern="yyyy-MM-dd HH:mm:ss"/>
								<fmt:formatDate value="${update_time}" pattern="yyyy-MM-dd HH:mm"/>(수정됨)
							</c:otherwise>
						</c:choose>	
					</span>
					<br>
					<span class="writer down5">${noticeVo.writer}</span>
					<span class="hits"><img src="/resources/image/eye_b.png" class="icon20"/>
						<fmt:formatNumber value="${noticeVo.hits}" pattern="#,###"/> views
					</span>
					<span class="reply_num"><img src="/resources/image/speech_bubble.png" class="icon20"/>
						${noticeVo.reply_count}
 					</span> 
				</td>
			</tr>
			<c:forEach items="${boardImgList}" var="list" varStatus="status">
				<tr>
					<td class="board_img">
					<img  class="downloadImg icon20" src="/resources/image/download.png"/>
					<span class="img_name" data-id="${list.img_url}">${list.img_origin_name}</span>
					<span class="img_size"><fmt:formatNumber value="${list.img_size/1024}" pattern="0"/>KB</span>
				</td>
				</tr>
			</c:forEach>
			<tr>
				<td class="board_content">${noticeVo.content}</td>
			</tr>
			<tr>
		</table>
	</form>
<div class="reply_div">
	<form action="/admin/reply/regisOk" method="POST" id="replyForm" onsubmit="return checkNull()">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" value="${noticeVo.board_pk}" id="board_pk" name="board_pk">
		<table>
			<tr class="reply_header"><td>답변&nbsp<span class="reply_count">${noticeVo.reply_count}</span></td></tr>
			<c:forEach items="${replyList}" var="list" varStatus="status">
				<tr class="reply${list.reply_seq}">
					<td class="reply_title">
							<div>
							<span class="reply_writer">
								<c:choose>
									<c:when test="${list.user_type=='ROLE_ADMIN'}">
										마크맨
									</c:when>
									<c:otherwise>
										${list.writer}
									</c:otherwise>
								</c:choose>
							</span>	
							<div class="reply-side">
								<span class="editReply" data-seq="${list.reply_seq}">수정</span>&nbsp|&nbsp<span class="deleteReply" data-seq="${list.reply_seq}">삭제</span>
							</div>
						</div>
						<div>
							<div>
								<span class="reply_content${list.reply_seq}">${list.content}</span>
								<p class="regis_date">
											<fmt:parseDate value="${list.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
											<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd HH:mm"/>
								</p>
							</div>
						</div>
					</td>
				</tr>
			</c:forEach>
			<tr class="reply-input">
				<td>
					<textarea placeholder="답글달기" class="reply_input" name="ir1" id="ir1"></textarea>
					<div class="regis_reply" align="right">
						<span id="word-num">0</span>/500
						<button class="margin0">등록</button>
					</div>
				</td>
			</tr>
		</table>
	</form>
</div>	
</div>
<div class="detail_btn" align="right">
	<button type="button" class="edit"><img src="/resources/image/edit.png" class="icon20"/><b>수정</b></button>
	<button type="button" class="delete"><img src="/resources/image/trash.png" class="icon20"/><b>삭제</b></button>
</div>
</article>
</body>
<script type="text/javascript" src="/resources/common/js/global.js"></script>
<script>
	$('.delete').click(function(){
		var data = {};
		data["board_pk"] = $('#board_pk').val();
		confirmm("해당 게시물을 삭제하시겠습니까?").then(function(){
      		$.ajax({
      			url : "/admin/notice/deleteOk",
				type : "POST",
				data : data,
				success : function(resultData) {
					if(resultData=="success"){
						alertt("삭제가 완료되었습니다.","success").then(function(){
							location.href="/admin/noticeList";
						});
					}else{
						alertt("삭제 실패");
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\n" + "error:" + error);
				}
			});  
    	},function(){});
	});
	$('.edit').click(function(){
		var form = $("#formId");
		form.submit();
	});
	function mvMenu(){location.href="/admin/noticeList";}
	$('.downloadImg').click(function(){
		var obj = $(this).next('span');
		fileUpload(obj);
	});
	$('.img_name').click(function(){
		fileUpload($(this));
	});
	function fileUpload(obj){
		var fileFullPath = obj.data('id');
		var fileOriginName = obj.html();
		location.href="/call/callDownload.do?fileFullPath="+fileFullPath+"&fileOriginName="+fileOriginName;
	}
	function regisReply(){
		var form = $("#replyForm");
		form.submit();
	}

	$(document).on("keypress keyup",".reply_input",function(){
		var word_num = $(this).val().length;
		if(word_num>500){
			var str=$(this).val().substring(500,0);
			$(this).val(str);
		}
			$(this).next('div').children('span[id=word-num]').empty();
			$(this).next('div').children('span[id=word-num]').append(word_num);
	}); 
	$('.deleteReply').click(function(){
		var data ={};
		var reply_seq = $(this).data("seq")
		data["reply_seq"] = reply_seq;
		confirmm("댓글을 삭제하시겠습니까?").then(function(){
			$.ajax({
				url : "/admin/reply/deleteOk",
				type : "POST",
				data : data,
				success : function(resultData) {
					if(resultData=="success"){
						alertt("삭제가 완료되었습니다.","success").then(function(){
							$('.reply'+reply_seq+'').remove();//댓글 삭제		
						});
					}else{
						alertt("삭제 실패");
					}
				},
				error : function(request, status, error) {
					console.log("code:" + request.status + "\n" + "error:" + error);
				} 
			});		
		},function(){});
	});
	function checkNull(){
		if($('.reply_input').text==""){//빈값 입력 방지
			alert($('.reply_input').text)
			return false;
		}
	}
	$('.editReply').click(function(){
		var seq=$(this).data("seq");
		var reply = $('.reply'+seq+'');
		var reply_content = $('.reply_content'+seq+'').text();
		var appendStr = '<td><textarea class="reply_input" name="ir1" id="ir1">'+reply_content+'</textarea>'
						+'<div class="regis_reply" align="right"><span id="word-num">'+reply_content.length+'</span>/500'
						+'<button type="button" class="margin0	editOkReply">수정완료</button>'
						+'</div></td>';
		reply.empty();//댓글 본문 비우기
		reply.append(appendStr);	
	});
	$('.editOkReply')
</script>
</html>