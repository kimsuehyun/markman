<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="/resources/common/js/bootstrap.js"></script>
<link href="/resources/common/css/bootstrap.min.css" rel="stylesheet"> 
</head>
<body>
<!--USERINFO MODAL -->

    <c:import url="/WEB-INF/views/import/admin/header.jsp" />
        <article>
            <div id="table_header">
                <h1>문의하기</h1>                
            </div>
          	<table class="manage_table">
          		<tr>
          			<th class="w20">아이디</th>
       				<th class="w20">이름</th>
       				<th class="w25">전화번호</th>
       				<th class="w25">이메일</th>
       				<th>기능</th>
          		</tr>
          		<tr>
          			<td>qwer917</td>
          			<td>이름</td>
          			<td>0100000000</td>
          			<td>godn917@naver.com</td>
          			<td>godn917</td>
          			
          		</tr>
          	</table>
            
        </article>
  

</body>
</html>