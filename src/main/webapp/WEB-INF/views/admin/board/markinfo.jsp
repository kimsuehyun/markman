<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
    <c:import url="/WEB-INF/views/import/admin/header.jsp" />
        <article>
            <div id="table_header">
                <h1>상표정보</h1>                
            </div>
            <form id="formId">
            <input type="hidden" name="pageNumber" id="pageNumber"/>
            <input type="hidden" id="seq" name="seq"/>
            <table class="manage_table">
                <tr>
                	<th></th>
                    <th class="w50">제목</th>
                    <th class="15">작성자</th>
                    <th class="w15">작성일</th>
                    <th>조회수</th>
                </tr>
           		<c:forEach items="${markInfoList}" var="list" varStatus="status">
             	  <tr>
                		<td class="thumnailimg"><img src="${list.thumnail_img}" alt="Thumnail"></td>
                		<td class="left-td"><span class="title" onclick="location.href='/admin/notice/mvDetail${list.board_pk}'">${list.title}</span></td>
                		<td>${list.writer}</td>
                		<!--dateFormat-->
                		<td>
	                 		<fmt:parseDate value="${list.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
	                 		<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
	                 		<jsp:useBean id="toDay" class="java.util.Date" />
							<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
							<c:choose>
								<c:when test="${regisDate==today}">
									<fmt:formatDate value="${regis_date}" pattern="HH:mm"/> 
								</c:when>
								<c:otherwise>
									${regisDate}
								</c:otherwise>
							</c:choose>
						</td>
                		<td>${list.hits}</td>
             	   </tr>
             	</c:forEach>   
            </table>
            </form>
       		<div id="paging">
       			${nation.getNavigator()}
       		</div>
       		<div align="right">
			  <button type="button" onclick="location.href='/admin/markInfo/mvRegis'" class="regis_btn">
				  <img alt="pencil" src="/resources/image/pencil.png" class="icon20">글쓰기
			  </button>    
        	</div>
        </article>
    </div>
<%-- <c:import url="/WEB-INF/views/import/admin_footer.jsp" /> --%>   
<script>
    //페이지 네이션
    function listarticle(pageNumber){
 	 	 $("#formId").attr("action","/admin/noticeList/"+pageNumber+"");
  		 $("#pageNumber").val(pageNumber);
  		 $("#formId").submit();
  	}
</script>
</body>
</html>