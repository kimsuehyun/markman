<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<link rel="icon" href="/resources/image/pavicon.png">
<title>Markman</title>
</head>
<c:import url="/WEB-INF/views/import/admin/header.jsp" />
<script src="/resources/editor/naver/js/service/HuskyEZCreator.js"></script>
<article>
<div class="admin_regis_form">
	<h1>FAQ 등록</h1>
	<form action="/admin/faq/regisOk" id="formId" method="POST" onsubmit="update()" enctype="multipart/form-data" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<table id="table">
			<tr>
				<td class="w15">제목</td>
				<td><input type="text" id="title" name="title" required></td>
			</tr>
		<!--파일 첨부 기능		
			<tr>
				<td><span class="down5">파일첨부</span><button type="button" class="plus_btn btn20">+</button></td>
				<td><input type="file" class="addImg" id="img0" name="img0" accept="image/gif, image/jpeg, image/png"></td>
			</tr> -->
			<c:forEach var="i" begin="1" end="2">
				<tr class="hide">
					<td></td>
					<td><input type="file" class="addImg" id="img${i}" name="img${i}" accept="image/gif, image/jpeg, image/png"></td>
				</tr>
			</c:forEach> 
		</table>
		<textarea name="ir1" id="ir1"></textarea>
		<div align="right">
	<input type="submit" class="regis_btn" value="등록하기">
</div>
<script>
/*네이버 SmartEditor */
var editor_object = [];
var file_num = 0;
nhn.husky.EZCreator.createInIFrame({
	oAppRef: editor_object,
	elPlaceHolder: "ir1",
	sSkinURI: "/resources/editor/naver/SmartEditor2Skin.jsp?textareaName=faq",
	fCreator: "createSEditor2"
});
function update(){
	naverEditorTextAreaUpdate(editor_object, "ir1");//textarea업데이트
}	

/* 
 * 파일 첨부 기능
 $(".plus_btn").click(function(){
	if(file_num<2){//첨부파일 개수 3개로 제한
		file_num++;
		$("#img"+file_num+"").parent('td').parent('tr').removeClass("hide");
	}else	alertt("3개 이하의 파일만 첨부할 수 있습니다.")
	
});

$(document).on("change",".addImg",function(){
    ext = $(this).val().split('.').pop().toLowerCase(); //확장자
    //배열에 추출한 확장자가 존재하는지 체크
    if($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
    	resetFormElement($(this)); //폼 초기화
        alertt('이미지 파일(gif, png, jpg, jpeg)만 업로드 가능합니다');
    }else {
    	//미리보기 활성화 시
	     file = $('#bannerimg').prop("files")[0];
  		blobURL = window.URL.createObjectURL(file);
       	$('#image_preview img').attr('src', blobURL);
        $('#image_preview').css("display","block"); //업로드한 이미지 미리보기 
        $(this).slideUp(); //파일 양식 감춤   
    }
});	
 
//이미지 폼 초기화
function resetFormElement(e) {
    e.wrap('<form>').closest('form').get(0).reset(); 
    //리셋하려는 폼양식 요소를 폼(<form>) 으로 감싸고 (wrap()) , 
    //요소를 감싸고 있는 가장 가까운 폼( closest('form')) 에서 Dom요소를 반환받고 ( get(0) ),
    //DOM에서 제공하는 초기화 메서드 reset()을 호출
    e.unwrap(); //감싼 <form> 태그를 제거
}
*/
</script>
</form>
</div>
</article>
</body>
</html>