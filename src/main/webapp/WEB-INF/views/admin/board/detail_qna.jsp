<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<c:import url="/WEB-INF/views/import/admin/header.jsp" />
<script src="/resources/editor/naver/js/service/HuskyEZCreator.js"></script>
<article>
<div id="board_header">
	<h1><span class="board_type" onclick="mvMenu()">상표상ㅁㅁㅁㅁ담</span><span class="board_pk">(${qnaVo.board_pk})</span></h1>
</div>
<div class="detail_div">
	<form action="/admin/mvModify/qna" name="formId" method="POST" enctype="multipart/form-data" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
		<input type="hidden" id="board_pk" name="board_pk" value="${qnaVo.board_pk}"/>
		<table>
			<tr>
				<td>
					<span class="qna-icon q">Q</span>	
					<span class="title">${qnaVo.title}</span>
					<c:if test="${qnaVo.is_secret==1}">
						<span class="is_secret">비공개</span>
					</c:if>
					<span class="board_type">| 상표상담</span>
					<span class="regis_date">
						<fmt:parseDate value="${qnaVo.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
						<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd HH:mm"/>
					</span>
					<br>
					<span class="writer down5">${qnaVo.writer}</span>
					<span class="hits"><img src="/resources/image/eye_b.png" class="icon20"/>
						<fmt:formatNumber value="${qnaVo.hits}" pattern="#,###"/> views
					</span>
					<span class="reply_num"><img src="/resources/image/speech_bubble.png" class="icon20"/>
						${qnaVo.reply_count}
					</span>
				</td>
			</tr>
			<tr>
				<td class="board_content">${qnaVo.content}</td>
			</tr>
			<tr>
			<tr>
				<td class="detail_btn">
					<span class="modify">수정</span>&nbsp|
					<span class="delete">삭제</span> 
				</td>
			</tr>
		</table>
	</form>
<div class="reply_div">
	<form action="/admin/reply/regisOk" method="POST" id="replyForm">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" value="${qnaVo.board_pk}" id="board_pk" name="board_pk">
		<table>
			<tr class="reply_header"><td colspan="2">답변&nbsp<span class="reply_count">${qnaVo.reply_count}</span></td></tr>
			<c:forEach items="${replyList}" var="list" varStatus="status">
				<tr>
					<td class="reply_title">
							<span class="qna-icon a">A</span>
							<div>
							<span class="reply_writer">
								<c:choose>
									<c:when test="${list.user_type=='ROLE_ADMIN'}">
										<b>마크맨</b>님의 답변
									</c:when>
									<c:otherwise>
										<b>${list.writer}</b>님의 답변
									</c:otherwise>
								</c:choose>
							</span>
							<p class="regis_date">
										<fmt:parseDate value="${list.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
										<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd HH:mm"/>
							</p>
						</div>
					</td>
					<td rowspan="2" class="reply_side">
						<button type="button" class="deleteReply" data-seq="${list.reply_seq}">삭제</button>
					</td>
				</tr>
				<tr>
					<td class="reply_content">
						${list.content}
					</td>
				</tr>
			</c:forEach>
			<tr>
				<td>
					<textarea placeholder="답변달기" class="reply_input" name="ir1" id="ir1"></textarea>
				</td>
				<td class="w10">
					<button type="button" onclick="regisReply();">등록</button>
				</td>
			</tr>
		</table>
	</form>
</div>
</div>
</article>
</body>
<script type="text/javascript" src="/resources/common/js/global.js"></script>
<script>
var editor_object = [];
$('.delete').click(function(){
	var data = {};
	data["board_pk"] = $('#board_pk').val();
	confirmm("해당 게시물을 삭제하시겠습니까?").then(function(){
     	$.ajax({
      		url : "/admin/qna/deleteOk",
			type : "POST",
			data : data,
			success : function(resultData) {
				if(resultData=="success"){
					alertt("삭제가 완료되었습니다.","success").then(function(){
						location.href="/admin/qnaList";
					});
				}else{
					alertt("삭제 실패");
				}
			},
			error : function(request, status, error) {
				console.log("code:" + request.status + "\n" + "error:" + error);
			}
		});  
    },function(){});
});
function mvMenu(){
	location.href="/admin/qnaList";
}
$(document).on("click","#ir1",function(){
	nhn.husky.EZCreator.createInIFrame({
	    oAppRef: editor_object,
	    elPlaceHolder: "ir1",
	    sSkinURI: "/resources/editor/naver/SmartEditor2Skin.jsp?textareaName=ir1",
	    fCreator: "createSEditor2",
	});
}); 

$('.deleteReply').click(function(){
	var data={};
	data["reply_seq"] = $(this).data("seq");
	confirmm("답변을 삭제하시겠습니끼?").then(function(){
		$.ajax({
			url : "/admin/reply/deleteOk",
			type : "POST",
			data : data,
			success : function(resultData){
				if(resultData=="success"){
					alertt("삭제가 완료되었습니다.","success").then(function(){
					});
				}else{
					alertt("삭제 실패");
				}
			},
			error : function(request, status, error){
				console.log("code:" + request.status + "\n" + "error:" + error);
			}
		});
	});
});
function regisReply(){
	var form = $("#replyForm");
	naverEditorTextAreaUpdate(editor_object,"ir1");
	form.submit();
}
</script>
</html>