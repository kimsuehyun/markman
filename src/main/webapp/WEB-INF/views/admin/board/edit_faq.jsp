<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<link rel="icon" href="/resources/image/pavicon.png">
<title>Markman</title>
</head>
<c:import url="/WEB-INF/views/import/admin/header.jsp" />
<script src="/resources/editor/naver/js/service/HuskyEZCreator.js"></script>
<article>
<div class="admin_regis_form">
	<h1>자주묻는질문 수정</h1>
	<form action="/admin/faq/editOk" id="formId" method="POST" onsubmit="update()" enctype="multipart/form-data" >
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" name="board_pk" id="board_pk" value="${faqVo.board_pk}"/>
		<table id="table">
			<tr>
				<td class="w15">제목</td>
				<td><input type="text" id="title" name="title" required value="${faqVo.title}"></td>
			</tr>
		<%-- 첨부파일			
			<c:forEach items="${boardImgList}" var="list" varStatus="status">
				<tr>
					<td>
						<c:if test="${status.count==1}">
							<span class="down5">파일첨부</span><button type="button" class="plus_btn btn20">+</button>
						</c:if>
					</td>
					<td class="board_img">				
						<img src="/resources/image/download.png" class="icon20 downloadImg"/>	
						<span class="img_name" data-id="${list.img_url}">${list.img_origin_name}</span>
						<span class="img_size"><fmt:formatNumber value="${list.img_size/1024}" pattern="0"/>KB</span>
						<b class="delete_file" data-id="${status.count}">x</b>
					</td>
				</tr>
			</c:forEach> --%>
			<c:if test="${fn:length(boardImgList)<3}">
				<c:forEach  var="i" begin="${fn:length(boardImgList)}" end="2">
					<tr class="hide">
						<td></td>
						<td>
							<input type="file" class="addImg" id="img${i}" name="img${i}" accept="image/gif, image/jpeg, image/png">
						</td>
					</tr>	
				</c:forEach>
			</c:if>
		</table>
		<textarea name="ir1" id="ir1">${faqVo.content}</textarea>
		<div align="right">
	<input type="submit" class="regis_btn" value="수정하기">
</div>
<script>
/*네이버 SmartEditor */
var editor_object = [];
var file_num = "${fn:length(boardImgList)}";
nhn.husky.EZCreator.createInIFrame({
	oAppRef: editor_object,
	elPlaceHolder: "ir1",
	sSkinURI: "/resources/editor/naver/SmartEditor2Skin.jsp?textareaName=notice",
	fCreator: "createSEditor2"
});
			
$(".plus_btn").click(function(){
	if(file_num<3){//첨부파일 개수 3개로 제한
		$("#img"+file_num+"").parent('td').parent('tr').removeClass("hide");
		file_num++;
	}else	alertt("3개 이하의 파일만 첨부할 수 있습니다.")
	
});

$(document).on("change",".addImg",function(){
    ext = $(this).val().split('.').pop().toLowerCase(); //확장자
    //배열에 추출한 확장자가 존재하는지 체크
    if($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
    	resetFormElement($(this)); //폼 초기화
        alertt('이미지 파일(gif, png, jpg, jpeg)만 업로드 가능합니다');
    }else {
    	//미리보기 활성화 시
/*      file = $('#bannerimg').prop("files")[0];
  		blobURL = window.URL.createObjectURL(file);
       	$('#image_preview img').attr('src', blobURL);
        $('#image_preview').css("display","block"); //업로드한 이미지 미리보기 
        $(this).slideUp(); //파일 양식 감춤   */
    }
});	

//이미지 폼 초기화
function resetFormElement(e) {
    e.wrap('<form>').closest('form').get(0).reset(); 
    //리셋하려는 폼양식 요소를 폼(<form>) 으로 감싸고 (wrap()) , 
    //요소를 감싸고 있는 가장 가까운 폼( closest('form')) 에서 Dom요소를 반환받고 ( get(0) ),
    //DOM에서 제공하는 초기화 메서드 reset()을 호출
    e.unwrap(); //감싼 <form> 태그를 제거
}
function update(){
	naverEditorTextAreaUpdate(editor_object, "ir1");//textarea업데이트
}

$('.downloadImg').click(function(){
	var obj = $(this).next('span');
	fileUpload(obj);
});
$('.img_name').click(function(){
	fileUpload($(this));
});
function fileUpload(obj){
	var fileFullPath = obj.data('id');
	var fileOriginName = obj.html();
	location.href="/call/callDownload.do?fileFullPath="+fileFullPath+"&fileOriginName="+fileOriginName;
}

$(document).on("click",".delete_file",function(){//파일 삭제
	var seq = $(this).data("id")-1;
	var td = $(this).parent('td');
	td.removeClass('board_img').empty();
	td.append('<input type="file" class="addImg" id="img'+seq+'" name="img'+seq+'" accept="image/gif, image/jpeg, image/png">');
});
</script>
</form>
</div>
</article>
</body>
</html>