<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
    <c:import url="/WEB-INF/views/import/admin/header.jsp" />
        <article>
        <!--변리사 지정 div창-->
        	<div class="black_wall" id="black_wall" style="visibility:hidden"></div>
			<div class="popup" id="popup" style="visibility:hidden">
			<input type="hidden" id="searchValue" name="searchValue" value="이름">
			   	<div class="pop_header">            
			       	<h2 style="color:#f9f9f9">마크맨</h2>
			       	<img src="/resources/image/close.png" alt="close" class="popup_close" onclick="closeload()" id="close">
			    </div>
				<div class="pop_cont">            
				       	<h2><span>변리사를 지정하세요</span></h2>
				      	<!-- 	<p style="color:#555">이름을 클릭하면 경력사항을 볼 수 있습니다.</p> -->
				       	<div align="right">
				       	<br>
				       		<select class="select-subject" onchange="choice(this.value);">
				       			<option value="0">이름/아이디</option>
				       			<option value=""></option>
				       		</select>
				       		<input type="text" id="searchPatent" onkeydown="enterPatent();">
				       		<button type="button" class="searchBtn btn btn-success">검색</button>
				       	</div>
				       	<div class="popPatentList">
				       	<table class="popTable">
					       	<tr>
					       		<td class='w25'>아이디</td>
					       		<td class='w20'>이름</td>
					       		<td class="w40">이메일</td>
					       		<td>지정</td>
					       	</tr>
				      		<tbody class="hide-tbody">
					       	<c:forEach items="${patentList}" var="list" varStatus="status">
					       		<tr class="patentList" style="overflow:auto;">
					       			<td>${list.id}</td> 
					       			<td>
					       				<b>${list.name}</b>
					       			</td>
					       			<td>${list.email}</td>
					       			<td>
					       				<button class='match' data-id="${list.id}">지정</button>
					       			</td>
					       		</tr>
					       	</c:forEach>
					       	</tbody>
					       	<tbody class="append-table">
				       		</tbody>
				       	</table>
					</div>
				</div>
			</div>
		<!--div end-->
            <div id="table_header">
                <h1>상표 관리</h1>                
            </div>
            <form id="formId" method="POST">
            <input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
            <input type="hidden" name="pageNumber" id="pageNumber"/>
            <input type="hidden" name="filter_type" id="filter_type" value="${type}"/>
            <input type="hidden" name="search_type" id="search_type" value="${search_type}"/>
            <input type="hidden" name="mark_pk" id="mark_pk"/>
            <input type="hidden" name="patent_id" id="patent_id"/>
            <input type="hidden" name="group" id="group" value="${menu}"/>
	            <div id="search_user" align="right" class="w100">
	            		<div>
		            		<table class="search_table">
		            			<tr>
			            			<td class="tt w20">상표 유형</td>
			            			<td class="select_td type">
			            				<button type="button" id="all">전체</button>
				            			<button type="button" id="kor">한글</button>
				            			<button type="button" id="eng">영문</button>
				            			<button type="button" id="figure">도형</button>
			            			</td>
		            			</tr>
		            			<tr>
			            			<td class="tt">등록일</td>
			            			<td class="date">
			            				<input type="text" id="start_date" name="start_date" class="datepicker" value="${start_date}">
			            				~
			            				<input type="text" id="end_date" name="end_date" class="datepicker" value="${end_date}">
			            				<button type="button" onclick="searchDate()">검색</button>
			            			</td>
		            			</tr>
		            			<tr>
			            			<td class="tt">검색</td>
			            			<td>
			            				<select class="w20 h30" name="search" onchange="selectSearch(this.value)">
			            					<option id="none" value="none">--</option>
			            					<option id="mark" value="mark">상표명</option>
			            					<option id="applicant" value="applicant">출원인</option>
			            					<option id="patent" value="patent">변리사</option>
			            				</select>
			            				<c:choose>
			            				 	<c:when test="${search_data!='all' and search_data != null}">
								            	<input type="text" id="search_data" name="search_data" onkeydown="enter();" value="${search_data}">
								            </c:when>
								            <c:otherwise>
								            	<input type="text" id="search_data" name="search_data" onkeydown="enter();">
								            </c:otherwise>
							            </c:choose>
							           	<button type="button" class="search_btn" onclick="searchData();">검색</button>
			            			</td>
		            			</tr>
		            		</table>
	            		</div>
	            </div>
            <table class="manage_table">
	            <tr>
    	   			<th class="w15">상표명</th>
       				<th class="w10">상표 유형</th>
       				<th class="w15">출원인id</th>
       				<th class="w10">출원인</th>
       				<th class="w10">지정 변리사</th>
     				<th class="w20">등록일</th>
     				<th class="w10">진행 상태</th>
     				<th class="w10">상세 정보</th>
       			</tr>
               	 <c:forEach items="${markList}" var="mark" varStatus="status">
			   		<tr>
				   		<td class="${mark.mark_name}" id="name${status.count}">${mark.mark_name}</td>
				   		<td id="type${status.count}">${mark.type}</td>	
				   		<td>${mark.applicant_id}</td>		 	   		
						<td>${mark.applicant_name}</td>
						<td>
							<c:choose>
								<c:when test="${mark.patent_name==null}">
									<button type="button" class="match-patent" data-pk="${mark.pk}">지정</button>
								</c:when>
								<c:otherwise>
									${mark.patent_name}
								</c:otherwise>
							</c:choose>
						</td>
						<td>${mark.mark_regisDate}</td>
						<td>
							<c:choose>
								<c:when test="${mark.step=='0'}">출원 준비</c:when>
								<c:when test="${mark.step=='1'}">출원 완료</c:when>
								<c:when test="${mark.step=='2'}">등록</c:when>
								<c:when test="${mark.step=='3'}">거절</c:when>
							</c:choose>
						</td>
						<td><img class="icon25" src="/resources/image/info_w.png"></td>
				   	</tr>
				 </c:forEach>             
            </table>
            <div id="paging">
       			${nation.getNavigator()}
       		</div>
			</form>
        </article>
   <%-- <c:import url="/WEB-INF/views/import/admin/footer.jsp" /> --%>
<script src="/resources/common/js/global.js"></script>
<script src="/resources/common/js/admin.js"></script>
<script>
	$(function(){
		$(".datepicker").datepicker({
			dateFormat:"yy-mm-dd"
		});
	});
	//회원 검색
    $(document).ready(function(){
      $('.manage_table tr:even').css("backgroundColor","#fff");     // odd 홀수
      $('.manage_table tr:odd').css("backgroundColor","#F6F6F6");   // even 짝수
	 //현재 필터링 하고 있는 버튼 값을 받아온다.
      var type = "${type}";//상표유형
      var start_date = "${start_date}";//등록일
      var end_date = "${end_date}";//등록일
      var search_type = "${search_type}";//검색 유형
      var search_data = "${search_data}";//검색 단어
      if(type!="")	$('.type  #'+type+'').addClass('highlight'); 
      if(start_date!="")	$('#start_date').val(start_date);
      if(end_date!="")	$('#end_date').val(end_date);
      if(search_type!="")	$('#'+search_type+'').attr("selected","selected")
      if($('#who').val()!='all'){//전체 리스트가 아닌경우(=검색리스트)
    		for(var i = 1; i<=${fn:length(userList)} ; i++){
    			var mark_id = $('#id'+i+'').text().replace(search,"<em>"+search+"</em>");
    			var mark_name = $('#name'+i+'').text().replace(search,"<em>"+search+"</em>");
    			$('#id'+i+'').empty();
    			$('#id'+i+'').append(mark_id);
    			$('#name'+i+'').empty();
    			$('#name'+i+'').append(mark_name);
    		}	
      }
    });
    function enter(){//검색창에서 엔터키를 눌렀을 때
    	if(window.event.keyCode==13){
    		searchData();
    	}
    }
    function listarticle(pageNumber){//페이지 네이션
 	 	 $("#formId").attr("action","/admin/markList/"+pageNumber+"");
  		 $("#pageNumber").val(pageNumber);
  		 $("#formId").submit();
  	}
    $('.select_td button').click(function(){//필터 버튼 누를 때 효과
    	$(this).addClass('highlight');
    	$(this).siblings().removeClass("highlight");
    });
    $('.type button').click(function(){
    	var type = $(this).attr('id');
    	$('#formId').attr("action","/admin/markList");
    	$('#filter_type').val(type);
    	$('#formId').submit();
    });
    function selectSearch(obj){//검색항목 선택
    	var search_type = obj;
    	$('#search_type').val(obj);
    }
    function searchData(){//검색
    	var search_type = $('#search_type').val();
    	if(search_type==""||search_type=="none"){
    		alert("검색 유형을 선택하십시오.");
    	}
    	else{
	    	$('#formId').attr("action","/admin/markList");
	    	$('#formId').submit();
    	}
    }
    function searchDate(){
    	var start_date = $('#start_date').val();
    	var end_date = $('#end_date').val();
    	if(start_date==""||end_date==""){
    		alert("날짜를 선택하십시오.");
    	}else{
    		$('#formId').attr("action","/admin/markList");
	    	$('#formId').submit();
    	}
    }
    
</script>
</body>
</html>