<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<c:import url="/WEB-INF/views/import/admin/header.jsp"/>
<article>
	<div id="table_header">
		<h1>배너 등록</h1>
	</div>
	<form action="/admin/banner/regisOk" id="formId" name="formId" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" name="banner_name" id="banner_name"/>
		<table id = "simple_td">
			<tr>
				<td class="t_name">위치</td>
				<td>
					<select id="loc" name="loc" onchange="setLocation(this.value)">
						<option>--</option>
						<c:forEach items="${bannerName}" var="vo">
							<option value="${vo.getName()}">${vo.getKor_name()}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td class="t_name">이미지</td>
				<td><button type="button">내 pc</button><input type="file" id="bannerimg" name="bannerimg" class="add_img_btn"></td>
			</tr>
			<tr>
				<td class="t_name"></td>
				<td>
					<div id="image_preview" style="display:none;">
						<img src="#"/>
						<a id="delete_bannerimg" class="delete_img">[삭제]</a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="t_name">연결 링크</td>
				<td><input type="text" id="link" name="link" placeholder="예) http://markman.co.kr/admin/bannerList"></td>
			</tr>
		</table>
		<div align="right" style="z-index:20">
			<button type="button" class="regis_btn">등록하기</button>
		</div>
	</form>
</div>
</article>
</body>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script>
	$('#bannerimg').on('change', function() {
	    ext = $(this).val().split('.').pop().toLowerCase(); //확장자
	    //배열에 추출한 확장자가 존재하는지 체크
	    if($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
	    	resetFormElement($(this)); //폼 초기화
	        window.alert('이미지 파일이 아닙니다! (gif, png, jpg, jpeg 만 업로드 가능)');
	    }else {
	        file = $('#bannerimg').prop("files")[0];
	  		blobURL = window.URL.createObjectURL(file);
	       	$('#image_preview img').attr('src', blobURL);
	        $('#image_preview').css("display","block"); //업로드한 이미지 미리보기
	        $(this).slideUp(); //파일 양식 감춤
	    }
	});
	$('#delete_bannerimg').bind('click', function() {
	    resetFormElement($('#bannerimg')); //전달한 양식 초기화
	    $('#bannerimg').slideDown(); //파일 양식 보여줌
	    $(this).parent().slideUp(); //미리 보기 영역 감춤
	    return false; //기본 이벤트 막음
	});
	//이미지 폼 초기화
	function resetFormElement(e) {
	    e.wrap('<form>').closest('form').get(0).reset();
	    //리셋하려는 폼양식 요소를 폼(<form>) 으로 감싸고 (wrap()) ,
	    //요소를 감싸고 있는 가장 가까운 폼( closest('form')) 에서 Dom요소를 반환받고 ( get(0) ),
	    //DOM에서 제공하는 초기화 메서드 reset()을 호출
	    e.unwrap(); //감싼 <form> 태그를 제거
	}
	function setLocation(obj){//배너 위치 지정
		$('#banner_name').val(obj);
		var options = {
		        success      : ajaxAfter  // ajaxSubmit 후처리 함수
		};
		var form  = $("#formId");
		form.attr("method","post");
		form.attr("action","/admin/banner/check");
		form.ajaxSubmit(options);
	}

	$('.regis_btn').click(function(){
		var form = $('#formId');
		var file = $('#bannerimg').val();
		var location = $('#loc').val();
		if(file==""){
			alertt("추가한 이미지가 없습니다");
		}else if(location=="--"){
			alertt("지정된 위치가 없습니다.");
		}else{
			form.attr("action","/admin/banner/regisOk");
			form.submit();
		}
	});

	function ajaxAfter(resultData, statusText, xhr, $form){
		if (statusText == "success"){
		    // ajax 통신 성공 후 처리영역
		    if (resultData>0 ){
		    	alertt("이미 등록된 배너입니다.");
		    	$('#banner_name').val();
		    	$('#loc').val("--");
		    }else {
		    }
		}else{
			 // ajax 통신 실패 처리영역
			alertt("서버와의 동신에 실패하였습니다.	에러내용 : "+statusText);
		}
	}
</script>
</html>