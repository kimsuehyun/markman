<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
    <c:import url="/WEB-INF/views/import/admin/header.jsp" />
        <article>
            <div id="table_header">
                <h1>배너 설정</h1>                
            </div>
            <form action="/admin/banner/mvRegis" id="formId">
          		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
	            <input type="hidden" name="pageNumber" id="pageNumber"/>
	            <input type="hidden" id="seq" name="seq"/>
	            <button class="plus_btn btn30">+</button>
	            <table class="manage_table" style="table-layout:fixed;">
	                <tr>
	                    <th class="w10">위치</th>
	                    <th class="w65">이미지</th>
	                    <th>등록인</th>
	                    <th class="w15">등록일</th>
	                </tr>
	                <c:forEach items="${bannerList}" var="list" varStatus="status">
	                 	<tr>
	                 		<td>${list.getKor_name()}</td>
	                 		<td><img class="banner_img" src="${list.getBanner_img()}"></td>
	                 		<td>${list.getRegistrant()}</td>
	                 		<td>
	                 			<!--dateFormat-->
	                 			<fmt:parseDate value="${list.getRegistration_date()}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
	                 			<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
	                 			<jsp:useBean id="toDay" class="java.util.Date" />
								<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
								<c:choose>
									<c:when test="${regisDate==today}">
										<fmt:formatDate value="${regis_date}" pattern="HH:mm"/> 
									</c:when>
									<c:otherwise>
										${regisDate}
									</c:otherwise>
								</c:choose>
	       					</td>         		
	                 	</tr>
	                </c:forEach>
	            </table>
            </form>
       		<div id="paging">
       			${nation.getNavigator()}
       		</div>
        </article>
    </div>
<%-- <c:import url="/WEB-INF/views/import/admin_footer.jsp" /> --%>   
<script>
    $(document).ready(function(){
      $('.manage_table tr:even').css("backgroundColor","#fff");     // odd 홀수
      $('.manage_table tr:odd').css("backgroundColor","#F6F6F6");   // even 짝수
    }); 
    //페이지 네이션
    function listarticle(pageNumber){
 	 	 $("#formId").attr("action","/admin/bannerList/"+pageNumber+"");
  		 $("#pageNumber").val(pageNumber);
  		 $("#formId").submit();
  	}
</script>
</body>
</html>