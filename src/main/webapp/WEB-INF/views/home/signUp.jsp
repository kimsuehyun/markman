<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<c:import url="/WEB-INF/views/import/header.jsp"/>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="/resources/common/js/jquery-3.1.0.min.js" type="text/javascript"></script>
<link href="/resources/common/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="/resources/common/css/join.css"/>
<link href="/resources/common/css/style.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
<form action = "/signUp" id = "signUpForm" method = "POST" onsubmit="return signUp();" enctype="multipart/form-data">
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<input type="hidden" id="user_type" name="user_type" value="ROLE_APPLICANT">
<input type="hidden" id="account_bank" name="account_bank">
<input type="hidden" id="profile" name="profile" value="profile_img">
<input type="hidden" id="regis" name="regis" value="patent_regis_img">

	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 tables_wrap">
				<div class="height_boxs"></div>
				<h2>회원가입</h2><br>
				<button class="btn btn-info people_btn ">출원인</button>
				<button class="btn people_btn">변리사</button>
				<div class="col-lg-12 img_box de if_patent">
					<div class="col-lg-6 col-lg-offset-3 text-center img_wrap">
						<span class="btn btn-default btn-file  col-lg-1 col-xs-12 h100 spans " style="padding:0px !important">
							<div class="h25"></div>

						</span>

					</div>
				</div>
				<table class=" table col-lg-12  join_tables">
					<tr>
						<td class="col-lg-1 col-xs-3 text-center" >이름 </td>
						<td class="col-lg-2 "> <input type="text" id="name" class="form-control col-xs-12 " name="name" placeholder="실명"></td>
						<td class="col-lg-4  "><span id="name_stts" class="stts"></span></td>
					</tr>
					<tr>
						<td class="col-lg-1 text-center">아이디 </td>
						<td class="col-lg-5"> <input type="text" id="id" name="id" class="form-control"></td>
						<td class="col-lg-4"><span id="id_stts" class="stts"></span></td>
					</tr>
					<tr>
						<td class="col-lg-1 text-center">비밀번호 </td>
						<td class="col-lg-5"><input type="password" id="pw" name="pw" class="form-control"></td>
						<td class="col-lg-4"><span id="pw_stts" class="stts"></span></td>
					</tr>

					<tr>
						<td class="col-lg-1 text-center">비밀번호 확인 </td>
						<td class="col-lg-5"><input type="password" placeholder="비밀번호 재확인" class="form-control" id="re_pw" onkeyup="comparePw()">	</td>
						<td class="col-lg-4"><span id="re_pw_stts" class="stts"></span></td>
					</tr>
					<tr>
						<td class="col-lg-1 text-center">이메일 주소 </td>
						<td class="col-lg-5"><input type="text" id="email" name="email" class="form-control"></td>
						<td class="col-lg-4"><span id="email_stts" class="stts"></span></td>
					</tr>
					<tr>
						<td class="col-lg-1 text-center">휴대 전화 번호 </td>
						<td class="col-lg-5"><input type="text" id="cell_num" name="cell_num" class="form-control"></td>
						<td class="col-lg-4"><span id="cell_num_stts" class="stts"></span></td>
					</tr>
					<tr>
						<td class="col-lg-1 text-center">유선 번호(선택)
						</td>
						<td class="col-lg-3 ">
						<select class="col-lg-5 col-xs-5 form-control" style="width: auto !important">
								<option>02</option>
								<option>051</option>
								<option>053</option>
								<option>032</option>
								<option>062</option>
								<option>042</option>
								<option>052</option>
								<option>044</option>
								<option>031</option>
								<option>033</option>
								<option>043</option>
								<option>041</option>
								<option>063</option>
								<option>061</option>
								<option>054</option>
								<option>055</option>
								<option>064</option>
							</select>
						<input  style="width:auto !important" type="text" id="tel_num" name="tel_num" class="padding_none   col-lg-6  col-xs-6 form-control">
						</td>
						<td class="col-lg-3"><span id="tel_num_stts" class="stts"></span></td>
					</tr>
					<tr style="border-bottom:2px solid #555">
						<td class="col-lg-1 text-center">우편번호</td>
						<td colspan="2">
							 <input type="text" name="addr1" id="addr1" value="${uv.getAddr1()}" placeholder="우편번호" readonly="readonly"
							 data-daum="addr1" style="width:30% !important" class="col-lg-5 col-xs-5 form-control">
							 <input type="button" style="width:auto !important;float:left" class="col-lg-5 col-xs-5 form-control  addrSearchBtn "value="우편번호검색" style=" margin-top: 5px; width:146px; cursor:pointer;" >
							<span id="addr_stts" class="stts"></span>
							 <input type="text" name="addr2" id="addr2" value="${uv.getAddr2()}" class="fl w40  form-control " placeholder="주소"
					     	readonly="readonly" data-daum="addr2" style="float:left; ">

						     <div id="layer" style="display: none; position: fixed; overflow: hidden; z-index: 1; -webkit-overflow-scrolling: touch;">
						     	<img src="//t1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer" style="cursor: pointer; position: absolute; right: -3px; top: -3px; z-index: 1" onclick="closeDaumPostcode()" alt="닫기 버튼">

						     </div>
					     	<br><br>
					     	 <input type="text" name="addr3" id="addr3" value="${uv.getAddr3()}" class="fl w40  form-control" placeholder="상세주소"
						     data-daum="addr3" style="width:auto !important;float:left ">

					     </td>
				    </tr>
				    </table>
				    <table class="if_patent table col-lg-12  join_tables">
				    	<tr>
				    		<td class="col-lg-1 text-center">변리사 번호</td>
				    		<td  class="col-lg-5">
				    			<input type="text" id="patent_num" name="patent_num" class="form-control">
				    		</td>
				    		<td>
				    		</td>
				    	</tr>
					    <tr>
					    	<td class="col-lg-1 text-center">
					    	변리사 등록증<br>
					    	사본
					    	</td>
					    	<td class="col-lg-5 ">
					    		<input type="file" class="form-control" name="patent_regis_img">
					    	</td>
					    	<td class="col-lg-3">
					    	</td>
					    </tr>
					    <tr>
					    	<td class="col-lg-1 text-center">
					    		경력기술
					    	</td>
					    	<td colspan="2">
					    		<textarea class="form-control" id="introduce" name="introduce"></textarea>
					    	</td>
					    </tr>
					    <tr>
					    	<td class="col-lg-1 text-center">계좌번호</td>
					    	<td>
						  		<select class="form-control col-lg-6 col-xs-12" style="width:auto !important" onchange="selectAccountBank(this.value)">
						            <option value="" selected>은행 선택 </option>
						            <option value="국민은행">국민은행</option>
						            <option value="우리은행">우리은행</option>
						            <option value="신한은행">신한은행</option>
						            <option value="하나은행">하나은행</option>
						            <option value="외환은행">외환은행</option>
						            <option value="SC제일은행">SC제일은행</option>
						            <option value="한국씨티은행">한국씨티은행</option>
						            <option value="부산은행">부산은행</option>
						            <option value="대구은행">대구은행</option>
						            <option value="경남은행">경남은행</option>
						            <option value="광주은행">광주은행</option>
						            <option value="전북은행">전북은행</option>
						            <option value="제주은행">제주은행</option>
						            <option value="농협은행">농협은행</option>
						            <option value="기업은행">기업은행</option>
						            <option value="산업은행">산업은행</option>
						            <option value="수출입은행">수출입은행</option>
						            <option value="수협은행">수협은행</option>
						         </select>
						         <input type="text" class="form-control"  name="account_num" placeholder="계좌번호" style="width:65% !important">
					    	</td>
					    	<td>
					    	</td>
					</tr>
				</table>
				<input type="submit" value="가입하기" class="pull-right btn btn-info">
			</div>
		</div>
	</div>
</form>
<article>
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>
<script src ="/resources/common/js/daumApiAddrSearchJs.js"></script>
<script src ="/resources/common/js/global.js"></script>
</body>
<script>
	
	var w =  $(document).width();
	var obj_li;
	var i = 0;
	$(".spans").on("click",function(e){

		if(i=="1"){
		}else{
		 i = i+1;
		var tag =
				"<div class='col-lg-12  imgss col-sm-12 col-md-12 col-xs-12' style='clear:both;position:relative;width:100%;height:100%;padding:0px !important'>"
					+"<input type='file'  class='inputs_imgs"+i+" inputs_imgs '  name='profile_img' >"
					+"<img  src='' class='change_img"+i+" change_img'>"
					+"<div class='x_btn' style='position:absolute;top:0%;'>"
						+"<button style='border:none;bakcground:rgba(0,0,0,0);width:100%;font-size:2vh'>x</button>"
					+"</div>"
			   +"</div>"
			obj_li =  $(".spans").prepend(tag);

				 $(".inputs_imgs"+i).trigger( "click");
				 $(".inputs_imgs"+i).on("change",function(e){
					 var files  = e.target.files[0];
	 				 var reader = new FileReader();
	 				reader.onload = function(e){
						$(".change_img"+i).attr('src',e.target.result)
	 				}
	 				reader.readAsDataURL(files);

				 })
		}
	})

	$(document).on("click",".x_btn",function(){
		alert("delete");
		i = i -1;
		alert(i);
		$(this).parent("div").remove();

	});




 	$('#name').blur(function(){
		checkName();
	});
 	$('#email').blur(function(){
		checkEmail();
	});
 	$('#id').blur(function(){
		checkId();
	});
 	$('#pw').blur(function(){
		checkPw();
	});
 	$('#re_pw').blur(function(){
 		comparePw();
 	})
 	$('#cell_num').blur(function(){
 		checkCellNum();
 	});
 	$('#tel_num').blur(function(){
 		checkTelNum();
 	});
	function signUp(){//회원가입 입력창 전체
		if(checkName()==false||
				checkId()==false||
				checkPw()==false||
				comparePw==false||
				checkEmail()==false||
				checkCellNum()==false||
				checkTelNum()==false||
				checkAddr()==false
			){
			return false;
		}else{
			start_loading()
			end_loading()
		}
			
	}
	function checkName(){//이름 검사
 		var name = $('#name').val();
		var element = $('#name_stts');
		var appendStr = '';
		var flag = true;
		if(name==''){
			appendStr = '필수 입력 항목입니다.';
			flag =  false;
		}else{
			//이름 형식 검사
	 		var reg_exp = new RegExp("^[가-힣]+$","g");
			var match = reg_exp.exec(name);
			if(match==null||name.length>5){
				appendStr = '2~4자의 한글만 사용 가능합니다';
				flag =  false;
			}
		}
		element.empty();
		element.append(appendStr);
		return flag;
	}
	function checkEmail(){//이메일 검사
		var email = $('#email').val();
		var reg_exp = new RegExp(/^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i);
		var match = reg_exp.exec(email);
		var element = $('#email_stts');
		var appendStr="";
		var flag = true;
		if(email==''){
			appendStr = '필수 입력 항목입니다.';
			flag = false;
		}else{
			if(match==null){
				appendStr = '입력하신 이메일이 정확한지 확인하여 주세요.';
				flag = false;
			}
		}
		element.empty();
		element.append(appendStr);
		return flag;
	}
	function checkId(){//아이디 검사
		var id = $('#id').val();
		var element = $('#id_stts');
		var appendStr = '';
		var flag = true;
		var reg_exp = new RegExp("^[a-z0-9]+$","g");
		var match = reg_exp.exec(id);
		if(id==''){
			appendStr = '필수 입력 항목입니다.';
			flag =  false;
		}else{
			if(match==null||id.length<5||id.length>20){
				appendStr = '5~20자의 영문 소문자, 숫자만 사용 가능합니다.';
				flag =  false;
			}else{
				checkSameId(id);
			}
		}
		element.empty();
		element.append(appendStr);
		return flag;
	}
	function checkPw(){
		var pw = $('#pw').val();
		var element = $("#pw_stts");
		var appendStr = '';
		var flag = true;
		if(pw==''){
			appendStr = '필수 입력 항목입니다.';
			flag =  false;
		}else {
			if(pw.length<5||pw.length>20){
				appendStr = '5~20자의 영문 대 소문자,숫자, 특수문자를 사용하세요.';
				flag = false;
			}
		}
		element.empty();
		element.append(appendStr);
		return flag;
	}
	function comparePw(){//입력한 비밀번호와 재입력한 비밀번호 일치 여부
		var pw= $('#pw').val();
		var re_pw= $('#re_pw').val();
		if(pw!=re_pw){
			$('#re_pw_stts').empty();
			$('#re_pw_stts').append('비밀번호가 일치하지 않습니다.');
			return false;
		}else{
			$('#re_pw_stts').empty();
		}
	}
	function checkCellNum(){
		var cellLen = $('#cell_num').val().length;
		var flag = "";
		var appendStr = "";

		if(cellLen==0){
			appendStr = "필수 항목입니다.";
			flag = false;
		}else{
			if(cellLen<11|cellLen>13){
				appendStr = "입력하신 핸드폰 번호가 정확한지 확인하여 주세요.";
				flag = false;
			}
		}
		$('#cell_num_stts').empty();
		$('#cell_num_stts').append(appendStr);
	}
	function checkTelNum(){
		var telLen = $('#tel_num').val().length;
		if(telLen<7|telLen>8&&telLen>0){
			$('#tel_num_stts').empty();
			$('#tel_num_stts').append('입력하신 유선 번호가 정확한지 확인하여 주세요.');
			return false;
		}else{
			$('#tel_num_stts').empty();
		}
	}
	function checkAddr(){//주소 입력 여부 확인
		var addr1 = $('#addr1').val();
		var addr2 = $('#addr2').val();
		var addr3 = $('#addr3').val();
		if(addr1==''||addr2==''||addr3==''){
			$('#addr_stts').empty();
			$('#addr_stts').append('필수 입력 항목입니다.');
			return false;
		}else{
			$('#addr_stts').empty();
		}
	}
	function checkSameId(id){
    	var data = {};
        data["id"] = id;
        $.ajax({
    	    url : "/checkSameId",
    	    type : "POST",
    	    data : data,
    	    success: function(resultData) {
    	    	appendStr = "";
    	    	if(resultData==0)	appendStr = "사용가능한 아이디 입니다.";
    	    	else	appendStr = "이미 등록된 아이디 입니다.";

    	    	$('#id_stts').empty();
    	    	$('#id_stts').append(appendStr);
    	    },
    	    error:function(request,status,error){
    	        alertt("code:"+request.status+"\n"+"error:"+error);
    	    }

    	});
    }
	var cellNum = document.getElementById('cell_num');
	cellNum.onkeyup = function(event){
	    event = event || window.event; //잘 모르는 부분
	    var _val = this.value.trim(); //trim?
	    this.value = autoHypenCell(_val) ;
	}
	var telNum = document.getElementById('tel_num');
	telNum.onkeyup = function(event){
	    event = event || window.event; //잘 모르는 부분
	    var _val = this.value.trim(); //trim?
	    this.value = autoHypenTel(_val) ;
	}
	function autoHypenCell(str){
        str = str.replace(/[^0-9]/g, '');
        var tmp = '';
        if( str.length < 4){
            return str;
        }else if(str.length < 7){
            tmp += str.substr(0, 3);
            tmp += '-';
            tmp += str.substr(3);
            return tmp;
        }else if(str.length < 11){
            tmp += str.substr(0, 3);
            tmp += '-';
            tmp += str.substr(3, 3);
            tmp += '-';
            tmp += str.substr(6);
            return tmp;
        }else{
            tmp += str.substr(0, 3);
            tmp += '-';
            tmp += str.substr(3, 4);
            tmp += '-';
            tmp += str.substr(7);
            return tmp;
        }
        return str;
    }
	function autoHypenTel(str){
        str = str.replace(/[^0-9]/g, '');
        var tmp = '';
        if( str.length < 4){
            return str;
        }else{
            tmp += str.substr(0, 3);
            tmp += '-';
            tmp += str.substr(3);
            return tmp;
        }
        return str;
    }

	$('.people_btn').click(function(){
		var user_type = $(this).text();
		var type="";
		//$(this).css({"background":"#31b0d5","color":"#fff"}).siblings("button").css({""})
		$(this).addClass("btn btn-info").siblings("button").removeClass("btn btn-info").addClass("btn")
		if(user_type=="출원인"){
			type="ROLE_APPLICANT";
			$('.if_patent').hide();
		}else if(user_type=="변리사"){
			type="ROLE_PATENT";
			$(".if_patent").show();
		}
		$('#user_type').val(type);
	});

	function selectAccountBank(obj){
		$('#account_bank').val(obj);
	}
</script>
</html>