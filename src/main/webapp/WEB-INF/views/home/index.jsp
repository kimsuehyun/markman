<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description=" Content="상표등록 마크맨 메인페이지">
<title>Markman</title>
<c:import url="/WEB-INF/views/import/header.jsp"></c:import>

<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--css-->
<link rel="stylesheet" href="/resources/common/css/main.css"/>
<link rel="stylesheet" href="/resources/common/css/style.css"/>

<!-- 부트스트랩 -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
<!-- 마크맨 main  <script src="/resources/common/js/main.js"></script> -->
<!--loading -->
<script src="/resources/common/js/global.js"></script>
</head>
<body>
<style>
	*{marign:0 auto !important;padding:0 !important;}
	.cu_pinter{cursor:pointer;}
	.box_over{overflow:hidden}
	.photo_img{transition:0.5s;width:100% !important;height:100% !important}
	.photo_img:hover{transform: scale(1.2,1.2)}
	 
	 
	 @media (max-width:560px){
	 	.slide_text1 > div{font-size:3.5vh}
	 	.slide_text1 > div:nth-child(2){font-size:1.8vh}
	 }
</style>

<section class="slide">
	<div class="width">
   		<ul>
       		<li>
       			<div class="slide_back">
       				<img src="/resources/images/[0]index_banner1_1.png" class="ba1">
       				<img src="/resources/images/[0]index_m_banner1_2.png" class="ba1-1" style="display:none">
       			</div>
       			<div class="slide_img1">
       				<img src="/resources/images/[0]index_banner1_2.png">
       			</div>
       			<div class="slide_text1">
       				<div class="slide_text_title">
       					10분 만에 작성하고<br>
       					하루 만에 출원!
       				</div>
       				<div>마크맨과 함께 쉽고 빠르게 출원하세요!</div>
       				<br><br>
       				<button class="btn_se" onclick="location.href='/applicant/mark/mvApply'" >바로 출원하러 가기</button>
       			</div>
       		</li>
       		
           	<li>
           		<div class="slide_back">
       				<img src="/resources/images/img_01-2.png" class="ba1">
					<img src="/resources/images/banner_markman_bg.png" class="ba1-1" style="display:none">
       			</div>
       			<div class="slide_img">
       				<img src="/resources/images/img_01-3.png">
       			</div>
       			<div class="slide_text">
       				<div class="slide_text_title">
       					상표전문가 마크맨<br>
       					24시간 마크중!
       				</div>
       				<div>스마트한 온라인 출원서비스,마크맨</div>
       				<br><br>
       				<button class="btn_se" onclick="location.href='/common/serviceInfo'">서비스 안내 보러가기</button>
       			</div>
           	
           	</li>
           	<li>
           		<div class="slide_back">
       				<img src="/resources/images/[0]index_banner3_1.png" class="ba1">
       				<img src="/resources/images/[0]index_m_banner3_2.png" style="display:none" class="ba1-1">
       			</div>
       			<div class="slide_img3">
       				<img src="/resources/images/[0]index_banner3_2.png">
       			</div>
       			<div class="slide_text3">
       				<div class="slide_text_title">
       					상표 출원 수수료<br>
       					55,000
       				</div>
       				<div>스마트한 가격,역시 마크맨!</div>
       				<br><br>
       				<button class="btn_se"  onClick="location.href='/common/costInfo'">비용 안내 보러가기</button>
       			</div>
           	</li>
       </ul>
       <div class="pos visible-lg-block">
		 	<div>
		 		<button id="pos0" onclick="slide('',0)">바로 출원하기</button>
		 	</div>
		 	<div>
		 		<button id="pos1" onclick="slide('',1)">마크맨 서비스</button>
		 		
		 	</div>
		 	<div>
		 		<button id="pos2" onclick="slide('',2)">최저가 상표출원</button>
		 		
		 	</div>
      </div>
      <div class="pos2 hidden-lg">
		 	<div>
		 		<button id="pos0" onclick="slide('',0)"><img src="/resources/images/[0]index_icon1.png"></button>
		 	</div>
		 	<div>
		 		<button id="pos1" onclick="slide('',1)"><img src="/resources/images/[0]index_icon2.png"></button>
		 		
		 	</div>
		 	<div>
		 		<button id="pos2" onclick="slide('',2)"><img src="/resources/images/[0]index_icon3.png"></button>
		 	</div>
      </div>
   </div>
</section>		
<div class="container-fluid">
	
		<section id="menu1" class="col-lg-10 col-lg-offset-1 col-xs-12">
			<div class="menu1_title w100 h15">
				<div class="w25 h100">
					<img src="/resources/images/img_26.png">
				</div>
				<div class="col-lg-6  col-md-12 col-xs-12 h100 title_text">
					<img src="/resources/images/img_15.png" alt="bener" title="bener" class="img-responsive col-xs-1">
					<div>
						<p>빠른 상표출원을 원하는 당신의 고민은?</p>
						<p>마크맨이 추천하는 상표출원전략</p>
					</div>
				</div>
				<div class="w25 h100">
					<img src="/resources/images/img_23.png">
				</div>	
			</div>
			
			<div class="left_be w13 h75" >
				<img src="/resources/images/img_25.png">
			</div>
			<div class="w74 h75 main_content_be" >
				<div class="col-lg-4 col-md-12 ">
					<div class="col-lg-11  text_boer" onClick="location.href='/common/biginnerServiceInfo'">
						<div class="main_be_img_box">
							<img src="/resources/images/text_hover_box1.png" class="img-responsive">
							<img src="/resources/images/img_05.png" class="img-responsive">
									
						</div>
						<div class="card1">
							<div>
								<p>처음 상표를 출원하는,</p>
								<p class="card_title">스타터Starter</p>
								마크맨의 꼼꼼하고 친절한 1:1상담 받아보세요.<br>
								상표 출원의 기본적인 내용부터<br>
								출원의 전체 과정에 대해서도 함께 알아봅시다!
							</div>
						</div>
					</div>
					<div class="col-lg-11 text_boer2" onClick="location.href='/common/biginnerServiceInfo'">
						<div>
							<img src="/resources/images/user_1.png " class="img-responsive" >
						</div>
						<div>
							<img src="/resources/images/user_1-1.png" class="img-responsive">
						</div>
					</div>
					
					
					<div class="col-lg-1 width_var hidden-sm hidden-xs hidden-md"></div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="col-lg-11 col-xs-12 center_var text_boer" onClick="location.href='/common/seniorInfoChoice'">
						<div class="main_be_img_box">
							<img src="/resources/images/text_hover_box3.png" class="img-responsive">
							<img src="/resources/images/img_06-1.png" class="img-responsive">		
						</div>
						<div class="card2">
							<div>
								<p>더 상세한 내용을 원하는,</p>
								<p class="card_title">어드밴스Advanced</p>
								여기저기 검색하느라 시간 낭비말고,<br>
								마크맨에서 스마트한 상표 출원을!<br>
								저렴한 가격에 상표 출원 노하우를 얻어가세요.
							</div>
						</div>
						
					</div>
					<div class="col-lg-11 text_boer2" onClick="location.href='/common/seniorInfoChoice'">
						<div>
							<img src="/resources/images/user_2.png" class="img-responsive">
						</div>
						<div>
							<img src="/resources/images/user_2-2.png" class="img-responsive">
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-12">
					<div class="col-lg-1 width_var hidden-sm hidden-xs hidden-md"></div>
					<div class="col-lg-11 text_boer " onClick="location.href='/common/master'">
						<div class="main_be_img_box">
							<img src="/resources/images/text_hover_box2.png" class="img-responsive">
							<img src="/resources/images/img_07-1.png" class="img-responsive">		
						</div>
						<div class="card3">
							<div>
								<p>상표 출원서를 숙지한,</p>
								<p class="card_title">마스터Master</p>
								상표 출원에 이미 일가견이 있는 당신.<br>
								마크맨의 진가를 알아보시는군요!<br>
								스마트한 가격으로 쉽고 빠르게 출원하세요.
							</div>
						</div>
					</div>
					<div class="col-lg-11 text_boer2" onClick="location.href='/common/master'">
						<div>
							<img src="/resources/images/user_3.png" class="img-responsive">
						</div>
						<div>
							<img src="/resources/images/user_3-3.png" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
			
			<div class="right_be w13 h75">
				<div class="w100 h75"></div>
				<img src="/resources/images/img_24.png">
			</div>
		</section>
		<section id="menu2" class="col-lg-10 col-lg-offset-1 col-xs-12" style="clear:both">
			<div class="col-xs-12 col-sm-12 ">
				<div class=" w74 main_content_be2">
					<div class="col-lg-5 col-xs-12 col-sm-12">
						<div class="menu2_title col-lg-12 col-xs-12 col-sm-12">
							<div class="w20" style="height:10vh">
								<img src="/resources/images/img_16.png" alt="icon" title="icon"  class="img-responsive">
							</div>
							<div class="w55" style="height:10vh">
								<p>빠른 1:1상담이 가능합니다.</p>
								<p>상표 상담</p>
							</div>
							<div class="w25" style="height:10vh">
								<div class="w100 h60"></div>
								<button class="view_btns h40 w100 pull-right" onclick="mvPage('counseling')">보러가기&#9654</button>
							</div>
						</div>
						<div class="height_box"></div>
						<table class="table col-md-12">
							<tr>
								<th class="w40">제목</th>
								<th class="w25">작성자</th>
								<th class="w35">일시</th>
							</tr>
								<c:forEach items="${counselingList}" var="list" end="5">
								<tr onclick="location.href='/common/counseling/mvDetail?pk=${list.board_pk }'">	
									<td>${list.title}</td>
									<td>${list.writer}</td>
									<!--dateFormat-->
			                 		<fmt:parseDate value="${list.registration_date}" var="regis_date" pattern="yyyy-MM-dd HH:mm:ss"/>
			                 		<fmt:formatDate value="${regis_date}" pattern="yyyy-MM-dd" var="regisDate" scope="request"/> 
			                 		<jsp:useBean id="toDay" class="java.util.Date" />
									<fmt:formatDate value="${toDay}" pattern="yyyy-MM-dd" var="today" scope="request"/>
									<c:choose>
										<c:when test="${regisDate==today}">
											<td><fmt:formatDate value="${regis_date}" pattern="HH:mm"/></td>
										</c:when>
										<c:otherwise>
											<td>${regisDate}</td>
										</c:otherwise>
									</c:choose>				   			
								</tr>							
							</c:forEach>
						</table>
					</div>
					
					<div class="col-lg-2" style="height:100%"></div>
					
					<div class="col-lg-5 col-xs-12 col-sm-12" >
						<div class="menu2_title col-lg-12 col-xs-12 col-sm-12">
							<div class="w20" style="height:10vh">
								<img src="/resources/images/img_17.png" alt="icon" title="icon"  class=" img-responsive">
							</div>
							<div class="w55" style="height:10vh">
								<p>궁금한 사항들을 한 눈에!</p>
								<p>자주 묻는 질문</p>
							</div>
							<div class="w25" style="height:10vh">
								<div class="w100 h60"></div>
								<button class="view_btns h40 w100 pull-right" onclick="mvPage('OftenQuestion')" >보러가기&#9654</button>
							</div>
						</div>
						<div class="height_box"></div>
						<table class="table col-md-12">
							<%-- <tr>
								<th>제목</th>
							<c:forEach items="${faqList}" var="list" end="5">
								<tr class="faq" data-pk="${list.board_pk }">
									<td>${list.title}</td>
								</tr>
							</c:forEach>
							</tr> --%>
							<tr >
								<th>제목</th>
							</tr>
							<tr class="faq" data-text="마크맨의 상표출원 서비스를 이용하시려면 회원가입을 하셔야 합니다.">
								<td>[회원]마크맨 서비슷는 회원등록을 해야만 이용할 수 있나요?</td>
							</tr>
							<tr class="faq" data-text="관리자에게 별도로 문의 부탁드립니다.">
								<td>[회원]회원 탈퇴는 어떻게 하나요?</td>
							</tr>
							
							<tr class="faq" data-text="특허청의 사정에 따라 다르지만 평균적으로 상표등록의 경우 10~12개월 정도 소요됩니다.">
								<td>[기간]출원에서 등록까지 얼마나 걸리나요?</td>
							</tr>
							<tr class="faq" data-text="관납료는 출원인이 특허청에 직접 납부해야 하는 비용이지만,절차의 편의를 위해 마크맨이 대신 납부해드리는 것이므로 현금으로 입금해주시면 됩니다.">
								<td>[비용]관납료는 카드 납부가 안되나요?</td>
							</tr>
							
							<tr class="faq" data-text="출원은 특허청에 상표를 보호해달라고 신청하는 것이고,등록은 출원된 상표를 심사한 후, 그 상표에 대한 권리를 인정해준다는 것입니다.">
								<td>[상표출원]출원과 등록은 어떤 차이가 있나요?</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</section>
		<section id="menu3" style="height:100vh;clear:both">
			<div class="col-xs-12 col-sm-12 col-lg-10 col-lg-offset-1">
				<div class="w74 main_content_be2">
					<div class="col-md-8 col-md-offset-2 col-xs-12">
						<img src="/resources/images/icon_info.png"  style="display:block;width:20%"><br>
						<div class="menu3_title text-center" >
							<p>상표지식사전</p>
							<b>상표에 대한 모든 정보를 담은 마크맨의 연재 콘텐츠 입니다.</b>
						</div>
					</div>
					<div class="col-md-2" style="height:15vh">
						<div class="w100 btn_height_box" style="height:130%"></div>
						<button class="menu3_btn" onclick="mvPage('information')">보러가기&#9654</button>
					</div>
					<div class="height_box"></div>
					
					<div class="col-md-12 col-xs-12 cu_pinter" style="height:40vh">
						<div class="col-md-5 col-xs-12 h100 before_box" onClick="location.href='/user/detailView1'" >
							<img src="/resources/images/content_img_04.png" class="photo_img img-responsive">
						</div>
						<div class="col-md-7 col-xs-12 h100 height_box_b">
							<div class="col-md-12 h50">
								<div class="col-md-4 col-xs-4 h100 box_over" onClick="location.href='/user/detailView2'">
									<img src="/resources/images/content_img_06.png" class="photo_img img-responsive">
								</div>
								<div class="col-md-4 col-xs-4 h100 box_over " onClick="location.href='/user/detailView3'">
									<img src="/resources/images/content_img_02.png" class="photo_img img-responsive">
								</div>
								<div class="col-md-4 col-xs-4 h100 box_over" onClick="location.href='/user/detailView4'">
									<img src="/resources/images/content_img_08.png" class="photo_img img-responsive">
								</div>
								
							</div>
							<div class="col-md-12 col-xs-12 h50">
								<div class="col-md-4 col-xs-4 h100 box_over"  onClick="location.href='/user/detailView5'">
									<img src="/resources/images/content_img_10.png" class="photo_img img-responsive">
								</div>
								<div class="col-md-4 col-xs-4 h100 box_over"  onClick="location.href='/user/detailView6'">
									<img src="/resources/images/content_img_12.png" class="photo_img img-responsive">
								</div>
								<div class="col-md-4 col-xs-4 h100 box_over"  onClick="location.href='/user/detailView7'">
									<img src="/resources/images/content_img_14.png" class="photo_img img-responsive">
								</div>
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>
</div>



<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>

<script>

var ytResize=function(){
   	//$("#menu1").height($(window).width()).height($(window).width()/16*9);
    //$("#menu2").height($(window).width()).height($(window).width()/16*9);
 }
 $(window).on("load resize",ytResize)
 
 
 var width = $(document).width();
 console.log(width);

 $(".text_boer").hover(function(){
	 //$(this).stop().css({"transform":"rotateY(360deg)","transition":"2.5s"});
	 $(this).find("div:eq(0)").stop().fadeOut();
	 $(this).find("div:eq(1)").stop().fadeIn();
 })
 
 $(".text_boer").mouseleave(function(){
	 //$(this).stop().css({"transform":"rotateY(-360deg)","transition":"2.5s"});
	 $(this).find("div:eq(1)").stop().fadeOut();
	 $(this).find("div:eq(0)").stop().fadeIn();
 })
 
 $(".text_boer2").hover(function(){
	 //$(this).stop().css({"transform":"rotateY(360deg)","transition":"2.5s"});
	 $(this).find("div:eq(0)").stop().fadeOut();
	 $(this).find("div:eq(1)").stop().fadeIn();
 })
 
  $(".text_boer2").mouseleave(function(){
	  //$(this).stop().css({"transform":"rotateY(-360deg)","transition":"2.5s"});
	 $(this).find("div:eq(1)").stop().fadeOut();
	 $(this).find("div:eq(0)").stop().fadeIn();
 })
 
 

var timer;
var pos=0;
$(function(){
	cbt();
	timer = setTimeout(function(){
	slide ("auto","")
		},2500);
	});
function slide (type,num){
	clearTimeout(timer);
	
	type =="auto" ? pos++ : type=="side" ? pos+=num :pos=num;
	pos = pos > 2 ? 0  : pos < 0 ? 2 : pos; 

	cbt();
	$(".slide ul").stop().animate({marginLeft:(pos *-100)+ "%"},800);
	
	timer=setTimeout(function(){
		slide("auto","");
	},2500);
}


function cbt(){
	$(".pos div button").eq(pos).css("background-color","rgba(0,0,0,0)")
	$(".pos div").eq(pos).siblings().find("button").css("background-color","rgba(47,47,47,0.6)");
	
	$(".pos2 div button").eq(pos).css("background-color","rgba(0,0,0,0)")
	$(".pos2 div").eq(pos).siblings().find("button").css("background-color","rgba(47,47,47,0.6)");
		
} 


$('.faq').click(function(){
	var text=  $(this).data("text");
	var title = $(this).find("td").text();
	var popup =  "<div  class='pop_s' style='position:fixed;width:100%;height:100vh;background:rgba(0,0,0,0.5);z-index:10'>"
					+"<div  class='col-lg-6 col-lg-offset-3 col-xs-10 col-xs-offset-1' style='height:25%;background:#fff;margin-top:15%;border-radius:10px;box-shadow: 3px 3px 3px #333;padding:1% !important'>"
						+"<button class=' close_btns pull-right btn btn-info' style='width:3vh;height:3vh'>x</button>"
						+"<div><h4><b style='color:red'>Q</b>"+title+"</h4></div>"
						+"<div class='text-center' style='margin-top:10%'><h5>A:"+text+"</h5></div>"
					+"</div>"
				+"</div>"
	$("body").prepend(popup);
				
				
	$(".close_btns").click(function(){
		$(".pop_s").hide();
	})			
	
	/* 
	var pk = $(this).data('pk');
	var url = "/common/support/OftenQuestion/detail/"+pk+""
	try {
		 	win = window.open(url, "", "toolbar=0,location=0,status=0,menubar=0,scrollbars=0,resizable=0,width=450,height=500");
		 	win.moveTo(200, 100);
		 	win.focus();
		 } catch(e){} */
		 
})
	
</script>
</body>

</html>