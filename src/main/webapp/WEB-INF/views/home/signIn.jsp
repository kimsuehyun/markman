<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Insert title here</title>
<c:import url="/WEB-INF/views/import/header.jsp"/>
<!--js -->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script> 
<!--css-->
<link rel="stylesheet" href="/resources/common/css/style.css"/>
<link rel="stylesheet" href="/resources/common/css/join.css"/>
<!--  부트스트랩 -->

<script src="/resources/common/js/bootstrap.js"></script>
</head>
<body>
<div class="height_box_login"></div>
<form id="loginForm" action="/login.do" method="POST" class="col-lg0-12" >
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>	
	<div class="col-lg-10 col-lg-offset-1  text-center">
		<h2>Login</h2>
		<div class="height_box_login"></div>
		<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 padding_none">
			<div class="col-lg-12 col-xs-12 padding_none">
				<input type="text"  name="id" placeholder="ID" class="form-control login_box_id">
			</div>
			<div class="height_box_login"></div>
			<div class="col-lg-12 col-xs-12 padding_none">
				<input type="password" name="pw" placeholder="PW" class="form-control login_box_pw"><br>
				<input type="checkbox" name="remember-me" class="pull-left" id="remember-me" value="true" checked="checked" style="height: auto;" > 
                  <span class="auto_login pull-left">자동로그인</span>
			</div>
			<div class="height_box_login"></div>
			<div class="col-lg-12 col-xs-12 padding_none">
				<button class="col-lg-12 col-xs-12 login_btn" type="submit">Login</button>
			</div>
			<div style="width:100%;height:2vh;clear:both;"></div>
			<div class="col-lg-12 col-xs-12 padding_none pull-right">
				<button class="join_btn col-lg-12 col-xs-12" type="button" onClick="location.href='/joinHowPage'">Join</button>
			</div>
			
			
		</div>
	</div>
</form>
<article style="clear:both">
	<c:import url="/WEB-INF/views/import/user/footer.jsp"/>
</article>	
</body>
</html>