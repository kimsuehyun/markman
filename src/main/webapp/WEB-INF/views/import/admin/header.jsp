<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<script src="/resources/common/js/sweetalert2.min.js"></script>
<script src="/resources/common/js/sweetalert.js"></script>
<link href="/resources/common/css/admin.css" rel="stylesheet">
<link href="/resources/common/css/style.css" rel="stylesheet">
<link href="/resources/common/css/sweetalert2.min.css" rel="stylesheet">
<script src="/resources/common/js/jquery-1.12.4.js"></script>
<script src="/resources/common/js/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/common/css/jquery-ui.css">
<link rel="icon" href="/resources/image/pavicon.png">
<title>Markman</title>
</head>
<body>
    <div id="admin_cont">
    <button><img src="/resources/image/list.png" class="hide-menu"></button>
    <form:form method="POST" id="secForm">
        <nav>
            <ul>
            	<li><img src="/resources/images/logo_1.png" alt="logo" onclick="location.href='/'" style="cursor: pointer;"></li>
            	<li id="header_info">
           			 <span><b>${currentUser.getName() }</b>님</span>
       			     <img src="/resources/image/logout_bb.png" class="icon25" onclick="logout()">
     		   	</li> 
    	         <li class="user upper" >회원 관리</li>
                <div class="sub">
                	<li class="all" onclick="location.href='/admin/userList?search_word=all'">- 전체</li>
                	<li class="applicant" onclick="location.href='/admin/applicantList?search_word=all'">- 출원인</li>
               		<li class="patent" onclick="location.href='/admin/patentList?search_word=all'">- 변리사</li>
               	</div>	
               	<li class="mark upper" >상표 관리</li>
               	<div class="sub">
               		<li class="all" onclick="location.href='/admin/markList?group=all'">- 전체</li>
               		<li class="in_apply" onclick="location.href='/admin/markList?group=in_apply'">- 출원 준비</li>
               		<li class="fin_apply" onclick="location.href='/admin/markList?group=fin_apply'">- 출원 완료</li>
               		<li class="fin_regis" onclick="location.href='/admin/markList?group=fin_regis'">- 심사 완료</li>
                </div class="sub">
                <li class="notice" onclick="location.href='/admin/noticeList'">공지사항 관리</li>
                <li class="qna" onclick="location.href='/admin/qnaList'">상표상담 관리</li>
                <li class="markInfo" onclick="location.href='/admin/markInfoList'">상표정보 관리</li>
               	<li class="faq" onclick="location.href='/admin/faqList'">FAQ 관리</li>     
               	<li class="upper" onclick="ready()">상표 설정 [준비중]</li>
                <div class="sub" >
                	<li>- 상표권 설정</li>
                	<li>- 상표 분류표 설정</li>
                </div>
                <li class="email" onclick="location.href='/admin/emailList'">메일 관리</li>
                <li onClick="location.href='/admin/inquisition'">문의하기</li>
                 <li class="banner" onclick="location.href='/admin/bannerList'"><img src="/resources/image/setting_bb.png" class="icon20"/>배너 설정</li>
                 
            </ul>  
        </nav>
    </form:form> 
<script src = "/resources/common/js/header.js"></script>
<script>
  	$(document).ready(function(){
 		var current_menu = sessionStorage.getItem("name");//현재 메뉴값을 받아온다. 
		var sub_menu = sessionStorage.getItem("sub"); //현재 하위 메뉴값을 받아온다.
		$('.'+current_menu+'').addClass('active');//현재 메뉴 표시
		$("."+current_menu+"").next('div').css("display","block");//현재 하위메뉴가 포함된 div만 활성화
		if(sub_menu!="") $("."+current_menu+"").next('div').children("."+sub_menu+"").css("text-decoration","underline"); //하위 메뉴 활성화 */
	});
	$('.upper').click(function(){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(this).next('div').slideUp();
		}else{
			$(this).addClass('active');
			$(this).next('div').slideDown();
		}
	}) ;
	function ready(){
		alertt("준비중");
	}
	$('.hide-menu').click(function(){
		if($(this).hasClass('has')){
			$(this).removeClass('has');
			$('#admin_cont nav').show('slide',{direction:'left'},750);
			$('.hide-menu').animate({left:'210px'},750);
		}else{
			$(this).addClass('has');
			$('#admin_cont nav').hide('slide',{direction:'left'},750);
			$('.hide-menu').animate({left:'10px'},750);
			
		}
	});
	
	function logout(){
		var form = $('#secForm');
		form.attr("action","/logout.do");
		form.submit();
	}
</script>