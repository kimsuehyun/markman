<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Markman</title>
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
 <c:import url="/WEB-INF/views/import/rimocon.jsp"></c:import>
<!--css-->
<link rel="stylesheet" href="/resources/common/css/main.css"/>
<link rel="stylesheet" href="/resources/common/css/sub_header.css"/>
<link href="/resources/common/css/sweetalert2.min.css" rel="stylesheet">

<script src="/resources/common/js/global.js"></script>
<script src="/resources/common/js/sweetalert2.min.js"></script>
<script src="/resources/common/js/sweetalert.js"></script>
<script src="/resources/common/js/information.js"></script>

<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="/resources/common/js/bootstrap.js"></script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-1234505240905608",
    enable_page_level_ads: true
  });
</script>
</head>
<body>
<style>
	/*box-shadow: 0px 4px 5px #888888;*/
	*{margin:0 auto ;padding:0;list-style: none;text-decoration:none;box-sizing: border-box;font-family:"맑은 고딕";}
	
</style>
<form:form method="POST" id="secForm">
<sec:authorize access="isAnonymous()">
      <input type="hidden" id="sessionId" value="">
      <c:set var="loginText" value="로그인"></c:set>
      <c:set var="loginFunctionName" value="회원가입"></c:set>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
      <input type="hidden" id="sessionId" value="true">
      <input type="hidden" id="userRole" value="${sessionScope.currentUser.getUser_type()}">
       <c:set var="loginText" value="로그아웃"></c:set>
       <c:set var="loginFunctionName" value="마이페이지"></c:set>
</sec:authorize>
<header class="visible-lg-block">
	<div class="menu">
		<div class="logo_wrap" style="padding-top:3% !important">
			<img src="/resources/images/logo_2.png" onClick="location.href='/'" alt="logo" title="logo" >
		</div>
		<div class="drop_menu_wrap">
			<ul class="drop_menu col-md-12">
				<li><span>상표 출원</span></li>
				<li><span>상표 검색</span></li>
				<li><span>상표 상담</span></li>
				<li><span>고객 지원</span></li>
				<li><span>마크맨 소개</span></li>
			</ul>
		</div>
		<div class="login_wrap">
			<a onclick="loginOut('${loginText }')">${loginText }</a>
          	<a onclick="loginOutFunction('${loginFunctionName }')">${loginFunctionName }</a>
		</div>
	</div>
	<div class="drop_down col-md-12">
		<div class="drop_down_wrap">
			<div class="drop_down_menu">
				<ul>
					<li><a onClick="location.href='/common/applyProcessInfo'">상표 출원 과정</a></li>
					<li><a onClick="location.href='/common/serviceInfo'">서비스 소개</a></li>
					<li><a onClick="location.href='/common/costInfo'">비용안내</a></li>
					<li><a onclick="location.href='/applicant/mark/mvApply'">상표출원 신청</a></li>
				</ul>
				<ul>
					<li><a  onclick="location.href='/common/markSearch'">상표검색</a></li>
				</ul>
				<ul>
					<li><a onclick="mvPage('counseling')">상표 상담</a></li>
				</ul>
				<ul>
					<li><a onclick="location.href='/common/noticeList'">공지사항</a></li>
					<li><a onclick="mvPage('inquisition')">문의하기</a></li>
					<li><a onclick="mvPage('OftenQuestion')">자주 묻는 질문</a></li>
					<li><a onclick="mvPage('review')">후기</a></li>
					<li><a onclick="mvPage('payment')">결제 안내</a></li>
					<li><a onclick="mvPage('information')">상표 지식사전</a></li>
				</ul>
				<ul>
					<li><a onclick="location.href='/introduce'" class="dropdown-toggle" data-toggle="dropdown">마크맨 소개</a></li>
				</ul>
			</div>
		</div>
	</div>
</header>
<header class="hidden-lg" style="box-shadow: none !important;">
	<div class="w100 menu_xs_wrap">
		<div class="w50 h100">
			<div class="h20"></div>
			<img src="/resources/images/logo_2.png" alt="logo" title="logo" onClick="location.href='/'"  class="img-responsive col-sm-7 col-xs-12 col-md-5" style="float:left;cursor:pointer;">
		</div>
		<div class="w50 h100">
			<div class="w100 h30"></div>
			<!-- <i class="fa fa-bars icon_bar"></i> -->
			<img src="/resources/images/icon_menu.png" class="icon_bar" style="width:15%">
		</div>
	</div>
	<div class="show_menu">
		<ul class="sub_menu_b">	
			<li><div><a>상표출원</a></div>			
				<ul>
					<li><a onClick="location.href='/common/applyProcessInfo'">상표 출원 과정</a></li>
					<li><a onClick="location.href='/common/serviceInfo'">서비스 소개</a></li>
					<li><a onClick="location.href='/common/costInfo'">비용안내</a></li>
					<li><a onclick="location.href='/applicant/mark/mvApply'">상표출원 신청</a></li>
				</ul>
			</li>	
				
			<li><div><a  onclick="location.href='/common/markSearch'">상표검색</a></div></li>
				
				
			<li><div><a onclick="mvPage('counseling')">상표 상담</a></div></li>
			
			<li><div><a>고객지원</a></div>				
				<ul>
					<li><a onclick="location.href='/common/noticeList'">공지사항</a></li>
					<li><a onclick="mvPage('inquisition')">문의하기</a></li>
					<li><a onclick="mvPage('OftenQuestion')">자주 묻는 질문</a></li>
					<li><a onclick="mvPage('review')">후기</a></li>
					<li><a onclick="mvPage('payment')">결제 안내</a></li>
					<li><a onclick="mvPage('information')">상표 지식사전</a></li>
				</ul>
			</li>	
			<li><div><a onclick="location.href='/introduce'" class="dropdown-toggle" data-toggle="dropdown">마크맨 소개</a></div></li>	
			<li><div><a>로그인</a></div>
			<li><div><a>회원가입</a></div>
			
		</ul>		
	</div>
</header>


</form:form>
</body>
<script>

$(".icon_bar").click(function(){
	$(".show_menu").slideToggle();
})

$(document).on("click",".back_top",function(){
	$("html , body").animate({scrollTop:"0"},500)
})

$(".sub_menu_b li").click(function(){
	if($(this).find("ul").is(":hidden")){
		$(this).find("ul").slideDown();	
	}else{
		$(this).find("ul").slideUp();
	}
	
	if($(this).siblings("li").find("ul").is(":visible")){
		$(this).siblings("li").find("ul").slideUp();
	}
})

$(".drop_menu_wrap  li").on("mouseover",function(){
	$(".drop_down").stop().fadeIn();	
});

$(".drop_down").mouseleave(function(){
	$(this).fadeOut();
})

  



function loginOutFunction(text){
	var formId = $("#secForm");
	var action = "";
	var userRole = $('#userRole').val();
	if(text =="회원가입"){
		action = "/joinHowPage";
	}else{
		if(userRole=="ROLE_ADMIN"){
			action = "/admin/"
		}else{
			action = "/user/main";
		}
	}
	formId.attr("action",action);
	formId.submit();
}
function loginOut(text){
	var formId = $("#secForm");
	var action = "";
	if(text =="로그아웃"){
		action = "/logout.do";
	}else{
		action = "/loginPage";		
	}
	formId.attr("action",action);
	formId.submit();
}
var search_input = $(".search_input");
search_input.click(function(){
	$(this).val("");
});
$(".line").click(function(){

	$(".line").hide();
	$(".line_x").show();
	$("nav").css({"background":"#00213d","position":"relative","z-index":"10"})
	
	
	$("header").css({"height":"10vh","transition":".5s"});
	$(".navbar-nav").slideDown("normal",function(){
		
	});
})
$(".line_x").click(function(){
	$(".line").show();
	$(".line_x").hide();
	$(".navbar-nav").slideUp("normal",function(){
		$("header").css({"transition":".5s","height":"43vh !important"});	
	});
})
$(".line").click(function(){
	$(".line").hide();
	$(".line_x").show();
	$("header").css({"transition":".5s"});
	$(".navbar-nav").slideDown("normal",function(){
		
		
		
	});
})
	$(".dropdown").click(function(){
		if($(this).find(".drop_sub_menuwrap").is(":hidden")){
			$(this).find(".drop_sub_menuwrap ").stop().show().addClass("show").removeClass("hidden")
			 	
			$(this).siblings().find(".drop_sub_menuwrap").stop().hide().removeClass("show").addClass("hidden");
		}else{
			$(this).find(".drop_sub_menuwrap").stop().hide().removeClass("show").addClass("hidden")
		}
	})
function mvPage(text){
	var action ="";
	if(text == "support"){
		action = "/common/mvSupport";
	}else if(text ==  "counseling"){
		action = "/common/mark/counseling";			
	}else if(text ==  "information"){
		action = "/common/support/information";			
	}else if(text ==  "inquisition"){
		action = "/common/support/inquisition";			
	}else if(text ==  "review"){
		action = "/common/support/review";			
	}else if(text ==  "OftenQuestion"){
		action = "/common/support/OftenQuestion";			
	}else if(text ==  "payment"){
		action = "/common/support/payment";			
	}
	location.href=action;
}

 function springSecurityloginCheck(){
	var flag = false;
	var id = $("#sessionId").val();
	var length = id.length;
	
	if(length > 0){
		flag = true;	
	}
	return flag;
}
 
 
 
 $(window).scroll(function(){
	 $(".rimocon").fadeIn();
		/* 
		var height = $(this).scrollTop();
		var obj = $("#menu1").offset();
		
		if(obj.top <=height){
			
			$(".rimocon").fadeIn();
		}else if(obj.top >=height){
			$(".rimocon").fadeOut();
		}
 */
	})
</script>

</html>