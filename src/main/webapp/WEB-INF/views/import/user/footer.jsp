<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--css-->
<script src="/resources/common/js/jquery-3.1.0.min.js"></script>
<link rel="stylesheet" href="/resources/common/css/bootstrap.min.css"/>
<script src="/resources/common/js/bootstrap.js"></script>
<!--loading -->
<script src="/resources/common/js/global.js"></script>
<title>Insert title here</title>

</head>
<body>
<style>


	.padding_none{padding:0px !important}	
	.height_box{height:3vh ;width:100%}
	.footer_box {height:15vh;}
	.footer_box > div:first-child{color:#fff;font-size:3vh;}
	.footer_box > div:last-child{color:#fff}
	footer{background:rgba(180,180,181,0.3)}
	.footer_box > div:last-child  p:first-child{font-size:3vh}
	.footer_box > div:last-child  p:last-child{color:#f7c189}
	.footer_btn{border:1px solid #fff;border-radius:10px;background:rgba(0,0,0,0);color:#fff;width:50%;height:50%;transition:0.5s}
	.footer_btn:hover{background:#fff;color:#000}
	footer > div:last-child{min-height:23vh;padding-top:1.5% !important;clear:both;height:auto}
	.footer_content  p{color:#434343;font-size:1.6vh}
	.footer_content div{color:#b4b4b5;font-size:1.2vh;line-height:2.5vh}
	.footer_title{height:25vh}
	.height_block{height:20vh;width:100%}
	
	@media(max-width:1024px){
		.footer_btn{margin-top:-5%;font-size:1.5vh}
		.xs_left{text-align:right;}
		footer > div:last-child{width:80%;margin-left:2% !important;}
		.footer_box > div:first-child{font-size:2.2vh}
		.footers_wrap{padding:0% 2% 0% 2% !important;}
	}
</style>
<div  style="width:100%;height:20vh"></div>
<footer >
	<div class="col-lg-12 col-xs-12 footers_wrap  padding_none" style="background:#717071;">
		<div class="footer_title col-lg-10 col-lg-offset-1 col-xs-10 padding_none  " >
			<div class="col-lg-9 footer_box   padding_none "  >
				
				<div class="col-lg-6 col-xs-6 h100  padding_none ">
					<div style="height:25%"></div>
					도움이 필요하신가요
				</div>
				<div class="col-lg-6 col-xs-6 h100 xs_left  padding_none ">
					<div style="height:25%"></div>	
					<p style="font-size:2.5vh">전국무료상담전화</p>
					<p style="font-size:3vh">070-8825-5004 <span style="font-size:2vh" class="col-xs-12">평일 9:00~18:00</span></p>
				</div>
			</div>
			<div class="col-lg-3 col-xs-8 h100  padding_none  " style="height:10vh">
				<div style="height:25%" class="hidden-xs hidden-sm"></div>	
				<button class="footer_btn" onclick="mvPage('counseling')">1:1상담 하러가기</button>
			</div>
		</div>
	</div>	
	<div  class=" col-md-12 col-xs-10   padding_none ">
		<div class="footer_content col-md-offset-1 col-md-10  padding_none">
			<div class="col-md-5 col-xs-12  padding_none">
				<p>개인정보취급방침 &nbsp;|&nbsp; 이용약관 &nbsp;|&nbsp; 이메일집단수집거부</p>
				주소:경기도 성남시 수정구 성남대로 1342번길 가천대학교 법학대학 7층 경기창조혁신센터<br>
				대표자명:전달용 | 사업자번호 :338-88-00131 | Copyright2017ⓒMARKMAN All Rights Reserved
			</div>
			<div class="height_box hidden-lg  "></div>	
			<div class="col-md-7 col-xs-12  padding_none">
				<div class="col-md-6  padding_none">
					<p>서비스 불편사항 및 개선사항 접수</p>
					ideaconcert@ideaconcert.com
				</div>
				<div class="height_box hidden-lg visible-xs-block "></div>
				<br>
				<div class="col-md-6 col-xs-12  padding_none">
					<img src="/resources/images/logo_1.png" alt="footer_logo" class="col-md-6 col-xs-5 col-sm-3 img-responsive  padding_none ">
				</div>
			</div>
		</div>
		<div class="height_block"></div>
	</div>
</footer>
</body>
</html>