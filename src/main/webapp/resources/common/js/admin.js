
// 변리사 지정 팝업창 - 검색주제 설정
function choice(value){
	$("#searchValue").val(value);
}

$('.match-patent').click(function(){
	$('#mark_pk').val($(this).data('pk'));
	$("#black_wall").css('visibility',"visible");
	$("#popup").css('visibility',"visible");
	$('.popPatentList').css("display","block");	
	$("#searchPatent").focus();	
});

function enterPatent(){
	if(window.event.keyCode==13){
		searchPatent();
	}
}

function closeload(){
	$("#black_wall").css('visibility',"hidden");
	$("#popup").css('visibility',"hidden");
	$('.popPatentList').css("display","none");
}

// 변리사 지정 팝업창  - 검색주제따라 ajax
$('.searchBtn').click(function(){
	var subject = $(".select-subject").val();
	$('.append-table').empty();
	if(subject==0) 
		searchPatent();		
	else{
	}
});

//변리사 지정 팝업창 - 이름/아이디으로 변리사 검색
function searchPatent(){
	var search = $("#searchPatent").val();
	$('.hide-tbody').css("display","none");
	$('.append-table').css("display","block");
	var data = {};
    data["search"] = search;
    $.ajax({
	    url : "/admin/findPatentAjax",
	    dataType : "json",
	    data : data,
	    success: function(data) {
	    	appendTable(data);
	    },
	    error:function(request,status,error){
	        alert("code:"+request.status+"\n"+"error:"+error);
		}
	 
	}) 
}
//변리사 지정 팝업창 - 검색결과 append
function appendTable(data) {
	var appendStr="";
	if(data.length == 0) {
		appendStr += "<tr><td colspan=4 class='no-searchData'>일치하는 검색 결과가 없습니다</td></tr>";
		$('.append-table').css("display","none");
		$('.hide-tbody').css("display","table-row-group");
	}
	for(var i=0; i<data.length;i++) {
		appendStr += "<tr><td>"+data[i].id+"</td>";
		appendStr += "<td><b>"+data[i].name+"</b></td>";
		appendStr += "<td>"+data[i].email+"</td>";
		appendStr += "<td><button class='match' data-id='"+data[i].id+"'>지정</button></td></tr>";
	}
	$('.hide-tbody').empty();
	$('.append-table').empty();
	$('.append-table').append(appendStr);
	$('.append-table').css("display","table-row-group");
}

$(document).on('click','.match',function(){
	var form = $('#formId');
	$('#patent_id').val($(this).data('id'));
	form.attr("action","/admin/mark/matchPatent");
	form.submit();
});
