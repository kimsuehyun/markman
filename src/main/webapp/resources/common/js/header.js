
$('.menu').click(function(){//클릭시 하위메뉴 활성호&비활성화
	if($(this).hasClass('active')){
		//하위 메뉴 숨기기
		$(this).next('div').slideUp();
		$(this).removeClass('active');
	}else{
		//하위메뉴 활성화
		$(this).addClass('active');
		$(this).next('div').slideDown();
	}
});