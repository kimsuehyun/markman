//대분류 선택시 중분류 보여주기
$(document).on("click",".bigCategory",function (){
	$('#next').css("visibility","hidden");
	var big = $(this).text();
	var type = "mid";
	var data = {};
	data["big"] = big;
	$.ajax({
		url : "/applicant/mark/getMidCategory",
		type : "POST",
		dataType : "json",
		//headers : headers,
		data : data,
		success : function(resultData){
			showCategory(resultData,type);
		},
		error : function(request, status, error){
			alert("code:" + request.status + "\n" + "error:" + error);
		}
	});
	$('#1').empty();
	$('#2').empty();
	$('#3').empty();
	$('#4').empty();
	$('#1').append('<span id="1">'+$(this).text()+'</span>');
	$('#service').empty();
	$('#mark').empty();
	$(this).addClass("bigSelected");
	$(this).siblings().removeClass("bigSelected");
});
//중분류 선택시 서비스 보여주기
   $(document).on("click",".midCategory",function(){
	  $('#next').css("visibility","hidden");
	  var data = {};
	  data["big"] = $('.bigSelected').text();
	  data["mid"] = $(this).text();
	  var type = "service";
	  $.ajax({
			url : "/applicant/mark/getServiceCategory",
			type : "POST",
			dataType : "json",
			data : data,
			success : function(resultData){
				showCategory(resultData,type);
			},
			error : function(request, status, error){
				alert("code:" + request.status + "\n" + "error:" + error);
			}
		});
	$('#2').empty();
	$('#3').empty();
	$('#4').empty();
	$('#2').append('<span id="1">>'+$(this).text()+'</span>');
	$('#mark').empty();
  	$(this).addClass("midSelected");
  	$(this).siblings().removeClass("midSelected");
   });  
   //서비스 선택시 상표항목 보여주기
   $(document).on("click",".serviceCategory",function(){
	  $('#next').css("visibility","visible");
	  var data = {};
	  data["big"] = $('.bigSelected').text();
	  data["mid"] = $('.midSelected').text();
	  data["service"] = $(this).text();
	  var type = "mark";
	  if($(this).text()!="--"){
	  $.ajax({
			url : "/applicant/mark/getMarkContent",
			type : "POST",
			dataType : "json",
			data : data,
			success : function(resultData){
				showCategory(resultData,type);
			},
			error : function(request, status, error){
				alert("code:" + request.status + "\n" + "error:" + error);
			}
		});
  	var reg_exp=new RegExp("^[0-99+|,]{1,100}","g");  
	var match=reg_exp.exec(service);
	if(match==null){
  		$(this).addClass("act");
  		$('#3').empty();
      	$('#3').append('<span id="1">>'+$(this).text()+'</span>');
	}
	$(this).addClass("serviceSelected");
	$(this).siblings().removeClass("serviceSelected");
	  }
   });  

//테이블 내용 가져오기
function showCategory(data,type){
	//대분류 눌렀을 때
	if(type=="mid"){
		var midList = data.midCategoryList;
		$('#mid').empty();
		var htmlString = '<ul>';
		for(var i = 0 ; i <midList.length; i++){
			htmlString += '<li class="midCategory">'+midList[i]+'</li>'; 
		}
		htmlString += '</ul>';
		$('#mid').append(htmlString);
	}
	//중분류 눌렀을 때
	if(type=="service"){
		var serviceList = data.serviceCategoryList;
		$('#service').empty();
		$('#mark').empty();
		var htmlString = '<ul>';
		var htmlString2 = '<ul>';
		for(var i = 0 ; i <serviceList.length; i++){
			var reg_exp=new RegExp("^[0-99+|,]{1,100}","g");  
			var match=reg_exp.exec(serviceList[i]);
			if(match==null){
				htmlString += '<li class="serviceCategory">'+serviceList[i]+'</li>'; 
			}else{
				 $('#next').css("visibility","visible");
				htmlString += '<li class="serviceCategory">--</li>'; 
				htmlString2 += '<li class="markContent">'+serviceList[i]+'</li>';
			}
		}
		htmlString += '</ul>';
		htmlString2 += '</ul>';
		$('#service').append(htmlString);
		$('#mark').append(htmlString2);
	}
	//서비스 눌렀을 때
	if(type=="mark"){
		var markList = data.markContentList;
		$('#mark').empty();
		var htmlString = '<ul>';
		 for(var i = 0 ; i <markList.length; i++){
			htmlString += '<li class="markContent">'+markList[i]+'</li>'; 
		} 
		htmlString += '</ul>';
		$('#mark').append(htmlString);
	}
}
	var i = 1;
	function addfile2(){
		i++;
		$(".inp_add").append("<input type='file' style='float: left; margin: 10px 0 10px 0; opacity: 1; padding-top: 5px;' id='copy_file"+i+"'>");		
	}

    function selectMark(){
		var directory = $('#markDirectory').text();
		var service = $('#service').text();
		var mark = $('#mark').text();
		var reg_exp=new RegExp("^[0-99+|,]{1,100}","g");  
   		var match=reg_exp.exec(service);
    	if(match==null){
    		$('#markContent').val(mark);
    	}else{
    		$('#markContent').val(service);
    	}
    	$('#directory').val(directory);
    	$('#selectMark').attr("action","/Mark/selectMark");
    	$('#selectMark').submit();
    }
