$.ajax({
        async:false
    });
//function alertt(content,type){
//	if(type == 'success'){
//		
//	}else{
//		type = 'error';
//	}
//	swal( content, "", type);
//};

var alertt = function(content,userType){
	var ok ="확인";
	var type="";
	if(userType == 'success'){
		type = userType;
	}else if(userType == 'info'){
		type = userType;
	}else{
		type = 'error';
	}
	swal({
		allowOutsideClick: false,
		title: content,
		type: type,
		confirmButtonClass:"swal2-btn",
		confirmButtonText: ok,
		allowEscapeKey:false
	})
	return new Promise(function (resolve,reject){
		$(".swal2-btn").click(function(){
			if($(this).text()==ok){
				resolve(true);
			}else{
				reject(Error(false));
		}})
	});
}
var confirmm = function(title,userType){
	var ok ="확인";
	var no ="취소";
	var type = 'warning';
	if(userType=="pw"){
		ok = "재시도";
		no = "메인으로";
	}else if(userType=="?"){
		type = "question";
	}else if(userType=="x"){
		type = "error";
	}
	swal({
		allowOutsideClick: false,
		title: title,
		type: type,
		confirmButtonColor:'#595959',
		confirmButtonClass:"swal2-btn",
		confirmButtonText: ok,
		showCancelButton: true,
		cancelButtonColor:'#ccc',
		cancelButtonText: no,
		cancelButtonClass:"swal2-btn",
		allowEscapeKey:false
	}).catch(function(){
	})
	return new Promise(function (resolve,reject){
		$(".swal2-btn").click(function(){
			if($(this).text()==ok){
				resolve(true);
			}else{
				reject(Error(false));
		}})
	});
}

function passwordCheck(){	
	swal({
		  title: '비밀번호를 입력하세요',
		  input: 'password',		  
		  inputAttributes: {
		    'maxlength': 10,
		    'autocapitalize': 'off',
		    'autocorrect': 'off',		    
		  },	
		  showCancelButton: true,
		  cancelButtonColor:'#ccc',
		  cancelButtonText: "취소",
		  cancelButtonClass:"swal2-btn",
		  confirmButtonText: "확인",
		  allowEscapeKey:false,
		  allowOutsideClick: false
		}).then(function (password) {
			passwordAjaxCheck(password);
		}).catch(function(){
			location.href="/common/login";
		});
}

function passwordAjaxCheck(pw){
	var csrfParameter = $("meta[name='_csrf_parameter']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	var csrfHeader = $("meta[name='_csrf_header']").attr("content"); // THIS WAS ADDED
	var headers = {};
	headers[csrfHeader] = csrfToken;
	
	var $form = $('<form></form>');
	$form.attr('action', '/common/pwCheck');
	$form.attr('method', 'post');
	$form.appendTo('body');
	var idx = $('<input type="hidden" value="'+pw+'" name="pw" id="pw">');
	$form.append(idx);
	var optionspW = {
	        success      : pwAjaxAfter,  // ajaxSubmit 후처리 함수
	        headers : headers
	};
	$form.ajaxSubmit(optionspW);
}

function pwAjaxAfter(resultData, statusText, xhr, $form){
	if (statusText == "success"){
	    // ajax 통신 성공 후 처리영역
	    if (resultData == "ok" ){	
	    	loadingOff();
	    	swal({ type: 'success', html: '확인되었습니다' });
	    }else {
	    	confirmm("비밀번호가 틀렸습니다.","pw").then(function(){
	    		passwordCheck();
	    	}
	    	,function(){
	    		location.href="/common/login";
	    	});	    	
	    }
	}else{
		 // ajax 통신 실패 처리영역
		loadingOff();
		alertt("잘못된 요청입니다.");
	}  
}
