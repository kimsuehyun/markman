/**
 * 
 */

  //daum-map
  var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
  mapOption = {
      center: new daum.maps.LatLng(37.449192, 127.127515), // 지도의 중심좌표
      level: 3 // 지도의 확대 레벨
  };  
  var map = new daum.maps.Map(mapContainer, mapOption); 
  // 결과값으로 받은 위치를 마커로 표시합니다
  var marker = new daum.maps.Marker({
      map: map,
      position: mapOption.center
  });
  // 인포윈도우로 장소에 대한 설명을 표시합니다
  var infowindow = new daum.maps.InfoWindow({
      content: '<div style="width:150px;text-align:center;padding:6px 0;">(주)아이디어콘서트</div>'
  });
  infowindow.open(map, marker);

  //일반 지도와 스카이뷰로 지도 타입을 전환할 수 있는 지도타입 컨트롤을 생성합니다
  var mapTypeControl = new daum.maps.MapTypeControl();
  //지도에 컨트롤을 추가해야 지도위에 표시됩니다
  //daum.maps.ControlPosition은 컨트롤이 표시될 위치를 정의하는데 TOPRIGHT는 오른쪽 위를 의미합니다
  map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);
  //지도 확대 축소를 제어할 수 있는  줌 컨트롤을 생성합니다
  var zoomControl = new daum.maps.ZoomControl();
  map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);
	
//kakao-map-share
Kakao.init('dd3cd58db09f66cfb64aafcaf8edd099');   // 사용할 앱의 JavaScript 키를 설정해 주세요.
var description = '경기 성남시 수정구 성남대로 1342 비전타워 7층'; //표시할 주소
var address = '신한은행 가천대학교지점'; //검색할 주소
	Kakao.Link.createDefaultButton({
	container : '#kakao-map-share', //이벤트 태그
	objectType : 'location', //default
	address : address,
	//지도 뷰에서 사용 할 주소, ex.성남시 분당구 판교역로 235
	addressTitle : '(주)아이디어콘서트',
	content : {
		title: '(주)아이디어콘서트',
		description : description,
		imageHeight : 150, 
		imageUrl : 'http://ideapc.co.kr/resources/image/imgMap.png',
		link: {
	          mobileWebUrl: 'http://ideapc.co.kr',//마크맨-오시는길
	          webUrl: 'http://ideapc.co.kr'//마크맨-오시는길
	        }
	},
     buttons: [
         {
           title: '웹으로 보기',
           link: {
             mobileWebUrl: 'http://ideapc.co.kr',//마크맨-오시는길
             webUrl: 'http://ideapc.co.kr'//마크맨-오시는길
           }
         }
       ]
});  