-- MySQL dump 10.13  Distrib 5.6.37, for Win64 (x86_64)
--
-- Host: localhost    Database: markman
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applicant`
--

DROP TABLE IF EXISTS `applicant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicant` (
  `id` varchar(50) NOT NULL,
  `copy_url` text,
  `copy_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicant`
--

LOCK TABLES `applicant` WRITE;
/*!40000 ALTER TABLE `applicant` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auto`
--

DROP TABLE IF EXISTS `auto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auto` (
  `word` varchar(20) DEFAULT NULL,
  `search` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auto`
--

LOCK TABLES `auto` WRITE;
/*!40000 ALTER TABLE `auto` DISABLE KEYS */;
INSERT INTO `auto` VALUES ('수현',4),('수지',5),('수가',1),('수나',20),('수서',11),('수비',6),('수티',123),('수치',33),('수상',0);
/*!40000 ALTER TABLE `auto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner`
--

DROP TABLE IF EXISTS `banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner` (
  `banner_seq` int(11) NOT NULL AUTO_INCREMENT,
  `banner_name` varchar(30) NOT NULL,
  `banner_img` text NOT NULL,
  `registrant` varchar(30) NOT NULL,
  `access_link` text,
  `registration_date` datetime NOT NULL,
  `location` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`banner_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (1,'main','https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/banner/20170825155512.jpg','suehyun','3','2017-08-25 15:55:14','home/index');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banner_name`
--

DROP TABLE IF EXISTS `banner_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banner_name` (
  `name` varchar(20) NOT NULL,
  `kor_name` varchar(20) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banner_name`
--

LOCK TABLES `banner_name` WRITE;
/*!40000 ALTER TABLE `banner_name` DISABLE KEYS */;
INSERT INTO `banner_name` VALUES ('main','메인 화면');
/*!40000 ALTER TABLE `banner_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `board`
--

DROP TABLE IF EXISTS `board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `board` (
  `board_pk` int(11) NOT NULL AUTO_INCREMENT,
  `writer` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `registration_date` datetime NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`board_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `board`
--

LOCK TABLES `board` WRITE;
/*!40000 ALTER TABLE `board` DISABLE KEYS */;
INSERT INTO `board` VALUES (2,'suehyun','상담2','상담내용2','2017-08-02 18:43:37',1),(10,'suehyun','공지사항1','공지사항 내용1','2017-08-04 18:02:30',0),(39,'suehyun','상담2','상담내용2','2017-08-11 10:34:04',5),(40,'suehyun','상담3','상담내용3','2017-08-11 10:34:40',13),(41,'suehyun','ㄹㄹ','<p>ㄹㄹ</p>','2017-08-11 10:46:12',24),(42,'suehyun','공지사항 test','<p>123123</p>','2017-08-11 11:03:10',60),(43,'suehyun','새로운 이미지','<p>123123</p>','2017-08-11 11:19:07',106),(46,'suehyun','gdgdg','<p>dgdgdgㅅㅅ12312312312</p>','2017-08-14 11:02:42',848),(47,'suehyun','블로그 링크 게시물1','마크맨 드디어 첫 개시!','2017-08-14 16:17:57',0),(48,'suehyun','블로그 링크 게시물1','마크맨 드디어 첫 개시!','2017-08-14 16:49:29',0),(49,'suehyun','블로그 링크 게시물1','마크맨 드디어 첫 개시!','2017-08-14 16:51:27',0),(50,'suehyun','블로그 링크 게시물1','마크맨 드디어 첫 개시!','2017-08-14 16:52:20',2),(51,'suehyun','ㅎㅇㄹ','123','2017-08-14 16:53:09',2),(52,'suehyun','블로그 링크 게시물1','마크맨 드디어 첫 개시!','2017-08-14 17:02:58',2),(53,'suehyun','블로그 링크 게시물2','이렇게 이쁜 색은!','2017-08-14 17:07:07',2),(54,'suehyun','eye','<p>eye2</p>','2017-08-24 11:16:22',10),(55,'suehyun','123123','33123','2017-08-24 11:22:12',0),(56,'suehyun','수정글','<p>수정글<img src=\"https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/notice/20170824141954.jpg\" title=\"Screenshot_1.jpg\">&nbsp;</p>','2017-08-24 14:01:04',8),(58,'suehyun','32','<p>123123123123</p>','2017-08-24 14:24:02',8),(59,'suehyun','오늘의 일기장','<p>제발....힘들다</p>','2017-08-25 18:29:50',36),(60,'qwer917','밥돌이 보고싶져!?','<p><br></p>','2017-10-12 17:16:36',24),(63,'qwer917','aaa','<p>aa</p>','2017-11-08 16:37:47',306),(65,'test3','aaa','aaa','2017-11-09 14:13:48',0),(66,'qwer917','sss','<p>sssss</p>','2017-11-09 14:25:02',0),(67,'qwer917','sss','<p>sssss</p>','2017-11-09 14:25:04',8),(68,'qwer917','dsfgsdfg','<p>dsdfgdfg</p>','2017-11-09 16:47:45',2),(69,'qwer917','dddddddddsdfasdfasdfdsfasdf','<p>ddddddddddsdfsdf</p>','2017-11-09 16:48:47',22);
/*!40000 ALTER TABLE `board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `board_img`
--

DROP TABLE IF EXISTS `board_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `board_img` (
  `board_img_seq` int(11) NOT NULL AUTO_INCREMENT,
  `board_pk` int(11) NOT NULL,
  `img_url` text NOT NULL,
  `img_origin_name` text NOT NULL,
  `img_size` int(11) NOT NULL,
  PRIMARY KEY (`board_img_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `board_img`
--

LOCK TABLES `board_img` WRITE;
/*!40000 ALTER TABLE `board_img` DISABLE KEYS */;
INSERT INTO `board_img` VALUES (1,48,'https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/markinfo/20170814164929.png','test_search.png',89916),(2,49,'https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/markinfo/20170814165127.png','test_search.png',89916),(3,50,'https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/markinfo/20170814165220.png','test_search.png',89916),(4,51,'https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/markinfo/20170814165309.png','question_mark.png',6120),(5,54,'https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/notice/20170824111622.png','eye.png',4133),(6,54,'https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/notice/20170824111624.png','eye_b.png',2342),(7,60,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/admin/image/notice/20171012171636.jpg','밥돌이.jpg',227810),(8,63,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/admin/image/notice/20171108163747.png','캡처.PNG',533054),(9,66,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/admin/image/notice/20171109142502.jpg','wallhaven-116463.jpg',300372),(10,67,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/admin/image/notice/20171109142504.jpg','wallhaven-116463.jpg',300372);
/*!40000 ALTER TABLE `board_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `email_pk` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(50) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `content` text,
  `send_date` datetime NOT NULL,
  `mail_type` int(11) NOT NULL,
  UNIQUE KEY `PK_email` (`email_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` VALUES (1,'suehyun','하이룽룽','<p>하이룽룽룽루울우룽룽<img src=\"https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/mail/20170818145705.jpg\" title=\"0718_indexUi.jpg\">&nbsp;</p>','2017-08-18 14:57:11',1),(2,'suehyun','하이룽룽','<p>하이룽룽룽루울우룽룽<img src=\"https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/mail/20170818145705.jpg\" title=\"0718_indexUi.jpg\">&nbsp;</p>','2017-08-18 14:59:43',1),(3,'suehyun','하이룽룽룽','<p>12323213123213123</p>','2017-08-18 15:01:56',1),(4,'suehyun','하이룽룽룽','<p>12323213123213123</p>','2017-08-18 15:10:16',1),(5,'suehyun','하이룽룽룽','<p>12323213123213123</p>','2017-08-18 15:10:47',1),(6,'suehyun','1231','<p>3333</p>','2017-08-18 15:13:00',1),(7,'suehyun','1231','<p>3333</p>','2017-08-18 15:13:24',1),(8,'suehyun','1231','<p>33331q2312321321312321</p>','2017-08-18 15:14:06',1),(9,'suehyun','erw','<p>werwerw</p>','2017-08-18 15:14:47',1),(10,'suehyun','213213','<p>123123123</p>','2017-08-18 15:16:14',1),(11,'suehyun','1332113','<p>2311321132</p>','2017-08-18 15:16:46',1),(12,'suehyun','12312','<p>3123123123</p>','2017-08-18 15:18:15',1),(13,'suehyun','하이룰ㅇ룽룽ㄹ','<p style=\"text-align: center;\" align=\"center\">23123123213</p>','2017-08-18 16:02:02',1),(14,'suehyun','12312312123','<p>1231231231</p>','2017-08-22 17:53:19',1),(15,'suehyun','12312312123','<p>1231231231</p>','2017-08-22 17:53:58',1),(16,'suehyun','123123123123123','<p>12312</p>','2017-08-22 17:57:07',1),(17,'suehyun','123123','<p>123123123</p>','2017-08-22 19:16:34',1),(18,'suehyun','3213','<p>1232312332</p>','2017-08-22 19:24:20',1),(19,'suehyun','21321312','<p>3123123123123</p>','2017-08-22 19:26:00',1),(20,'suehyun','21321','<p>3213123123</p>','2017-08-22 19:27:41',1),(21,'suehyun','123123','<p>123123123</p>','2017-08-22 19:28:09',1),(22,'suehyun','123213','<p>123123213</p>','2017-08-22 19:28:37',1),(23,'suehyun','하하','<p>호호</p>','2017-08-24 16:57:08',1),(24,'suehyun','123','<p>123</p>','2017-08-24 17:01:14',1),(25,'suehyun','333','<p>123</p>','2017-08-24 17:04:19',1),(26,'suehyun','하이잇','<p>3</p>','2017-08-24 17:14:38',1),(27,'suehyun','1213213','<p>333</p>','2017-08-24 17:34:13',1),(28,'qwer917','밥밥밥ㅂ','<p>ㅁㅁㅁㅁ</p>','2017-10-12 17:35:55',1),(29,'qwer917','aaaaa','<p>aaaaa</p>','2017-11-09 13:20:14',1),(30,'qwer917','aaaaa','<p>aaaaa</p>','2017-11-09 13:26:13',1),(31,'qwer917','df','<p>sdf</p>','2017-11-09 13:27:37',1);
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_file`
--

DROP TABLE IF EXISTS `email_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_file` (
  `email_pk` int(11) NOT NULL,
  `file_url` text NOT NULL,
  `file_origin_name` text NOT NULL,
  UNIQUE KEY `PK_email_file` (`email_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_file`
--

LOCK TABLES `email_file` WRITE;
/*!40000 ALTER TABLE `email_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_receiver`
--

DROP TABLE IF EXISTS `email_receiver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_receiver` (
  `email_pk` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id` varchar(30) NOT NULL,
  `receiver_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_receiver`
--

LOCK TABLES `email_receiver` WRITE;
/*!40000 ALTER TABLE `email_receiver` DISABLE KEYS */;
INSERT INTO `email_receiver` VALUES (1,'bhw123@daum.net','bhw123',0),(1,'nunu0321@naver.com','suehyun',0),(1,'nunu0321@daum.net','yoona',0),(1,'bhw123@daum.net','bhw123',0),(1,'nunu0321@naver.com','suehyun',0),(27,'bhw123@daum.net','bhw123',0),(27,'nunu0321@naver.com','suehyun',0);
/*!40000 ALTER TABLE `email_receiver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_receiver_type`
--

DROP TABLE IF EXISTS `email_receiver_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_receiver_type` (
  `email_receiver_type_number` int(11) NOT NULL,
  `email_receiver_type_name` varchar(30) NOT NULL,
  UNIQUE KEY `PK_email_receiver_type` (`email_receiver_type_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_receiver_type`
--

LOCK TABLES `email_receiver_type` WRITE;
/*!40000 ALTER TABLE `email_receiver_type` DISABLE KEYS */;
INSERT INTO `email_receiver_type` VALUES (0,'TO'),(1,'CC'),(2,'BCC');
/*!40000 ALTER TABLE `email_receiver_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_type`
--

DROP TABLE IF EXISTS `email_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_type` (
  `email_type_number` int(11) NOT NULL,
  `email_type_name` varchar(30) NOT NULL,
  UNIQUE KEY `PK_email_type` (`email_type_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_type`
--

LOCK TABLES `email_type` WRITE;
/*!40000 ALTER TABLE `email_type` DISABLE KEYS */;
INSERT INTO `email_type` VALUES (-1,'RESERVE_MAIL'),(1,'SEND_MAIL'),(9,'SAVE_MAIL');
/*!40000 ALTER TABLE `email_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `board_pk` int(11) NOT NULL,
  PRIMARY KEY (`board_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` VALUES (56),(58),(68);
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest_post`
--

DROP TABLE IF EXISTS `guest_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest_post` (
  `nonmember_post_seq` int(11) NOT NULL,
  `post_type` varchar(50) NOT NULL,
  `post_seq` int(11) NOT NULL,
  `password` text NOT NULL,
  `writer` varchar(30) NOT NULL,
  `cell_num` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `sns_receive` int(11) NOT NULL,
  PRIMARY KEY (`nonmember_post_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest_post`
--

LOCK TABLES `guest_post` WRITE;
/*!40000 ALTER TABLE `guest_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `guest_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark`
--

DROP TABLE IF EXISTS `mark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark` (
  `mark_pk` int(11) NOT NULL AUTO_INCREMENT,
  `patent_id` varchar(30) DEFAULT NULL,
  `applicant_id` varchar(30) DEFAULT NULL,
  `step` int(11) DEFAULT NULL,
  `mark_regisDate` datetime DEFAULT NULL,
  UNIQUE KEY `PK_mark` (`mark_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark`
--

LOCK TABLES `mark` WRITE;
/*!40000 ALTER TABLE `mark` DISABLE KEYS */;
INSERT INTO `mark` VALUES (1,'qwer917','qwer917',0,'2017-09-19 10:33:32'),(2,'qwer917','qwer917',0,'2017-09-21 14:00:16'),(3,'qwer917','qwer917',0,'2017-09-21 14:00:17'),(4,'qwer917','qwer917',0,'2017-09-21 14:03:34'),(5,'','qwer917',0,'2017-09-25 10:33:11'),(6,'','qwer917',0,'2017-09-25 10:33:18'),(7,'','qwer917',0,'2017-09-25 10:50:30'),(8,'','qwer917',0,'2017-09-25 10:51:00'),(9,'','qwer917',0,'2017-09-25 10:51:16'),(10,'','qwer917',0,'2017-09-25 11:07:02'),(11,'','qwer917',0,'2017-09-25 11:07:17'),(12,'','qwer917',0,'2017-09-25 11:45:49'),(13,'','qwer917',0,'2017-09-25 11:51:11'),(14,'','qwer917',0,'2017-09-25 11:53:31'),(15,'','qwer917',0,'2017-09-25 15:27:33'),(16,'','yusu88',0,'2017-09-25 15:32:09'),(17,'','jisook5',0,'2017-09-25 15:37:47'),(18,'','babdol2',0,'2017-09-25 15:53:04'),(19,'','godn917',0,'2017-10-10 10:42:08'),(20,'','godn917',0,'2017-10-10 10:42:55'),(21,'','godn917',0,'2017-10-10 11:01:01'),(22,'','test12',0,'2017-10-10 12:54:56'),(23,'','qwer917',0,'2017-10-10 17:38:20'),(24,'','qwer917',0,'2017-10-10 17:41:58'),(25,'','qwer917',0,'2017-10-11 13:38:34'),(26,'','qwer917',0,'2017-10-12 11:29:15'),(27,'','qwer917',0,'2017-10-12 11:54:27'),(28,'','qwer2',0,'2017-10-12 17:30:11'),(29,'','qwer2',0,'2017-10-12 17:30:36'),(30,'','qwer2',0,'2017-10-12 17:30:41'),(31,'','qwer2',0,'2017-10-12 17:30:45'),(32,'','qwer2',0,'2017-10-12 17:30:46'),(33,'','qwer2',0,'2017-10-12 17:30:47'),(34,'','qwer2',0,'2017-10-12 17:30:48'),(35,'','qwer2',0,'2017-10-12 17:33:02'),(36,'','qwer2',0,'2017-10-12 17:33:32'),(37,'','qwer2',0,'2017-10-12 17:33:35'),(38,'','qwer2',0,'2017-10-13 14:58:06'),(39,'','qwer2',0,'2017-10-13 16:05:13'),(40,'','qwer2',0,'2017-10-13 16:42:03'),(41,'','qwer2',0,'2017-10-13 16:49:24'),(42,'','qwer2',0,'2017-10-13 17:03:43'),(43,'','qwer2',0,'2017-10-16 10:25:26'),(44,'','qwer2',0,'2017-10-16 10:48:47'),(45,'','test3',0,'2017-11-01 18:57:20'),(46,'','test3',0,'2017-11-02 11:46:41'),(47,'','test3',0,'2017-11-03 10:54:09'),(48,'','ip9816',0,'2017-11-03 15:20:29'),(49,'','test3',0,'2017-11-06 13:58:18'),(50,'','test3',0,'2017-11-06 13:58:35'),(51,'','test3',0,'2017-11-06 13:58:42'),(52,'','test3',0,'2017-11-08 13:26:42'),(53,'','test3',0,'2017-11-08 13:26:43'),(54,'','test3',0,'2017-11-08 13:26:44'),(55,'','test3',0,'2017-11-08 13:26:45'),(56,'','test3',0,'2017-11-08 13:26:49'),(57,'','test3',0,'2017-11-08 13:27:19'),(58,'','test3',0,'2017-11-08 14:09:18'),(59,'','test3',0,'2017-11-09 13:46:33'),(60,'','test3',0,'2017-11-09 17:08:36');
/*!40000 ALTER TABLE `mark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_apply`
--

DROP TABLE IF EXISTS `mark_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_apply` (
  `mark_apply_req_seq` int(11) NOT NULL,
  `is_regis` varchar(30) NOT NULL,
  `attorney` text,
  `apply_num_img` text,
  `apply_file` text,
  `apply_prove_file` text,
  `update_date` datetime NOT NULL,
  `receipt1` text,
  `receipt2` text,
  `receipt3` text,
  `opinion_file` text,
  `apply_num_img_name` varchar(50) DEFAULT NULL,
  `apply_file_name` text,
  `apply_prove_file_name` varchar(50) DEFAULT NULL,
  `attorney_name` varchar(50) DEFAULT NULL,
  `receipt1_name` varchar(50) DEFAULT NULL,
  `receipt2_name` varchar(50) DEFAULT NULL,
  `receipt3_name` varchar(50) DEFAULT NULL,
  `opinion_file_name` varchar(50) DEFAULT NULL,
  `opinion_feedback` text,
  PRIMARY KEY (`mark_apply_req_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_apply`
--

LOCK TABLES `mark_apply` WRITE;
/*!40000 ALTER TABLE `mark_apply` DISABLE KEYS */;
INSERT INTO `mark_apply` VALUES (2,'-1','asd','asd',NULL,'asd','2015-03-30 00:00:00','as','sdf','dfg','asdf','sdf',NULL,'sdf','fgh','qwe','werr','xcv','asdg','sadfg');
/*!40000 ALTER TABLE `mark_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_apply_req`
--

DROP TABLE IF EXISTS `mark_apply_req`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_apply_req` (
  `mark_apply_req_seq` int(11) NOT NULL AUTO_INCREMENT,
  `patent_id` varchar(50) NOT NULL,
  `applicant_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `where_use` varchar(100) NOT NULL,
  `category` text NOT NULL,
  `select_content` text,
  `patent_feedback` text,
  `regis_date` datetime NOT NULL,
  `feedback_date` datetime DEFAULT NULL,
  `is_apply` int(11) NOT NULL,
  `base_mark_pk` int(11) NOT NULL,
  PRIMARY KEY (`mark_apply_req_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_apply_req`
--

LOCK TABLES `mark_apply_req` WRITE;
/*!40000 ALTER TABLE `mark_apply_req` DISABLE KEYS */;
INSERT INTO `mark_apply_req` VALUES (2,'hido1','hojin1','이호진원','asd','asdf','sdsf','sdf','sdf','2014-03-12 00:00:00','2014-03-21 00:00:00',0,2),(3,'hido1','hojin1','이호진원','asd','asdf','sdsf','sdf','sdf','2014-03-31 00:00:00','2014-04-02 00:00:00',0,3),(4,'hido1','hojin1','이호진원','asd','asdf','sdsf','sdf','sdf','2014-03-01 00:00:00','2014-03-21 00:00:00',-1,3),(6,'hido1','hojin1','이호진원','asd','asdf','sdsf','sdf','sdf','2014-03-01 00:00:00','2014-03-21 00:00:00',-1,3),(7,'hido1','hojin1','이호진원','asd','asdf','sdsf','sdf','sdf','2014-03-01 00:00:00','2014-03-21 00:00:00',-1,3),(8,'hido1','hojin1','이호진원','asd','asdf','sdsf','sdf','sdf','2014-03-01 00:00:00','2014-03-21 00:00:00',-1,3),(9,'hido1','yoona','상표명1','도형','사용처1','2','3','피드백입니당','2014-03-01 00:00:00','2014-03-21 00:00:00',-1,4),(10,'hido1','yoona','상표명2','한글','사용처2','4','5','다른 피드백입니당','2014-05-10 00:00:00','2014-05-21 00:00:00',-1,3),(11,'hido1','yoona','상표명2','한글','사용처2수정','4','5',NULL,'2014-05-11 00:00:00',NULL,-1,3),(12,'hojin','yoona','상표명3','영문','사용처3','1','3','피드백입니당ㅎㅎㅎ','2014-07-01 00:00:00','2014-07-21 00:00:00',-1,5),(13,'hojin','yoona','상표명6','도형','사용처6','1','3','피드백ㅎㅎ','2014-08-01 00:00:00','2014-08-21 00:00:00',1,6),(14,'hojin','yoona','상표명1','힌글','사용처아','22','23','피드백입니당~~!','2014-09-01 00:00:00','2014-09-21 00:00:00',-1,8),(15,'hojin','yoona','상표명1수정주성','힌글','사용처아','22','23',NULL,'2014-09-22 00:00:00',NULL,-1,8),(16,'hido1','yoona','상표명11','도형','사용처1','12','13','피드백입니당','2014-10-01 00:00:00','2014-10-21 00:00:00',-1,11);
/*!40000 ALTER TABLE `mark_apply_req` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_category`
--

DROP TABLE IF EXISTS `mark_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_category` (
  `mark_category_seq` int(11) NOT NULL,
  `supervisor` varchar(30) NOT NULL,
  `directory` varchar(100) NOT NULL,
  `sub` varchar(30) NOT NULL,
  PRIMARY KEY (`mark_category_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_category`
--

LOCK TABLES `mark_category` WRITE;
/*!40000 ALTER TABLE `mark_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `mark_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_content`
--

DROP TABLE IF EXISTS `mark_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_content` (
  `mark_num` int(11) NOT NULL,
  `info` text NOT NULL,
  `base_right` text NOT NULL,
  `add_right` text NOT NULL,
  PRIMARY KEY (`mark_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_content`
--

LOCK TABLES `mark_content` WRITE;
/*!40000 ALTER TABLE `mark_content` DISABLE KEYS */;
INSERT INTO `mark_content` VALUES (1,'공업용, 과학용, 사진용, 농업용, 원예용 및 임업용 화학품; 미가공 인조수지, 미가공 플라 스틱; 비료; 소화제; 조질제 및 용접용 조제; 식품보존제; 무두질제; 공업용 접착제','가정 식물용 비료,공업용 전분,가성소다(수산화나트륨),가스정화제,계면활성제,공업용 무기염,공업용 발포제,광촉매제,구연산,기포방지제,맥주보존제,메틸알코올,방수제,붕산,비식품보존용 소금,소포제,시멘트혼합제,식품보존제,염화칼슘,오일분산제',''),(2,'페인트, 니스, 래커; 방청제 및 목재보존제; 착색제; 매염제; 미가공 천연수지; 도장용, 장식용, 인쇄용 및 미술용 금속박과 금속분','녹 방지제,목재보존제,구두용 염료,유화염료,도장/장식/인쇄 및 미술용 금속박,무기안료,연단(鉛丹),회화용 청동분,내약품도료,내열도료,래커,바니시,발수성 도료,석회도료,에나멜,자동차용 페인트,형광도료,복사용 잉크,프린터용 잉크,착색제',''),(3,'표백제 및 기타 세탁제재; 청정제, 광택제, 연마제; 비누; 향료, 정유, 화장품, 헤어로션; 치약','과자용 향미료(정유),가정용 표백제,세탁용 린스제,얼룩제거제,기능성 화장품,노화방지용 화장품,립스틱,마스카라,모발 염색 및 표백제,모이스처라이저 로션,목욕 및 샤워용 겔,보습제,선블록로션,선탠크림 및 로션,세안클렌저,손톱페인트(화장품),애프터셰이브크림,탈모용 크림,피부미백제,실내용 방향제',''),(4,'공업용 유 및 유지; 먼지흡수제, 먼지습윤제 및 먼지흡착제; 연료(원동기용 연료를 포함한다) 및 발광체; 조명용 양초 및 심지','공업용 동물유지,공업용 콩기름,페인트 및 비누제조용 야채유,먼지억제제,윤활제,신발용 그리스,무연탄,참숯,연료가스,내연기관용 연료,액체연료,공업용 가솔린,공업용 그리스,윤활유 및 윤활용 그리스,아로마향초,양초,훈제 및 그릴링식품용 삼나무조각,엔진오일용 첨가제(화학제는 제외),항공기 및 선박용 연료,바디보드용 왁스',''),(5,'약제, 수의과용 약제; 의료용 위생제; 식이요법제, 유아용 식품; 고약, 외과 및 외상 처치용 붕대류; 치과용 충전재료 및 치과용 왁스; 소독제; 유해동물박멸제; 살균제, 제초제','영아용 분유,가정용 살충제,근육이완제,기침약,남성호르몬제,독감방지용 백신,멘톨,미네랄 식품보충제,부인용 한방약,비타민 및 비타민제,살균제,소화촉진용 식이섬유,식욕억제제,식이보충제용 건강보조식품,여드름 치료제,의료용 로션,제초제,피부과용 크림,공기탈취제,생리대',''),(6,'일반금속 및 그 합금; 금속제 건축재료; 이동식 금속제 건축물; 철도노선용 금속재료; 일반금속제 케이블 및 와이어(전기용은 제외한다); 철제품, 소형금속제품; 금속관; 금고; 타류에 속하지 아니하는 일반금속제품','금광석,금속제 간판,금속제 벽사다리,금속제 드럼통,금속제 음료캔,금속제 수납상자,포장용 금속제 뚜껑,금속제 가구용 나사,전자식 금고,옥외용 금속제 블라인드,금속제 걸쇠,금속제 미가공 열쇠,강철,스테인레스관,알루미늄강판,알루미늄합금,금속제 배수관,미닫이문용 금속제 프레임,알루미늄제 문,금속제 타일',''),(7,'기계 및 공작기계; 모터 및 엔진(육상차량용은 제외한다); 기계연결기 및 전동장치의 구성부품(육상차량용은 제외한다); 농업용 기구(수동식은 제외한다); 부란기','자동판매기,전기식 잔디깎는 기계,기계식 리프트,굴착기용 유압식 절단기,불도저,공구(기계부품),보링머신,주형(기계부품),쓰레기압축기계,섬유기계,산업용 제빵기,UV프린터(인쇄기계),내연기관용 점화플러그,송풍기,기계/엔진 또는 모터용 제어장치,볼베어링,세척기계,발전기,진공청소기,OLED제조용 식각장치',''),(8,'수공구 및 수동기구; 칼붙이류; 휴대용 무기; 면도칼','가정용 칼,귀금속제 스푼,비전기식 과일 껍질 벗기는 기구,난로용 철물기구,전기다리미,밀링커터(수공구),니퍼,석공용 해머,주조용 철제기구,코킹공구,풀러(수공구),연마기구,줄(공구),면도날,비전기식 손톱깎이,안전면도기,호신봉,낫,식탁용 포크,다기능 접이식 휴대용 수공구',''),(9,'과학용, 항해용, 측량용, 사진용, 영화용, 광학용, 계량용, 측정용, 신호용, 검사(감시)용, 구명용 및 교육용으로 전기의 전도, 전환, 변형, 축적, 조절 또는 통제를 위한 기기; 음향 또는 영상의 기록, 송신','디지털식 전기자물쇠,실시간 가스 분석기기,광학렌즈,디지털 캠코더,보안카메라,카메라,방사선감지장치,온도센서,안경,경보기,태양열 보조전지,음향케이블,디지털 뮤직 플레이어,TV 모니터,헤드폰,스마트폰 보호필름,휴대용 통신기기,내려받기 가능한 소프트웨어,전자계산기(컴퓨터)용 프로그램을 기억시킨 기억매체,노트북컴퓨터',''),(10,'외과용 ,내과용, 치과용 및 수의과용 기계기구, 의지, 의안, 의치; 정형외과용품; 봉합용 재료','고정 및 신축 의료용 내시경,당뇨측정기,레이저치료기,맥박계,복대,소변검사기,수의과용 기구,스텐트,안과용 의료기기,외과용 메스,유전자검사기,의료용 세정기,의료용 온도계,의료용 초단파미용기기,저주파치료기계기구,체지방계,치과용 구강세척기,젖병,의료용 마스크,의료기기',''),(11,'조명용, 가열용, 증기발생용, 조리용, 냉각용, 건조용, 환기용, 급수용 및 위생용 장치','바비큐용구,가스레인지,빵굽는 기계,싱크대,일회용 손난로,수족관용 여과장치,비데,비의료용 좌욕기,샤워기,가정용 보일러,냉각용 장치 및 설비,천장용 팬,급수설비,공기건조기,물소독장치,소독장치,전기식 공기청정기,LED 조명기구,냉장고,수송기계기구용 히터',''),(12,'수송기계기구; 육상, 공중 또는 수상이동장치','낙하산,스키리프트,고무보트,모터보트,선박용 프로펠라,민간용 드론,비행기,여객열차,소형 오토바이,어린이용 카시트,자동차,자동차앞유리용 와이퍼,자동차용 앞유리 햇빛가리개,캠핑카,자전거,쇼핑용 손수레,유모차,유모차용 후드,상용차용 타이어,브레이크 디스크',''),(13,'화기; 총포탄 및 발사체; 화약류; 불꽃','가스총,권총,무기용 발사체,사냥총,산탄총 및 그 부품,소총용 조준경,소총케이스,최루가스화기,화기용 탄창,가스탄,개인방어목적용 스프레이,기폭용 폭죽,로켓 추진제,로켓탄,불꽃제품,신호탄,기폭제,스포츠용 탄약통,유도폭탄,화기용 견착대',''),(14,'귀금속 및 그 합금과 귀금속제품 또는 귀금속도금제품으로서 타류에 속하지 아니 것; 보석류, 귀석; 시계용구','미가공 금강석,귀금속제 기념주화,기념주화,귀금속제 보석상자,가죽제 키홀더,금속제 열쇠고리,귀금속제 시계,귀금속제 보석,보석,백금,목걸이(장신구),반지(장신구),브로치(장신구),장식핀,MP3 플레이어 기능이 있는 시계,카메라 기능이 있는 시계,팔찌(장신구),핀(장신구),귀금속제 커프스 단추,귀금속제 조각품',''),(15,'악기','악기 조율기,금관악기,기계식 피아노,기타,단소,드럼 및 타악기,디지털피아노,마림바,목관악기,뮤직박스,바이올린,베이스기타,북(한국악기),소리굽쇠,스틸드럼(악기),악기,오보에,장구,클라리넷,하프',''),(16,'종이, 판지 및 이들의 제품으로서 타류에 속하지 아니하는 것; 인쇄물(신문, 서적 및 정기간행물); 제본용 재료; 사진; 문방구; 문방구 또는 가정용 접착제; 미술용 재료; 화필 및 도장용 브러시; 타자기 및 사무','가정용 풀,종이제 간판,골판지,복사기용지,포장용지,여행용 티슈,문방구,앨범,연습장,필기구,골판지상자,종이제 가방,전기타자기,달력,카드,카탈로그,그림,서적,정기간행물,학습지',''),(17,'고무, 구타페르카, 고무액(Gum), 석면 운모 및 이들의 제품으로서 타류에 속하지 아니하는 것; 제조용 압출성형플라스틱; 충전용, 마개용 및 절연용 재료','건물 썬팅용 필름,단열필름,반가공 플라스틱,스크래치 방지 필름,열차단필름,플라스틱제 튜브,플라스틱호스,경화고무,우레탄고무,포장용 고무판,폴리우레탄시트,고무제 병뚜껑,단열바닥재,문틈 바람막이,음향판(音響板),실리콘 실란트,파이프용 개스킷,절연도료,공업용 마스킹테이프,경화섬유',''),(18,'피혁 및 모조피혁과 그 제품으로서 타류에 속하지 아니하는 것; 수피; 트렁크 및 여행용 가방; 우산, 양산 및 지팡이; 채찍, 마구','화장도구 케이스(빈 것),애완동물용 의류,인조가죽,가죽제 명함지갑,도시락가방,등산용 가방,명함케이스,배낭,보스턴백,비치백,서류가방,슈트백,스포츠용 가방,신용카드 케이스,아동용 가방,지갑,책가방,카드지갑,파우치백,골프용 양산',''),(19,'비금속제 건축재료; 비금속제 건축용 경질관; 아스팔트, 피치 및 역청; 비금속제 이동식 건축물; 비금속제 기념물','콜타르,건축용 대리석,골재,석회석,건축용 강화유리,스테인드글라스,안전유리,가구용 목재,건축용 시멘트,차도블록,도로경계석,벽구축용 석재,건축석재용 실리콘,건축용 비금속제 몰딩,건축용 아스팔트,비금속제 셔터,비금속제 수도관,비금속제 천장판,구운벽돌,석제 조각품',''),(20,'가구, 거울, 액자; 목재, 코르크, 갈대, 등나무, 고리버들, 뿔, 상아, 고래수염, 패각, 뼈, 호박, 진주모, 해포석을 재료로 하는 제품과 이들 재료의 대용품 또는 플라스틱제품(타류에 속하는 것은 제외한다)','목제 간판,비금속제 사다리,미가공 또는 반가공 거북 등,목제 통,가구,가구용 목제칸막이,거울,대리석식탁,벤치,비금속제 옷걸이,상품판매대,선반(가구),옥외용 가구,유아용 침대,장식장(가구),캐비닛/서랍장/가구용 석제 손잡이,실내용 블라인드,액자,유아용 보행기,마네킹',''),(21,'가정용 또는 주방용 기구 및 용기(귀금속제, 귀금속도금제품은 제외한다); 빗 및 스펀지; 솔(화필 및 도장용 브러시는 제외한다); 솔제조용 재료; 청소용구; 강철울(Steel wool); 미가공 또는 반가공 유리','메이크업브러시,빗,화장용 스펀지,칫솔,빗자루,세척용 솔,쓰레기통,반가공 유리,장식용 유리구,귀금속제 주전자,프라이팬,머그컵,식탁용 식기,가정용 비금속제 쟁반,밥상,젓가락,얼음통,주방용장갑,음료용 용기,세라믹제 조각',''),(22,'로프, 끈, 망, 텐트, 차양막, 타포린, 돛, 포대(타류에 속하는 것은 제외한다); 충전용 재료(고무제 또는 플라스틱제는 제외한다); 직물용 미가공 섬유','줄사다리,충전용 짚,공업용 면대,보관용 신발가방,세탁물자루,천막(캠핑용은 제외),해먹,등산용 로프,캠핑용 텐트,견섬유,충전용 다운깃털,직물용 탄소섬유,비금속제 케이블,포장용 끈,네트,직물용 섬유,모시섬유,그늘막,면섬유,비금속제 결속용구',''),(23,'직물용 사','견련사,견사,극세사,난연사,레이온사,면방사,명주실사,사(絲),소모사(梳毛絲),손명주실,앙고라사,연사,유리섬유사,은색사,직물용 사,직물용 플라스틱사,합성사,혼방사,캐시미어사,혼방면사',''),(24,'직물 및 직물제품으로서 타류에 속하지 아니하는 것; 침대 및 테이블커버','건조용 행주,직물제 라벨,직물제 배너,직물제 페넌트,직물제 변기커버,자수용 도안직물,샤워커튼,가구용 플라스틱제 커버,문커튼,비종이제 식탁보,실내장식용 직물,직물제 식탁매트,직물제 테이블보,다운 이불,모포,베개커버,목욕용 수건,면직물,방수직물,직물시트',''),(25,'의류, 신발, 모자','단화, 운동화, 여성용신발, 반바지, 스커트, 슬랙스, 원피스, 점퍼, 청바지, 티셔츠, 남방셔츠, 블라우스, 넥타이, 레깅스, 스카프, 양말, 모자, 허리끈, 장갑, 운동용 스타킹',''),(26,'레이스 및 자수포, 리본 및 머리밴드; 단추, 훅 및 아이(Hook and eyes), 핀 및 바늘; 조화','헤어롤,가발,자수용 실 또는 울 고정용 실패(기계부품 제외),구두끈,뜨개바늘,전기 헤어캡,비귀금속제 핸드폰 장식고리,휴대폰용 장식고리,가장자리용 레이스,리본 및 장식용 끈,의류부착용 가장자리장식,의류용 가장자리장식,의류용 끈,의류용 주름장식,장식용 끈,장식용 의류패치,헤어 곱창밴드,단추,지퍼,인조식물',''),(27,'카펫, 융단, 매트, 리놀륨 및 기타 바닥깔개용 재료; 비직물제 벽걸이','욕실용 매트,종이제 벽지,고무 매트,러그(깔개),비닐제 벽걸이,어린이용 안전매트,자동차용 매트,자동차용 카펫,장식용 비직물제 벽걸이,카펫,인조잔디,요가러그,플라스틱제 벽지,목제 현관매트,카펫용 안감,운동용 매트,벽지 및 카펫,실내용 비닐제 바닥매트,직물제 바닥깔개,현관매트',''),(28,'오락 및 유희용구; 체조용품 및 운동용품으로서 타류에 속하지 아니하는 것; 크리스마스트리용 장식품','인조 크리스마스트리,고무제 캐릭터 완구,교육용 완구,놀이용 텐트,로봇완구,목제(木製) 완구,물놀이완구,봉제인형,블록완구,완구악기,완구용 무기,장난감 공구,조립식 완구,게임카드,비디오 게임기,놀이터용 미끄럼대,라켓,벤치프레스,골프가방,골프채',''),(29,'육류, 어류, 가금 및 수렵대상이 되는 조수; 육 추출물; 보존처리, 건조처리 및 조리된 과실 및 야채; 젤리, 잼, 설탕에 절인 과일; 계란, 우유 및 유제품; 식용 유지','냉동감자,가공된 견과류,건조된 홍삼,과일퓌레,김치,동결건조된 채소,보존처리된 마늘,야채칩,잼,채소가공식품,홍시,두부,냉동돈육,난(卵)을 주원료로 하는 건강기능식품,가공된 인삼,햄,과실을 주성분으로 한 요거트,크림 치즈,식용유,해초가공식품',''),(30,'커피, 차, 코코아, 설탕, 쌀, 타피오카, 사고(Sago), 대용커피; 곡분 및 곡물조제품, 빵, 과자, 빙과; 꿀, 당밀; 효모, 베이킹파우더; 소금, 겨자; 식초, 소스(조미료); 향신료; 얼음','백미,밀가루,곡물가공식품,냉동피자,만두,샌드위치,시리얼바,핫도그,엿기름,과자,냉동 요구르트케이크,베이글,캔디,꿀,양념장,고추가루,향미료 및 향신료,녹차,가공된 커피음료,차를 주성분으로 하는 음료',''),(31,'농업, 원예 및 임업 생산물, 곡물로서 타류에 속하지 아니하는 것; 살아있는 동물; 신선한 과실 및 야채; 종자, 자연식물 및 꽃; 사료, 맥아','미가공 두류,생옥수수,신선한 고구마,신선한 토마토,가축용 사료,강아지사료,꽃종자,신선한 감귤,신선한 포도,살아있는 나무,조경수,생 굴,신선한 김,통나무,살아있는 조개,신선한 해초,살아있는 잔디,미가공 홉,식용 개껌,유기농 신선한 채소',''),(32,'맥주; 광천수, 탄산수 및 기타 무주정 음료; 과실음료 및 과실주스; 시럽 및 기타 음료용 조제품','맥주제조용 홉 진액,과실음료 및 과실주스,과일탄산주스,석류음료,알로에베라 주스,오디음료,인삼주스(음료),차향(茶香)을 첨가한 비알코올성 음료,홍삼음료,광천수,라거비어,맥아맥주,알코올성분을 제거한 맥주,음료용 흑삼분말,홍삼시럽,맥주함유 칵테일,향이 첨가된 맥주,채소주스음료,청량음료,음료용 견과류즙',''),(33,'알콜음료(맥주 제외)','막걸리,소주,쌀로 빚은 술,약주,청주,과실주,레드와인,리큐르,매실주,블렌드 위스키,알코올성 과일음료,양주,조리용 맛술,칵테일,벌꿀주,알코올성 음료(맥주는 제외),증류주,인삼주,샴페인,알코올진액',''),(34,'담배; 흡연용품; 성냥','담배,담배대용품,멘톨담배,여송연,잎담배,전자담배,전자담배용 니코틴,전자담배용 향미료(정유(精油)는 제외),귀금속제 담배파이프,귀금속제 재떨이,비귀금속제 성냥갑,시가케이스,전자권연케이스,흡연용구,성냥,담배라이터,담배필터,여송연홀더,권연,무연담배',''),(35,'광고업; 기업관리업; 기업경영업; 사무처리업','사업경영/관리 및 사무처리업,간행물광고업,관광 및 여행 분야 광고업,광고 및 마케팅상담업,광고기획업,광고목적의 인쇄물출판업,라디오 및 텔레비전 광고대행업,스포츠마케팅서비스업,온라인 광고업,회계자문업,브랜드제작업,사업평가업,소비자연구조사업,창업컨설팅업,직업소개업,온라인 데이터처리업,의류 판매대행업,기능성 화장품 판매대행업,신발 판매대행업,골프채 소매업',''),(36,'보험업; 재무업; 금융업; 부동산업','금융 또는 재무에 관한 상담업,은행 및 보험업,거래분석 및 금융정보제공업,금융파생상품 거래/자문/관리/처리 관련 금융서비스업,무선단말기를 이용한 신용 카드서비스업,사이버머니발행업,인터넷을 통한 증권거래업,투자상담업,생명보험 관련 상담 및 중개업,기업 인수합병용 재무자문업,온라인 재무정보제공업,건물 임차 중개업,건물감정업,공인중개업,부동산임대업,아파트 관리업,임대자산의 부동산 관리업,토지취득 관련 정보제공업,신용등급평가업,보험업',''),(37,'건축물건설업; 수선업; 설치서비스업','조명기기 수리업,거주용 건물 건설공사업,건물 건축 및 관리업,건물리모델링업,공공사업 건축/관리/개조업,누수방지설비업,도로공사업,목조주택건축업,방음공사업,쇼핑센터/산업단지/사무용 건물/거주용 건물/기타 부동산개발 개조 및 관리업,웹사이트를 통한 건축 정보제공업,전기공사업,간판도색업,가구 수리 및 관리업,도난경보기 설치 및 수리업,사무기기 수리업,자동차 내외장 관리업,네트워크용 서버컴퓨터 설치업,전기통신기계기구 수리업,태양광 발전기 설치업',''),(38,'전기통신업; 방송업','오디오/비디오/정지 및 동영상 이미지/텍스트/데이터 전송/방송/수신업,인터넷을 통한 필름 및 영화 비디오방송 및 전송업,인터넷포털서비스업,원격통신업,가상사설망(VPN)서비스 제공업,게시판 접속제공업,글로벌 컴퓨터네트워크 사용자접속제공업,내려받기 가능한 전자 출판물 전송업,뉴스전송업,디지털 미디어 스트리밍 서비스업,소셜네트워크용 대화방제공업,스마트폰 애플리케이션을 통한 문자/사진/영상전송업,영어논술교육방송 인터넷이용자 접속제공업,오디오 및 영상 콘텐츠의 전자 전송/전달업,웹사이트 제공업,이미지 디지털전송업,인터넷상의 포털 접속제공업,인터넷을 이용한 소프트웨어 전송업,핸드폰을 통한 모바일콘텐츠 접속제공업,온라인 게임 전문 인터넷 방송업',''),(39,'운송업; 물품의 포장 및 보관업; 여행예약업','상품의 포장 및 보관업,창고 저장업,꽃배달업,대리운전 경영업,렌터카 대여업,수송기계기구리스업,승객운송업,온라인 배달중개업,온라인을 통한 대리운전대행업,음식배달업,이삿짐운송업,주차장서비스업,차량견인업,캠핑카 예약업,컴퓨터 또는 GPS을 통한 화물차량 추적업(운송정보제공업),택시운송업,냉동식품창고업,관광서비스업,여행 관련 상담 및 예약업,의료관광컨설팅업',''),(40,'재료처리업','플라스틱 가공업,세라믹가공업,목재가공업,전자제품 재활용업,에너지 가공 및 변형업,귀금속세공업,알루미늄처리업,열쇠가공업,용접업,섬유염색가공업,직물 인쇄업,직조업,곡물가공업,식육처리업,커피분쇄처리업,반도체 소자가공업,3D프린팅업,디자인인쇄대행업,서적인쇄업,직물재단업',''),(41,'교육업; 훈련제공업; 연예업; 스포츠 및 문화활동업','인터넷 온라인을 통한 연예오락정보제공업,영화/쇼/연극/음악 또는 교육훈련용 시설제공업,교육시험제공업,교육정보제공업,내려받기 불가능한 주문형 비디오를 통한 영화 및 텔레비전프로그램 제공업,디지털 영상처리업,만화영화 각색 및 편집업,인터넷용 내려받기 불가능한 디지털음악 제공업,교과서/서적/잡지 및 기타 인쇄물 출판업,내려받기 불가능한 온라인 전자서적 출판업,서적/잡지출판업,학습지출판업,나이트클럽업,전자도서관업,어학교육제공업,입시학원경영업,세미나 준비업,골프시설운영업,게임시설제공업,만화방서비스업',''),(42,'과학적 기술적 서비스업 및 관련 연구 디자인업; 산업분석 및 연구 서비스업; 컴퓨터 하드웨어 및 소프트웨어의 디자인 및 개발업; 법률서비스업','의약품연구업,건축디자인업,건축설계업,실내장식디자인업,그래픽디자이너업,인쇄미술디자인업,전기제어기기 설계업,제품설계업,조명시스템 디자인업,의상디자인업,내려받기 불가능한 소프트웨어 온라인 임시사용 제공업,도메인판매알선업,반도체디자인업,온라인 컨텐츠 전송을 위한 플랫폼 제공업,웹디자인업,전자상거래 분야 소프트웨어 관련 상담업,컴퓨터게임 컴퓨터프로그래밍업,환경검사업,유전자분석업,과학연구관련 정보제공업',''),(43,'음식료품을 제공하는 서비스업; 임시숙박업','간이식당서비스업,도시락전문 음식점경영업,도시락전문체인점업,레스토랑서비스업,식당체인업,음식준비업,음식준비조달업,카페 및 카페테리아업,한식점업,포장마차업,포장판매식당업,이동식레스토랑업,레스토랑 예약업,뷔페식당업,식음료 준비업,파스타전문 레스토랑업,포장마차업(음식점업),주점업,관광음식점업,다방업',''),(44,'의료서비스업; 수의사업; 인간 또는 동물을 위한 위생 및 미용업; 농업, 원예 및 임업 서비스','스킨케어서비스업,병원서비스업,의료 및 건강관리서비스업,농업/원예 및 임업 관련 온라인 정보제공업,치과서비스업,건강상담업,모발이식업,미용성형외과업,유아심리치료업,수의업,메이크업 상담 및 응용서비스업,모발관리업,문신업,미용서비스업,손톱미용업,피부미용업,헬스스파서비스업,건강마사지업,꽃꽂이업,심리상담업',''),(45,'사적인 수요를 충족시키기 위해 타인에 의해 제공되는 개인적인 또는 사회적인 서비스; 재산 및 개인을 보호하기 위한 보안 서비스','온라인 소셜네트워킹 서비스업,개인법률사건 관련 상담업,기술라이선싱업,디자인관리업,산업디자인 보호 관련 상담업,상표감시서비스업,지식재산가치평가업,특허관리업,개인패션상담업,아동복대여업,의복대여업,가사서비스업,분실물반환서비스업,개인경호업,소방업,결혼상담업,결혼식장업,운명감정업,목회상담업,캠핑모임조직업','');
/*!40000 ALTER TABLE `mark_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_directory`
--

DROP TABLE IF EXISTS `mark_directory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_directory` (
  `mark_directory_seq` int(11) NOT NULL,
  `supervisor` varchar(30) NOT NULL,
  `directory` varchar(100) NOT NULL,
  `sub` varchar(30) NOT NULL,
  PRIMARY KEY (`mark_directory_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_directory`
--

LOCK TABLES `mark_directory` WRITE;
/*!40000 ALTER TABLE `mark_directory` DISABLE KEYS */;
INSERT INTO `mark_directory` VALUES (1,'바이오','바이오>생명공학>42,44','42,44'),(2,'바이오','바이오>개인건강정보>41,44','41,44'),(3,'ICT/IOT','ICT/IOT>정보통신>9,42','9,42'),(4,'상호/간판','상호/간판>의료서비스>1,44,2,10','1,44,2,10'),(5,'상호/간판','상호/간판>농업관련서비스>1,44,2,31,3,29','1,44,2,31,3,29'),(6,'상호/간판','상호/간판>과학 및 연구서비스>1,42,2,9','1,42,2,9'),(7,'상호/간판','상호/간판>소프트웨어개발및디자인>1,42,2,9','1,42,2,9'),(8,'상호/간판','상호/간판>법률서비스>45','45'),(9,'상호/간판','상호/간판>음식업/숙박업>음식점이름>1,43,2,30,3,32','1,43,2,30,3,32'),(10,'상호/간판','상호/간판>음식업/숙박업>음식이름>1,29,2,30,3,31,4,32','1,29,2,30,3,31,4,32'),(11,'상호/간판','상호/간판>음식업/숙박업>O2O서비스>1,39,2,43,3,44','1,39,2,43,3,44'),(12,'상호/간판','상호/간판>음식업/숙박업>숙박서비스>43','43'),(13,'상호/간판','상호/간판>미용서비스>헤어샵>1,44,2,3','1,44,2,3'),(14,'상호/간판','상호/간판>미용서비스>네일샵>1,44,2,3','1,44,2,3'),(15,'상호/간판','상호/간판>미용서비스>마사지샵>1,44,2,3','1,44,2,3'),(16,'상호/간판','상호/간판>미용서비스>문신>1,44,2','1,44,2'),(17,'상호/간판','상호/간판>미용서비스>왁싱>44','44'),(18,'상호/간판','상호/간판>광고마케팅서비스>1,35,2,9','1,35,2,9'),(19,'상호/간판','상호/간판>보험관련서비스>35','35'),(20,'상호/간판','상호/간판>건축관련서비스>37','37'),(21,'상호/간판','상호/간판>통신관련서비스>38','38'),(22,'상호/간판','상호/간판>운송관련서비스>39','39'),(23,'상호/간판','상호/간판>교육서비스>41','41'),(24,'플랫폼서비스','플랫폼서비스>전자상거래(쇼핑)> O2O서비스>35,36','35,36'),(25,'플랫폼서비스','플랫폼서비스>전자상거래(쇼핑)>소셜커머스>35,36','35,36'),(26,'플랫폼서비스','플랫폼서비스>전자상거래(쇼핑)>오픈마켓>35,36,38','35,36,38'),(27,'플랫폼서비스','플랫폼서비스>전자상거래(쇼핑)>쇼핑몰>35,36,38','35,36,38'),(28,'플랫폼서비스','플랫폼서비스>부동산서비스>웹/앱 서비스>9,35,36,37,38','9,35,36,37,38'),(29,'플랫폼서비스','플랫폼서비스>부동산서비스>포털서비스>38','38'),(30,'플랫폼서비스','플랫폼서비스>부동산서비스>중개거래업>36','36'),(31,'플랫폼서비스','플랫폼서비스>웰니스케어>웹/앱 서비스>9,41','9,41'),(32,'플랫폼서비스','플랫폼서비스>웰니스케어>포털서비스>38,41','38,41'),(33,'플랫폼서비스','플랫폼서비스>웰니스케어>식품>29,30,31,32,35','29,30,31,32,35'),(34,'플랫폼서비스','플랫폼서비스>웰니스케어>뷰티>35,44','35,44'),(35,'플랫폼서비스','플랫폼서비스>웰니스케어>퍼스널케어>41,44','41,44'),(36,'플랫폼서비스','플랫폼서비스>웰니스케어>의학>44','44'),(37,'플랫폼서비스','플랫폼서비스>웰니스케어>관광/의료>35,39,44','35,39,44'),(38,'플랫폼서비스','플랫폼서비스>웰니스케어>의약품>5,35,44','5,35,44'),(39,'플랫폼서비스','플랫폼서비스>웰니스케어>피트니스>41','41'),(40,'플랫폼서비스','플랫폼서비스>인력중계서비스>가사인력>35','35'),(41,'플랫폼서비스','플랫폼서비스>인력중계서비스>현장인력>35','35'),(42,'플랫폼서비스','플랫폼서비스>인력중계서비스>보모인력>35','35'),(43,'플랫폼서비스','플랫폼서비스>인력중계서비스>요양인력>35','35'),(44,'플랫폼서비스','플랫폼서비스>교통>39','39'),(45,'플랫폼서비스','플랫폼서비스>ICT/IOT>공장자동화>9,37,42','9,37,42'),(46,'플랫폼서비스','플랫폼서비스>ICT/IOT>센서자동화>9,37,42','9,37,42'),(47,'플랫폼서비스','플랫폼서비스>헬스/건강정보>웹/앱 서비스>9,41','9,41'),(48,'플랫폼서비스','플랫폼서비스>헬스/건강정보>포털서비스>38,41','38,41'),(49,'플랫폼서비스','플랫폼서비스>헬스/건강정보>피트니스>41','41'),(50,'플랫폼서비스','플랫폼서비스>헬스/건강정보>개인건강정보>44','44'),(51,'플랫폼서비스','플랫폼서비스>의료정보서비스>웹/앱 서비스>9,44','9,44'),(52,'플랫폼서비스','플랫폼서비스>의료정보서비스>포털서비스>38,44','38,44'),(53,'플랫폼서비스','플랫폼서비스>의료정보서비스>건강검진>44','44'),(54,'플랫폼서비스','플랫폼서비스>의료정보서비스>의료서비스>44','44'),(55,'플랫폼서비스','플랫폼서비스>식품>건강식품>29,30,31,35','29,30,31,35'),(56,'플랫폼서비스','플랫폼서비스>식품>유아용식품>29,30,31,35','29,30,31,35'),(57,'플랫폼서비스','플랫폼서비스>식품>주류>32,33,35','32,33,35'),(58,'플랫폼서비스','플랫폼서비스>식품>주스>32,35','32,35'),(59,'플랫폼서비스','플랫폼서비스>식품>가공식품>29,30,31,32,33,35','29,30,31,32,33,35'),(60,'플랫폼서비스','플랫폼서비스>식품>차류>30,35','30,35'),(61,'플랫폼서비스','플랫폼서비스>패션서비스>웹/앱 서비스>9,41','9,41'),(62,'플랫폼서비스','플랫폼서비스>패션서비스>포털서비스>38','38'),(63,'플랫폼서비스','플랫폼서비스>패션서비스>의류>25,35','25,35'),(64,'플랫폼서비스','플랫폼서비스>패션서비스>악세사리>9,14,35','9,14,35'),(65,'플랫폼서비스','플랫폼서비스>패션서비스>가방>18,35','18,35'),(66,'플랫폼서비스','플랫폼서비스>패션서비스>신발>25,35','25,35'),(67,'플랫폼서비스','플랫폼서비스>화장품/미용서비스>웹/앱 서비스>9,38','9,38'),(68,'플랫폼서비스','플랫폼서비스>화장품/미용서비스>포털서비스>38','38'),(69,'플랫폼서비스','플랫폼서비스>화장품/미용서비스>피부상태측정>44','44'),(70,'플랫폼서비스','플랫폼서비스>화장품/미용서비스>O2O서비스>44','44'),(71,'플랫폼서비스','플랫폼서비스>화장품/미용서비스>화장품제조>3','3'),(72,'ICT/IOT','ICT/IOT>정보통신>9,42','9,42'),(73,'ICT/IOT','ICT/IOT>공장자동화>9,37,42','9,37,42'),(74,'ICT/IOT','ICT/IOT>자동화센싱기술>9,37,42','9,37,42'),(75,'ICT/IOT','ICT/IOT>개인건강정보>9,37,42','9,37,42'),(76,'제조','제조>가구/인테리어>가구>20,35','20,35'),(77,'제조','제조>가구/인테리어>천막/블라인드>22,35','22,35'),(78,'제조','제조>가구/인테리어>침구류>24,35','24,35'),(79,'제조','제조>가구/인테리어>도자기>21,35','21,35'),(80,'제조','제조>가구/인테리어>인테리어소품>21,35','21,35'),(81,'제조','제조>전자제품(부품)>9','9'),(82,'제조','제조>패션제품>14,18,25','14,18,25'),(83,'제조','제조>직물제품>22,24','22,24'),(84,'제조','제조>오락 및 운동용품>28','28'),(85,'제조','제조>미용 및 세정제품>3','3'),(86,'제조','제조>음향 및 영상제품>9,18','9,18'),(87,'제조','제조>악세사리제품>9,14','9,14'),(88,'제조','제조>종이,미술,사무용제품>16','16'),(89,'제조','제조>의료제품>10','10'),(90,'제조','제조>컴퓨터관련제품>9','9'),(91,'제조','제조>음료,주류제품>32,33','32,33');
/*!40000 ALTER TABLE `mark_directory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_file`
--

DROP TABLE IF EXISTS `mark_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_file` (
  `mark_pk` int(11) NOT NULL,
  `mark_file_pk` int(11) NOT NULL AUTO_INCREMENT,
  `file_url` varchar(1000) DEFAULT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `file_type` int(11) DEFAULT NULL,
  `file_uploder` varchar(50) DEFAULT NULL,
  `logtime` datetime DEFAULT NULL,
  UNIQUE KEY `PK_mark_file` (`mark_file_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_file`
--

LOCK TABLES `mark_file` WRITE;
/*!40000 ALTER TABLE `mark_file` DISABLE KEYS */;
INSERT INTO `mark_file` VALUES (1,1,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/jumin/20170919150803.jpg','20170919143240.jpg',3,'qwer917','2017-09-19 14:32:40'),(1,2,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/entrust/20170919150422.jpg','20170919150422.jpg',2,'qwer917','2017-09-19 15:04:22'),(1,3,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20170922182858.jpg','20170922182858.jpg',1,'qwer917','2017-09-22 18:28:58'),(13,4,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20170925115132.jpg','20170925115132.jpg',1,'qwer917','2017-09-25 11:51:32'),(14,5,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/entrust/20170925115716.jpg','20170925115716.jpg',2,'qwer917','2017-09-25 11:57:16'),(14,6,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20170925115725.png','20170925115725.png',1,'qwer917','2017-09-25 11:57:25'),(14,7,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/jumin/20170925115740.jpg','20170925115740.jpg',3,'qwer917','2017-09-25 11:57:40'),(11,8,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20170925152549.png','20170925152549.png',1,'qwer917','2017-09-25 15:25:49'),(11,9,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/entrust/20170925152559.jpg','20170925152559.jpg',2,'qwer917','2017-09-25 15:25:59'),(15,10,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20170925152803.jpg','20170925152803.jpg',1,'qwer917','2017-09-25 15:28:03'),(16,11,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/yusu88/mark/seal/20170925153231.jpg','20170925153231.jpg',1,'yusu88','2017-09-25 15:32:31'),(17,12,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/jisook5/mark/seal/20170925153756.png','20170925153756.png',1,'jisook5','2017-09-25 15:37:56'),(17,13,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/jisook5/mark/entrust/20170925153800.png','20170925153800.png',2,'jisook5','2017-09-25 15:38:00'),(18,14,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/babdol2/mark/seal/20170925155315.jpg','20170925155315.jpg',1,'babdol2','2017-09-25 15:53:15'),(18,15,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/babdol2/mark/entrust/20170925155319.jpg','20170925155319.jpg',2,'babdol2','2017-09-25 15:53:19'),(18,16,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/babdol2/mark/jumin/20170925155322.jpg','20170925155322.jpg',3,'babdol2','2017-09-25 15:53:22'),(9,17,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20170926115037.jpg','20170926115037.jpg',1,'qwer917','2017-09-26 11:50:37'),(9,18,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/entrust/20170926115044.png','20170926115044.png',2,'qwer917','2017-09-26 11:50:44'),(9,19,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/jumin/20170926115051.jpg','20170926115051.jpg',3,'qwer917','2017-09-26 11:50:51'),(20,20,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/godn917/mark/seal/20171010104309.jpg','20171010104309.jpg',1,'godn917','2017-10-10 10:43:09'),(22,21,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test12/mark/seal/20171010125546.jpg','20171010125546.jpg',1,'test12','2017-10-10 12:55:46'),(11,22,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/jumin/20171010173837.jpg','20171010173837.jpg',3,'qwer917','2017-10-10 17:38:37'),(13,23,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/entrust/20171011114131.jpg','20171011114131.jpg',2,'qwer917','2017-10-11 11:41:31'),(13,24,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/jumin/20171011114139.jpg','20171011114139.jpg',3,'qwer917','2017-10-11 11:41:39'),(25,25,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20171011133923.png','20171011133923.png',1,'qwer917','2017-10-11 13:39:23'),(25,26,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/entrust/20171011133929.jpg','20171011133929.jpg',2,'qwer917','2017-10-11 13:39:29'),(25,27,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/jumin/20171011133936.jpg','20171011133936.jpg',3,'qwer917','2017-10-11 13:39:36'),(26,28,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20171012112930.jpg','20171012112930.jpg',1,'qwer917','2017-10-12 11:29:30'),(26,29,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/entrust/20171012112938.jpg','20171012112938.jpg',2,'qwer917','2017-10-12 11:29:38'),(27,30,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/seal/20171012115454.jpg','20171012115454.jpg',1,'qwer917','2017-10-12 11:54:54'),(26,31,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer917/mark/jumin/20171012151910.jpg','20171012151910.jpg',3,'qwer917','2017-10-12 15:19:10'),(34,32,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/seal/20171012173059.jpg','20171012173059.jpg',1,'qwer2','2017-10-12 17:30:59'),(34,33,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/entrust/20171012173102.jpg','20171012173102.jpg',2,'qwer2','2017-10-12 17:31:02'),(34,34,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/jumin/20171012173105.jpg','20171012173105.jpg',3,'qwer2','2017-10-12 17:31:05'),(35,35,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/entrust/20171012173314.jpg','20171012173314.jpg',2,'qwer2','2017-10-12 17:33:14'),(28,36,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/seal/20171013114151.jpg','20171013114151.jpg',1,'qwer2','2017-10-13 11:41:51'),(28,37,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/entrust/20171013114155.jpg','20171013114155.jpg',2,'qwer2','2017-10-13 11:41:55'),(28,38,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/jumin/20171013114200.jpg','20171013114200.jpg',3,'qwer2','2017-10-13 11:42:00'),(37,39,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/seal/20171013115631.jpg','20171013115631.jpg',1,'qwer2','2017-10-13 11:56:31'),(37,40,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/entrust/20171013115635.jpg','20171013115635.jpg',2,'qwer2','2017-10-13 11:56:35'),(37,41,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/jumin/20171013115640.jpg','20171013115640.jpg',3,'qwer2','2017-10-13 11:56:40'),(39,42,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/seal/20171013160521.png','20171013160521.png',1,'qwer2','2017-10-13 16:05:21'),(39,43,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/entrust/20171013160523.jpg','20171013160523.jpg',2,'qwer2','2017-10-13 16:05:23'),(39,44,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/jumin/20171013160528.png','20171013160528.png',3,'qwer2','2017-10-13 16:05:28'),(41,45,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/seal/20171013170402.png','20171013170402.png',1,'qwer2','2017-10-13 17:04:02'),(43,46,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/seal/20171016102732.jpg','20171016102732.jpg',1,'qwer2','2017-10-16 10:27:32'),(43,47,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/entrust/20171016102734.png','20171016102734.png',2,'qwer2','2017-10-16 10:27:34'),(43,48,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/jumin/20171016103017.png','20171016103017.png',3,'qwer2','2017-10-16 10:30:17'),(44,49,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/seal/20171016104903.png','20171016104903.png',1,'qwer2','2017-10-16 10:49:03'),(44,50,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/entrust/20171016104912.jpg','20171016104912.jpg',2,'qwer2','2017-10-16 10:49:12'),(44,51,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/qwer2/mark/jumin/20171016105126.jpg','20171016105126.jpg',3,'qwer2','2017-10-16 10:51:26'),(45,52,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/seal/20171101185838.jpg','20171101185838.jpg',1,'test3','2017-11-01 18:58:38'),(46,53,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/jumin/20171102114657.jpg','20171102114657.jpg',3,'test3','2017-11-02 11:46:57'),(46,54,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/entrust/20171102114659.jpg','20171102114659.jpg',2,'test3','2017-11-02 11:46:59'),(45,55,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/entrust/20171102144717.png','20171102144717.png',2,'test3','2017-11-02 14:47:17'),(45,56,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/jumin/20171102144721.jpg','20171102144721.jpg',3,'test3','2017-11-02 14:47:21'),(46,57,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/seal/20171103101621.png','20171103101621.png',1,'test3','2017-11-03 10:16:21'),(47,58,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/seal/20171103105548.png','20171103105548.png',1,'test3','2017-11-03 10:55:48'),(47,59,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/entrust/20171103105553.png','20171103105553.png',2,'test3','2017-11-03 10:55:53'),(47,60,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/jumin/20171103105557.gif','20171103105557.gif',3,'test3','2017-11-03 10:55:57'),(48,61,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/ip9816/mark/seal/20171103152049.jpg','20171103152049.jpg',1,'ip9816','2017-11-03 15:20:49'),(48,62,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/ip9816/mark/entrust/20171103152118.png','20171103152118.png',2,'ip9816','2017-11-03 15:21:18'),(50,63,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/seal/20171108102738.png','20171108102738.png',1,'test3','2017-11-08 10:27:38'),(58,64,'https://s3.ap-northeast-2.amazonaws.com/ideamarkman/user/upload/test3/mark/jumin/20171109154355.jpg','20171109154355.jpg',3,'test3','2017-11-09 15:43:55');
/*!40000 ALTER TABLE `mark_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_history`
--

DROP TABLE IF EXISTS `mark_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_history` (
  `mark_history_pk` int(11) NOT NULL AUTO_INCREMENT,
  `mark_pk` int(11) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `where_use` varchar(300) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `select_content` varchar(300) DEFAULT NULL,
  `patent_feedback` varchar(1000) DEFAULT NULL,
  `regis_date` datetime DEFAULT NULL,
  `feedback_date` datetime DEFAULT NULL,
  `regis_posibility` varchar(4) DEFAULT NULL,
  UNIQUE KEY `PK_mark` (`mark_history_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_history`
--

LOCK TABLES `mark_history` WRITE;
/*!40000 ALTER TABLE `mark_history` DISABLE KEYS */;
INSERT INTO `mark_history` VALUES (1,1,'asdasd','kor','asdasdasd',NULL,'',NULL,'2017-09-19 10:33:32',NULL,NULL),(2,2,'','','',NULL,'',NULL,'2017-09-21 14:00:16',NULL,NULL),(3,3,'','','',NULL,'',NULL,'2017-09-21 14:00:17',NULL,NULL),(4,4,'','','',NULL,'',NULL,'2017-09-21 14:03:34',NULL,NULL),(5,7,'afdg','','dfadsfadf',NULL,'',NULL,'2017-09-25 10:50:30',NULL,NULL),(6,8,'asdadfasdf','','sasdfasdfsdf',NULL,'',NULL,'2017-09-25 10:51:00',NULL,NULL),(7,9,'asdadfasdf','','sasdfasdfsdf',NULL,'',NULL,'2017-09-25 10:51:16',NULL,NULL),(8,10,'sdfsdfa','','asdfasdf',NULL,'',NULL,'2017-09-25 11:07:02',NULL,NULL),(9,11,'sdfsdfa','','asdfasdf',NULL,'',NULL,'2017-09-25 11:07:17',NULL,NULL),(10,12,'aaa','','aaaa',NULL,'',NULL,'2017-09-25 11:45:49',NULL,NULL),(11,13,'1!!!!','','1!!!!',NULL,'',NULL,'2017-09-25 11:51:11',NULL,NULL),(12,14,'a','','a',NULL,'',NULL,'2017-09-25 11:53:31',NULL,NULL),(13,15,'aaa','','aaaa',NULL,'',NULL,'2017-09-25 15:27:33',NULL,NULL),(14,16,'�䵹��','','�������,�۾��Ҷ�',NULL,'',NULL,'2017-09-25 15:32:09',NULL,NULL),(15,17,'�䵹��','','�䵹��!!!',NULL,'',NULL,'2017-09-25 15:37:47',NULL,NULL),(16,18,'�䵹��','','���� ģ��',NULL,'',NULL,'2017-09-25 15:53:04',NULL,NULL),(17,19,'','','',NULL,'',NULL,'2017-10-10 10:42:08',NULL,NULL),(18,20,'aa','','aa',NULL,'',NULL,'2017-10-10 10:42:55',NULL,NULL),(19,21,'','','',NULL,'',NULL,'2017-10-10 11:01:01',NULL,NULL),(20,22,'aaa','','aaa',NULL,'',NULL,'2017-10-10 12:54:56',NULL,NULL),(21,23,'','','',NULL,'',NULL,'2017-10-10 17:38:20',NULL,NULL),(22,24,'aaa','','aaaaa',NULL,'',NULL,'2017-10-10 17:41:58',NULL,NULL),(23,25,'ss','','sss',NULL,'',NULL,'2017-10-11 13:38:34',NULL,NULL),(24,26,'ss','','sss',NULL,'',NULL,'2017-10-12 11:29:15',NULL,NULL),(25,27,'�䵹��','','�Ϳ��� �� ģ��',NULL,'',NULL,'2017-10-12 11:54:27',NULL,NULL),(26,28,'�䵹��','','���� ��Ա�',NULL,'',NULL,'2017-10-12 17:30:11',NULL,NULL),(27,29,'�䵹��','','���� ��Ա�',NULL,'',NULL,'2017-10-12 17:30:36',NULL,NULL),(28,30,'�䵹��','','���� ��Ա�',NULL,'',NULL,'2017-10-12 17:30:41',NULL,NULL),(29,31,'�䵹��','','���� ��Ա�',NULL,'',NULL,'2017-10-12 17:30:45',NULL,NULL),(30,32,'�䵹��','','���� ��Ա�',NULL,'',NULL,'2017-10-12 17:30:46',NULL,NULL),(31,33,'�䵹��','','���� ��Ա�',NULL,'',NULL,'2017-10-12 17:30:47',NULL,NULL),(32,34,'�䵹��','','���� ��Ա�',NULL,'',NULL,'2017-10-12 17:30:48',NULL,NULL),(33,35,'a','','a',NULL,'',NULL,'2017-10-12 17:33:03',NULL,NULL),(34,36,'a','kor','a',NULL,'',NULL,'2017-10-12 17:33:32',NULL,NULL),(35,37,'a','kor','a',NULL,'',NULL,'2017-10-12 17:33:35',NULL,NULL),(36,38,'����','','����',NULL,'',NULL,'2017-10-13 14:58:06',NULL,NULL),(37,39,'����','','����',NULL,'',NULL,'2017-10-13 16:05:13',NULL,NULL),(38,40,'�䵹��','','�䵹��',NULL,'',NULL,'2017-10-13 16:42:03',NULL,NULL),(39,41,'���� ��','','��',NULL,'',NULL,'2017-10-13 16:49:24',NULL,NULL),(40,42,'','','',NULL,'',NULL,'2017-10-13 17:03:43',NULL,NULL),(41,43,'a','','a',NULL,'',NULL,'2017-10-16 10:25:26',NULL,NULL),(42,44,'s','','s',NULL,'',NULL,'2017-10-16 10:48:47',NULL,NULL),(43,45,'a','','a',NULL,'',NULL,'2017-11-01 18:57:20',NULL,NULL),(44,46,'f','','gd',NULL,'',NULL,'2017-11-02 11:46:41',NULL,NULL),(45,47,'aaa','','aa',NULL,'',NULL,'2017-11-03 10:54:09',NULL,NULL),(46,48,'�� ����ģ��','','�� ����� ����ȭ��',NULL,'',NULL,'2017-11-03 15:20:29',NULL,NULL),(47,49,'test','','test',NULL,'',NULL,'2017-11-06 13:58:18',NULL,NULL),(48,50,'test','','test',NULL,'',NULL,'2017-11-06 13:58:35',NULL,NULL),(49,51,'test','','test',NULL,'',NULL,'2017-11-06 13:58:42',NULL,NULL),(50,52,'','','',NULL,'',NULL,'2017-11-08 13:26:42',NULL,NULL),(51,53,'','','',NULL,'',NULL,'2017-11-08 13:26:43',NULL,NULL),(52,54,'','','',NULL,'',NULL,'2017-11-08 13:26:44',NULL,NULL),(53,55,'','','',NULL,'',NULL,'2017-11-08 13:26:45',NULL,NULL),(54,56,'','','',NULL,'',NULL,'2017-11-08 13:26:49',NULL,NULL),(55,57,'','','',NULL,'',NULL,'2017-11-08 13:27:19',NULL,NULL),(56,58,'fxf','','rrdtrd',NULL,'',NULL,'2017-11-08 14:09:18',NULL,NULL),(57,59,'aaa','','aAAA',NULL,'',NULL,'2017-11-09 13:46:33',NULL,NULL),(58,60,'���������䵹��','','�䵹��',NULL,'',NULL,'2017-11-09 17:08:36',NULL,NULL);
/*!40000 ALTER TABLE `mark_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_info`
--

DROP TABLE IF EXISTS `mark_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_info` (
  `board_pk` int(11) NOT NULL,
  `access_link` text NOT NULL,
  `thumnail_img` text NOT NULL,
  UNIQUE KEY `PK_mark_info` (`board_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_info`
--

LOCK TABLES `mark_info` WRITE;
/*!40000 ALTER TABLE `mark_info` DISABLE KEYS */;
INSERT INTO `mark_info` VALUES (55,'123123123','https://s3.ap-northeast-2.amazonaws.com/ideaprotectioncenter/admin/image/markinfo/20170824112212.png');
/*!40000 ALTER TABLE `mark_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mark_search_req`
--

DROP TABLE IF EXISTS `mark_search_req`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mark_search_req` (
  `board_pk` int(11) NOT NULL,
  `mark_type` varchar(50) NOT NULL,
  `where_use` varchar(100) NOT NULL,
  `reply_patent` varchar(30) NOT NULL,
  `mark_file_url` text,
  `mark_file_user` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`board_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mark_search_req`
--

LOCK TABLES `mark_search_req` WRITE;
/*!40000 ALTER TABLE `mark_search_req` DISABLE KEYS */;
/*!40000 ALTER TABLE `mark_search_req` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_board`
--

DROP TABLE IF EXISTS `news_board`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_board` (
  `board_bk` int(11) DEFAULT NULL,
  `news_board_pk` int(11) NOT NULL AUTO_INCREMENT,
  `news_board_content` text,
  `media_company` varchar(30) DEFAULT NULL,
  `original_link` varchar(30) DEFAULT NULL,
  `thumail_igm_user_name` text,
  `thumail_igm_server_name` text,
  `thumail_igm_url` text,
  PRIMARY KEY (`news_board_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_board`
--

LOCK TABLES `news_board` WRITE;
/*!40000 ALTER TABLE `news_board` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_board` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice` (
  `board_pk` int(11) NOT NULL,
  `update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
INSERT INTO `notice` VALUES (27,'2017-08-09 18:16:58'),(30,'2017-08-10 12:00:36'),(36,'2017-08-10 15:08:44'),(41,'2017-08-11 10:46:12'),(42,'2017-08-11 11:03:10'),(43,'2017-08-11 11:19:07'),(46,'2017-08-24 11:15:57'),(54,'2017-08-24 11:16:22'),(59,'2017-08-25 18:29:50'),(60,'2017-10-12 17:16:36'),(63,'2017-11-08 16:37:47'),(66,'2017-11-09 14:25:02'),(67,'2017-11-09 14:25:04'),(69,'2017-11-09 17:21:21');
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patent`
--

DROP TABLE IF EXISTS `patent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patent` (
  `id` varchar(50) NOT NULL,
  `profile_img` text NOT NULL,
  `profile_img_name` varchar(30) NOT NULL,
  `patent_num` int(11) NOT NULL,
  `patent_regis_img` text NOT NULL,
  `introduce` text NOT NULL,
  `account_num` varchar(100) NOT NULL,
  `account_bank` varchar(50) NOT NULL,
  `patent_regis_img_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patent`
--

LOCK TABLES `patent` WRITE;
/*!40000 ALTER TABLE `patent` DISABLE KEYS */;
/*!40000 ALTER TABLE `patent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qna`
--

DROP TABLE IF EXISTS `qna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qna` (
  `board_pk` int(11) NOT NULL,
  `is_secret` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qna`
--

LOCK TABLES `qna` WRITE;
/*!40000 ALTER TABLE `qna` DISABLE KEYS */;
INSERT INTO `qna` VALUES (28,1),(39,0),(40,1),(65,0);
/*!40000 ALTER TABLE `qna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reply`
--

DROP TABLE IF EXISTS `reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reply` (
  `reply_seq` int(11) NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `registration_date` datetime NOT NULL,
  `board_pk` int(11) NOT NULL,
  `writer` varchar(50) NOT NULL,
  PRIMARY KEY (`reply_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reply`
--

LOCK TABLES `reply` WRITE;
/*!40000 ALTER TABLE `reply` DISABLE KEYS */;
INSERT INTO `reply` VALUES (10,'<p>sdfsdfsdfdsfsdsdfs</p>','2017-08-08 16:57:58',2,'suehyun'),(11,'df','2017-08-08 17:00:43',2,'suehyun'),(12,'df','2017-08-08 17:00:48',2,'suehyun'),(13,'<p><span style=\"font-family: &quot;Nanum Gothic&quot;, sans-serif;\">안녕하세요 IT 서비스 특허권을 내려고 하는 사람입니다.&nbsp;</span><br style=\"margin-right: auto; margin-left: auto; border-collapse: collapse; box-sizing: border-box; font-family: &quot;Nanum Gothic&quot;, sans-serif;\"><span style=\"font-family: &quot;Nanum Gothic&quot;, sans-serif;\">기존의 예약 시스템에서 발전된 방향의 서비스를 특허를 내려고 하는데요.&nbsp;</span><br style=\"margin-right: auto; margin-left: auto; border-collapse: collapse; box-sizing: border-box; font-family: &quot;Nanum Gothic&quot;, sans-serif;\"><span style=\"font-family: &quot;Nanum Gothic&quot;, sans-serif;\">기술의 경우 아직 구체적으로 기술 구현 방식에 대해서는 완료하지 못한 상태이나 구현은 가능한 기술 입니다.&nbsp;</span><br style=\"margin-right: auto; margin-left: auto; border-collapse: collapse; box-sizing: border-box; font-family: &quot;Nanum Gothic&quot;, sans-serif;\"><span style=\"font-family: &quot;Nanum Gothic&quot;, sans-serif;\">또한 해당 기술은 기존의 있는 기술들을 통합하여 하나의 새로운 방식을 창출한 기술인데요.&nbsp;</span><br style=\"margin-right: auto; margin-left: auto; border-collapse: collapse; box-sizing: border-box; font-family: &quot;Nanum Gothic&quot;, sans-serif;\"><br style=\"margin-right: auto; margin-left: auto; border-collapse: collapse; box-sizing: border-box; font-family: &quot;Nanum Gothic&quot;, sans-serif;\"><span style=\"font-family: &quot;Nanum Gothic&quot;, sans-serif;\">이러한 경우에도 특허 등록이 가능한지 궁금합니다.&nbsp;</span><br style=\"margin-right: auto; margin-left: auto; border-collapse: collapse; box-sizing: border-box; font-family: &quot;Nanum Gothic&quot;, sans-serif;\"><br style=\"margin-right: auto; margin-left: auto; border-collapse: collapse; box-sizing: border-box; font-family: &quot;Nanum Gothic&quot;, sans-serif;\"><span style=\"font-family: &quot;Nanum Gothic&quot;, sans-serif;\">가능하다면 기술은 도면에 해당하는 부분에 어떻게 기입을 해야하나요?</span>&nbsp;</p>','2017-08-08 17:13:31',2,'suehyun'),(14,'<p>ASDASD</p>','2017-08-10 10:53:50',28,'suehyun'),(15,'<p>SDFSDF</p>','2017-08-10 10:53:55',28,'suehyun'),(19,'ssss','2017-08-14 11:56:00',46,'suehyun'),(20,'kl;','2017-08-14 12:45:27',46,'suehyun'),(21,'hjk','2017-08-14 12:45:29',46,'suehyun'),(22,'','2017-08-14 12:55:47',46,'suehyun'),(23,'12312312312312312323','2017-08-14 13:02:00',46,'suehyun'),(24,'안녕하세요\r\n좋은 정보 감사합니다\r\n','2017-08-14 13:18:56',46,'suehyun'),(25,'안녕하세요 \r\n좋은 정보 감사합니다\r\n\r\n','2017-08-14 13:19:18',46,'suehyun'),(26,'ㅎ\r\nㅇ\r\nㅎ\r\nㅇ\r\n','2017-08-14 13:19:30',46,'suehyun'),(27,'ㅇㅎㅇㅀㅇ','2017-08-14 13:23:26',46,'suehyun'),(28,'ㅀㅇㅀ','2017-08-14 13:23:28',46,'suehyun'),(29,'ㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹㅌㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㄴㅇㄹㅇㄹ','2017-08-14 13:24:54',46,'suehyun'),(30,'hhhh','2017-08-14 13:26:37',46,'suehyun'),(31,'123123','2017-08-14 13:32:14',46,'suehyun'),(32,'ㅎㅇㄹ','2017-08-14 13:32:36',46,'suehyun'),(33,'ㄹㄴㄹ','2017-08-14 13:33:39',46,'suehyun'),(34,'가장 최근\r\n','2017-08-14 14:08:43',46,'suehyun'),(35,'123123123','2017-08-14 17:48:20',44,'suehyun'),(36,'123','2017-08-23 17:41:02',40,'suehyun'),(37,'123','2017-08-23 17:48:06',43,'suehyun'),(38,'123','2017-08-23 17:48:09',43,'suehyun'),(39,'123','2017-08-23 17:48:11',43,'suehyun'),(40,'123','2017-08-23 17:48:13',43,'suehyun'),(41,'123\r\n123\r\n12\r\n3','2017-08-23 17:55:39',43,'suehyun'),(44,'3213','2017-08-23 18:22:50',46,'suehyun'),(45,'123','2017-08-23 18:22:59',46,'suehyun'),(56,'1123213232','2017-08-24 11:15:37',40,'suehyun'),(57,'<p>123</p>','2017-08-24 14:48:29',40,'suehyun'),(58,'123213132','2017-08-25 18:38:22',59,'suehyun'),(60,'하이룽','2017-08-25 18:38:31',59,'suehyun'),(61,'하하하','2017-08-25 18:38:45',59,'suehyun'),(62,'혜우 왔다감!!!!','2017-08-25 18:56:42',59,'suehyun'),(63,'배고파요','2017-10-12 17:18:49',39,'qwer917'),(64,'fffff','2017-10-12 17:40:30',46,'qwer917'),(65,'aaaaa','2017-11-09 14:25:51',67,'qwer917'),(66,'밥돌이 공격','2017-11-09 14:26:15',39,'qwer917'),(67,'22222','2017-11-09 17:01:42',69,'qwer917');
/*!40000 ALTER TABLE `reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` varchar(50) NOT NULL,
  `pw` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `cell_num` varchar(100) NOT NULL,
  `tel_num` varchar(100) DEFAULT NULL,
  `addr1` varchar(50) NOT NULL,
  `addr2` varchar(100) NOT NULL,
  `addr3` varchar(100) NOT NULL,
  `sign_up_date` datetime NOT NULL,
  `sns_receive` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('babdol2','1000:1a43b62b7d01db635758178aa68bca49582f71cdcb2e8627:b3d1924608b8c5779fe2f32be6dc83b8a308b0f184324b41','alsgns0219@ideaconcert.com','ROLE_APPLICANT','010-1111-1111','','13306','경기 성남시 수정구 성남대로 지하 1332 (태평동, 가천대역)','아이디어콘서트','2017-09-25 15:51:29',0,'쩡이네'),('bhw1234','1000:9d95ff0bbd13220a008b97e7f06a7ca984f0b4a26a568166:dee6a1933eb5f229eba0bb37d1207c1a754efe402bcda199','bhw@na.com','ROLE_APPLICANT','01000000000','0313333333','34672','대전 동구 판교1길 3 (판암동)','노숙자...','2017-08-25 17:36:33',0,'수현니'),('godn917','1000:79976d3515723066f317ee4bec1b7abc0a92fcf31649180f:523d5698f8b25f0e87f6ee737036c5cbde04fe76eafd37f4','godn917@naver.com','ROLE_APPLICANT','010-1234-5678','333-3333','34672','대전 동구 판교1길 3 (판암동)','후에에에ㅔ에에엥','2017-10-10 10:31:08',0,'혜우'),('hojin','1000:b4c845ecfc36c52b0203819532fee69d17bbaf2f2786c72b:094c5fc1b90123713f62025d0a784f47d18c0e3925a41379','bhw123@daum.net','ROLE_APPLICANT','0101231233','1231231231','06116','서울 강남구 논현로123길 13-3 (논현동, 논현빌라)','123','2017-08-22 13:51:36',0,'배혜우'),('ip9816','1000:224e582d009841020af4f879cef52b1db15a409ef0f56070:73662650e740e98a05c87c4b3245f6186fa2663909eb01a8','ip9816@naver.com','ROLE_APPLICANT','010-7220-9816','442-8589','22766','인천 서구 비지니스로 41 (경서동, 청라한양수자인)','551동 303호','2017-11-03 15:16:08',0,'조성근'),('jisook5','1000:ecb55ae9d692a45e2eb3cb4ffdd7710118957ea7ecfa5f61:b65c6e2075a3deb859850fd6de57e06cc6629c4a3bfb0e18','hellojs5@naver.com','ROLE_APPLICANT','010-7125-9525','449-2279','05710','서울 송파구 송파대로32길 15 (가락동, 가락금호아파트)','107-501','2017-09-25 15:36:55',0,'김지수'),('qweqwe','1000:0846d36f151a00309820f8460ebdb1bfbf22f40aa05afa3a:c06199f0c95c9282aa9f99b65774a2905a55392dc4e33cfc','powerdhrwk@naver.com','ROLE_APPLICANT','010-1234-1234','','13536','경기 성남시 분당구 판교역로 4 (백현동)','111','2017-11-08 17:53:13',0,'현아'),('qwer0917','1000:f3869071cc0582d6c719d1a55e7d6c97bd3e9936d51a9e02:601e9d18aa61178a423904053dfdbcee563bff77d0fec7c8','aa@aa.com','ROLE_APPLICANT','010-011-1111','111-111','34672','대전 동구 판교1길 3 (판암동)','ㅁㅁㅁ','2017-09-14 13:22:25',0,'배혜우'),('qwer2','1000:c53273eccabb9f889d2dbc10327f38ecf41b77cfd1bf51d2:0bf8342bb1e38b9243845fd849da02b4c617108fab8bfd19','qwer2@naver.com','ROLE_APPLICANT','000-0000-0000','000-0000','25639','강원 강릉시 왕산면 옷밥골길 17 (도마리)','밥돌이 하우스','2017-10-12 17:29:31',0,'밥돌이'),('qwer917','1000:4c504f9efb5cb9104b1e381bb59046dbdc10f97f45e4aa63:c862aeb162f3582b06388aa3c64373f7d812a88dae9dbf1a','aaa@aa.com','ROLE_ADMIN','000-111-000','033-333','34672','대전 동구 판교1길 3 (판암동)','ㅁㅇㄹ','2017-09-11 10:36:22',0,'혜우'),('suehyun','1000:b0a386712e41e9f68d0881c7787adb78f69f5a9f5ce4a3ee:c7d173596e267ffe587e7e56fd227b7ef25be1e107b34613','nunu0321@naver.com','ROLE_ADMIN','01087234161','','05315','서울 강동구 양재대로123길 7 (천호동, 강동그랑시아)','123','2017-08-01 17:02:39',0,'김수현'),('test1','1000:de43900537d50e1b34b3726daf1a1377a67bf73a8e05d8d8:38ec9afc1746681ccac071c8a69370eefc2fa487ae397be1','sh960203@gmail.com','ROLE_APPLICANT','010-4456-1235','321-5451','28562','충북 청주시 서원구 1순환로 627 (사창동, 에덴건축)','321321','2017-09-09 18:13:19',0,'김슈현'),('test12','1000:20d722c556d47dce508bf17575778371cacb1d47c64bcf44:eb3719195c3d2374af116d7b8360ca477c856b968d821116','godn917@naver.com','ROLE_APPLICANT','010-123-456','333-333','13485','경기 성남시 분당구 판교로 20 (판교동, 판교원마을3단지아파트)','노숙자 또르릅','2017-10-10 12:52:22',0,'밥돌이'),('test3','1000:9a7da84de71b7b69f639affe231b730aa7b8267c6c78c79c:eac7fd007c939e9e209867512f06de335f0ffd64e9f5608d','aaa@aaa.com','ROLE_APPLICANT','000-0000-0000','000-0000','08218','서울 구로구 목동남로 4 (고척동, 현준프라자)','1111','2017-11-01 18:47:17',0,'배배배'),('test6','1000:f985f526f8f809213ce467d5add20f62ffe6c2a9b9b4a535:f101ef67da1a1d5413838e1f6c9a429e651dd0a11527ed4b','ggg@ggg.com','ROLE_APPLICANT','010-0000-0000','000-0000','34672','대전 동구 판교1길 3 (판암동)','ㅁㅁㅁ','2017-11-09 15:06:46',0,'테스트'),('test7','1000:5834eeacb2b1a204ad18f5067385dddfa7bca3ae7097f2f9:747b8e19df6825e377d3a2493cbe7d82fd6ba2d03e6f8c9c','aaaa@aaa.aaa','ROLE_APPLICANT','010-3333-3333','333-3333','13536','경기 성남시 분당구 판교역로 4 (백현동)','노숙자(집 없음) ','2017-11-09 17:10:50',0,'변리사'),('yoona','1000:796c072d072bc08cecab338c29656576edc1addc391f0b5e:311eab50c9d21d9384a4cb935666307b803228c3e7936ac8','nunu0321@daum.net','ROLE_APPLICANT','1231231231','1231231231','05315','서울 강동구 양재대로123길 7 (천호동, 강동그랑시아)','123','2017-08-16 14:57:33',0,'박윤아');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-09 17:21:25
